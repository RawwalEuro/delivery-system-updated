package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppSettings {

    @SerializedName("WebsiteSettingData")
    @Expose
    private WebSettingData websiteSettingData;
    @SerializedName("RegionSettingData")
    @Expose
    private RegionSettingData regionSettingData;

    @SerializedName("Languages")
    @Expose
    private List<Languages> languages;


    @SerializedName("VehcileTypeList")
    @Expose
    private List<VehicleTypeList> VehcileTypeList;


    public List<VehicleTypeList> getVehcileTypeList() {
        return VehcileTypeList;
    }

    public void setVehcileTypeList(List<VehicleTypeList> vehcileTypeList) {
        VehcileTypeList = vehcileTypeList;
    }

    public WebSettingData getWebsiteSettingData() {
        return websiteSettingData;
    }

    public void setWebsiteSettingData(WebSettingData websiteSettingData) {
        this.websiteSettingData = websiteSettingData;
    }

    public RegionSettingData getRegionSettingData() {
        return regionSettingData;
    }

    public void setRegionSettingData(RegionSettingData regionSettingData) {
        this.regionSettingData = regionSettingData;
    }


    public List<Languages> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Languages> languages) {
        this.languages = languages;
    }
}
