package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.InputValidatorHelper;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomResponseString;
import com.facebook.login.widget.LoginButton;
import com.fxn.stash.Stash;
import com.google.android.material.textfield.TextInputEditText;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView alreadyHaveAcc;
    private LinearLayout btnCreateAcc;
    private TextInputEditText edtUserName, edtUserEmail;
    private EditText edtPassword, edtMobileNum;
    private InputValidatorHelper inputValidatorHelper;
    private String userNum, userEmail, userPass, userName, userLastName;
    private ImageView viewPassToggle;
    private final String TAG = "SignupActivity";
    private CountryCodePicker ccp;
    private LoginButton login_button;
    private boolean isFirstClick = true;
    private RelativeLayout rlProgressBar;
    private RelativeLayout mainRl;
    private CircularProgressBar circularProgressBar;
    private EditText edtUserlastname;
    private TextView signUpHeader, signDesc, tvCreateAcc;
    private MasterPojo masterPojo;
    private TextView haveAcc;
    private Integer value = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        inputValidatorHelper = new InputValidatorHelper();

        Stash.init(this);
        initViews();
    }

    private void initViews() {

        btnCreateAcc = findViewById(R.id.btnCreateAcc);
        viewPassToggle = findViewById(R.id.viewPassToggle);
        ccp = findViewById(R.id.ccp);
        ccp.setVisibility(View.GONE);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);

        //Language
        signUpHeader = findViewById(R.id.signUpHeader);
        signDesc = findViewById(R.id.signDesc);
        edtUserName = findViewById(R.id.edtUserFirstName);
        edtUserlastname = findViewById(R.id.edtUserLastName);
        edtUserEmail = findViewById(R.id.edtUserEmail);
        edtPassword = findViewById(R.id.edtPassword);
        edtMobileNum = findViewById(R.id.edtMobileNum);
        tvCreateAcc = findViewById(R.id.tvCreateAcc);
        alreadyHaveAcc = findViewById(R.id.alreadyHaveAcc);
        haveAcc = findViewById(R.id.haveAcc);

        signUpHeader.setText(masterPojo.getSignUp());
        signDesc.setText(masterPojo.getSignUpWithEmail());
        edtUserName.setHint(masterPojo.getFirstName());
        edtUserlastname.setHint(masterPojo.getLastName());
        edtPassword.setHint(masterPojo.getPassword());
        edtMobileNum.setHint(masterPojo.getMobileNumber());
        edtUserEmail.setHint(masterPojo.getEmailAddress());
        tvCreateAcc.setText(masterPojo.getCreateAccount());
        tvCreateAcc.setTextColor(Color.WHITE);
        alreadyHaveAcc.setText(masterPojo.getLogin());
        haveAcc.setText(masterPojo.getAlreadyHaveAcc());


        alreadyHaveAcc.setOnClickListener(this);
        btnCreateAcc.setOnClickListener(this);
        viewPassToggle.setOnClickListener(this);
//       edtMobileNum.setOnClickListener(this);
        ccp.setOnClickListener(this);


        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {

            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.alreadyHaveAcc:
                Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(intent);

                break;


            case R.id.btnCreateAcc:
                validate();
                break;

            case R.id.viewPassToggle:
                viewPassToggleState(v);

                break;


            case R.id.edtMobileNum:

                break;

        }
    }


    private void validate() {

        userName = edtUserName.getText().toString();
        userEmail = edtUserEmail.getText().toString();
        userPass = edtPassword.getText().toString();
        userNum = edtMobileNum.getText().toString();
        userLastName = edtUserlastname.getText().toString();


        if (inputValidatorHelper.isNullOrEmpty(userName)) {
            Toasty.warning(this, masterPojo.getFirstNameEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(userLastName)) {
            Toasty.warning(this, masterPojo.getLastNameEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(userEmail)) {
            Toasty.warning(this, masterPojo.getEmptyEmail(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isValidEmail(userEmail) == false) {
            Toasty.warning(this, masterPojo.getInvalidEmail(), Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(userNum)) {

            Toasty.warning(this, masterPojo.getNumberIsEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (userNum.length() < 5) {

            Toasty.warning(this, masterPojo.getNumberToShort(), Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(userPass)) {

            Toasty.warning(this, masterPojo.getPasswordIsEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (userPass.length() < 6) {
            Toasty.warning(this, masterPojo.getPassStrong(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        User user = new User();
        user.setFirstName(userName);
        user.setLastName(userLastName);
        user.setCustomerPassword(userPass);
        user.setCustomerPhoneNumber("+" + userNum);
        user.setCustomerEmail(userEmail);
        user.setCreateDate("0001-01-01T00:00:00");
        user.setCustomerDeviceId(android_id);
        user.setAction("add");
        user.setStatus(true);
        user.setId(1);


        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            progressVisiblityGone();
            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }
        callApiUserAuthenticate(user);

    }


    private void callApiUserAuthenticate(User user) {


        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomResponseString<String>> call = apiService.authorizationCode(user);

        call.enqueue(new Callback<CustomResponseString<String>>() {
            @Override
            public void onResponse(Call<CustomResponseString<String>> call, Response<CustomResponseString<String>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    progressVisiblityGone();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    progressVisiblityGone();
                    user.setCustomerVerificationCode(response.body().getData());
                    Stash.put(Constants.IS_SIGN_UP_COMPLETED, false);
                    Stash.put((String) response.body().getData(), Constants.OTP_CODE);
                    user.setSignUpCompleted(false);
                    Log.e("Response", response.message());
                    Toasty.success(getApplicationContext(), response.body().getMessage(), Toasty.LENGTH_LONG).show();
                    Stash.put(Constants.TEMP_USER, user);
                    Intent signUpIntent = new Intent(SignUpActivity.this, OtpVerifyActivity.class);
                    signUpIntent.putExtra("userObject", user);
                    startActivity(signUpIntent);
                    // callApiSignUp(user);
                }
            }


            @Override
            public void onFailure(Call<CustomResponseString<String>> call, Throwable t) {
                progressVisiblityGone();
                //Toasty.error(SignUpActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }

    private void viewPassToggleState(View view) {

        if (edtPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
            ((ImageView) (view)).setImageResource(R.drawable.password_toggle);
            //Show Password
            edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_remove_red_eye_24);
            //Hide Password
            edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }


}
