package com.eurosoft.customerapp.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
@Entity
public class User implements Serializable {


    @PrimaryKey(autoGenerate = true)
    private int idRoom;


    @ColumnInfo(name = "Id")
    @SerializedName("Id")
    @Expose
    private Integer id;


    @ColumnInfo(name = "CustomerName")
    @SerializedName("CustomerName")
    @Expose
    private String customerName;

    @ColumnInfo(name = "CustomerEmail")
    @SerializedName("CustomerEmail")
    @Expose
    private String customerEmail;

    @ColumnInfo(name = "CustomerPassword")
    @SerializedName("CustomerPassword")
    @Expose
    private String customerPassword;


    @ColumnInfo(name = "CustomerPhoneNumber")
    @SerializedName("CustomerPhoneNumber")
    @Expose
    private String customerPhoneNumber;

    @ColumnInfo(name = "CustomerAddress")
    @SerializedName("CustomerAddress")
    @Expose
    private String customerAddress;

    @ColumnInfo(name = "CustomerLocationTypeId")
    @SerializedName("CustomerLocationTypeId")
    @Expose
    private String customerLocationTypeId;

    @ColumnInfo(name = "CustomerLocationType")
    @SerializedName("CustomerLocationType")
    @Expose
    private String customerLocationType;

    @ColumnInfo(name = "CustomerLatitude")
    @SerializedName("CustomerLatitude")
    @Expose
    private Double customerLatitude;


    @ColumnInfo(name = "CustomerLongitude")
    @SerializedName("CustomerLongitude")
    @Expose
    private Double customerLongitude;


    @ColumnInfo(name = "CustomerDeviceId")
    @SerializedName("CustomerDeviceId")
    @Expose
    private String CustomerDeviceId;


    @ColumnInfo(name = "CustomerVerificationCode")
    @SerializedName("CustomerVerificationCode")
    @Expose
    private String CustomerVerificationCode;


    @ColumnInfo(name = "IsCustomerEmailVerify")
    @SerializedName("IsCustomerEmailVerify")
    @Expose
    private Boolean isCustomerEmailVerify;

    @ColumnInfo(name = "IsCustomerNumberVerify")
    @SerializedName("IsCustomerNumberVerify")
    @Expose
    private Boolean isCustomerNumberVerify;

    @ColumnInfo(name = "Status")
    @SerializedName("Status")
    @Expose
    private Boolean status;


    @ColumnInfo(name = "CreateDate")
    @SerializedName("CreateDate")
    @Expose
    private String createDate;


    @ColumnInfo(name = "action")
    @SerializedName("action")
    @Expose
    private String action;

    @ColumnInfo(name = "password")
    @SerializedName("password")
    @Expose
    private String password;

    @ColumnInfo(name = "CustomerFirstName")
    @SerializedName("CustomerFirstName")
    @Expose
    private String firstName;


    @ColumnInfo(name = "CustomerLastName")
    @SerializedName("CustomerLastName")
    @Expose
    private String lastName;

    @ColumnInfo(name = "Token")
    @SerializedName("Token")
    @Expose
    private String Token;


    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Expose
    private boolean isSignUpCompleted;

    public boolean isSignUpCompleted() {
        return isSignUpCompleted;
    }

    public void setSignUpCompleted(boolean signUpCompleted) {
        isSignUpCompleted = signUpCompleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerLocationTypeId() {
        return customerLocationTypeId;
    }

    public void setCustomerLocationTypeId(String customerLocationTypeId) {
        this.customerLocationTypeId = customerLocationTypeId;
    }

    public String getCustomerLocationType() {
        return customerLocationType;
    }

    public void setCustomerLocationType(String customerLocationType) {
        this.customerLocationType = customerLocationType;
    }

    public Double getCustomerLatitude() {
        return customerLatitude;
    }

    public void setCustomerLatitude(Double customerLatitude) {
        this.customerLatitude = customerLatitude;
    }

    public Double getCustomerLongitude() {
        return customerLongitude;
    }

    public void setCustomerLongitude(Double customerLongitude) {
        this.customerLongitude = customerLongitude;
    }

    public Boolean getIsCustomerEmailVerify() {
        return isCustomerEmailVerify;
    }

    public void setIsCustomerEmailVerify(Boolean isCustomerEmailVerify) {
        this.isCustomerEmailVerify = isCustomerEmailVerify;
    }

    public Boolean getIsCustomerNumberVerify() {
        return isCustomerNumberVerify;
    }

    public void setIsCustomerNumberVerify(Boolean isCustomerNumberVerify) {
        this.isCustomerNumberVerify = isCustomerNumberVerify;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }


    public String getCustomerDeviceId() {
        return CustomerDeviceId;
    }

    public void setCustomerDeviceId(String customerDeviceId) {
        CustomerDeviceId = customerDeviceId;
    }

    public String getCustomerVerificationCode() {
        return CustomerVerificationCode;
    }

    public void setCustomerVerificationCode(String customerVerificationCode) {
        CustomerVerificationCode = customerVerificationCode;
    }





    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
