package com.eurosoft.customerapp.Model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Cart implements Serializable {

  @PrimaryKey(autoGenerate = true)
  private int cartId;

   /* @ColumnInfo(name = "listItems")
    private ArrayList<Item> listItems;*/

  @ColumnInfo(name = "cartAmount")
  private String cartAmount;


  public int getCartId() {
    return cartId;
  }

  public void setCartId(int cartId) {
    this.cartId = cartId;
  }

  public String getCartAmount() {
    return cartAmount;
  }

  public void setCartAmount(String cartAmount) {
    this.cartAmount = cartAmount;
  }
}
