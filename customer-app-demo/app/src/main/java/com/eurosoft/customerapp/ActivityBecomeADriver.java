package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.eurosoft.customerapp.Adapters.AdapterWishList;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.BecomeADriver;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.VehicleTypeList;
import com.eurosoft.customerapp.Model.WishList;
import com.eurosoft.customerapp.Utils.InputValidatorHelper;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.eurosoft.customerapp.spinnerdialog.OnSpinerItemClick;
import com.eurosoft.customerapp.spinnerdialog.SpinnerDialog;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityBecomeADriver extends AppCompatActivity {

    private ImageView bckPress;
    private EditText driverNameEt, dobEdit, emailEt, phoneEt, vehicleOwnerEt, vehicleNumberEt, vehicleColorEt, vehicleMakeEt, vehicleModelEt, vehicleType;
    private RelativeLayout btnConfirm;
    private InputValidatorHelper inputValidatorHelper;
    private MasterPojo masterPojo;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private DatePickerDialog picker;
    private AppSettings appSettings;
    private List<VehicleTypeList> vehicleList = new ArrayList<>();
    private ArrayList<String> vehicleListStringName = new ArrayList<>();
    private SpinnerDialog spinnerDialog;
    private Integer vehicleTypeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_adriver);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        inputValidatorHelper = new InputValidatorHelper();
        initViews();
        try {
            vehicleList = appSettings.getVehcileTypeList();
            if (vehicleList == null || vehicleList.size() == 0) {
                return;
            }

            for (int i = 0; i < vehicleList.size(); i++) {
                vehicleListStringName.add(vehicleList.get(i).getVehicleType());
            }

            spinnerDialog = new SpinnerDialog(ActivityBecomeADriver.this, vehicleListStringName, "Select Vehicle Type", R.style.TextAppearance_AppCompat_Title, "Close");// With 	Animation

            spinnerDialog.setCancellable(true); // for cancellable
            spinnerDialog.setShowKeyboard(false);// for open keyboard by default

            vehicleType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    spinnerDialog.showSpinerDialog();
                }
            });


            spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                @Override
                public void onClick(String item, int position) {
                    vehicleType.setText(item);
                    vehicleTypeId = vehicleList.get(position).getId();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            vehicleList = new ArrayList<>();
        }
    }

    private void initViews() {
        bckPress = findViewById(R.id.bckPress);
        driverNameEt = findViewById(R.id.driverNameEt);
        dobEdit = findViewById(R.id.dobEdit);
        emailEt = findViewById(R.id.emailEt);
        phoneEt = findViewById(R.id.phoneEt);

        vehicleOwnerEt = findViewById(R.id.vehicleOwnerEt);
        vehicleNumberEt = findViewById(R.id.vehicleNumberEt);
        vehicleColorEt = findViewById(R.id.vehicleColorEt);
        vehicleMakeEt = findViewById(R.id.vehicleMakeEt);
        vehicleModelEt = findViewById(R.id.vehicleModelEt);
        vehicleType = findViewById(R.id.vehicleType);
        btnConfirm = findViewById(R.id.btnConfirm);

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);

        bckPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateFields();
            }
        });


        dobEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(ActivityBecomeADriver.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                dobEdit.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });


        vehicleType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void validateFields() {

        String driverName = driverNameEt.getText().toString().trim();
        String dob = dobEdit.getText().toString().trim();
        String email = emailEt.getText().toString().trim();
        String phone = phoneEt.getText().toString().trim();

        String vehicleOwner = vehicleOwnerEt.getText().toString().trim();
        String vehicleNumber = vehicleNumberEt.getText().toString().trim();
        String vehicleColor = vehicleColorEt.getText().toString().trim();
        String vehicleMake = vehicleMakeEt.getText().toString().trim();
        String vehicleModel = vehicleModelEt.getText().toString().trim();
        String vehicleTy = vehicleType.getText().toString().trim();

        if (inputValidatorHelper.isNullOrEmpty(driverName)) {
            Toasty.warning(this, "Enter Driver Name", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(dob)) {
            Toasty.warning(this, "Enter DOB", Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(email)) {
            Toasty.warning(this, "Enter email", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isValidEmail(email) == false) {
            Toasty.warning(this, masterPojo.getInvalidEmail(), Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(phone)) {
            Toasty.warning(this, "Enter phone number", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(vehicleOwner)) {
            Toasty.warning(this, "Enter vehicle owner", Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(vehicleNumber)) {
            Toasty.warning(this, "Enter vehicle number", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(vehicleColor)) {
            Toasty.warning(this, "Enter vehicle color", Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(vehicleMake)) {
            Toasty.warning(this, "Enter vehicle make", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(vehicleModel)) {
            Toasty.warning(this, "Enter vehicle model", Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(vehicleTy)) {
            Toasty.warning(this, "Enter vehicle type", Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (!AppNetWorkStatus.getInstance(this).isOnline()) {
            Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }


        BecomeADriver becomeADriver = new BecomeADriver();
        becomeADriver.setName(driverName);
        becomeADriver.setDob(dob);
        becomeADriver.setEmail(email);
        becomeADriver.setMobile(phone);

        becomeADriver.setVehOwner(vehicleOwner);
        becomeADriver.setVehNo(vehicleNumber);
        becomeADriver.setVehColor(vehicleColor);
        becomeADriver.setVehMake(vehicleMake);
        becomeADriver.setVehModel(vehicleModel);

        if(vehicleTypeId == 0){
            becomeADriver.setVehType(null);
        } else {
            becomeADriver.setVehType(vehicleTypeId);
        }

        becomeADriver.setStatus(0);
        becomeADriver.setRequestedDate(getDateTime());

        callApiBecomeDriver(becomeADriver);
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private void callApiBecomeDriver(BecomeADriver becomeADriver) {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<WishList>> call = apiService.becomeADriver(becomeADriver);

        call.enqueue(new Callback<WebResponse<WishList>>() {
            @Override
            public void onResponse(Call<WebResponse<WishList>> call, Response<WebResponse<WishList>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage(),Toasty.LENGTH_SHORT).show();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage(),Toasty.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<WishList>> call, Throwable t) {
                Toasty.error(getApplicationContext(), "Something went wrong", Toasty.LENGTH_SHORT).show();
            }
        });

    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }
}