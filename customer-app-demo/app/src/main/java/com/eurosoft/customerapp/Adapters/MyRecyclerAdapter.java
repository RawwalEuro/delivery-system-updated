package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Header;
import com.eurosoft.customerapp.Model.ListItem;
import com.eurosoft.customerapp.Model.ProductDetails;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;

import java.util.List;

public class MyRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    //Header header;
    List<ListItem> list;
    private Context context;
    private OnItemClickListener mOnItemClickListener;
    private AppSettings appSettings;


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }


    public MyRecyclerAdapter(Context context, List<ListItem> headerItems, AppSettings appSettings, OnItemClickListener onItemClickListener) {
        this.list = headerItems;
        this.context = context;
        mOnItemClickListener = onItemClickListener;
        this.appSettings = appSettings;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_HEADER) {
            View v = inflater.inflate(R.layout.item_header, parent, false);
            return new VHHeader(v);
        } else {
            View v = inflater.inflate(R.layout.adapter_order_details, parent, false);
            final VHItem viewHolder = new VHItem(v);

            viewHolder.productName.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
                }
            });


            return viewHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHHeader) {
            // VHHeader VHheader = (VHHeader)holder;
            Header currentItem = (Header) list.get(position);
            VHHeader VHheader = (VHHeader) holder;
            VHheader.txtTitle.setText(currentItem.getHeader());
        } else if (holder instanceof VHItem) {
            ProductDetails currentItem = (ProductDetails) list.get(position);
            VHItem VHitem = (VHItem) holder;
            VHitem.productName.setText(currentItem.getProductName());
            VHitem.productPricing.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(Math.round(currentItem.getQuantity()) * currentItem.getPrice()) + "");
            VHitem.value.setText(Math.round(currentItem.getQuantity()) + " x");
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return list.get(position) instanceof Header;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class VHHeader extends RecyclerView.ViewHolder {
        TextView txtTitle;

        public VHHeader(View itemView) {
            super(itemView);
            this.txtTitle = (TextView) itemView.findViewById(R.id.title_header);
        }
    }

    class VHItem extends RecyclerView.ViewHolder {
        private TextView productName, value, productPricing;

        public VHItem(View itemView) {
            super(itemView);
            this.productName = (TextView) itemView.findViewById(R.id.productName);
            this.productPricing = itemView.findViewById(R.id.pricingDetails);
            this.value = itemView.findViewById(R.id.value);

        }
    }
}