package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.ActivityAllCategories;
import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.StoreList;
import com.eurosoft.customerapp.R;
import com.fxn.stash.Stash;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class AdapterHome extends RecyclerView.Adapter<AdapterHome.ViewHolder> {

  private Context context;
  private List<StoreList> storesArrayList;
  private MasterPojo masterPojo;
  private AppSettings appSetting;
  private OnItemClickListener mOnItemClickListener;
  private AdapterNestedHome adapterNestedHome;

  public AdapterHome(Context ctx, List<StoreList> storesList, AppSettings appSettings, MasterPojo masterPojoo, OnItemClickListener onItemClickListener) {
    context = ctx;
    storesArrayList = storesList;
    appSetting = appSettings;
    masterPojo = masterPojoo;
    mOnItemClickListener = onItemClickListener;
  }


  @Override
  public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(context).inflate(R.layout.adapter_home_main, parent, false);
    Stash.init(context);
    return new AdapterHome.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
    StoreList store = storesArrayList.get(position);

    holder.categoryTitle.setText(store.getName());
    AdapterNestedHome adapter=new AdapterNestedHome(context,store.getStoreList(),appSetting,masterPojo);

    if(store.getStoreList() == null || store.getStoreList().size() == 0){
      holder.mainLayout.setVisibility(View.GONE);
    }

    holder.recyclerViewNested.setAdapter(adapter);
    holder.recyclerViewNested.setHasFixedSize(true);
    LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
    holder.recyclerViewNested.setLayoutManager(layoutManager);

    holder.seeMore.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(),store.getStoreList());
      }
    });
  }

  private void disableEnableControls(boolean enable, ViewGroup vg){
    for (int i = 0; i < vg.getChildCount(); i++){
      View child = vg.getChildAt(i);
      child.setEnabled(enable);
      if (child instanceof ViewGroup){
        disableEnableControls(enable, (ViewGroup)child);
      }
    }
  }


  public interface OnItemClickListener {
    public void onItemClick(View view, int position,List<Store> storeList);
  }

  @Override
  public int getItemCount() {
    return storesArrayList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    private TextView categoryTitle,seeMore;
    private RecyclerView recyclerViewNested;
    private RelativeLayout mainLayout;

    public ViewHolder(@NonNull @NotNull View itemView) {
      super(itemView);

      categoryTitle = itemView.findViewById(R.id.categoryTitle);
      seeMore = itemView.findViewById(R.id.seeMore);
      recyclerViewNested = itemView.findViewById(R.id.homeRecylerMain);
      mainLayout = itemView.findViewById(R.id.mainLayout);
    }

  }
}
