package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Feedback {


    public Feedback(Integer id, Integer bookingId, Integer type, String rating, String message, Boolean status, String createDate) {
        this.id = id;
        this.bookingId = bookingId;
        this.type = type;
        this.rating = rating;
        this.message = message;
        this.status = status;
        this.createDate = createDate;
    }

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bookingId")
    @Expose
    private Integer bookingId;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("createDate")
    @Expose
    private String createDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}
