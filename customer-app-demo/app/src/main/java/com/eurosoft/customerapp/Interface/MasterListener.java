package com.eurosoft.customerapp.Interface;


import com.eurosoft.customerapp.Model.MasterPojo;

public interface MasterListener {
    void onMasterPojoReceived(MasterPojo masterPojo);
}
