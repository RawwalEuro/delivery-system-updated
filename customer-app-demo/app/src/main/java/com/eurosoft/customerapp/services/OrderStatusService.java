package com.eurosoft.customerapp.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.eurosoft.customerapp.ActivityPastOrders;
import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.NotificationStatus;
import com.eurosoft.customerapp.Model.Order;
import com.eurosoft.customerapp.Model.OrderModel;
import com.eurosoft.customerapp.Model.OrderStatus;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OrderStatusService extends Service {


    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 10000;
    public static String receiver = "receiver.orderstatus";
    Intent intent;
    private User currentUser;
    private List<OrderModel> listActiveOrders = new ArrayList<>();
    private ArrayList<NotificationStatus> listTempStatus;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();

        listTempStatus = new ArrayList<>();
        Stash.init(this);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        getOrdersByUserId();
        mTimer = new Timer();
        mTimer.schedule(new TimeToGetStatus(), 100, notify_interval);
        intent = new Intent(receiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        if (intent.getAction().equalsIgnoreCase(Constants.STARTFOREGROUND_ACTION)) {


            String input = intent.getStringExtra("inputExtra");
            Intent notificationIntent = new Intent(this, ActivityPastOrders.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,
                    0, notificationIntent, 0);
            createNotificationChannel();
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("Papa Jol's")
                    .setContentText("Tap here to see all active orders")
                    .setSmallIcon(R.drawable.app_icon)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(false)
                    .build();
            startForeground(1, notification);
        } else {
            stopForeground(true);
            stopSelfResult(startId);
        }
        return super.onStartCommand(intent, flags, startId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private class TimeToGetStatus extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    getOrdersByUserId();
                }
            });

        }
    }

    int iterator = 0;

    private void callApiGetOrderStatus(int orderId) {
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<OrderStatus>> call = apiService.getOrderStatus(currentUser.getId(), orderId, currentUser.getToken());

        call.enqueue(new Callback<WebResponse<OrderStatus>>() {
            @Override
            public void onResponse(Call<WebResponse<OrderStatus>> call, Response<WebResponse<OrderStatus>> response) {
                if (response.body().getData() == null) {
                    callApiGetOrderStatus(listActiveOrders.get(iterator).getId());
                    return;
                }
                if (response.isSuccessful() && response.code() == 200) {
                    String orderStatus = response.body().getData().getOrderStatus();
                    int orderId = response.body().getData().getId();
                    //createNotificationStatus(response.body().getData().get(0).getOrderStatus(), response.body().getData().get(0).getOrderNo(), response.body().getData().get(0).getId());
                    if (listTempStatus == null || listTempStatus.size() == 0) {
                        NotificationStatus notificationStatus = new NotificationStatus(response.body().getData().getOrderStatus(), response.body().getData().getId());
                        listTempStatus.add(notificationStatus);
                        //NotificationBuilder(response.body().getData().getOrderStatus(), response.body().getData().getId());
                        update();
                    } else if (listTempStatus.size() > 0) {
                        if (isStatusSame(orderStatus, orderId)) {
                            return;
                        } else if (!isStatusSame(orderStatus, orderId)) {
                            NotificationStatus notificationStatus = new NotificationStatus(response.body().getData().getOrderStatus(), response.body().getData().getId());
                            listTempStatus.add(notificationStatus);
                            //NotificationBuilder(response.body().getData().getOrderStatus(), response.body().getData().getId());
                            update();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<OrderStatus>> call, Throwable t) {


            }
        });

    }

    private boolean isStatusSame(String orderStatus, int orderId) {
        for (int i = 0; i < listTempStatus.size(); i++) {
            if (listTempStatus.get(i).getOrderStatus().equalsIgnoreCase(orderStatus)
                    && listTempStatus.get(i).getId().equals(orderId)) {
                return true;
            }
        }
        return false;
    }


    private void getOrdersByUserId() {

        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            return;
        }


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<OrderModel>> call = apiService.getAllActivieNPastOrders(currentUser.getId(), 2, currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<OrderModel>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<OrderModel>> call, Response<CustomWebResponse<OrderModel>> response) {
                if (response.code() != 200 || response.body() == null) {

                    return;
                }
                if (response.body().getSuccess()) {
                    if (response.body().getData().size() == 0) {

                        Stash.put(Constants.IS_ACTIVE_ORDERS_PRESENT, false);
                        stopSelf();
                        return;
                    } else {
                        Stash.put(Constants.IS_ACTIVE_ORDERS_PRESENT, true);
                        listActiveOrders = response.body().getData();
                        for (int i = 0; i < listActiveOrders.size(); i++) {
                            //TODO
                            // callApiGetOrderStatus(listActiveOrders.get(i).getId());
                            NotificationBuilder(listActiveOrders.get(i).getOrderStatus(), listActiveOrders.get(i).getId(), listActiveOrders.get(i).getOrderStatusId());
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<OrderModel>> call, Throwable t) {

            }
        });

    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);

        }
    }


    public void NotificationBuilder(String status, int orderId, int orderStatusId) {

        if (orderStatusId == OrderModel.PreOrder || orderStatusId == OrderModel.RecentOrder ||
                orderStatusId == OrderModel.Confirmed || orderStatusId == OrderModel.Recover ||
                orderStatusId == OrderModel.AcceptPending || orderStatusId == OrderModel.Decline || orderStatusId == OrderModel.Accept  || orderStatusId == OrderModel.Collect) {
            update();
            return;
        }


        if (listTempStatus == null || listTempStatus.size() == 0) {
            NotificationStatus notificationStatus = new NotificationStatus(status, orderId);
            listTempStatus.add(notificationStatus);
            NotificationBuilder(status, orderId, orderStatusId);
            update();
        } else if (listTempStatus.size() > 0) {
            if (isStatusSame(status, orderId)) {
                return;
            } else if (!isStatusSame(status, orderId)) {
                NotificationStatus notificationStatus = new NotificationStatus(status, orderId);
                listTempStatus.add(notificationStatus);
                NotificationBuilder(status, orderId, orderStatusId);
            }
        }


        try {
            createNotificationChannel();

            if (orderStatusId == OrderModel.Cancelled || orderStatusId == OrderModel.Deliver) {
                Intent notificationIntent = new Intent(this, ActivityPastOrders.class);
                notificationIntent.putExtra("frgToLoad", "1");
                notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,
                        0, notificationIntent, 0);
                Notification mNotification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("Papa Jol's")
                        .setContentText("Your order " + "EC- " + orderId + " is " + status)
                        .setSmallIcon(R.drawable.notification_icon)
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true)
                        .build();
                NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(orderId, mNotification);
            } else {
                Intent notificationIntent = new Intent(this, ActivityPastOrders.class);
                notificationIntent.putExtra("frgToLoad", "0");
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(this,
                        0, notificationIntent, 0);

                Notification mNotification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle("Papa Jol's")
                        .setContentText("Your order " + "EC- " + orderId + " is " + status)
                        .setSmallIcon(R.drawable.app_icon)
                        .setContentIntent(pendingIntent)
                        .setNumber(0)
                        .setAutoCancel(true)
                        .build();
                NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
                nm.notify(orderId, mNotification);
            }


            if (orderStatusId == OrderModel.Cancelled) {
                update();
                if (listActiveOrders.size() == 0) {

                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 120000);

                }
            }


            if (orderStatusId == OrderModel.Completed) {
                update();
                if (listActiveOrders.size() == 0) {
                    //  stopSelf();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            //stopSelf();
                        }
                    }, 120000);
                }
            }


            if (orderStatusId == OrderModel.GotoCustomer) {
                update();
                if (listActiveOrders.size() == 0) {
                    //  stopSelf();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            //  stopSelf();
                        }
                    }, 120000);
                }
            }


            if (orderStatusId == OrderModel.Arrive) {
                update();
                if (listActiveOrders.size() == 0) {
                    //  stopSelf();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                            //  stopSelf();
                        }
                    }, 120000);
                }

            }


            if (orderStatusId == OrderModel.Deliver) {
                //getLastCompletedOrder(orderId, status);

                update();
                if (listActiveOrders.size() == 0) {
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 120000);
                }
            }


            if (orderStatusId == OrderModel.Collect) {
                update();
                if (listActiveOrders.size() == 0) {
                    //stopSelf();
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 120000);
                }
            }

            if (orderStatusId == OrderModel.Waiting) {
                if (listActiveOrders.size() == 0) {
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 120000);
                }
            }

            if (orderStatusId == OrderModel.AcceptPending) {
                update();
                if (listActiveOrders.size() == 0) {
                    final Handler handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }, 120000);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void updateComplete(Order order) {

        if (order == null || order.isFeedbackGiven() == true) {
            return;
        } else {
            Stash.put(Constants.ORDER_REVIEW, order);
            sendBroadcast(intent);
        }
    }

    private void update() {
        intent.putExtra("abc", "");
        sendBroadcast(intent);
    }


    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                stopSelf();
                return true;
            }
        }
        return false;
    }
}
