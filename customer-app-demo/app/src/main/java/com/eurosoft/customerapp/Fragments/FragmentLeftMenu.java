package com.eurosoft.customerapp.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.eurosoft.customerapp.ActivityBecomeADriver;
import com.eurosoft.customerapp.ActivityPastOrders;
import com.eurosoft.customerapp.ActivityPayment;
import com.eurosoft.customerapp.ActivityProfile;
import com.eurosoft.customerapp.ActivitySettings;
import com.eurosoft.customerapp.ActivityWishList;
import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.LoginActivity;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.ServiceTools;
import com.eurosoft.customerapp.services.OrderStatusService;
import com.facebook.login.LoginManager;
import com.fxn.stash.Stash;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

import org.jetbrains.annotations.NotNull;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class FragmentLeftMenu extends Fragment implements View.OnClickListener {

    private LinearLayout llOrders, logout, profile, payments, Language, settings;
    private User currentUser;
    private TextView name, orders, profile_text, logout_text, settingsLL, wishListLL,becomeADriver;
    private ImageView profile_image;
    private TextView email;
    private FirebaseAuth firebaseAuth;
    private GoogleApiClient mGoogleApiClient;
    private MasterPojo masterPojo;
    private onCLoseDrawer onCLoseDrawerListeners;


    public interface onCLoseDrawer {
        public void sendEventCloseDrawer(boolean closeDrawer);
    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.left_navigation_drawer, container, false);

        init(view);
        setData(true);
        return view;
    }

    private void setData(boolean checkBoolean) {
        Stash.init(getContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = (User) Stash.getObject(Constants.USER, User.class);

        name.setText(currentUser.getFirstName() + " " + currentUser.getLastName());
        email.setText(currentUser.getCustomerEmail() + "");

        orders.setText(masterPojo.getOrders());
        profile_text.setText(masterPojo.getProfile());
        logout_text.setText(masterPojo.getLogOut());
        settingsLL.setText(masterPojo.getSettings());
        //Picasso.get().load("https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?r=pg").into(profile_image);

    }

    private void init(View view) {

        llOrders = view.findViewById(R.id.llOrders);
        logout = view.findViewById(R.id.logout);
        name = view.findViewById(R.id.name);
        email = view.findViewById(R.id.email);
        profile_image = view.findViewById(R.id.profile_image);
        profile = view.findViewById(R.id.profile);
        payments = view.findViewById(R.id.payments);
        Language = view.findViewById(R.id.Language);
        settings = view.findViewById(R.id.settings);
        settingsLL = view.findViewById(R.id.settingsLL);
        wishListLL = view.findViewById(R.id.wishListLL);
        becomeADriver = view.findViewById(R.id.becomeADriver);

        //Language
        orders = view.findViewById(R.id.orders);
        profile_text = view.findViewById(R.id.profile_text);
        logout_text = view.findViewById(R.id.logout_text);

        llOrders.setOnClickListener(this);
        logout.setOnClickListener(this);
        profile_image.setOnClickListener(this);
        profile.setOnClickListener(this);
        payments.setOnClickListener(this);
        settingsLL.setOnClickListener(this);
        wishListLL.setOnClickListener(this);
        becomeADriver.setOnClickListener(this);
    }

    public static Fragment newInstance() {
        FragmentLeftMenu f = new FragmentLeftMenu();
        return f;
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            onCLoseDrawerListeners = (onCLoseDrawer) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement MyInterface ");
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.llOrders:
                onCLoseDrawerListeners.sendEventCloseDrawer(true);
                Intent intent = new Intent(getActivity(), ActivityPastOrders.class);
                intent.putExtra("frgToLoad", "0");
                startActivity(intent);

                break;

            case R.id.payments:
                onCLoseDrawerListeners.sendEventCloseDrawer(true);
                startActivity(new Intent(getActivity(), ActivityPayment.class));

                break;

            case R.id.logout:


                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(masterPojo.getLogutQustion())
                        .setConfirmText(masterPojo.getYes())

                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog
                                        .setTitleText(masterPojo.getLoggedOut())
                                        .setConfirmClickListener(null)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                logoutApp();
                            }
                        })

                        .setCancelText(masterPojo.getNo())
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        })
                        .show();
                //logoutApp();

                break;


            case R.id.profile:
                onCLoseDrawerListeners.sendEventCloseDrawer(true);
                startActivity(new Intent(getActivity(), ActivityProfile.class));

                break;


            case R.id.profile_image:
                onCLoseDrawerListeners.sendEventCloseDrawer(true);
                startActivity(new Intent(getActivity(), ActivityProfile.class));
                break;


            case R.id.settingsLL:
                onCLoseDrawerListeners.sendEventCloseDrawer(true);
                startActivity(new Intent(getActivity(), ActivitySettings.class));
                break;

            case R.id.wishListLL:
                onCLoseDrawerListeners.sendEventCloseDrawer(true);
                startActivity(new Intent(getActivity(), ActivityWishList.class));
                break;



            case R.id.becomeADriver:
                onCLoseDrawerListeners.sendEventCloseDrawer(true);
                startActivity(new Intent(getActivity(), ActivityBecomeADriver.class));
                break;


        }

    }


    private void logoutApp() {
        try {
            Boolean isServiceRunning = ServiceTools.isServiceRunning(
                    getActivity(),
                    OrderStatusService.class);

            if (isServiceRunning) {
                Intent stopIntent = new Intent(getActivity(), OrderStatusService.class);
                stopIntent.setAction(Constants.STOPFOREGROUND_ACTION);
                getActivity().startService(stopIntent);
            } else {
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        Toast.makeText(getActivity(), masterPojo.getLoggedOut(), Toast.LENGTH_LONG).show();
        Stash.clear(Constants.isLoggenIn);
        Stash.clear(Constants.USER);
        Stash.clear(Constants.USER_ID);
        Stash.clear(Constants.IS_LANGUAGE_SELECTED);
        Intent i = new Intent(getActivity(), LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);


        if (Stash.getBoolean(Constants.isLoggenIn)) {
            LoginManager.getInstance().logOut();
            Stash.put(Constants.LOGIN_FROM_FB, false);
        }


        if (Auth.GoogleSignInApi == null)
            return;

        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {

                        // ...
                    }
                });
    }


    @Override
    public void onResume() {
        super.onResume();
        setData(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }
}
