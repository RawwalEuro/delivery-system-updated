package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stores {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @SerializedName("StoreImage")
    @Expose
    private String storeImage;
    @SerializedName("OrderBy")
    @Expose
    private Integer orderBy;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;

    @SerializedName("StoreRating")
    @Expose
    private Double storeRating;


    @SerializedName("StoreOpeningTime")
    @Expose
    private String storeOpeningTime;

    @SerializedName("StoreClosingTime")
    @Expose
    private String storeClosingTime;

    @SerializedName("StoreDeliveryEstimatedTime")
    @Expose
    private String storeDeliveryEstimatedTime;


    @SerializedName("StoreAddress")
    @Expose
    private String storeAddress;

    public String getStoreAddress() {

        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public Double getStoreRating() {


        return storeRating;
    }

    public void setStoreRating(Double storeRating) {
        this.storeRating = storeRating;
    }

    public String getStoreOpeningTime() {


        return storeOpeningTime;
    }

    public void setStoreOpeningTime(String storeOpeningTime) {

        this.storeOpeningTime = storeOpeningTime;
    }

    public String getStoreClosingTime() {


        return storeClosingTime;
    }

    public void setStoreClosingTime(String storeClosingTime) {
        this.storeClosingTime = storeClosingTime;
    }

    public String getStoreDeliveryEstimatedTime() {

        return storeDeliveryEstimatedTime;
    }

    public void setStoreDeliveryEstimatedTime(String storeDeliveryEstimatedTime) {
        this.storeDeliveryEstimatedTime = storeDeliveryEstimatedTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }


}
