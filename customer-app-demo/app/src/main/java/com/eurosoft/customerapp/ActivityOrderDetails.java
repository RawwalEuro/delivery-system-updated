package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Adapters.MyRecyclerAdapter;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Header;
import com.eurosoft.customerapp.Model.ListItem;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.OrderModel;
import com.eurosoft.customerapp.Model.ProductDetails;
import com.eurosoft.customerapp.Model.ReviewModel;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;
import com.google.gson.Gson;
import com.hosseiniseyro.apprating.AppRatingDialog;
import com.hosseiniseyro.apprating.listener.RatingDialogListener;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityOrderDetails extends AppCompatActivity implements View.OnClickListener, RatingDialogListener {

    private TextView orderId, orderStatus, cash, subTotal, deliveryFee, totalBill, estimateTime, estimateHeading;
    private TextView restruantName;
    private RecyclerView cartRv;
    private OrderModel order;
    private ImageView close;
    private User currentUser;
    private LinearLayout rlTransId;
    private TextView transId;
    private AppRatingDialog appDialog;
    private AppSettings appSettings;
    private MasterPojo masterPojo;
    private TextView transactionId, paymentMethod, subtotalTv, deliveryFeeTv, total, inclusive_tax;
    private RelativeLayout deliveryRl;
    private TextView deliveryAddress;
    private TextView orderIdd;
    private TextView deliveryHeading;
    private TextView orderHeading;
    private TextView servicesChargesValue,tipValueTv;
    private RelativeLayout rlServicesCharges,rlTip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        initViews();
        getData();

    }

    private void getData() {
        Gson gson = new Gson();
        order = gson.fromJson(getIntent().getStringExtra("myjson"), OrderModel.class);
        if (order == null) {
            return;
        } else {
            setData(order);
        }
    }

    private void setData(OrderModel order) {
        // orderId.setText(order.getOrderPlacedId() + "");


        if (order.getProductList().size() == 0) {
            return;
        }
        restruantName.setText(order.getProductList().get(0).getStoreName() + "");
        orderStatus.setText(order.getOrderStatus() + "");

        subTotal.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(order.getSubTotal()) + "");
        deliveryFee.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(order.getShippingCharges()) + "");
        totalBill.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(order.getTotalAmount()) + "");
        deliveryAddress.setText(order.getDropoffAddress());
        orderIdd.setText(order.getOrderNo());
        if (!order.getDeliveryTime().equalsIgnoreCase("")) {
            estimateTime.setText(order.getDeliveryTime() + " mins");
            estimateTime.setVisibility(View.VISIBLE);
        } else {
            estimateTime.setVisibility(View.INVISIBLE);
        }


        if (order.getPaymentTypeName().equalsIgnoreCase("Cash")) {
            cash.setText(masterPojo.getCash());
            rlTransId.setVisibility(View.GONE);

        } else if (order.getPaymentTypeName().equalsIgnoreCase("Card")) {
            cash.setText(masterPojo.getCard());
            rlTransId.setVisibility(View.VISIBLE);
            transId.setText(order.getTransactionId() + "");
        }
        setStickyRecyler();


        if (order.getOrderStatusId() == OrderModel.Deliver || order.getOrderStatusId() == OrderModel.Cancelled || order.getOrderStatusId() == OrderModel.GotoCustomer || order.getOrderStatusId() == OrderModel.Arrive) {
            deliveryRl.setVisibility(View.GONE);
        } else {
            deliveryRl.setVisibility(View.VISIBLE);
        }

        if (order.getOrderStatusId() == OrderModel.Deliver && !order.getIsFeedBackGiven()) {
            showDialog();
        }

        if(order.getTipValue() == null || order.getTipValue() == 00.00){
            rlTip.setVisibility(View.GONE);
        } else {
            rlTip.setVisibility(View.VISIBLE);
            tipValueTv.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(order.getTipValue()) + "");
        }

        if(order.getServiceCharges() == null || order.getServiceCharges() == 00.00){
            rlServicesCharges.setVisibility(View.GONE);
        } else {
            rlServicesCharges.setVisibility(View.VISIBLE);
            servicesChargesValue.setText(Converters.roundfloat(order.getServiceCharges()) + "");
        }
    }

    private void initViews() {

        restruantName = findViewById(R.id.restruantName);
        orderStatus = findViewById(R.id.orderStatus);
        cartRv = findViewById(R.id.cartRv);
        cash = findViewById(R.id.cash);
        subTotal = findViewById(R.id.subTotal);
        deliveryFee = findViewById(R.id.deliveryFee);
        totalBill = findViewById(R.id.totalBill);
        close = findViewById(R.id.close);
        rlTransId = findViewById(R.id.rlTransId);
        transId = findViewById(R.id.transId);
        deliveryRl = findViewById(R.id.deliveryRl);
        servicesChargesValue = findViewById(R.id.servicesChargesValue);
        tipValueTv = findViewById(R.id.tipValueTv);
        rlServicesCharges = findViewById(R.id.rlServicesCharges);
        rlTip = findViewById(R.id.rlTip);

        deliveryAddress = findViewById(R.id.deliveryAddress);
        orderIdd = findViewById(R.id.orderIdd);


        //Language
        orderId = findViewById(R.id.orderId);
        estimateHeading = findViewById(R.id.estimateHeading);
        estimateTime = findViewById(R.id.estimateTime);
        transactionId = findViewById(R.id.transactionId);
        paymentMethod = findViewById(R.id.paymentMethod);
        subtotalTv = findViewById(R.id.subtotalTv);
        deliveryFeeTv = findViewById(R.id.deliveryFeeTv);
        total = findViewById(R.id.total);
        inclusive_tax = findViewById(R.id.inclusive_tax);
        deliveryHeading = findViewById(R.id.deliveryHeading);
        orderHeading = findViewById(R.id.orderHeading);


        orderId.setText(masterPojo.getOrderDetails());
        estimateHeading.setText(masterPojo.getEstimateHeading());
        estimateTime.setText(masterPojo.getEstimateTime());
        transactionId.setText(masterPojo.getTransactionId());
        paymentMethod.setText(masterPojo.getPaymentMethod());
        subtotalTv.setText(masterPojo.getSubTotal());
        deliveryFeeTv.setText(masterPojo.getDeliveryFee());
        total.setText(masterPojo.getTotal());
        inclusive_tax.setText(masterPojo.getInclusiveTax());
        deliveryHeading.setText(masterPojo.getDeliveryAddress());
        orderHeading.setText(masterPojo.getOrderId());
        close.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //registerReceiver(broadcastReceiver, new IntentFilter(OrderStatusService.receiver));
    }


    @Override
    protected void onStop() {
        super.onStop();
        // unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.close:
                finish();
                break;
        }
    }

    private void setStickyRecyler() {
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(this, getList(), appSettings, new MyRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        });
        cartRv.setLayoutManager(new LinearLayoutManager(this));
        cartRv.setAdapter(adapter);

    }


    private ArrayList<ListItem> getList() {
        ArrayList<ListItem> arrayList = new ArrayList<>();
        List<ProductDetails> productsList = order.getProductList();///order products List
        for (int j = 0; j < productsList.size(); j++) {
            Header header = new Header();
            if (j == 0) {//header set
                header.setHeader(productsList.get(j).getStoreName());
                arrayList.add(header);
                arrayList.add(productsList.get(j));
            } else {

                if (productsList.get(j - 1).getStoreId() != productsList.get(j).getStoreId()) {
                    header.setHeader(productsList.get(j).getStoreName());
                    arrayList.add(header);
                }
                arrayList.add(productsList.get(j));
                /*for (int i = 0; i < arrayList.size(); i++) {
                    if(arrayList.get(i) instanceof  Header){
                     if(arrayList.get(i).getName().equals(productsList.get(j).getStoreName())) {
                         List<ProductDetails> productsListTemp =  ((List<ProductDetails>) arrayList.get(i+1));

                         arrayList.set(i+1,productsList.get(j));

                     }
                    }
                }*/
            }
        }

        return arrayList;
    }


    private void showDialog() {

        new AppRatingDialog.Builder()
                .setPositiveButtonText(masterPojo.getSubmit())
                .setNegativeButtonText(masterPojo.getSkip())
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(2)
                .setThreshold(5)
                .setTitle(masterPojo.getTotalBill() +" " + appSettings.getRegionSettingData().getCurrencySymbol()+ Converters.roundfloat(order.getTotalAmount()) + "\n" + order.getOrderNo())
                .setDescription(masterPojo.getSelectStars())
                .setCommentInputEnabled(true)
                .setDefaultComment(masterPojo.getGoodExperience())
                .setStarColor(R.color.colorPrimary)
                .setNoteDescriptionTextColor(R.color.grey)
                .setTitleTextColor(R.color.black)
                .setDescriptionTextColor(R.color.black)
                .setHint(masterPojo.getPleaseWrite())
                .setHintTextColor(R.color.colorPrimary)
                .setCommentTextColor(R.color.black)
                .setCommentBackgroundColor(R.color.light_greu)
                .setDialogBackgroundColor(R.color.white)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(ActivityOrderDetails.this)
                .show();

    }


    @Override
    public void onNegativeButtonClicked() {
        callApiGiveReview(5, "", 1, false);
    }

    @Override
    public void onNeutralButtonClicked() {
        callApiGiveReview(5, "", 1, false);
    }

    @Override
    public void onPositiveButtonClickedWithComment(int i, @NotNull String s) {
        callApiGiveReview(i, s, 1, true);
    }

    private void callApiGiveReview(int rating, String feedback, int type, boolean showToast) {

        ReviewModel reviewModel = new ReviewModel(order.getProductList().get(0).getStoreId(), order.getId(), rating, rating, rating, feedback);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<ReviewModel>> call = apiService.orderFeedback(reviewModel,currentUser.getToken());
        call.enqueue(new Callback<WebResponse<ReviewModel>>() {
            @Override
            public void onResponse(Call<WebResponse<ReviewModel>> call, Response<WebResponse<ReviewModel>> response) {

                if (response.code() != 200 || response.body() == null) {
                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toasty.LENGTH_LONG).show();
                    return;
                }


                if (response.isSuccessful() && response.code() == 200) {

                    order.setIsFeedBackGiven(true);
                    if (showToast) {
                        Toasty.success(getApplicationContext(), response.body().getMessage(), Toasty.LENGTH_LONG).show();
                    }

                }

            }

            @Override
            public void onFailure(Call<WebResponse<ReviewModel>> call, Throwable t) {

            }
        });


    }


    public static String parseDateToddMMyyyy(String format) {
        SimpleDateFormat outputFormat = new SimpleDateFormat(format);
        Date date = null;
        String str = null;

        try {
            date = Calendar.getInstance().getTime();
            str = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public void onPositiveButtonClickedWithoutComment(int i) {

    }
}