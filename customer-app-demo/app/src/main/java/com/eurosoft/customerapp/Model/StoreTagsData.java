package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StoreTagsData implements Serializable {

  @SerializedName("Id")
  @Expose
  private Integer id;
  @SerializedName("Name")
  @Expose
  private String name;
  @SerializedName("BaseUrl")
  @Expose
  private String baseUrl;
  @SerializedName("Icon")
  @Expose
  private String icon;
  @SerializedName("SortOrder")
  @Expose
  private Integer sortOrder;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public String getIcon() {
    return icon;
  }

  public void setIcon(String icon) {
    this.icon = icon;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }
}
