package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VehicleTypeList implements Serializable {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("VehicleType")
    @Expose
    private String vehicleType;
    @SerializedName("Passengers")
    @Expose
    private Integer passengers;
    @SerializedName("Luggages")
    @Expose
    private Integer luggages;
    @SerializedName("HandLuggages")
    @Expose
    private Integer handLuggages;
    @SerializedName("VehicleImage")
    @Expose
    private String vehicleImage;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("VehicleIcon")
    @Expose
    private String vehicleIcon;
    @SerializedName("IsDeleted")
    @Expose
    private Boolean isDeleted;
    @SerializedName("tbl_DriverVehicles")
    @Expose
    private List<Object> tblDriverVehicles = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public Integer getPassengers() {
        return passengers;
    }

    public void setPassengers(Integer passengers) {
        this.passengers = passengers;
    }

    public Integer getLuggages() {
        return luggages;
    }

    public void setLuggages(Integer luggages) {
        this.luggages = luggages;
    }

    public Integer getHandLuggages() {
        return handLuggages;
    }

    public void setHandLuggages(Integer handLuggages) {
        this.handLuggages = handLuggages;
    }

    public String getVehicleImage() {
        return vehicleImage;
    }

    public void setVehicleImage(String vehicleImage) {
        this.vehicleImage = vehicleImage;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getVehicleIcon() {
        return vehicleIcon;
    }

    public void setVehicleIcon(String vehicleIcon) {
        this.vehicleIcon = vehicleIcon;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public List<Object> getTblDriverVehicles() {
        return tblDriverVehicles;
    }

    public void setTblDriverVehicles(List<Object> tblDriverVehicles) {
        this.tblDriverVehicles = tblDriverVehicles;
    }
}
