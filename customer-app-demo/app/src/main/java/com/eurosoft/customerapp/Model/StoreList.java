package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StoreList implements Serializable {


    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("BaseUrl")
    @Expose
    private String baseUrl;
    @SerializedName("Icon")
    @Expose
    private String icon;
    @SerializedName("SortOrder")
    @Expose
    private Integer sortOrder;
    @SerializedName("StoreList")
    @Expose
    private List<Store> storeList = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public List<Store> getStoreList() {
        return storeList;
    }

    public void setStoreList(List<Store> storeList) {
        this.storeList = storeList;
    }


   /* @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("StoreTypeName")
    @Expose
    private String storeTypeName;
    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @SerializedName("StoreOpeningTime")
    @Expose
    private String storeOpeningTime;
    @SerializedName("StoreClosingTime")
    @Expose
    private String storeClosingTime;
    @SerializedName("StoreDeliveryEstimatedTime")
    @Expose
    private String storeDeliveryEstimatedTime;
    @SerializedName("StoreDeliveryFee")
    @Expose
    private Double storeDeliveryFee;
    @SerializedName("StorePhoneNumber")
    @Expose
    private String storePhoneNumber;
    @SerializedName("StoreBaseUrl")
    @Expose
    private String storeBaseUrl;
    @SerializedName("StoreImage")
    @Expose
    private String storeImage;
    @SerializedName("StoreAddress")
    @Expose
    private String storeAddress;
    @SerializedName("StoreLocationTypeId")
    @Expose
    private String storeLocationTypeId;
    @SerializedName("StoreLocationType")
    @Expose
    private String storeLocationType;
    @SerializedName("StoreLatitude")
    @Expose
    private Double storeLatitude;
    @SerializedName("StoreLongitude")
    @Expose
    private Double storeLongitude;
    @SerializedName("OrderBy")
    @Expose
    private Integer orderBy;
    @SerializedName("IsStoreClosed")
    @Expose
    private Boolean isStoreOpen;
    @SerializedName("IsDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;




    public Store(int storeTagId) {
        this.storeTagId = storeTagId;
    }

    public Boolean getStoreOpen() {
        return isStoreOpen;
    }

    public void setStoreOpen(Boolean storeOpen) {
        isStoreOpen = storeOpen;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public int getStoreTagId() {
        return storeTagId;
    }

    public void setStoreTagId(int storeTagId) {
        this.storeTagId = storeTagId;
    }

    @Expose
    private int storeTagId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStoreTypeName() {
        return storeTypeName;
    }

    public void setStoreTypeName(String storeTypeName) {
        this.storeTypeName = storeTypeName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getStoreOpeningTime() {
        return storeOpeningTime;
    }

    public void setStoreOpeningTime(String storeOpeningTime) {
        this.storeOpeningTime = storeOpeningTime;
    }

    public String getStoreClosingTime() {
        return storeClosingTime;
    }

    public void setStoreClosingTime(String storeClosingTime) {
        this.storeClosingTime = storeClosingTime;
    }

    public String getStoreDeliveryEstimatedTime() {
        return storeDeliveryEstimatedTime;
    }

    public void setStoreDeliveryEstimatedTime(String storeDeliveryEstimatedTime) {
        this.storeDeliveryEstimatedTime = storeDeliveryEstimatedTime;
    }

    public Double getStoreDeliveryFee() {
        return storeDeliveryFee;
    }

    public void setStoreDeliveryFee(Double storeDeliveryFee) {
        this.storeDeliveryFee = storeDeliveryFee;
    }

    public String getStorePhoneNumber() {
        return storePhoneNumber;
    }

    public void setStorePhoneNumber(String storePhoneNumber) {
        this.storePhoneNumber = storePhoneNumber;
    }

    public String getStoreBaseUrl() {
        return storeBaseUrl;
    }

    public void setStoreBaseUrl(String storeBaseUrl) {
        this.storeBaseUrl = storeBaseUrl;
    }

    public String getStoreImage() {
        return storeImage;
    }

    public void setStoreImage(String storeImage) {
        this.storeImage = storeImage;
    }

    public String getStoreAddress() {
        return storeAddress;
    }

    public void setStoreAddress(String storeAddress) {
        this.storeAddress = storeAddress;
    }

    public String getStoreLocationTypeId() {
        return storeLocationTypeId;
    }

    public void setStoreLocationTypeId(String storeLocationTypeId) {
        this.storeLocationTypeId = storeLocationTypeId;
    }

    public String getStoreLocationType() {
        return storeLocationType;
    }

    public void setStoreLocationType(String storeLocationType) {
        this.storeLocationType = storeLocationType;
    }

    public Double getStoreLatitude() {
        return storeLatitude;
    }

    public void setStoreLatitude(Double storeLatitude) {
        this.storeLatitude = storeLatitude;
    }

    public Double getStoreLongitude() {
        return storeLongitude;
    }

    public void setStoreLongitude(Double storeLongitude) {
        this.storeLongitude = storeLongitude;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Boolean getIsStoreOpen() {
        if(isStoreOpen == null){
            return isStoreOpen = false;
        }

        return isStoreOpen;
    }

    public void setIsStoreOpen(Boolean isStoreOpen) {
        this.isStoreOpen = isStoreOpen;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }*/

}
