package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterSubCategory extends RecyclerView.Adapter<AdapterSubCategory.ViewHolder> {


    private Context mCtx;
    private ArrayList<Product> productList;
    private AppSettings appSettings;
    private OnItemClickListener mOnItemClickListener;

    public AdapterSubCategory(Context mCtx, ArrayList<Product> productArrayList, AppSettings appSetting,OnItemClickListener onItemClicked) {
        this.mCtx = mCtx;
        this.productList = productArrayList;
        this.appSettings = appSetting;
        this.mOnItemClickListener = onItemClicked;
    }

    @Override
    public AdapterSubCategory.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.adapter_restruant_menu, parent, false);
        return new AdapterSubCategory.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterSubCategory.ViewHolder holder, int position) {

        Product product = productList.get(position);
        if (product.isFirst()) {
            holder.heading.setVisibility(View.VISIBLE);
            holder.heading.setText(product.getcatName());
            //  ViewGroup.LayoutParams holder.itemView.getLayoutParams();
        } else {
            holder.heading.setVisibility(View.GONE);
        }


        if (product.isLast()) {
            ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(holder.restruant_menu.getLayoutParams());
            marginLayoutParams.setMargins(0, 0, 0, 10);
            holder.restruant_menu.setLayoutParams(marginLayoutParams);
            holder.line.setVisibility(View.GONE);
        } else {
            holder.line.setVisibility(View.VISIBLE);
        }

        holder.productName.setText(product.getProductName() + "");
        holder.productDesc.setText(product.getProductDesc() + "");
        holder.productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(product.getPrice()) + "");

        try {
            Picasso.get().load(product.getBaseUrl() + product.getId() + "/" + product.getPrimaryImage()).placeholder(R.color.grey).into(holder.loadImage);
        } catch (Exception e) {
            e.printStackTrace();
        }


        holder.restruant_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mOnItemClickListener.onItemClick(v,holder.getAdapterPosition());

                /*Intent intent = new Intent(mCtx, ProductDetailsActivity.class);
                // Toast.makeText(mCtx.getApplicationContext(), product.getStoreId(), Toast.LENGTH_LONG).show();
                Gson gson = new Gson();
                String myJson = gson.toJson(product);
                intent.putExtra("myjson", myJson);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mCtx.startActivity(intent);*/




            }
        });

    }


    public void filterList(ArrayList<Product> filteredList) {
        productList = filteredList;
        notifyDataSetChanged();
    }


    public ArrayList<Product> getProductList() {
        return productList;
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout restruant_menu;
        TextView productName, productDesc, productPrice, heading;
        ImageView loadImage;
        View line;

        public ViewHolder(View itemView) {
            super(itemView);
            restruant_menu = itemView.findViewById(R.id.restruant_menu);
            productName = itemView.findViewById(R.id.productName);
            productDesc = itemView.findViewById(R.id.productDesc);
            productPrice = itemView.findViewById(R.id.productPrice);
            loadImage = itemView.findViewById(R.id.loadImage);
            heading = itemView.findViewById(R.id.heading);
            line = itemView.findViewById(R.id.line);
        }

        @Override
        public void onClick(View view) {


        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}