package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.WishList;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;
import com.fxn.stash.Stash;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterWishList extends RecyclerView.Adapter<AdapterWishList.ViewHolder>{


    private AppSettings appSettings;
    private Context mCtx;
    private ArrayList<WishList> wishLists;
    private OnItemClickListener mOnItemClickListener;

    public AdapterWishList(Context mCtx, ArrayList<WishList> wishList, AppSettings appSetting, OnItemClickListener onItemClicked) {
        this.mCtx = mCtx;
        this.wishLists = wishList;
        this.appSettings = appSetting;
        this.mOnItemClickListener = onItemClicked;
    }


    @Override
    public AdapterWishList.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.adapter_wishlist, parent, false);
        return new AdapterWishList.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        WishList wishList = wishLists.get(position);
        holder.productName.setText(wishList.getProductName() + "");
        holder.productDesc.setText("");


        try {
            holder.productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(wishList.getPrice()) + " ");
            Picasso.get().load(wishList.getBaseUrl()  + wishList.getPrimaryImage()).placeholder(R.color.grey).into(holder.loadImage);

        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.restruant_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, wishList);

            }
        });
    }

    @Override
    public int getItemCount() {
        return wishLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout restruant_menu;
        TextView productName, productDesc, productPrice;
        private ImageView loadImage;

        public ViewHolder(View itemView) {
            super(itemView);
            restruant_menu = itemView.findViewById(R.id.restruant_menu);
            productName = itemView.findViewById(R.id.productName);
            productDesc = itemView.findViewById(R.id.productDesc);
            productPrice = itemView.findViewById(R.id.productPrice);
            loadImage = itemView.findViewById(R.id.loadImage);
        }
    }
    public interface OnItemClickListener {
        public void onItemClick(View view, WishList wishListItem);
    }
}
