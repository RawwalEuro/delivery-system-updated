package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterCheckOutProducts extends RecyclerView.Adapter<AdapterCheckOutProducts.ViewHolder> {


    private Context context;
    private List<Product> productArrayList;
    private AppSettings appSettings;
    private AdapterCheckOutOptionDetail adapterCheckOutProducts;

    public AdapterCheckOutProducts(Context ctx, List<Product> productArrayLists,AppSettings appSetting) {
        context = ctx;
        productArrayList = productArrayLists;
        appSettings = appSetting;
    }


    @Override
    public AdapterCheckOutProducts.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_checkout_products, parent, false);
        final AdapterCheckOutProducts.ViewHolder viewHolder = new AdapterCheckOutProducts.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterCheckOutProducts.ViewHolder holder, int position) {
        Product product = productArrayList.get(position);
        holder.quantityXname.setText(product.getQuantity() + "x " + product.getProductName() + "");
        holder.price.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(product.getPrice()) + "");
        adapterCheckOutProducts = new AdapterCheckOutOptionDetail(context,product.getOrderDetailData());
        holder.rvProductOptionDetails.setLayoutManager(new LinearLayoutManager(context));
        holder.rvProductOptionDetails.setAdapter(adapterCheckOutProducts);
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView price,quantityXname;
        private RecyclerView rvProductOptionDetails;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            price = itemView.findViewById(R.id.price);
            quantityXname = itemView.findViewById(R.id.quantityXname);
            rvProductOptionDetails = itemView.findViewById(R.id.rvProductOptionDetails);

        }
    }
}
