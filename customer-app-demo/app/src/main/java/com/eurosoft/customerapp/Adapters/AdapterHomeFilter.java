package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.Filter;
import com.eurosoft.customerapp.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AdapterHomeFilter extends RecyclerView.Adapter<AdapterHomeFilter.ViewHolder> {


    private Context context;
    private ArrayList<Filter> stringLists;
    private OnItemClickListener mOnItemClickListener;
    private int row_index = -1;

    public AdapterHomeFilter(Context ctx, ArrayList<Filter> stringList, OnItemClickListener onItemClickListener) {
        context = ctx;

        stringLists = stringList;
        mOnItemClickListener = onItemClickListener;
    }


    @Override
    public AdapterHomeFilter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_home_filter, parent, false);
        final AdapterHomeFilter.ViewHolder viewHolder = new AdapterHomeFilter.ViewHolder(view);

        viewHolder.rlFilterBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });



        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterHomeFilter.ViewHolder holder, int position) {


        Filter filter = stringLists.get(position);

        if (position == 0) {
            holder.rlFilterBg.setPadding(10, 10, 10, 10);
            holder.iconDropDown.setPadding(25, 8, 25, 8);
            holder.filterTv.setVisibility(View.GONE);
            holder.iconDropDown.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_metro));
        } else {
            holder.filterTv.setText(filter.getName());
        }


    }

    @Override
    public int getItemCount() {
        return stringLists.size();
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlFilterBg;
        TextView filterTv;
        ImageView iconDropDown;


        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            rlFilterBg = itemView.findViewById(R.id.rlFilterBg);
            filterTv = itemView.findViewById(R.id.filterTv);
            iconDropDown = itemView.findViewById(R.id.iconDropDown);
        }
    }


}
