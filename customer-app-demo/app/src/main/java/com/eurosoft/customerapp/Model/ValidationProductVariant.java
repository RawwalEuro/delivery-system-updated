package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ValidationProductVariant {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("type")
    @Expose
    private int type;

    @SerializedName("isRequired")
    @Expose
    private boolean isRequired;

    @SerializedName("lstDataObject")
    @Expose
    private List<ProductVariant> lstDataObject;


    @SerializedName("limit")
    @Expose
    private int limit;


    public int getLimit() {
        if(limit == 0){
            return 2;
        }
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public boolean isRequired() {
        return isRequired;
    }

    public void setRequired(boolean required) {
        isRequired = required;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductVariant> getLstDataObject() {
        return lstDataObject;
    }

    public void setLstDataObject(List<ProductVariant> lstDataObject) {
        this.lstDataObject = lstDataObject;
    }


    public ValidationProductVariant(String name, List<ProductVariant> lstDataObject,int type,boolean isRequired,int limit) {
        this.name = name;
        this.lstDataObject = lstDataObject;
        this.type = type;
        this.isRequired = isRequired;
        this.limit = limit;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
