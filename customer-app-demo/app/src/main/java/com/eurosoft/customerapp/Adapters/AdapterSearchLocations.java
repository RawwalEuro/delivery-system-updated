package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.SearchAddress;
import com.eurosoft.customerapp.R;

import java.util.List;

public class AdapterSearchLocations extends RecyclerView.Adapter<AdapterSearchLocations.ViewHolder> {


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;
    private Context mCtx;
    private List<SearchAddress> datumArrayList;

    public AdapterSearchLocations(Context mCtx, List<SearchAddress> datumArrayList,OnItemClickListener onItemClickListener) {
        this.mCtx = mCtx;
        this.datumArrayList = datumArrayList;
        mOnItemClickListener = onItemClickListener;
    }

    @Override
    public AdapterSearchLocations.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.adapter_search, parent, false);
        final AdapterSearchLocations.ViewHolder viewHolder = new AdapterSearchLocations.ViewHolder(view);


        viewHolder.locationText.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdapterSearchLocations.ViewHolder holder, int position) {
        SearchAddress datum = datumArrayList.get(position);
        holder.locationText.setText(datum.getAddressLine1() + "");
    }


    @Override
    public int getItemCount() {
        return datumArrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView locationText;

        public ViewHolder(View itemView) {
            super(itemView);
            locationText = itemView.findViewById(R.id.locationText);

        }

    }
}
