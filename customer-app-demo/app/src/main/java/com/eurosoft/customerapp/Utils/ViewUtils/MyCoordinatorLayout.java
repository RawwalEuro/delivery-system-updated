package com.eurosoft.customerapp.Utils.ViewUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

public class MyCoordinatorLayout extends CoordinatorLayout {

  private boolean allowForScroll = false;

  public MyCoordinatorLayout(Context context) {
    super(context);
  }

  public MyCoordinatorLayout(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  @Override public boolean onStartNestedScroll(View child, View target, int nestedScrollAxes) {
    return allowForScroll && super.onStartNestedScroll(child, target, nestedScrollAxes);
  }

  public boolean isAllowForScroll() {
    return allowForScroll;
  }

  public void setAllowForScroll(boolean allowForScroll) {
    this.allowForScroll = allowForScroll;
  }
}