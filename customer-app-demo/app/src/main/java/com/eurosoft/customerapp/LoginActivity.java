package com.eurosoft.customerapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.InputValidatorHelper;
import com.eurosoft.customerapp.Utils.LocationService.LocationEnable;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomResponseString;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.fxn.stash.Stash;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private int RC_SIGN_IN = 1;
    private LinearLayout btnLogin;
    private TextView signUpText, welcome, signToContinue, textViewLogin;
    private TextInputLayout passwordTextInput, emailTextInput;
    private InputValidatorHelper inputValidatorHelper;
    private TextInputEditText edtEmail;
    private EditText edtPassword;
    private String email, password;
    private ImageView viewPassToggle;
    private CallbackManager mCallBackManager;
    private FirebaseAuth firebaseAuth;
    private LoginButton login_button;
    private static final String TAG = "FacebookAuthentication";
    private static final String TAG_ONE = "LoginActivity";
    private FirebaseAuth.AuthStateListener authStateListener;
    private AccessTokenTracker accessTokenTracker;
    private RelativeLayout rlFblayout;
    private SignInButton gmailButton;
    private GoogleSignInClient mGoogleSignInClient;
    private RelativeLayout rlGmaillayout;
    private RelativeLayout rlProgressBar;

    private CircularProgressBar circularProgressBar;
    private RelativeLayout mainRl;
    private int i = 0;
    private CheckBox checkBoxRm;
    private TextView forgetPass;
    private ForgotDialog forgotDialog;
    private Toast toasty;
    private MasterPojo masterPojo;
    private TextView orLoginWith, textDontHave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        forgotDialog = new ForgotDialog();
        inputValidatorHelper = new InputValidatorHelper();
        initViews();
        initFbLogin();
    }


    private void initFbLogin() {

        FacebookSdk.sdkInitialize(getApplicationContext());
        firebaseAuth = FirebaseAuth.getInstance();
        mCallBackManager = CallbackManager.Factory.create();
        login_button.setReadPermissions("email", "public_profile");
        login_button.registerCallback(mCallBackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                progressVisiblityVisible();

                handleFacebookToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                progressVisiblityGone();
            }

            @Override
            public void onError(FacebookException error) {
                progressVisiblityGone();
            }
        });

        login_button.setReadPermissions("email", "public_profile", "user_friends");

        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull @NotNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    updateUi(user);
                } else {
                    updateUi(null);
                }
            }
        };


        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                firebaseAuth.signOut();
            }
        };


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    private void signInGmail() {
        progressVisiblityVisible();
        Intent intent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(intent, RC_SIGN_IN);
    }

    private void handleFacebookToken(AccessToken accessToken) {
        progressVisiblityVisible();
        AuthCredential credential = FacebookAuthProvider.getCredential(accessToken.getToken());
        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    updateUi(user);
                } else {
                    Toasty.error(getApplicationContext(), masterPojo.getAuthFailed());
                    updateUi(null);
                    progressVisiblityGone();
                }

            }
        });

    }

    private void updateUi(FirebaseUser user) {

        if (user != null) {
            progressVisiblityGone();
            Toasty.success(getApplicationContext(), user.getDisplayName());
            if (user.getPhotoUrl() != null) {
                String photoUrl = user.getPhotoUrl().toString();
                photoUrl = photoUrl + "?type=large";
            }
            callApiSocialLogin(user.getEmail(), user.getDisplayName(), "123", "abc123");


        } else {
            progressVisiblityGone();
        }

    }

    private void initViews() {
        signUpText = findViewById(R.id.signUpText);
        welcome = findViewById(R.id.welcome);
        signToContinue = findViewById(R.id.signToContinue);
        orLoginWith = findViewById(R.id.orLoginWith);
        textDontHave = findViewById(R.id.textDontHave);
        textViewLogin = findViewById(R.id.textViewLogin);

        btnLogin = findViewById(R.id.btnLogin);
        emailTextInput = findViewById(R.id.input_layout_email);
        edtEmail = findViewById(R.id.edtEmail);
        passwordTextInput = findViewById(R.id.input_layout_password);
        edtPassword = findViewById(R.id.edtPassword);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        viewPassToggle = findViewById(R.id.viewPassToggle);
        login_button = findViewById(R.id.login_button);
        rlFblayout = findViewById(R.id.rlFblayout);
        gmailButton = findViewById(R.id.signInGmail);
        rlGmaillayout = findViewById(R.id.rlGmaillayout);
        mainRl = findViewById(R.id.mainRl);
        checkBoxRm = findViewById(R.id.checkBoxRm);
        forgetPass = findViewById(R.id.forgetPass);

        signUpText.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        viewPassToggle.setOnClickListener(this);
        rlFblayout.setOnClickListener(this);
        welcome.setOnClickListener(this);
        rlGmaillayout.setOnClickListener(this);
        checkBoxRm.setOnClickListener(this);
        forgetPass.setOnClickListener(this);



        if(masterPojo == null){
            return;
        }
        welcome.setText(masterPojo.getWelcome());
        signToContinue.setText(masterPojo.getSignInToContinie());
        edtEmail.setHint(masterPojo.getEmailAddress());
        edtPassword.setHint(masterPojo.getPassword());
        forgetPass.setText(masterPojo.getForgetPassword());
        orLoginWith.setText(masterPojo.getOrLoginWith());
        textDontHave.setText(masterPojo.getHaveAccount());
        signUpText.setText(masterPojo.getSignUp());
        textViewLogin.setText(masterPojo.getLogin());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {

        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            mCallBackManager.onActivityResult(requestCode, resultCode, data);

        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount acc = task.getResult(ApiException.class);
            Toasty.success(getApplicationContext(), masterPojo.getSignInSucess());
            fireBaseGoogleAuth(acc);
        } catch (Exception e) {
            Toasty.error(getApplicationContext(), masterPojo.getSignInFailed());
            fireBaseGoogleAuth(null);
        }
    }

    private void fireBaseGoogleAuth(GoogleSignInAccount acc) {

        if (acc == null) {
            progressVisiblityGone();
            return;
        }

        AuthCredential authCredential = GoogleAuthProvider.getCredential(acc.getIdToken(), null);
        firebaseAuth.signInWithCredential(authCredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toasty.success(getApplicationContext(), masterPojo.getAuthSucess());
                    FirebaseUser user = firebaseAuth.getCurrentUser();

                    updateUiGmailUser(user);

                } else {
                    Toasty.success(getApplicationContext(), masterPojo.getAuthFailed());
                    progressVisiblityGone();
                }


            }
        });
    }

    private void updateUiGmailUser(FirebaseUser user) {


        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(getApplicationContext());
        if (account != null) {
            callApiSocialLogin(user.getEmail(), user.getDisplayName(), "123", "abc123");
        }
        progressVisiblityGone();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnLogin:

                validateFields();

                break;


            case R.id.signUpText:
                Intent signUpToLogin = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(signUpToLogin);
                break;


            case R.id.viewPassToggle:
                viewPassToggleState(v);
                break;


            case R.id.rlFblayout:

                if (!AppNetWorkStatus.getInstance(this).isOnline()) {
                    progressVisiblityGone();
                    Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                    return;
                }

                login_button.performClick();


                break;


            case R.id.rlGmaillayout:
                if (!AppNetWorkStatus.getInstance(this).isOnline()) {
                    progressVisiblityGone();
                    Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                    return;
                }
                signInGmail();

                break;


            case R.id.forgetPass:

                try {
                    forgotDialog.showDialog(this);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                break;

        }
    }


    private void viewPassToggleState(View view) {


        if(edtPassword.getText().toString().equalsIgnoreCase("")){
            return;
        }

        if (edtPassword.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())) {
            ((ImageView) (view)).setImageResource(R.drawable.password_toggle);
            //Show Password
            edtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        } else {
            ((ImageView) (view)).setImageResource(R.drawable.ic_baseline_remove_red_eye_24);
            //Hide Password
            edtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());


        }
    }

    private void validateFields() {

        email = edtEmail.getText().toString().replace(" ", "");
        password = edtPassword.getText().toString();

        if (inputValidatorHelper.isNullOrEmpty(email)) {
            Toasty.warning(this, masterPojo.getEmptyEmail(), Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isValidEmail(email) == false) {
            Toasty.warning(this, masterPojo.getInvalidEmail(), Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (inputValidatorHelper.isNullOrEmpty(password)) {
            Toasty.warning(this, masterPojo.getPasswordIsEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (!AppNetWorkStatus.getInstance(this).isOnline()) {
            progressVisiblityGone();
            Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        } else {
            callApiLogin(email, password);
        }

    }

    private void callApiLogin(String email, String password) {
        progressVisiblityVisible();
        User user = new User();
        user.setCustomerEmail(email);
        user.setCustomerPassword(password);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<User>> call = apiService.loginUser(user);

        call.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    User user = response.body().getData();
                    Toasty.success(getApplicationContext(), masterPojo.getLoginSucess(), Toast.LENGTH_SHORT, true).show();
                    progressVisiblityGone();



                    if (LocationEnable.isLocationEnabled(getApplicationContext())) {
                        Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                        Stash.put(Constants.isLoggenIn, true);
                        Stash.put(Constants.USER, user);
                        startActivity(homeIntent);
                        finish();
                        return;
                    } else if(!LocationEnable.isLocationEnabled(LoginActivity.this)){
                        Intent mainIntentI = new Intent(LoginActivity.this, ActivityTurnOnLocation.class);
                        Stash.put(Constants.isLoggenIn, true);
                        Stash.put(Constants.USER, user);
                        startActivity(mainIntentI);
                        finish();
                    } else {

                    }


                   /* if(checkBoxRm.isChecked()){
                        Stash.put(Constants.isLoggenIn,true);
                    } else {
                        Stash.put(Constants.isLoggenIn,false);
                    }*/
                }
            }

            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                //Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                progressVisiblityGone();
            }
        });
    }


    private void callApiSocialLogin(String email, String name, String number, String password) {

        String firstName = "";
        String lastName = "";

        if (name != null && !name.isEmpty() && name.contains(" ")) {
            String[] splitStr = name.split("\\s+");
            firstName = splitStr[0];
            lastName = splitStr[1];
        } else {
            firstName = name;
            lastName = "";
        }


        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        progressVisiblityVisible();
        User user = new User();
        user.setCustomerEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setCustomerPhoneNumber("123");
        user.setCustomerPassword(password);
        user.setCustomerDeviceId(android_id);
        user.setIsCustomerEmailVerify(true);
        user.setStatus(true);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<User>> call = apiService.socialLoginUser(user);

        call.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }
                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    User user = response.body().getData();
                    Toasty.success(getApplicationContext(), masterPojo.getLoginSucess(), Toast.LENGTH_SHORT, true).show();
                    progressVisiblityGone();
                    Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(homeIntent);
                    Stash.put(Constants.isLoggenIn, true);
                    Stash.put(Constants.USER, user);
                    Stash.put(Constants.LOGIN_FROM_FB, true);
                }
            }

            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                progressVisiblityGone();

                //Toast.makeText(LoginActivity.this, masterPojo.getUserAlreadyExist(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressVisiblityGone();
        // firebaseAuth.addAuthStateListener(authStateListener);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (authStateListener != null) {
            firebaseAuth.removeAuthStateListener(authStateListener);
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }


    public class ForgotDialog {

        public void showDialog(Activity activity) throws ParseException {
            final Dialog dialogForgot = new Dialog(activity, R.style.Theme_Dialog);
            dialogForgot.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialogForgot.setCancelable(false);
            dialogForgot.setContentView(R.layout.dialog_forget_pass);
            dialogForgot.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            Button btnSubmit = (Button) dialogForgot.findViewById(R.id.btnSubmit);
            Button btnCancel = (Button) dialogForgot.findViewById(R.id.btnCancel);
            EditText emailForgot = (EditText) dialogForgot.findViewById(R.id.emailForgot);
            TextView header = (TextView) dialogForgot.findViewById(R.id.header);
            TextView title = (TextView) dialogForgot.findViewById(R.id.title);


            header.setText(masterPojo.getHeaderForgetPass());
            btnSubmit.setText(masterPojo.getSubmit());
            btnCancel.setText(masterPojo.getCancel());
            emailForgot.setHint(masterPojo.getEmailAddress());
            title.setText(masterPojo.getForgetPassword());

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String emailText = emailForgot.getText().toString().replace(" ", "");

                    if (inputValidatorHelper.isNullOrEmpty(emailText)) {
                        toasty = Toasty.warning(getApplicationContext(), masterPojo.getEmptyEmail(), Toast.LENGTH_SHORT, true);
                        toasty.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                        toasty.show();
                        return;
                    }


                    if (inputValidatorHelper.isValidEmail(emailText) == false) {
                        toasty = Toasty.warning(getApplicationContext(), masterPojo.getInvalidEmail(), Toast.LENGTH_SHORT, true);
                        toasty.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
                        toasty.show();
                        return;
                    }

                    if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                        progressVisiblityGone();
                        Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                        return;
                    }

                    callApiForget(emailText, dialogForgot);


                }
            });

            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogForgot.dismiss();
                }
            });

            dialogForgot.show();
        }
    }

    private void callApiForget(String emailText, Dialog forgotDialog) {

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomResponseString<String>> call = apiService.forgetPassword(emailText);


        call.enqueue(new Callback<CustomResponseString<String>>() {
            @Override
            public void onResponse(Call<CustomResponseString<String>> call, Response<CustomResponseString<String>> response) {
                if (response.code() != 200 || response.body() == null) {
                    forgotDialog.dismiss();
                    return;

                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    forgotDialog.dismiss();
                    return;

                }

                if (response.isSuccessful() && response.code() == 200) {
                    Toast vwToast = Toasty.success(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT);
                    TextView tv = (TextView) vwToast.getView().findViewById(android.R.id.message);
                    if (tv != null) {
                        tv.setGravity(Gravity.CENTER_HORIZONTAL);
                    }
                    vwToast.show();
                    forgotDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<CustomResponseString<String>> call, Throwable t) {
                progressVisiblityGone();
                forgotDialog.dismiss();
            }
        });


    }

}