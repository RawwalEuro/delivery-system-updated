package com.eurosoft.customerapp.Interface;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.eurosoft.customerapp.Model.User;

import java.util.List;

@Dao
public interface UserDao {

  @Query("SELECT * FROM User")
  List<User> getAll();

  @Insert
  void insert(User user);

  @Delete
  void delete(User user);

  @Update
  void update(User user);
}
