package com.eurosoft.customerapp.Interface;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.eurosoft.customerapp.Model.Order;

import java.util.List;

@Dao
public interface OrderDao {
  @Query("SELECT * FROM 'order'")
  List<Order> getAll();

  @Insert
  void insert(Order order);

  @Delete
  void delete(Order order);

  @Update
  void update(Order order);

  @Query("SELECT * FROM `order` WHERE userid=:userid ")
  List<Order> getAllByUserId(int userid);

  // select * from products where orderid = 123 order by store id ASC

  @Query("SELECT * FROM `order` WHERE userid=:userid AND status=:status ")
  List<Order> getAllByUserIdNStatus(int userid, int status);


  @Query("SELECT * FROM `order` WHERE userid=:userid AND status like '%3' OR  status like '%4' OR  status like '%12'")
  List<Order> getAllUnActiveOrders(int userid);


  @Query("SELECT * FROM `order` WHERE userid=:userid AND status like '%1' OR  status like '%2' OR  status like '%11'")
  List<Order> getAllActiveOrders(int userid);


  @Query("SELECT * FROM `order` WHERE orderId=:orderId AND status like '%12'")
  Order getLastCompleteOrder(int orderId);

}
