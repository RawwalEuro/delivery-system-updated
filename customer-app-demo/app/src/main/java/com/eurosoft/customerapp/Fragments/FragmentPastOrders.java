package com.eurosoft.customerapp.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eurosoft.customerapp.ActivityOrderDetails;
import com.eurosoft.customerapp.ActivityPastOrders;
import com.eurosoft.customerapp.Adapters.AdapterCartItems;
import com.eurosoft.customerapp.Adapters.AdapterPastActiveOrders;
import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.OrderModel;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentPastOrders extends Fragment {
    public RecyclerView recyclerViewOrders;
    public LinearLayoutManager layoutManager;
    public AdapterPastActiveOrders mAdapter;
    public List<OrderModel> orderList;
    public User currentUser;
    public LinearLayout rlLayout;
    public RelativeLayout rlNoData;
    private AppSettings appSettings;
    private MasterPojo masterPojo;
    private TextView noOrderFound;
    private ViewConfirmReturnOrderDialog viewConfirmReturnOrderDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_past, container, false);
        Stash.init(getActivity());
        viewConfirmReturnOrderDialog = new ViewConfirmReturnOrderDialog();
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        initViews(view);
        getOrdersByUserIdAndStatus(false);
        return view;
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getOrdersByUserIdAndStatus(true);
        } else {
        }
    }

    public void initViews(View view) {
        recyclerViewOrders = view.findViewById(R.id.pastRl);
        recyclerViewOrders.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());

        rlLayout = (LinearLayout) view.findViewById(R.id.mainRl);
        rlNoData = (RelativeLayout) view.findViewById(R.id.rlNoData);
        noOrderFound = (TextView) view.findViewById(R.id.noOrderFound);


        noOrderFound.setText(masterPojo.getNoOrdersFound());

        final SwipeRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrdersByUserIdAndStatus(false); // your code
                pullToRefresh.setRefreshing(false);

            }
        });

    }

    public void setupRecycler(List<OrderModel> orders) {
        recyclerViewOrders.setLayoutManager(layoutManager);
        mAdapter = new AdapterPastActiveOrders(getActivity(), orderList, appSettings, masterPojo, new AdapterCartItems.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                switch (view.getId()) {

                    case R.id.details:
                        Intent intent = new Intent(getActivity(), ActivityOrderDetails.class);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(orderList.get(position));
                        intent.putExtra("myjson", myJson);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(intent);
                        break;


                    case R.id.returnOrderBtn:
                        //Toasty.success(getActivity(),"Do you wanna return",Toasty.LENGTH_SHORT).show();
                        viewConfirmReturnOrderDialog.showDialog(getActivity(), currentUser.getId(), orderList.get(position).getId(), "ABC");
                        break;

                }


            }
        });
        recyclerViewOrders.setAdapter(mAdapter);
        recyclerViewOrders.setVisibility(View.VISIBLE);
        checkIfDataExixts();
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
    }


    @Override
    public void onResume() {
        super.onResume();
        getOrdersByUserIdAndStatus(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            getActivity().unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkIfDataExixts() {
        if (orderList == null || orderList.size() == 0) {
            rlNoData.setVisibility(View.VISIBLE);
            return;
        } else {
            rlNoData.setVisibility(View.GONE);
            rlLayout.setVisibility(View.VISIBLE);

        }
    }

    public void getOrdersByUserIdAndStatus(boolean loader) {

        if (!AppNetWorkStatus.getInstance(getActivity()).isOnline()) {
            ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingFinished();
            Toasty.warning(getActivity(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (loader) {
        } else {
            // ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingStarted();
        }

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<OrderModel>> call = apiService.getAllActivieNPastOrders(currentUser.getId(), 1, currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<OrderModel>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<OrderModel>> call, Response<CustomWebResponse<OrderModel>> response) {
                if (response.code() != 200 || response.body() == null) {
                    ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingFinished();
                    return;
                }
                if (response.body().getSuccess() && response.code() == 200) {
                    if (response.body().getData() == null || response.body().getData().size() == 0) {
                        // ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingFinished();
                        rlNoData.setVisibility(View.VISIBLE);
                        recyclerViewOrders.setVisibility(View.GONE);
                        return;
                    } else {
                        orderList = new ArrayList<>();
                        orderList = response.body().getData();
                        Collections.reverse(orderList);
                        rlNoData.setVisibility(View.GONE);
                        recyclerViewOrders.setVisibility(View.VISIBLE);
                        setupRecycler(orderList);
                        //  ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingFinished();
                    }
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<OrderModel>> call, Throwable t) {
                //    ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingFinished();
            }
        });
    }


    public void returnOrder(int customerId, int orderId, String reason) {
        if (!AppNetWorkStatus.getInstance(getActivity()).isOnline()) {
            Toasty.warning(getActivity(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<String>> call = apiService.returnOrder(orderId, customerId, reason, currentUser.getToken());

        call.enqueue(new Callback<WebResponse<String>>() {
            @Override
            public void onResponse(Call<WebResponse<String>> call, Response<WebResponse<String>> response) {
                if (response.code() != 200 || response.body() == null) {
                    ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingFinished();
                    return;
                }
                if (response.code() == 200) {

                    if (response.isSuccessful() == false) {
                        Toasty.error(getActivity(), response.body().getMessage(), Toasty.LENGTH_SHORT).show();
                    }
                    Toasty.success(getActivity(), response.body().getMessage(), Toasty.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<WebResponse<String>> call, Throwable t) {
                // ((ActivityPastOrders) getActivity()).getActivityPastOrders().loadingFinished();
                Toasty.success(getActivity(), "Something went wrong", Toasty.LENGTH_SHORT).show();
            }
        });
    }


    public class ViewConfirmReturnOrderDialog {
        public void showDialog(Activity activity, int customerId, int orderId, String reason) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_return);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


            Button btnRturn = (Button) dialog.findViewById(R.id.btnRturn);
            Button cancel = (Button) dialog.findViewById(R.id.cancel);
            btnRturn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    returnOrder(customerId, orderId, reason);
                    dialog.dismiss();
                }
            });


            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getOrdersByUserIdAndStatus(false);
        }
    };
}
