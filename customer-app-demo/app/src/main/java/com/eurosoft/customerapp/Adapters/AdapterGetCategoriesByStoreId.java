package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.ActivityRestruantMenu;
import com.eurosoft.customerapp.Model.Categories;
import com.eurosoft.customerapp.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AdapterGetCategoriesByStoreId extends RecyclerView.Adapter<AdapterGetCategoriesByStoreId.ViewHolder> {

  private Context ctx;
  private ArrayList<Categories> categoriesList;

  public AdapterGetCategoriesByStoreId(Context context, ArrayList<Categories> categoriesArrayList) {
    ctx = context;
    categoriesList = categoriesArrayList;
  }


  @Override
  public AdapterGetCategoriesByStoreId.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_get_categories_by_store, parent, false);
    return new AdapterGetCategoriesByStoreId.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull @NotNull AdapterGetCategoriesByStoreId.ViewHolder holder, int position) {
    Categories categories = categoriesList.get(position);

    holder.catNameTv.setText(categories.getCategoryName());
    if(categories.getCategoryBaseUrl().isEmpty() || categories.getCategoryIcon().isEmpty()){

    } else {
    Picasso.get().load(categories.getCategoryBaseUrl()+categories.getCategoryIcon()).placeholder(R.color.grey).into(holder.catImage);
    }
  }

  @Override
  public int getItemCount() {
    return categoriesList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {

    private TextView catNameTv;
    private ImageView catImage;
    private LinearLayout LlCatMain;

    public ViewHolder(@NonNull @NotNull View itemView) {
      super(itemView);

      catNameTv = itemView.findViewById(R.id.categoryName);
      catImage = itemView.findViewById(R.id.categoryImage);
      LlCatMain = itemView.findViewById(R.id.LlCatMain);


      LlCatMain.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          int pos = getAdapterPosition();
          Categories categories = categoriesList.get(pos);
          Intent intent = new Intent(v.getContext(), ActivityRestruantMenu.class);
          intent.putExtra("id", categoriesList.get(pos).getId() + "");
          intent.putExtra("IsSubCategory", categoriesList.get(pos).getIsSubCategory());
          intent.putExtra("restruantName", categoriesList.get(pos).getCategoryName());
          intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
          v.getContext().startActivity(intent);
        }
      });

    }
  }


  public void filterList(ArrayList<Categories> filteredList) {
    categoriesList = filteredList;
    notifyDataSetChanged();
  }
}
