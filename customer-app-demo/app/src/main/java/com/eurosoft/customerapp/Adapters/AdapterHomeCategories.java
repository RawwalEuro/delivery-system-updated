package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.StoreTagsData;
import com.eurosoft.customerapp.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AdapterHomeCategories extends RecyclerView.Adapter<AdapterHomeCategories.ViewHolder> {


    private Context context;
    private ArrayList<StoreTagsData> stringLists;
    private OnItemClickListenerCat mOnItemClickListener;
    private int row_index = -1;


    public AdapterHomeCategories(Context ctx, ArrayList<StoreTagsData> stringList, OnItemClickListenerCat onItemClickListener) {
        context = ctx;
        stringLists = stringList;
        mOnItemClickListener = onItemClickListener;
    }


    @Override
    public AdapterHomeCategories.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_home_categories, parent, false);
        final AdapterHomeCategories.ViewHolder viewHolder = new AdapterHomeCategories.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterHomeCategories.ViewHolder holder, int position) {


        holder.parentLL.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition());
                row_index = position;
                notifyDataSetChanged();
            }
        });


        if (row_index == position) {

            holder.parentLL.setBackgroundResource(R.drawable.drawable_curved_rectangle);
            holder.catName.setTextColor(Color.parseColor("#000000"));
        } else {
            holder.parentLL.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.catName.setTextColor(Color.parseColor("#000000"));
        }

        StoreTagsData storeTagsData = stringLists.get(position);
        holder.catName.setText(storeTagsData.getName());

        if (position == 0) {
            holder.catIcon.setImageDrawable(context.getResources().getDrawable(R.drawable.store));
        } else {
            Picasso.get().load(storeTagsData.getBaseUrl() + storeTagsData.getIcon()).placeholder(R.drawable.default_placeholder).into(holder.catIcon);

        }

    }

    @Override
    public int getItemCount() {
        return stringLists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout parentLL;
        private ImageView catIcon;
        private TextView catName;
        private CardView cardItem;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            parentLL = itemView.findViewById(R.id.parentLL);
            catIcon = itemView.findViewById(R.id.catIcon);
            catName = itemView.findViewById(R.id.catName);
        }
    }


    public interface OnItemClickListenerCat {
        public void onItemClick(View view, int position);
    }

}
