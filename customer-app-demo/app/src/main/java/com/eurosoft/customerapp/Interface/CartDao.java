package com.eurosoft.customerapp.Interface;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.eurosoft.customerapp.Model.Cart;

import java.util.List;

@Dao
public interface CartDao {

  @Query("SELECT * FROM cart")
  List<Cart> getAll();

  @Insert
  void insert(Cart cart);

  @Delete
  void delete(Cart cart);

  @Update
  void update(Cart cart);
}
