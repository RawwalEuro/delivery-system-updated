package com.eurosoft.customerapp.Utils.NetworkUtils;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.BecomeADriver;
import com.eurosoft.customerapp.Model.Categories;
import com.eurosoft.customerapp.Model.Data;
import com.eurosoft.customerapp.Model.DriverTracking;
import com.eurosoft.customerapp.Model.FilterBody;
import com.eurosoft.customerapp.Model.HomeAllDetails;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.OptionsList;
import com.eurosoft.customerapp.Model.OrderModel;
import com.eurosoft.customerapp.Model.OrderPlaced;
import com.eurosoft.customerapp.Model.OrderStatus;
import com.eurosoft.customerapp.Model.PlaceOrder;
import com.eurosoft.customerapp.Model.ReviewModel;
import com.eurosoft.customerapp.Model.SearchAddress;
import com.eurosoft.customerapp.Model.SearchItemProduct;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.StoreList;
import com.eurosoft.customerapp.Model.SubCategoryWithProducts;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.Stores;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Model.WishList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("CustomerLogin")
    Call<WebResponse<User>> loginUser(@Body User user);

    @POST("CustomerSignUp")
    Call<WebResponse<User>> signUpUser(@Body User user);


    @POST("CustomerVerificationAndAuthorization")
    Call<CustomResponseString<String>> authorizationCode(@Body User user);


    @POST("CustomerForgetPassword")
    Call<CustomResponseString<String>> forgetPassword(@Query("CustomerEmail") String CustomerEmail);


    @GET("GetStoreList")
    Call<CustomWebResponse<Stores>> getAllCategories();

    @GET("GetCategoriesListByStoreId")
    Call<CustomWebResponse<Categories>> getSubCategoriesById(@Query("StoreId") String StoreId);


    @POST("GetSubCategoriesAndProductByCategoryId")
    Call<CustomWebResponse<Product>> getSubCategoriesProductsById(@Query("CategoryId") String CategoryId,@Query("CustomerId") Integer CustomerId, @Header("Authorization") String auth);


    @POST("GetSubCategoriesAndProductByCategoryId")
    Call<CustomWebResponse<SubCategoryWithProducts>> getSubCategoriesProductsByIdTab(@Query("CategoryId") String CategoryId,@Query("CustomerId") Integer CustomerId, @Header("Authorization") String auth);


    @POST("CustomerSocialSignUp")
    Call<WebResponse<User>> socialLoginUser(@Body User user);


    @POST("PlaceOrder")
    Call<OrderPlaced> placeOrder(@Body PlaceOrder placeOrder, @Header("Authorization") String auth);

    @POST("SearchAddress")
    Call<WebResponse<Data>> searchAddress(@Query("AddressPrefix") String addressPrefix);

    @POST("SearchAddress")
    Call<CustomWebResponse<SearchAddress>> searchAddressPrefix(@Query("AddressPrefix") String addressPrefix, @Header("Authorization") String auth);
  /*  @POST("SearchAddress")
    Call<WebResponse<Data>> searchAddress(@Query("AddressPrefix") String addressPrefix);*/

    @POST("CancelledOrderByCustomerId")
    Call<WebResponse<Data>> cancelOrderByOrderId(@Query("OrderId") String CustomerId, @Query("OrderStatus") int OrderStatus, @Header("Authorization") String auth);


    @POST("GetUpdatedOrderStatusByCustomerAndOrderId")
    Call<WebResponse<OrderStatus>> getOrderStatus(@Query("CustomerId") int CustomerId, @Query("OrderId") int OrderId, @Header("Authorization") String auth);


    @GET("GetWebsiteAndRegionSetting")
    Call<WebResponse<AppSettings>> getAppSettings();

    @POST("UpdateCustomerProfileSetting")
    Call<WebResponse<User>> updateProfile(@Body User user, @Header("Authorization") String auth);

    @POST("TrackOrderByCustomerAndOrderId")
    Call<WebResponse<DriverTracking>> trackOrderByCustomerAndOrderId(@Query("CustomerId") int CustomerId, @Query("OrderId") int OrderId, @Header("Authorization") String auth);

    @POST("OrderFeedBack")
    Call<WebResponse<ReviewModel>> orderFeedback(@Body ReviewModel reviewModel, @Header("Authorization") String auth);

    @GET("GetHomeData")
    Call<WebResponse<HomeAllDetails>> homeAllDetails(@Header("Authorization") String auth);


    @POST("GetAllCategoriesByStoreId")
    Call<CustomWebResponse<Categories>> getAllCatByStoreId(@Query("StoreId") int storeId, @Header("Authorization") String auth);


    @POST("GetProductOptionsByProductId")
    Call<CustomWebResponse<OptionsList>> getProductVariations(@Query("ProductId") int categoryId, @Header("Authorization") String auth);

    @POST("GetAllStores")
    Call<CustomWebResponse<StoreList>> getAllStoresByTag(@Body Store store, @Header("Authorization") String auth);

    @POST("GetAllOrderByCustomerIdAndStatusWise")
    Call<CustomWebResponse<OrderModel>> getAllActivieNPastOrders(@Query("CustomerId") int CustomerId, @Query("Type") int Type, @Header("Authorization") String auth);

    @POST("GetRelatedProductByProductid")
    Call<CustomWebResponse<Product>> getRelatedProducts(@Query("ProductIds") String ProductIds,@Query("CustomerId") Integer CustomerId, @Header("Authorization") String auth);

    @POST("SearchAllCategoriesAndProducts")
    Call<CustomWebResponse<SearchItemProduct>> searchProducts(@Query("StoreId") String StoreId, @Query("Prefix") String addressPrefix,@Query("CustomerId") Integer CustomerId, @Header("Authorization") String auth);

    @POST("SearchStoreByFilters")
    Call<CustomWebResponse<StoreList>> searchStoreByFilters(@Body FilterBody filterBody, @Header("Authorization") String auth);

    @POST("ChangeLanguage")
    Call<WebResponse<MasterPojo>> setLanguage(@Query("LanguagePrefix") String languagePrefix);


    @POST("AddProuductToWishList")
    Call<WebResponse<MasterPojo>> addToWishlist(@Query("CustomerId") int cutomerId, @Query("ProductId") int ProductId,@Query("IsLiked") boolean isLiked, @Header("Authorization") String auth);

    @GET("GetWishListByCustomerId")
    Call<CustomWebResponse<WishList>> getWishList(@Query("CustomerId") int cutomerId, @Header("Authorization") String auth);

    @POST("BecomeADriverRequest")
    Call<WebResponse<WishList>> becomeADriver(@Body BecomeADriver becomeADriver);

    @POST("ReturnOrderByCustomerId")
    Call<WebResponse<String>> returnOrder(@Query("Orderid") int ordrId,@Query("CustomerId") int customerId,@Query("ReturnReason") String returnReason, @Header("Authorization") String auth);

}
