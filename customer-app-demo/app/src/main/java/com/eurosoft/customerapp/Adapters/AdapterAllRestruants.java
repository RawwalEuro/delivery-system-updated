package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.ActivityRestruantMenu;
import com.eurosoft.customerapp.R;

import java.util.List;

public class AdapterAllRestruants extends RecyclerView.Adapter<AdapterAllRestruants.ViewHolder> {


  private Context mCtx;
  private List<String> restruantList;

  public AdapterAllRestruants(Context mCtx, List<String> restruantList) {
    this.mCtx = mCtx;
    this.restruantList = restruantList;
  }

  @Override
  public AdapterAllRestruants.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View view = LayoutInflater.from(mCtx).inflate(R.layout.adapter_all_restruants, parent, false);
    return new AdapterAllRestruants.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(AdapterAllRestruants.ViewHolder holder, int position) {


  }



  @Override
  public int getItemCount() {
    return restruantList.size();
  }

  class ViewHolder extends RecyclerView.ViewHolder  {

    LinearLayout llRestruant;

    public ViewHolder(View itemView) {
      super(itemView);

      llRestruant = itemView.findViewById(R.id.llRestruant);

      llRestruant.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent intent = new Intent(mCtx, ActivityRestruantMenu.class);
          mCtx.startActivity(intent);
        }
      });
    }


  }
}
