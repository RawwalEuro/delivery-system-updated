package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OptionDetailedData {

  @SerializedName("Id")
  @Expose
  private Integer id;
  @SerializedName("OptionMasterId")
  @Expose
  private Integer optionMasterId;


  @SerializedName("OrderDetailMasterId")
  @Expose
  private Integer OrderDetailMasterId;

  @SerializedName("OptionsId")
  @Expose
  private Integer optionsId;

  @SerializedName("OptionId")
  @Expose
  private Integer optionId;


  @SerializedName("OptionValue")
  @Expose
  private String OptionValue;


  @SerializedName("OptionPrice")
  @Expose
  private Double OptionPrice;

  @SerializedName("ProductId")
  @Expose
  private Integer productId;
  @SerializedName("Value")
  @Expose
  private String value;
  @SerializedName("Quantity")
  @Expose
  private Double quantity;
  @SerializedName("SubsTrackStock")
  @Expose
  private Object subsTrackStock;
  @SerializedName("PriceOperator")
  @Expose
  private String priceOperator;
  @SerializedName("Price")
  @Expose
  private Double price;

  public OptionDetailedData(Integer optionMasterId, Integer optionId, String optionValue, Double optionPrice, Integer productId) {
    this.optionMasterId = optionMasterId;
    this.optionId = optionId;
    OptionValue = optionValue;
    OptionPrice = optionPrice;
    this.productId = productId;
  }

  @SerializedName("PointsOperator")
  @Expose
  private String pointsOperator;
  @SerializedName("Points")
  @Expose
  private Object points;
  @SerializedName("WeightOperator")
  @Expose
  private String weightOperator;
  @SerializedName("Weight")
  @Expose
  private Double weight;




  public Integer getOrderDetailMasterId() {
    return OrderDetailMasterId;
  }

  public void setOrderDetailMasterId(Integer orderDetailMasterId) {
    OrderDetailMasterId = orderDetailMasterId;
  }

  public Integer getOptionId() {
    return optionId;
  }

  public void setOptionId(Integer optionId) {
    this.optionId = optionId;
  }

  public String getOptionValue() {
    return OptionValue;
  }

  public void setOptionValue(String optionValue) {
    OptionValue = optionValue;
  }

  public Double getOptionPrice() {
    return OptionPrice;
  }

  public void setOptionPrice(Double optionPrice) {
    OptionPrice = optionPrice;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getOptionMasterId() {
    return optionMasterId;
  }

  public void setOptionMasterId(Integer optionMasterId) {
    this.optionMasterId = optionMasterId;
  }

  public Integer getOptionsId() {
    return optionsId;
  }

  public void setOptionsId(Integer optionsId) {
    this.optionsId = optionsId;
  }

  public Integer getProductId() {
    return productId;
  }

  public void setProductId(Integer productId) {
    this.productId = productId;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Double getQuantity() {
    return quantity;
  }

  public void setQuantity(Double quantity) {
    this.quantity = quantity;
  }

  public Object getSubsTrackStock() {
    return subsTrackStock;
  }

  public void setSubsTrackStock(Object subsTrackStock) {
    this.subsTrackStock = subsTrackStock;
  }

  public String getPriceOperator() {
    return priceOperator;
  }

  public void setPriceOperator(String priceOperator) {
    this.priceOperator = priceOperator;
  }

  public Double getPrice() {
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public String getPointsOperator() {
    return pointsOperator;
  }

  public void setPointsOperator(String pointsOperator) {
    this.pointsOperator = pointsOperator;
  }

  public Object getPoints() {
    return points;
  }

  public void setPoints(Object points) {
    this.points = points;
  }

  public String getWeightOperator() {
    return weightOperator;
  }

  public void setWeightOperator(String weightOperator) {
    this.weightOperator = weightOperator;
  }

  public Double getWeight() {
    return weight;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

}
