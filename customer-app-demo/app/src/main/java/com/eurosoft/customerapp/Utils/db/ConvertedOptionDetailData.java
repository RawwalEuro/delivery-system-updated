package com.eurosoft.customerapp.Utils.db;

import androidx.room.TypeConverter;

import com.eurosoft.customerapp.Model.OptionDetailedData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ConvertedOptionDetailData {

  @TypeConverter
  public String fromListOptionDetailedData(List<OptionDetailedData> list) {
    if (list == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<OptionDetailedData>>() {}.getType();
    String json = gson.toJson(list, type);
    return json;
  }

  @TypeConverter
  public List<OptionDetailedData> toListOptionDetailedData(String optionDetails) {
    if (optionDetails == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<OptionDetailedData>>() {}.getType();
    List<OptionDetailedData> list = gson.fromJson(optionDetails, type);
    return list;
  }

}
