package com.eurosoft.customerapp.Utils.ViewUtils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.appbar.AppBarLayout;

public class DisableableAppBarLayoutBehavior extends AppBarLayout.Behavior {
  private boolean mEnabled;

  public DisableableAppBarLayoutBehavior() {
    super();
  }

  public DisableableAppBarLayoutBehavior(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public void setEnabled(boolean enabled) {
    mEnabled = enabled;
  }

  @Override
  public boolean onStartNestedScroll(CoordinatorLayout parent, AppBarLayout child, View directTargetChild, View target, int nestedScrollAxes) {
    return mEnabled && super.onStartNestedScroll(parent, child, directTargetChild, target, nestedScrollAxes);
  }

  public boolean isEnabled() {
    return mEnabled;
  }
}
