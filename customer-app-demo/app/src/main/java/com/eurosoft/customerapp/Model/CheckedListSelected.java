package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CheckedListSelected {
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("List")
    @Expose
    private ArrayList<ProductVariant> List;


    public CheckedListSelected(Integer position, ArrayList<ProductVariant> list) {
        this.position = position;
        List = list;
    }




    public CheckedListSelected() {
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public ArrayList<ProductVariant> getList() {
        return List;
    }

    public void setList(ArrayList<ProductVariant> list) {
        List = list;
    }
}
