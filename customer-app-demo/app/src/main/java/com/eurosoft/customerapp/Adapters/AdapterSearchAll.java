package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.ActivityRestruantMenu;
import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.SearchItemProduct;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.ProductDetailFragment;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;
import com.fxn.stash.Stash;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class AdapterSearchAll extends RecyclerView.Adapter {


    private Context context;
    private List<SearchItemProduct> searchItemProductArrayList;
    private AppSettings appSettings;
    private OnItemClickListener mOnItemClickListener;

    public AdapterSearchAll(Context ctx, List<SearchItemProduct> searchItemProducts, AppSettings settings, OnItemClickListener onItemClickListener) {
        this.context = ctx;
        this.searchItemProductArrayList = searchItemProducts;
        this.appSettings = settings;
        this.mOnItemClickListener = onItemClickListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case SearchItemProduct.TYPE_CAT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_get_categories_by_store, parent, false);
                return new AdapterSearchAll.CategoryViewHolder(view);
            case SearchItemProduct.TYPE_PRODUCT:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_restruant_menu, parent, false);
                return new AdapterSearchAll.ProductViewHolder(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {


        switch (searchItemProductArrayList.get(position).getDataType()) {
            case 0:
                return SearchItemProduct.TYPE_CAT;
            case 1:
                return SearchItemProduct.TYPE_PRODUCT;

            default:
                return -1;
        }

    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, Product product);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {

        switch (searchItemProductArrayList.get(position).getDataType()) {

            case SearchItemProduct.TYPE_CAT:
                ((CategoryViewHolder) holder).catNameTv.setText(searchItemProductArrayList.get(position).getCategoryName());
                ((CategoryViewHolder) holder).storeName.setText(searchItemProductArrayList.get(position).getStoreName());
                Picasso.get().load(searchItemProductArrayList.get(position).getCategoryBaseUrl() + searchItemProductArrayList.get(position).getCategoryIcon()).placeholder(R.color.grey).into(((CategoryViewHolder) holder).catImage);
                break;

            case SearchItemProduct.TYPE_PRODUCT:
                ((ProductViewHolder) holder).productName.setText(searchItemProductArrayList.get(position).getProductName() + "");
                ((ProductViewHolder) holder).productDesc.setText(searchItemProductArrayList.get(position).getProductDesc() + "");

                try {
                    ((ProductViewHolder) holder).productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(searchItemProductArrayList.get(position).getPrice()) + " ");
                    ((ProductViewHolder) holder).storeName.setText(searchItemProductArrayList.get(position).getStoreName() + " ");
                    Picasso.get().load(searchItemProductArrayList.get(position).getBaseUrl() + searchItemProductArrayList.get(position).getProductId() + "/" + searchItemProductArrayList.get(position).getPrimaryImage()).placeholder(R.color.grey).into(((ProductViewHolder) holder).loadImage);

                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
        }


    }

    @Override
    public int getItemCount() {
        return searchItemProductArrayList.size();
    }


    public class CategoryViewHolder extends RecyclerView.ViewHolder {

        private TextView catNameTv,storeName;
        private ImageView catImage;
        private LinearLayout LlCatMain;

        public CategoryViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            catNameTv = itemView.findViewById(R.id.categoryName);
            catImage = itemView.findViewById(R.id.categoryImage);
            LlCatMain = itemView.findViewById(R.id.LlCatMain);
            storeName = itemView.findViewById(R.id.storeName);


            LlCatMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    if (searchItemProductArrayList.get(pos).getStoreClosed()) {
                        Toasty.error(context, "Category containing in the store is closed at the moment", Toasty.LENGTH_SHORT).show();
                        return;
                    }

                    Intent intent = new Intent(v.getContext(), ActivityRestruantMenu.class);
                    intent.putExtra("id", searchItemProductArrayList.get(pos).getCategoryId() + "");
                    intent.putExtra("IsSubCategory", searchItemProductArrayList.get(pos).getSubCategory());
                    intent.putExtra("restruantName", searchItemProductArrayList.get(pos).getCategoryName());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    Store store = new Store();
                    store.setTip(searchItemProductArrayList.get(pos).getTip());
                    store.setServiceChargesEnable(searchItemProductArrayList.get(pos).getServiceChargesEnable());
                    store.setServiceCharges(searchItemProductArrayList.get(pos).getServiceCharges());
                    store.setServiceCharges(searchItemProductArrayList.get(pos).getMinimumOrderLimit());


                    Stash.put(Constants.TEMPORARY_STORE,store);
                    v.getContext().startActivity(intent);
                }
            });
        }
    }


    class ProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout restruant_menu;
        TextView productName, productDesc, productPrice, storeName;
        private ImageView loadImage;

        public ProductViewHolder(View itemView) {
            super(itemView);
            restruant_menu = itemView.findViewById(R.id.restruant_menu);
            productName = itemView.findViewById(R.id.productName);
            productDesc = itemView.findViewById(R.id.productDesc);
            productPrice = itemView.findViewById(R.id.productPrice);
            storeName = itemView.findViewById(R.id.storeName);
            loadImage = itemView.findViewById(R.id.loadImage);

            restruant_menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    if (searchItemProductArrayList.get(pos).getStoreClosed()) {
                        Toasty.error(context, "Store is closed at the moment", Toasty.LENGTH_SHORT).show();
                        return;
                    } else {
                        Product product = new Product();
                        product.setId(searchItemProductArrayList.get(pos).getProductId());
                        product.setStoreId(searchItemProductArrayList.get(pos).getStoreId());
                        product.setStoreName(searchItemProductArrayList.get(pos).getStoreName());
                        product.setStoreDeliveryFee(searchItemProductArrayList.get(pos).getStoreDeliveryFee());
                        product.setStoreDeliveryEstimatedTime(searchItemProductArrayList.get(pos).getStoreDeliveryEstimatedTime());
                        product.setProductName(searchItemProductArrayList.get(pos).getProductName());
                        product.setProductDesc(searchItemProductArrayList.get(pos).getProductDesc());
                        product.setProductModel(searchItemProductArrayList.get(pos).getProductModel());
                        product.setQuantity(searchItemProductArrayList.get(pos).getQuantity());
                        product.setMinimumQty(searchItemProductArrayList.get(pos).getMinimumQty());
                        product.setPrice(searchItemProductArrayList.get(pos).getPrice());
                        product.setRequriesShipping(searchItemProductArrayList.get(pos).getRequriesShipping());
                        product.setBaseUrl(searchItemProductArrayList.get(pos).getBaseUrl());
                        product.setPrimaryImage(searchItemProductArrayList.get(pos).getPrimaryImage());
                        product.setDateAvailable(searchItemProductArrayList.get(pos).getDateAvailable());
                        product.setProductOutOfStockName(searchItemProductArrayList.get(pos).getProductOutOfStockName());
                        product.setProductOptions(searchItemProductArrayList.get(pos).getProductOptions());


                        Store store = new Store();
                        store.setTip(searchItemProductArrayList.get(pos).getTip());
                        store.setServiceChargesEnable(searchItemProductArrayList.get(pos).getServiceChargesEnable());
                        store.setServiceCharges(searchItemProductArrayList.get(pos).getServiceCharges());


                        Stash.put(Constants.TEMPORARY_STORE,store);
                        mOnItemClickListener.onItemClick(v, pos, product);
                    }
                    //TODO

                  /*  Intent intent = new Intent(context, ProductDetailFragment.class);
                    Gson gson = new Gson();
                    String myJson = gson.toJson(product);
                    intent.putExtra("myjson", myJson);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);*/


                }
            });
        }


        @Override
        public void onClick(View view) {


        }
    }


}
