package com.eurosoft.customerapp.Utils.db;

import androidx.room.TypeConverter;

import com.eurosoft.customerapp.Model.ProductImages;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ConverterProductImages {

  @TypeConverter
  public String fromListProductImages(List<ProductImages> productImages) {
    if (productImages == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<ProductImages>>() {}.getType();
    String json = gson.toJson(productImages, type);
    return json;
  }

  @TypeConverter
  public List<ProductImages> toListProductImages(String productImages) {
    if (productImages == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<ProductImages>>() {}.getType();
    List<ProductImages> list = gson.fromJson(productImages, type);
    return list;
  }


}
