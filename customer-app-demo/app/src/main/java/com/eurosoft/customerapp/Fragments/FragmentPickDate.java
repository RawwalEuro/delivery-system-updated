package com.eurosoft.customerapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.eurosoft.customerapp.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class FragmentPickDate  extends BottomSheetDialogFragment {

  Context context;
  private BottomSheetListenerPicked bottomSheetListener;
  private TextView text;

  public FragmentPickDate(Context context, String total, BottomSheetListenerPicked bottomSheetListener){
    this.context=context;
    this.bottomSheetListener=bottomSheetListener;
    this.text=text;
  }


  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.pick_filter_date, container, false);
    init(v);
    setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
    return v;
  }

  private void init(View v) {

  }


  public interface BottomSheetListenerPicked {
    void onButtonClicked(String text);
  }


}
