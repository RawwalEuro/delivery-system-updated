package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Adapters.AdapterCartItems;
import com.eurosoft.customerapp.Adapters.AdapterRelatedProducts;
import com.eurosoft.customerapp.Fragments.AddCardFragment;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityReviewPayment extends AppCompatActivity implements View.OnClickListener, AddCardFragment.BottomSheetListener {

    private RelativeLayout btnRpa;
    private RecyclerView rvItems, relatedProductsRv;
    private List<Product> productArrayListl = new ArrayList<>();
    private AdapterCartItems adapterCartItem;
    private AdapterRelatedProducts adapterRelatedProducts;
    private int quantity = 1;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager linearLayoutManagerHor;
    private TextView totalBill;
    private RelativeLayout rlProgressBar;
    private RelativeLayout mainRl;
    private CircularProgressBar circularProgressBar;
    private User currentUser;
    private ImageView close;
    private TextView subtotal;
    private Button continueShopping;
    private RelativeLayout continueShoppingRl;
    private AppSettings appSettings;
    private String productIds = "";
    private ArrayList<Product> listRelatedProducts = new ArrayList<>();
    private TextView deliveryFees;
    private TextView estimateTime;
    private MasterPojo masterPojo;
    private TextView cart, tvAppName, estimateHeading, subTotalHeading, deliveryHeading, inclusive_tax, total, yourCartEmpty, reviewPaymentNAddress;
    private ViewDialogClearCart alertDialogCart;
    private ImageView deltCart;
    private ProductDetailFragment bottomSheetProductDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_payment);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManagerHor = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        alertDialogCart = new ViewDialogClearCart();
        init();
        getCartDetails();
    }


    @Override
    protected void onResume() {
        super.onResume();
        getCartDetails();
    }

    private void setRecycler() {

        if (productArrayListl.size() == 0) {
        } else {
            tvAppName.setText(productArrayListl.get(0).getStoreName() + "");
        }

        StringBuilder productIds = new StringBuilder(100);
        for (int i = 0; i < productArrayListl.size(); i++) {
            if (i == productArrayListl.size() - 1) {
                productIds.append(productArrayListl.get(i).getId() + "");
            } else {
                productIds.append(productArrayListl.get(i).getId() + ",");
            }
        }
        setUpRelatedRv(productIds);

        adapterCartItem = new AdapterCartItems(this, productArrayListl, appSettings, new AdapterCartItems.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.increment:
                        setIncrement(position);
                        break;


                    case R.id.decrement:
                        setDecrement(position);
                        break;


                    case R.id.value:
                        break;

                    case R.id.close:
                        finish();
                        break;


                    case R.id.cross:
                        deleteItem(position);
                        break;


                }

            }
        });
        rvItems.setLayoutManager(linearLayoutManager);
        rvItems.setAdapter(adapterCartItem);

        calculateTotal();
    }


    private void setUpRelatedRv(StringBuilder productIds) {
        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            progressVisiblityGone();
            return;
        }

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<Product>> call = apiService.getRelatedProducts(productIds + "",currentUser.getId(), currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<Product>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Product>> call, Response<CustomWebResponse<Product>> response) {

                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }


                if (response.isSuccessful() && response.code() == 200) {
                    listRelatedProducts = response.body().getData();
                    adapterRelatedProducts = new AdapterRelatedProducts(getApplicationContext(), listRelatedProducts, appSettings, new AdapterRelatedProducts.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            switch (view.getId()) {

                                case R.id.cardView:

                                    //Toast.makeText(getApplicationContext(),listRelatedProducts.get(position).getName(),LENGTH_LONG).show();
                                    Product product = listRelatedProducts.get(position);


                                    bottomSheetProductDetails = new ProductDetailFragment(ActivityReviewPayment.this, product, new ProductDetailFragment.BottomSheetListener() {
                                        @Override
                                        public void onButtonClicked(boolean value) {
                                            bottomSheetProductDetails.dismiss();
                                            getCartDetails();
                                        }
                                    });
                                    bottomSheetProductDetails.show(getSupportFragmentManager(), "addCardFragment");

                                    /*Intent intent = new Intent(getApplicationContext(), ProductDetailsActivity.class);
                                    Gson gson = new Gson();
                                    String myJson = gson.toJson(product);
                                    intent.putExtra("myjson", myJson);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    getApplicationContext().startActivity(intent);*/

                                    break;
                            }
                        }
                    });
                    relatedProductsRv.setLayoutManager(linearLayoutManagerHor);
                    relatedProductsRv.setAdapter(adapterRelatedProducts);

                    progressVisiblityGone();
                }


            }

            @Override
            public void onFailure(Call<CustomWebResponse<Product>> call, Throwable t) {
                progressVisiblityGone();
            }
        });


    }


    public class ViewDialogClearCart {
        public void showDialog(Activity activity, Product productII) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_same_restruant);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            Button btnClear = (Button) dialog.findViewById(R.id.btnClear);
            Button cancel = (Button) dialog.findViewById(R.id.cancel);
            btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delAllProductAsync(currentUser.getId());
                    dialog.dismiss();
                    Toasty.success(getApplicationContext(), masterPojo.getCartClearSucess(), Toasty.LENGTH_LONG).show();
                    getCartDetails();
                    addProductToCart(productII);
                }
            });


            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    private void addProductToCart(Product productToAddRelated) {
        for (int i = 0; i < productArrayListl.size(); i++) {
            for (int y = i + 1; y <= productArrayListl.size(); y++) {
                boolean check = productArrayListl.get(i).getId().equals(productToAddRelated.getId());
                if (check) {
                    Product product = productArrayListl.get(i);
                    product.setName(product.getProductName());
                    product.setMaxQuantity(product.getQuantity());
                    product.setMinimumQty(product.getMinimumQty());
                    product.setQuantity(product.getQuantity() + quantity);
                    product.setActucalPrice(product.getPrice());
                    product.setPrice(productToAddRelated.getPrice() * product.getQuantity());
                    product.setStoreId(product.getStoreId());
                    product.setSpecialInstructions(" ");
                    if (product.getProductOptions() != 0) {
                        product.setOrderDetailData(new ArrayList<>());
                    } else {
                        product.setOrderDetailData(new ArrayList<>());
                    }
                    updateProductAsync(product);
                    getCartDetails();
                    setRecycler();
                    return;
                }
            }
        }


        Product productToAdd = productToAddRelated;
        productToAdd.setName(productToAddRelated.getProductName());
        productToAdd.setQuantity(quantity);
        productToAdd.setMaxQuantity(productToAddRelated.getQuantity());
        productToAdd.setMinimumQty(productToAddRelated.getMinimumQty());
        productToAdd.setActucalPrice(productToAddRelated.getPrice());
        productToAdd.setStoreId(productToAddRelated.getStoreId());
        productToAdd.setPrice(productToAddRelated.getPrice() * quantity);
        productToAdd.setStoreName(Stash.getString(Constants.PRODUCT_STORE_NAME));
        productToAdd.setUserid(currentUser.getId());
        productToAdd.setSpecialInstructions(" ");
        productToAdd.setOrderDetailData(new ArrayList<>());

//            product=productToAdd;
        addProductAsync(productToAdd);
        getCartDetails();
        setRecycler();

    }

    private void addProductAsync(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .insert(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //progressVisiblityGone();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }

    private void delAllProductAsync(int userId) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .delAllUsersById(userId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                continueShoppingRl.setVisibility(View.VISIBLE);
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    private void deleteItem(int position) {
        try {
            Product product = productArrayListl.get(position);
            delProductAsync(product);
            productArrayListl.remove(position);
            adapterCartItem.notifyItemRemoved(position);


            if (productArrayListl.size() == 0) {
                continueShoppingRl.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {

        }

    }

    private void setDecrement(int position) {

        quantity = productArrayListl.get(position).getQuantity();

        if (quantity == 1) {
            Product product = productArrayListl.get(position);
            delProductAsync(product);
            productArrayListl.remove(position);
            adapterCartItem.notifyItemRemoved(position);

            if (productArrayListl.size() == 0) {
                continueShoppingRl.setVisibility(View.VISIBLE);
            }

            return;
        }

       /* if (quantity <= productArrayListl.get(position).getMinimumQty()) {
            Toast.makeText(getApplicationContext(), productArrayListl.get(position).getMinimumQty() + " is the min quantity", Toast.LENGTH_LONG).show();
            return;
        }*/

        double price = productArrayListl.get(position).getGetActucalPrice();
        Product product = productArrayListl.get(position);
        quantity--;
        product.setQuantity((quantity));
        product.setPrice(price * quantity);
        productArrayListl.set(position, product);
        adapterCartItem.notifyItemChanged(position);
        delProductAsync(product);
    }

    private void setIncrement(int position) {
        /*if(quantity >= 5){
            Toast.makeText(getApplicationContext(),"Can't add more than 5 products",LENGTH_LONG).show();
            return;
        }*/
        quantity = productArrayListl.get(position).getQuantity();

        if (quantity >= productArrayListl.get(position).getMaxQuantity()) {
            Toast.makeText(getApplicationContext(), productArrayListl.get(position).getMaxQuantity() + " is the max quantity", Toast.LENGTH_LONG).show();
            return;
        }

        double price = productArrayListl.get(position).getGetActucalPrice();
        Product product = productArrayListl.get(position);
        quantity++;
        product.setQuantity(quantity);
        product.setPrice(price * quantity);
        productArrayListl.set(position, product);
        adapterCartItem.notifyItemChanged(position);
        updateProductAsync(product);
    }


    private void init() {

        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        btnRpa = findViewById(R.id.btnRpa);
        rvItems = findViewById(R.id.cartRv);
        relatedProductsRv = findViewById(R.id.relatedProductsRv);
        totalBill = findViewById(R.id.totalBill);
        deliveryFees = findViewById(R.id.deliveryFees);
        subtotal = findViewById(R.id.subtotal);
        close = findViewById(R.id.close);
        continueShopping = findViewById(R.id.continueShopping);
        continueShoppingRl = findViewById(R.id.continueShoppingRl);
        estimateTime = findViewById(R.id.estimateTime);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        deltCart = findViewById(R.id.deltCart);

        //Language
        cart = findViewById(R.id.cart);
        tvAppName = findViewById(R.id.tvAppName);
        estimateHeading = findViewById(R.id.estimateHeading);
        subTotalHeading = findViewById(R.id.subTotalHeading);
        deliveryHeading = findViewById(R.id.deliveryHeading);
        total = findViewById(R.id.total);
        inclusive_tax = findViewById(R.id.inclusive_tax);
        reviewPaymentNAddress = findViewById(R.id.reviewPaymentNAddress);
        yourCartEmpty = findViewById(R.id.yourCartEmpty);

        continueShopping.setText(masterPojo.getContinueShopping());
        cart.setText(masterPojo.getCart().trim());

        estimateHeading.setText(masterPojo.getEstimateHeading());
        subTotalHeading.setText(masterPojo.getSubTotal());
        deliveryHeading.setText(masterPojo.getDeliveryFee());
        total.setText(masterPojo.getTotal());
        inclusive_tax.setText(masterPojo.getInclusiveTax());
        reviewPaymentNAddress.setText(masterPojo.getReviewPaymentNAddress());
        yourCartEmpty.setText(masterPojo.getCartEmpty());


        btnRpa.setOnClickListener(this);
        close.setOnClickListener(this);
        continueShopping.setOnClickListener(this);
        deltCart.setOnClickListener(this);

    }


    @Override
    public void onButtonClicked(String text) {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnRpa:
                Stash.put("productList", productArrayListl);
                Intent i = new Intent(this, ActivityCheckOut.class);
                startActivity(i);

                break;

            case R.id.close:
                finish();
                break;


            case R.id.continueShopping:
                Intent intent = new Intent(ActivityReviewPayment.this, HomeActivity.class);
                startActivity(intent);
                finish();
                break;


            case R.id.deltCart:
                confirmDeleteDialog();
                break;

        }
    }

    private void confirmDeleteDialog() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Are you sure you want to clear your cart?")
                .setConfirmText(masterPojo.getYes())
                .setCancelText(masterPojo.getNo())

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog
                                .setTitleText(masterPojo.getDone())
                                .setConfirmClickListener(null);

                        delAllProductAsync(currentUser.getId());
                        sDialog.dismiss();

                    }
                })

                .setCancelText(masterPojo.getNo())
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }


    public void calculateTotal() {
        int i = 0;
        double total = 0;

        if (productArrayListl.isEmpty() || productArrayListl == null) {
            totalBill.setText("");
            subtotal.setText("");
            deliveryFees.setText("");
            return;
        }

        while (i < productArrayListl.size()) {
            total = total + Double.valueOf(productArrayListl.get(i).getPrice());
            i++;
        }

        if (productArrayListl.get(0).getStoreDeliveryFee() != null) {
            deliveryFees.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(productArrayListl.get(0).getStoreDeliveryFee()));
            double totalValue = total + productArrayListl.get(0).getStoreDeliveryFee();
            totalBill.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(totalValue));
            subtotal.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(total));
        } else {
            deliveryFees.setText("--");
            totalBill.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(total));
            subtotal.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(total));
        }
        estimateTime.setText(productArrayListl.get(0).getStoreDeliveryEstimatedTime());
    }


    private void getCartDetails() {
        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {

            @Override
            protected List<Product> doInBackground(Void... voids) {
                productArrayListl = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productArrayListl;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productArrayListl == null || productArrayListl.size() == 0) {
                    Log.e("Being Return", "Being Return");
                    return;
                } else {
                    setRecycler();
                    Log.e("Review", productArrayListl.get(0).getStoreName() + "");
                    if (productArrayListl.size() == 0) {
                    } else {
                        tvAppName.setText(productArrayListl.get(0).getStoreName() + "");
                    }
                    //setStickyRecyler();
                }
            }
        }

        GetCartItem gt = new GetCartItem();
        gt.execute();

    }


    private void delProductAsync(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .delete(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                calculateTotal();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    private void updateProductAsync(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .update(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                calculateTotal();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        rlProgressBar.bringToFront();
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);

    }


}