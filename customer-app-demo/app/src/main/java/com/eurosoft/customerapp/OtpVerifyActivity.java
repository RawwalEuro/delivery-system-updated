package com.eurosoft.customerapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.LocationService.LocationEnable;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomResponseString;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;
import in.aabhasjindal.otptextview.OTPListener;
import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpVerifyActivity extends AppCompatActivity implements View.OnClickListener {

    private OtpTextView otpTextView;
    private LinearLayout btnConfrim;
    private static String TAG = "OtpVerifyActivity";
    private FirebaseAuth mAuth;
    private String verificationId;
    private TextView resendOtp;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback;
    private String phoneNumber;
    private PhoneAuthProvider.ForceResendingToken forceResendToken;
    private User user;
    private RelativeLayout rlProgressBar;
    private RelativeLayout mainRl;
    private CircularProgressBar circularProgressBar;
    private int i = 0;
    private TextView countDown, verfication, verificationDesc, textConfirm, didntReceive;
    private MasterPojo masterPojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verify);

        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        otpTextView = findViewById(R.id.otp_view);
        btnConfrim = findViewById(R.id.btnConfrim);
        resendOtp = findViewById(R.id.resendOtp);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        countDown = findViewById(R.id.countDown);
        mainRl = findViewById(R.id.mainRl);

        btnConfrim.setOnClickListener(this);
        resendOtp.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();

        user = (User) Stash.getObject(Constants.TEMP_USER, User.class);


        //Language
        verfication = findViewById(R.id.verfication);
        verificationDesc = findViewById(R.id.verificationDesc);
        textConfirm = findViewById(R.id.textConfirm);
        didntReceive = findViewById(R.id.didntReceive);
        resendOtp = findViewById(R.id.resendOtp);


        verfication.setText(masterPojo.getVerfication());
        verificationDesc.setText(masterPojo.getVerficationDesc());
        textConfirm.setText(masterPojo.getConfirm());
        didntReceive.setText(masterPojo.getDidntReceive());
        resendOtp.setText(masterPojo.getResend());


        disableResend(true);

        otpTextView.setOtpListener(new OTPListener() {
            @Override
            public void onInteractionListener() {

            }

            @Override
            public void onOTPComplete(String otp) {

            }
        });

    }


    private void sendVerificationCode(String phoneNumber) {
        progressVisiblityVisible();
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                mCallBack
        );
    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallBack = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            forceResendToken = forceResendingToken;
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            final String code = phoneAuthCredential.getSmsCode();
            progressVisiblityGone();
            if (code != null) {
                otpTextView.setOTP(code);
                verifyCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(OtpVerifyActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
            progressVisiblityGone();
        }
    };


    private void verifyCode(String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithCredential(credential);
    }


    private void signInWithCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {


                            Toast.makeText(getApplicationContext(), task.isSuccessful() + "", Toast.LENGTH_LONG).show();


                            callApiSignUp(user);

                        } else {
                            Toast.makeText(OtpVerifyActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                            progressVisiblityGone();
                        }
                    }
                });
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallBack,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnConfrim:
                String text = Stash.getString(Constants.OTP_CODE);
                if (TextUtils.isEmpty(otpTextView.getOTP())) {
                    Toast.makeText(OtpVerifyActivity.this, masterPojo.getPleaseEnterOtp(), Toast.LENGTH_SHORT).show();
                    return;
                } else {

                    if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                        progressVisiblityGone();
                        Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                        return;
                    }


                    String num = user.getCustomerPhoneNumber().replace("+440", "+44");
                    user.setCustomerPhoneNumber(num);
                    callApiSignUp(user);
                }


                break;


            case R.id.resendOtp:

               /* if (TextUtils.isEmpty(forceResendToken.toString())) {
                    return;
                }
                disableResend();
                resendVerificationCode(phoneNumber, forceResendToken);*/

                disableResend(false);
                break;

        }
    }


    private void callApiSignUp(User user) {

        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<User>> call = apiService.signUpUser(user);

        call.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;

                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    progressVisiblityGone();
                    return;

                }

                if (response.isSuccessful() && response.code() == 200) {
                    Toast.makeText(getApplicationContext(),response.body().getData().getCustomerEmail() + "", Toast.LENGTH_LONG).show();
                    if (LocationEnable.isLocationEnabled(getApplicationContext())) {
                        Intent i = new Intent(OtpVerifyActivity.this, HomeActivity.class);
                        progressVisiblityGone();
                        Stash.put(Constants.isLoggenIn, true);
                        Stash.put(Constants.IS_SIGN_UP_COMPLETED, true);
                        Stash.put(Constants.USER, response.body().getData());
                        startActivity(i);
                        finish();
                    } else if (!LocationEnable.isLocationEnabled(getApplicationContext())) {
                        Intent i = new Intent(OtpVerifyActivity.this, ActivityTurnOnLocation.class);
                        progressVisiblityGone();
                        Stash.put(Constants.isLoggenIn, true);
                        Stash.put(Constants.IS_SIGN_UP_COMPLETED, true);
                        Stash.put(Constants.USER, response.body().getData());
                        startActivity(i);
                        finish();
                    } else {

                    }
                }
            }


            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                progressVisiblityGone();
                // Toasty.error(OtpVerifyActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
                //Toasty.error(OtpVerifyActivity.this, masterPojo.getUserAlreadyExist(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }


    public void disableResend(boolean isFirstTime) {
        if (!isFirstTime) {
            callApiUserAuthenticate(user);
            countDown.setVisibility(View.VISIBLE);
            resendOtp.setEnabled(false);
            resendOtp.setTextColor(getResources().getColor(R.color.grey));
            new CountDownTimer(60000, 10) { //Set Timer for 6 seconds
                public void onTick(long millisUntilFinished) {
                    countDown.setText(masterPojo.getSecsRemaining() + millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    resendOtp.setTextColor(getResources().getColor(R.color.red));
                    resendOtp.setEnabled(true);
                    countDown.setVisibility(View.GONE);
                }
            }.start();
        } else {
            countDown.setVisibility(View.VISIBLE);
            resendOtp.setEnabled(false);
            resendOtp.setTextColor(getResources().getColor(R.color.grey));
            new CountDownTimer(60000, 10) { //Set Timer for 60 seconds
                public void onTick(long millisUntilFinished) {
                    countDown.setText(masterPojo.getSecsRemaining() + millisUntilFinished / 1000);
                }

                @Override
                public void onFinish() {
                    resendOtp.setTextColor(getResources().getColor(R.color.red));
                    resendOtp.setEnabled(true);
                    countDown.setVisibility(View.GONE);
                }
            }.start();
        }
    }

    @Override
    public void onBackPressed() {
        if (!Stash.getBoolean(Constants.IS_SIGN_UP_COMPLETED)) {
            startActivity(new Intent(OtpVerifyActivity.this, LoginActivity.class));
        }
        super.onBackPressed();

    }


    private void callApiUserAuthenticate(User user) {


        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomResponseString<String>> call = apiService.authorizationCode(user);

        call.enqueue(new Callback<CustomResponseString<String>>() {
            @Override
            public void onResponse(Call<CustomResponseString<String>> call, Response<CustomResponseString<String>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;

                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    progressVisiblityGone();
                    return;

                }

                if (response.isSuccessful() && response.code() == 200) {
                    progressVisiblityGone();
                    //  Toasty.success(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();

                    user.setCustomerVerificationCode(response.body().getData());
                    Stash.put(Constants.IS_SIGN_UP_COMPLETED, false);
                    Stash.put((String) response.body().getData(), Constants.OTP_CODE);
                    user.setSignUpCompleted(false);
                    Stash.put(Constants.TEMP_USER, user);

                    // callApiSignUp(user);
                }
            }


            @Override
            public void onFailure(Call<CustomResponseString<String>> call, Throwable t) {
                progressVisiblityGone();
                //Toasty.error(OtpVerifyActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
}