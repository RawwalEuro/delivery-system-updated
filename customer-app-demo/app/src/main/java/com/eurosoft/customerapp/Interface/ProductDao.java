package com.eurosoft.customerapp.Interface;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.eurosoft.customerapp.Model.Product;

import java.util.List;

@Dao
public interface ProductDao {

  @Query("SELECT * FROM product")
  List<Product> getAll();

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  void insert(Product product);

  @Delete
  void delete(Product product);

  @Update
  void update(Product product);

  @Query("SELECT * FROM product WHERE userid=:userid ")
  List<Product> getAllByUserId(int userid);


  @Query("DELETE  FROM product WHERE userid=:userid ")
  void delAllUsersById(int userid);
}
