package com.eurosoft.customerapp.Model;

public class TrackDriver {

  private Double originLat;
  private Double originLong;

  private Double driverLat;
  private Double driverLong;


  public TrackDriver(Double originLat, Double originLong, Double driverLat, Double driverLong) {
    this.originLat = originLat;
    this.originLong = originLong;
    this.driverLat = driverLat;
    this.driverLong = driverLong;
  }

  public Double getOriginLat() {
    return originLat;
  }

  public void setOriginLat(Double originLat) {
    this.originLat = originLat;
  }

  public Double getOriginLong() {
    return originLong;
  }

  public void setOriginLong(Double originLong) {
    this.originLong = originLong;
  }

  public Double getDriverLat() {
    return driverLat;
  }

  public void setDriverLat(Double driverLat) {
    this.driverLat = driverLat;
  }

  public Double getDriverLong() {
    return driverLong;
  }

  public void setDriverLong(Double driverLong) {
    this.driverLong = driverLong;
  }
}
