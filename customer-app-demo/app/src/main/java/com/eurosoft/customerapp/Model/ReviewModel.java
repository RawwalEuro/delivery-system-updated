package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReviewModel {


    @SerializedName("storeId")
    @Expose
    private Integer storeId;
    @SerializedName("orderId")
    @Expose
    private Integer orderId;
    @SerializedName("driverRating")
    @Expose
    private Integer driverRating;
    @SerializedName("packingRating")
    @Expose
    private Integer packingRating;
    @SerializedName("timelyDeliver")
    @Expose
    private Integer timelyDeliver;
    @SerializedName("message")
    @Expose
    private String message;


    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(Integer driverRating) {
        this.driverRating = driverRating;
    }

    public Integer getPackingRating() {
        return packingRating;
    }

    public void setPackingRating(Integer packingRating) {
        this.packingRating = packingRating;
    }

    public Integer getTimelyDeliver() {
        return timelyDeliver;
    }

    public void setTimelyDeliver(Integer timelyDeliver) {
        this.timelyDeliver = timelyDeliver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReviewModel(Integer storeId, Integer orderId, Integer driverRating, Integer packingRating, Integer timelyDeliver, String message) {
        this.storeId = storeId;
        this.orderId = orderId;
        this.driverRating = driverRating;
        this.packingRating = packingRating;
        this.timelyDeliver = timelyDeliver;
        this.message = message;
    }
}
