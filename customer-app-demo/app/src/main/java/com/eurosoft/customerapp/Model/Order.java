package com.eurosoft.customerapp.Model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.eurosoft.customerapp.Utils.db.DataConverter;

import java.io.Serializable;
import java.util.List;

@Entity
public class Order implements Serializable {

    public boolean isFeedbackGiven;

    public boolean isFeedbackGiven() {
        return isFeedbackGiven;
    }

    public void setFeedbackGiven(boolean feedbackGiven) {
        isFeedbackGiven = feedbackGiven;
    }

    @PrimaryKey(autoGenerate = true)
    private int roomId;

    @ColumnInfo(name = "grandTotal")
    private Double grandTotal;

    @ColumnInfo(name = "subTotal")
    private Double subTotal;

    @ColumnInfo(name = "deliveryFee")
    private Double deliveryFee;

    @ColumnInfo(name = "orderPlacedId")
    private String orderPlacedId;

    @ColumnInfo(name = "userId")
    private String userId;

    @ColumnInfo(name = "storeId")
    private int storeId;

    @ColumnInfo(name = "storeName")
    private String storeName;


    @ColumnInfo(name = "paidBy")
    private String paidBy;


    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "productName")
    private String productNames;


    @ColumnInfo(name = "orderId")
    private Integer orderId;


    @TypeConverters(DataConverter.class)
    @ColumnInfo(name = "productItems")
    private List<Product> productList;

    @ColumnInfo(name = "status")
    private int status;


    @ColumnInfo(name = "payBy")
    private int payBy;


    @ColumnInfo(name = "transId")
    private String transId;

    public int getPayBy() {
        return payBy;
    }

    public void setPayBy(int payBy) {
        this.payBy = payBy;
    }

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public Double getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(Double grandTotal) {
        this.grandTotal = grandTotal;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(Double deliveryFee) {
        this.deliveryFee = deliveryFee;
    }



    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }


    public String getOrderPlacedId() {
        return orderPlacedId;
    }

    public void setOrderPlacedId(String orderPlacedId) {
        this.orderPlacedId = orderPlacedId;
    }


    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }


    public String getProductNames() {
        return productNames;
    }

    public void setProductNames(String productNames) {
        this.productNames = productNames;
    }
    @TypeConverters(DataConverter.class)
    public List<Product> getProductList() {
        return productList;
    }

    @TypeConverters(DataConverter.class)
    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }


    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Order (){

    }


    public Order(Double grandTotal, Double subTotal, Double deliveryFee, String orderPlacedId, String userId, int storeId, String storeName, String paidBy, String productNames,String date,int status,List<Product> productList,int orderId,String transId,int payBy) {

        this.grandTotal = grandTotal;
        this.subTotal = subTotal;
        this.deliveryFee = deliveryFee;
        this.orderPlacedId = orderPlacedId;
        this.userId = userId;
        this.storeId = storeId;
        this.storeName = storeName;
        this.paidBy = paidBy;
        this.productNames = productNames;
        this.date = date;
        this.status = status;
        this.productList = productList;
        this.orderId = orderId;
        this.transId = transId;
        this.payBy = payBy;
    }


    @Override
    public boolean equals(@Nullable @org.jetbrains.annotations.Nullable Object obj) {
        if(obj instanceof Order){
            Order p = (Order) obj;
            return this.orderId.equals(p.getOrderId());
        } else
            return false;
    }
}
