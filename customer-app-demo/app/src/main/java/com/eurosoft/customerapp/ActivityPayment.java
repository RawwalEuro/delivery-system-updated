package com.eurosoft.customerapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Model.AppUserDetail;
import com.eurosoft.customerapp.Model.CardJudoModel;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.db.SharedPreferenceHelper;
import com.fxn.stash.Stash;
import com.judopay.Judo;
import com.judopay.model.Currency;
import com.judopay.model.Receipt;
import com.tfb.fbtoast.FBToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;

import static com.judopay.Judo.JUDO_RECEIPT;

public class ActivityPayment extends AppCompatActivity {


    private static final int REGISTER_CARD_REQUEST = 1;
    private static String judoId = "";
    private static String judoToken = "";
    private static String judoSecret = "";

    private RecyclerView rv;
    private CAdapter adapter;
    CardJudoModel cardJudoModelFromAdapter;
    int selectedIndexFromAdapter;
    private SharedPreferenceHelper spHelper;
    private ArrayList<CardJudoModel> judoCardList=new ArrayList<>();
    private boolean isLoad = false;
    private TextView cardManage;
    private ImageView bckPress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        spHelper = new SharedPreferenceHelper();
        judoId = "";
        judoSecret = "";
        judoToken = "";
        onCreateWork();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REGISTER_CARD_REQUEST:
                handleRegisterCardResult(resultCode, data);
                break;
        }
        if (resultCode == RESULT_OK) {
            new RequestCCList(false).execute();
        }

    }

    private void handleRegisterCardResult(int resultCode, Intent data) {
        switch (resultCode) {
            case Judo.RESULT_SUCCESS:
                Receipt receipt = data.getParcelableExtra(JUDO_RECEIPT);
                CardJudoModel cardJudoModel = new CardJudoModel();
                cardJudoModel.setToken(receipt.getCardDetails().getToken());
                cardJudoModel.setEndDate(receipt.getCardDetails().getFormattedEndDate());
                cardJudoModel.setLastFour(receipt.getCardDetails().getLastFour());
                cardJudoModel.setConsumerToken(receipt.getConsumer().getConsumerToken());
                cardJudoModel.setConsumerReference(receipt.getConsumer().getYourConsumerReference());
                cardJudoModel.setType(receipt.getCardDetails().getType());
                cardJudoModel.set3DS(receipt.is3dSecureRequired());
                ////arraylist
                new SaveCardReciept(1).execute(cardJudoModel);
                break;
            case Judo.RESULT_CANCELED:
                break;
            case Judo.RESULT_ERROR:
                Toasty.error(getApplicationContext(), "Invalid Card Details", Toasty.LENGTH_SHORT).show();
                break;
            case Judo.RESULT_DECLINED:
                Toasty.error(getApplicationContext(), "Card Declined!", Toasty.LENGTH_SHORT).show();
                break;
        }
    }


    public void onCreateWork() {
        rv = findViewById(R.id.rv);
        bckPress = findViewById(R.id.bckPress);
        cardManage = findViewById(R.id.cardManage);
        rv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rv.hasFixedSize();


        bckPress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void onResumeWork() {
        SharedPreferenceHelper s = new SharedPreferenceHelper();
        if (s.getJudoCardList(getApplicationContext()) != null) {
            try {
                adapter = new CAdapter(getApplicationContext(), s.getJudoCardList(getApplicationContext()));
                rv.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rv.setAdapter(null);
        }
    }

    public Judo getJudo() {
        return new Judo.Builder()
                .setJudoId(judoId)
                .setApiToken(judoToken)
                .setApiSecret(judoSecret)
                .setEnvironment(1)
                .setAmount("1.01")
                .setCurrency(Currency.GBP)
                .setConsumerReference("my-consumer-reference")
                .setMaestroEnabled(true)
                .setAmexEnabled(true)
                .build();
    }


    private class SaveCardReciept extends AsyncTask<CardJudoModel, Void, String> {
        private SweetAlertDialog mDialog;
        CardJudoModel receipt;
        int isAddToken;
        public static final String METHOD_NAME = "UpdateAppUserDetail";

        SaveCardReciept(int isAddToken) {
            this.isAddToken = isAddToken;
        }//1 add ,2 remove,3 default

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                mDialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.PROGRESS_TYPE);
                if (isAddToken == 1) {
                    mDialog.setTitleText("Saving Card Details");
                } else if (isAddToken == 2) {
                    mDialog.setTitleText("Removing Card Details");
                } else {
                    mDialog.setTitleText("Updating changes");
                }
                mDialog.setContentText("Please Wait");
                mDialog.setCancelable(false);
                mDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(CardJudoModel... strings) {
            SharedPreferenceHelper spHelper = new SharedPreferenceHelper();
            receipt = strings[0];
            String consumerDetails =
                    "Token|" + receipt.getToken()
                            + "<<<consumer|" + receipt.getConsumerReference()
                            + "<<<consumertoken|" + receipt.getConsumerToken()
                            + "<<<lastfour|" + receipt.getLastFour()
                            + "<<<enddate|" + receipt.getEndDate()
                            + "<<<type|" + receipt.getType();
            String jsonString = "";
            AppUserDetail appUserDetail = new AppUserDetail();
            appUserDetail.setPasswrd("Password");
            appUserDetail.setEmail("Email");
            appUserDetail.setPickDetails("yes");
            appUserDetail.setDefaultClientId(1);
            appUserDetail.setUniqueValue(null);
            if (isAddToken == 1) {
                appUserDetail.setTokenDetails(consumerDetails);
                appUserDetail.setRecordId(0);
                appUserDetail.setDefaultRecordId(0);
                appUserDetail.setDefaultTokenDetails("");
                try {
                    appUserDetail.setDefault(judoCardList.size() == 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (isAddToken == 2) {
                appUserDetail.setRecordId(receipt.getCardId());
                appUserDetail.setDefault(receipt.isDefault());
                appUserDetail.setTokenDetails("");
                if (receipt.isDefault()) {
                    if (judoCardList != null && judoCardList.size() > 0) {
                        try {
                            appUserDetail.setDefaultRecordId(judoCardList.get(0).getCardId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        String consumerDetails_ =
                                "Token|" + judoCardList.get(0).getToken()
                                        + "<<<consumer|" + judoCardList.get(0).getConsumerReference()
                                        + "<<<consumertoken|" + judoCardList.get(0).getConsumerToken()
                                        + "<<<lastfour|" + judoCardList.get(0).getLastFour()
                                        + "<<<enddate|" + judoCardList.get(0).getEndDate()
                                        + "<<<type|" + judoCardList.get(0).getType();
                        appUserDetail.setDefaultTokenDetails(consumerDetails_);
                    } else {
                        appUserDetail.setDefaultRecordId(0);
                        appUserDetail.setDefaultTokenDetails("");
                    }
                } else {
                    appUserDetail.setDefaultRecordId(0);
                    appUserDetail.setDefaultTokenDetails("");
                }
            } else if (isAddToken == 3) {
                appUserDetail.setDefaultRecordId(0);
                appUserDetail.setDefault(true);
                appUserDetail.setRecordId(receipt.getCardId());
                appUserDetail.setTokenDetails(consumerDetails);
                appUserDetail.setDefaultTokenDetails("");
//
//                jsonString = "{" +
//                        "Passwrd:" + "\"" + settingsModel.getPassword() + "\"" + "," +
//                        "Email:" + "\"" + settingsModel.getEmail().trim() + "\"" + "," +
//                        "TokenDetails:" + "\"" + "" + "\"" + "," +
//                        "PickDetails:\"yes\"" +
//                        "}";
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject parentObject = new JSONObject(result);
                    if (parentObject.getBoolean("HasError")) {
                        new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error!")
                                .setContentText(parentObject.getString("Message"))
                                .setConfirmText("OK")
                                .showCancelButton(false)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }

                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
                                    }

                                })
                                .show();
                    } else {
                        if (isAddToken == 1) {
                            FBToast.successToast(getApplicationContext(), parentObject.getString("Message"), FBToast.LENGTH_SHORT);

                        } else if (isAddToken == 2) {
                            FBToast.successToast(getApplicationContext(), parentObject.getString("Message"), FBToast.LENGTH_SHORT);
                            new RequestCCList(false).execute();
                        } else if (isAddToken == 3) {
                            if (cardJudoModelFromAdapter != null) {
                                spHelper.putIntVal("cardSelectedIndex", selectedIndexFromAdapter,getApplicationContext());
                                spHelper.saveCardJudoModel(cardJudoModelFromAdapter,getApplicationContext());
                                adapter.setDefault(selectedIndexFromAdapter);
                                FBToast.successToast(getApplicationContext(), parentObject.getString("Message"), FBToast.LENGTH_SHORT);
                            }
                        } else {
                            FBToast.successToast(getApplicationContext(), parentObject.getString("Message"), FBToast.LENGTH_SHORT);
                        }
                    }
                } catch (Exception e) {
                    FBToast.warningToast(getApplicationContext(), "Please check your internet connection", FBToast.LENGTH_SHORT);
                }
            } else {
                FBToast.warningToast(getApplicationContext(), "Please check your internet connection", FBToast.LENGTH_SHORT);
            }
            if (mDialog != null) {
                mDialog.dismiss();
            }
        }
    }


    public class CAdapter extends RecyclerView.Adapter<CAdapter.MyViewHolder> {
        int selectedIndex = 0;
        private Context context;
        private ArrayList<CardJudoModel> arrayList;
        private String msg = "";

        public CAdapter(Context context, ArrayList<CardJudoModel> arrayList) {
            this.context = context;
            this.arrayList = arrayList;
        }

        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View contactView = inflater.inflate(R.layout.card_item, parent, false);
            return new MyViewHolder(contactView);
        }

        @Override
        public void onBindViewHolder(@NonNull CAdapter.MyViewHolder holder, int position) {
            CardJudoModel c = arrayList.get(position);
            if (c != null) {
                if (c.isDefault()) {
                    selectedIndex = position;
                }
                holder.cardNoTv.setText("**** **** ****" + c.getLastFour());

                holder.expiresTv.setText("Expire " + c.getEndDate());
                if (c.isDefault()) {
                    holder.card_bg.setBackground(ContextCompat.getDrawable(context, R.drawable.select_card_bg));
                    holder.check_view.setVisibility(View.VISIBLE);
                } else {
                    holder.card_bg.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                    holder.check_view.setVisibility(View.GONE);
                }
                // click
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        cardJudoModelFromAdapter = arrayList.get(position);
                        selectedIndexFromAdapter = position;
                        selectedIndex = position;
                        new SaveCardReciept(3).execute(arrayList.get(position));
                    }
                });
                holder.deleteCardIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Are you sure?")
                                .setContentText("You want to remove card details?")
                                .setCancelText("No")
                                .setConfirmText("Delete")
                                .showCancelButton(true)
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
                                    }

                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        new SaveCardReciept(2).execute(arrayList.get(position));
                                    }

                                })
                                .show();
                    }
                });
            }
        }

        public void setDefault(int pos) {
            for (int i = 0; i < arrayList.size(); i++) {
                if (i == pos) {
                    arrayList.get(i).setDefault(true);
                } else {
                    arrayList.get(i).setDefault(false);
                }
            }
            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            TextView cardNoTv, expiresTv, doneBtn;
            RelativeLayout card_bg;
            ImageView check_view, deleteCardIv, cardIv;

            public MyViewHolder(@NonNull View itemView) {
                super(itemView);
                cardNoTv = itemView.findViewById(R.id.cardNoTv);
                expiresTv = itemView.findViewById(R.id.expiresTv);
                card_bg = itemView.findViewById(R.id.card_bg);
                check_view = itemView.findViewById(R.id.check_view);
                deleteCardIv = itemView.findViewById(R.id.deleteCardIv);
                cardIv = itemView.findViewById(R.id.cardIv);
            }
        }
    }


    private class RequestCCList extends AsyncTask<CardJudoModel, Void, String> {
        private SweetAlertDialog mDialog;
        CardJudoModel receipt;
        boolean isAddToken;
        RequestCCList(boolean isAddToken) {
            this.isAddToken = isAddToken;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                mDialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.PROGRESS_TYPE);
                mDialog.setTitleText("Requesting Card Details");
                mDialog.setContentText("Please Wait");
                mDialog.setCancelable(false);
                mDialog.show();
                rv.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(CardJudoModel... strings) {
            SharedPreferenceHelper spHelper = new SharedPreferenceHelper();
            User userSetting = (User) Stash.getObject(Constants.USER_HOME,User.class);
            String jsonString = "";
            if (isAddToken) {
                try {
                    receipt = strings[0];
                    String consumerDetails = "Token|" + receipt.getToken() + "<<<consumer|" + receipt.getConsumerReference() + "<<<consumertoken|" + receipt.getConsumerToken() + "<<<lastfour|" + receipt.getLastFour() + "<<<enddate|" + receipt.getEndDate();
                    jsonString = "{" +
                            "PhoneNo:" + "\"" + "" + "\"" + "," +
                            "UserName:" + "\"" + "" + "\"" + "," +
                            "Passwrd:" + "\"" + userSetting.getPassword() + "\"" + "," +
                            "Email:" + "\"" + userSetting.getCustomerEmail().trim() + "\"" + "," +
                            "defaultClientId:" + "\"" + "ClientId" + "\"" + "," +
                            "uniqueValue:" + "\"" + "ClientId" + "4321orue" + "\"" + "," +
                            "TokenDetails:" + "\"" + consumerDetails + "\"" + "," +
                            "PickDetails:\"yes\"" +
                            "}";
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                jsonString = "{" +
                        "PhoneNo:" + "\"" + "" + "\"" + "," +
                        "UserName:" + "\"" + "" + "\"" + "," +
                        "Passwrd:" + "\"" + userSetting.getPassword() + "\"" + "," +
                        "defaultClientId:" + "\"" + "" + "\"" + "," +
                        "uniqueValue:" + "\"" + "" + "4321orue" + "\"" + "," +
                        "Email:" + "\"" + userSetting.getCustomerEmail() + "\"" + "," +
                        "TokenDetails:" + "\"" + "" + "\"" + "," +
                        "PickDetails:\"yes\"" +
                        "}";
            }

            return null;
        }
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result != null) {
                try {
                    JSONObject parentObject = new JSONObject(result);
                    if (parentObject.getBoolean("HasError")) {
                        new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("Error")
                                .setContentText(parentObject.getString("Message"))
                                .setConfirmText("OK")
                                .showCancelButton(false)
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
//                                            new InitializeAppDb(Default_Payment.this, true).execute();
                                    }

                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.cancel();
//                            getActivity().finish();
                                    }

                                })
                                .show();
                    } else {
                        JSONArray jsonArray = new JSONArray(parentObject.getString("Data"));
                        ArrayList<CardJudoModel> cardDetailsModelArrayList = new ArrayList<>();
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            boolean IsDefault = jsonObject.optBoolean("IsDefault");
                            long RecordId = jsonObject.optLong("RecordId");
                            String token = jsonObject.optString("CCDetails");
                            CardJudoModel cardJudoModel = new CardJudoModel();
                            cardJudoModel.setToken(token);
                            try {
                                String[] consumer__ = token.replace("Token|", "").replace("consumer|", "").replace("consumertoken|", "").replace("lastfour|", "").replace("enddate|", "").replace("type|", "").trim().split("<<<");
                                cardJudoModel.setLastFour(consumer__[3]);
                                cardJudoModel.setEndDate(consumer__[4]);
                                cardJudoModel.setConsumerReference(consumer__[1]);
                                cardJudoModel.setConsumerToken(consumer__[2]);
                                cardJudoModel.setType(0);
                                cardJudoModel.set3DS(true);
                                cardJudoModel.setDefault(IsDefault);
                                cardJudoModel.setCardId(RecordId);
                                cardJudoModel.setCardLabel("+Add Card Label");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            cardDetailsModelArrayList.add(cardJudoModel);
                        }
                        if (spHelper == null) {
                            spHelper = new SharedPreferenceHelper();
                        }
                        if (cardDetailsModelArrayList.size() == 0) {
                            spHelper.removeJudoCardModelArrayList(getApplicationContext());
                        } else if (cardDetailsModelArrayList.size() > 0) {
                            spHelper.putJudoCardModelArrayList(getApplicationContext(),cardDetailsModelArrayList);
                        }
                        isLoad = true;
                        onResumeWork();
                        if (cardDetailsModelArrayList.size() == 1) {
                            selectedIndexFromAdapter = 0;
                            new SaveCardReciept(3).execute(cardDetailsModelArrayList.get(0));
                        }
                    }
                } catch (Exception e) {
                    FBToast.errorToast(getApplicationContext(), e.getMessage(), FBToast.LENGTH_SHORT);
                }
            } else {
                FBToast.errorToast(getApplicationContext(), "No Data found", FBToast.LENGTH_SHORT);
            }
            if (mDialog != null) {
                mDialog.dismiss();
            }
            rv.setVisibility(View.VISIBLE);
        }
    }

}