package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderStatus {
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("OrderTime")
    @Expose
    private String orderTime;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("CustomerPhoneNumber")
    @Expose
    private String customerPhoneNumber;
    @SerializedName("PickupAddress")
    @Expose
    private String pickupAddress;
    @SerializedName("DropoffAddress")
    @Expose
    private String dropoffAddress;
    @SerializedName("TotalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("PaymentTypeName")
    @Expose
    private String paymentTypeName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public void setPickupAddress(String pickupAddress) {
        this.pickupAddress = pickupAddress;
    }

    public String getDropoffAddress() {
        return dropoffAddress;
    }

    public void setDropoffAddress(String dropoffAddress) {
        this.dropoffAddress = dropoffAddress;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }
}
