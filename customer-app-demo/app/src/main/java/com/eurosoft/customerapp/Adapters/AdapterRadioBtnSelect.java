package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.OptionDetailedData;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterRadioBtnSelect extends RecyclerView.Adapter<AdapterRadioBtnSelect.ViewHolder> {

    private Context context;
    private List<OptionDetailedData> optionDetailedDataList;
    private OnItemClickListener mOnItemClickListener;
    private AppSettings appSetting;
    public int lastCheckedPosition = -1;
    int copyOfLastCheckedPosition = 99;

    public AdapterRadioBtnSelect(Context ctx, List<OptionDetailedData> optionDetailedData, AppSettings appSettings, OnItemClickListener onItemClickListener) {
        context = ctx;
        optionDetailedDataList = optionDetailedData;
        mOnItemClickListener = onItemClickListener;
        appSetting = appSettings;
    }


    @Override
    public AdapterRadioBtnSelect.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_radio_btn_select, parent, false);
        final AdapterRadioBtnSelect.ViewHolder viewHolder = new AdapterRadioBtnSelect.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterRadioBtnSelect.ViewHolder holder, int position) {
        OptionDetailedData optionDetailedData = optionDetailedDataList.get(position);
        holder.radioBtn.setText(optionDetailedData.getValue());
        holder.variantPrice.setText(appSetting.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(optionDetailedData.getPrice()));
        holder.radioBtn.setChecked(position == lastCheckedPosition);
        holder.checkBox.setChecked(position == lastCheckedPosition);

        if(holder.checkBox.isChecked()){
            holder.variantPrice.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.radioBtn.setTextColor(context.getResources().getColor(R.color.colorPrimary));
            holder.circle.setVisibility(View.VISIBLE);
        } else {
            holder.variantPrice.setTextColor(context.getResources().getColor(R.color.black));
            holder.radioBtn.setTextColor(context.getResources().getColor(R.color.black));
            holder.circle.setVisibility(View.GONE);
        }

        holder.radioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  mOnItemClickListener.onItemClick(v, holder.getAdapterPosition());

                copyOfLastCheckedPosition = lastCheckedPosition;
                lastCheckedPosition = holder.getAdapterPosition();
                notifyItemChanged(copyOfLastCheckedPosition);
                notifyItemChanged(lastCheckedPosition);
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition(),copyOfLastCheckedPosition);
            }
        });

    }

    @Override
    public int getItemCount() {
        return optionDetailedDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RadioButton radioBtn;
        private TextView variantPrice;
        private LinearLayout rlParent;
        private CheckBox checkBox;
        private ImageView circle;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            radioBtn = itemView.findViewById(R.id.radioBtn);
            variantPrice = itemView.findViewById(R.id.variantPrice);
            rlParent = itemView.findViewById(R.id.rlParent);
            checkBox = itemView.findViewById(R.id.checkBox);

            circle = itemView.findViewById(R.id.circle);

        }
    }


    public int getCopyOfLastCheckedPosition() {
        return copyOfLastCheckedPosition;
    }

    public void setCopyOfLastCheckedPosition(int copyOfLastCheckedPosition) {
        this.copyOfLastCheckedPosition = copyOfLastCheckedPosition;
    }

    public int getLastCheckedPosition() {
        return lastCheckedPosition;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position , int lastCheck);
    }
}
