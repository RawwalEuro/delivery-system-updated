package com.eurosoft.customerapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.util.Log;

import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;

public class GeocoderHelperClass {
    private Handler mHandler;
    private int mMaxResults = 1;
    private SetResult mSetResultInterface;
    private Context mContext;
    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    public GeocoderHelperClass(Context context) {
        mHandler = new Handler(Looper.getMainLooper());
        mContext = context;
    }

    public GeocoderHelperClass setMaxResults(int maxResult) {
        mMaxResults = maxResult;
        //dsaklsakdlj
        return this;
    }

    public void execute(String location) {
        execute((Object[]) new String[]{location});
    }

    public void execute(double lat, double lon) {
        execute((Object[]) new Double[]{lat, lon});
    }

    private void execute(final Object... objects) {
        try {
            if (objects != null && Geocoder.isPresent()) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        List<Address> list = new ArrayList<Address>();
                        List<Address> Templist = new ArrayList<Address>();
                        String formatedAdress = null;
                        try {
                            if (objects.length >= 2 && objects[0] instanceof Double) {
                                double lat = (Double) objects[0];
                                double lon = (Double) objects[1];
                                final SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
                                AppSettings appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);

                                String DirectionClien;
                                if (appSettings.getWebsiteSettingData().getMapapi() == null || appSettings.getWebsiteSettingData() == null) {
                                    callApiAppSettings();
                                    AppSettings appSettingsReload = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);

                                    DirectionClien = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=false&key=" + appSettingsReload.getWebsiteSettingData().getMapapi() + "";
                                    Log.e("Direct",DirectionClien);
                                } else {
                                    DirectionClien = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&sensor=false&key=" + appSettings.getWebsiteSettingData().getMapapi() + "";
                                    Log.e("Direct-Else",DirectionClien);
                                }

                                OkHttpClient client = new OkHttpClient();
                                Request request = new Request.Builder()
                                        .url(DirectionClien)
                                        .build();
                                Response response = null;
                                try {
                                    response = client.newCall(request).execute();
                                    JSONObject jsonData = new JSONObject(response.body().string());
                                    JSONArray jsonarr = jsonData.getJSONArray("results");
                                    JSONObject jsonobj = jsonarr.getJSONObject(0);
                                    JSONArray types = jsonobj.getJSONArray("types");
                                    formatedAdress = jsonobj.getString("formatted_address");
                                    if (types.length() == 2 && types.getString(0).equals("locality") && types.getString(1).equals("political")) {
                                        jsonobj = jsonarr.getJSONObject(1);
                                        formatedAdress = jsonobj.getString("formatted_address");

                                    } else {

                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                Address a = new Address(Locale.UK);
                                a.setAddressLine(0, formatedAdress);
                                a.setLatitude(lat);
                                a.setLongitude(lon);
                                list = new ArrayList<Address>();
                                list.add(a);
                                //  GetDataFromGoogle.execute(DirectionClien);
                                //list = new Geocoder(mContext, Locale.UK).getFromLocation(lat, lon, mMaxResults);
                                // list=formatedAdress;
                                //list.add((Address) formatedAdress);
                            } else if (objects.length >= 1 && objects[0] instanceof String) {
                                String location = (String) objects[0];
                                list = new Geocoder(mContext, Locale.UK).getFromLocationName(location, mMaxResults);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            final List<Address> result = list;
                            if (result != null && result.size() > 0) {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSetResultInterface != null)
                                            mSetResultInterface.onGetResult(result);
                                    }
                                });
                            } else {
                                mHandler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (mSetResultInterface != null)
                                            mSetResultInterface.onGetResult(null);
                                    }
                                });
                            }
                        }
                    }
                }, "GeoCoderRunner").start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GeocoderHelperClass setResultInterface(SetResult result) {
        mSetResultInterface = result;
        return this;
    }

    public interface SetResult {
        public abstract void onGetResult(List<Address> list);
    }


    private void callApiAppSettings() {


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<AppSettings>> call = apiService.getAppSettings();

        call.enqueue(new Callback<WebResponse<AppSettings>>() {
            @Override
            public void onResponse(Call<WebResponse<AppSettings>> call, retrofit2.Response<WebResponse<AppSettings>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }

                if (!response.body().getSuccess()) {

                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    Stash.put(Constants.APP_SETTINGS, response.body().getData());
                    AppSettings appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);

                }
            }


            @Override
            public void onFailure(Call<WebResponse<AppSettings>> call, Throwable t) {
            }
        });

    }
}
