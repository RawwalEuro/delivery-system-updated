package com.eurosoft.customerapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.OptionDetailedData;
import com.eurosoft.customerapp.Utils.Converters;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import es.dmoral.toasty.Toasty;

public class AdapterCheckBoxSelect extends RecyclerView.Adapter<AdapterCheckBoxSelect.ViewHolder> {

    private Context context;
    private List<OptionDetailedData> optionDetailedDataList;
    private OnItemClickListener mOnItemClickListener;
    private AppSettings appSetting;
    public int lastCheckedPosition = -1;
    int copyOfLastCheckedPosition = 0;
    boolean checked;
    int maxSelection,selectedCountChecked;

    public AdapterCheckBoxSelect(Context ctx, List<OptionDetailedData> optionDetailedData, AppSettings appSettings,int maxSelectionCount,int selectedCount, OnItemClickListener onItemClickListener) {
        context = ctx;
        optionDetailedDataList = optionDetailedData;
        mOnItemClickListener = onItemClickListener;
        appSetting = appSettings;
        maxSelection = maxSelectionCount;
        selectedCountChecked = selectedCount;
    }


    @Override
    public AdapterCheckBoxSelect.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_checkbx_btn_select, parent, false);
        final AdapterCheckBoxSelect.ViewHolder viewHolder = new AdapterCheckBoxSelect.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterCheckBoxSelect.ViewHolder holder, int position) {
        OptionDetailedData optionDetailedData = optionDetailedDataList.get(position);
        holder.checkBox.setText(optionDetailedData.getValue());
        holder.variantPrice.setText(appSetting.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(optionDetailedData.getPrice()));

        if(selectedCountChecked >=  maxSelection){
            Toasty.warning(context, "Can't add more than " + maxSelection, Toasty.LENGTH_SHORT).show();
            return;
        }

        holder.rlParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, position);
            }
        });


        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                if(selectedCountChecked >=  maxSelection){
                    Toasty.warning(context, "Can't add more than " + maxSelection, Toasty.LENGTH_SHORT).show();
                    return;
                }

                if(isChecked){
                    checked = true;
                    holder.variantPrice.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.checkBox.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    holder.circle.setVisibility(View.VISIBLE);
                } else {
                    checked = false;
                    holder.variantPrice.setTextColor(context.getResources().getColor(R.color.black));
                    holder.checkBox.setTextColor(context.getResources().getColor(R.color.black));
                    holder.circle.setVisibility(View.GONE);
                }
            }
        });

        holder.checkBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(selectedCountChecked >=  maxSelection){
                    Toasty.warning(context, "Can't add more than " + maxSelection, Toasty.LENGTH_SHORT).show();
                    return;
                }
                mOnItemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return optionDetailedDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox checkBox;
        private TextView variantPrice;
        private RelativeLayout rlParent;
        private ImageView circle;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            checkBox = itemView.findViewById(R.id.checkBox);
            variantPrice = itemView.findViewById(R.id.variantPrice);
            rlParent = itemView.findViewById(R.id.rlParent);
            circle = itemView.findViewById(R.id.circle);

        }
    }

    public boolean isChecked() {
        return checked;
    }

    public int getCopyOfLastCheckedPosition() {
        return copyOfLastCheckedPosition;
    }

    public int getLastCheckedPosition() {
        return lastCheckedPosition;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}
