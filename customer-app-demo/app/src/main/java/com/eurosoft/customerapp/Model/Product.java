package com.eurosoft.customerapp.Model;

import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.eurosoft.customerapp.Utils.db.ConvertedOptionDetailData;
import com.eurosoft.customerapp.Utils.db.ConverterProductImages;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

@Entity
public class Product extends ListItem implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @Expose
    private Integer roomId;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @ColumnInfo(name = "StoreName")
    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @ColumnInfo(name = "StoreId")
    @SerializedName("StoreId")
    @Expose
    private int storeId;
    @Ignore
    @ColumnInfo(name = "IsWishList")
    @SerializedName("IsWishList")
    @Expose
    private Boolean IsWishList;
    @ColumnInfo(name = "CategoryId")
    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;

    @ColumnInfo(name = "userid")
    @Expose
    private Integer userid;

    @ColumnInfo(name = "SubCategoryId")
    @SerializedName("SubCategoryId")
    @Expose
    private Integer subCategoryId;

    @ColumnInfo(name = "ProductName")
    @SerializedName("ProductName")
    @Expose
    private String productName;

    @ColumnInfo(name = "ProductDesc")
    @SerializedName("ProductDesc")
    @Expose
    private String productDesc;

    @ColumnInfo(name = "ProductModel")
    @SerializedName("ProductModel")
    @Expose
    private String productModel;

    @ColumnInfo(name = "SKU")
    @SerializedName("SKU")
    @Expose
    private String sku;

    @ColumnInfo(name = "UPC")
    @SerializedName("UPC")
    @Expose
    private String upc;

    @ColumnInfo(name = "EAN")
    @SerializedName("EAN")
    @Expose
    private String ean;

    @ColumnInfo(name = "JAN")
    @SerializedName("JAN")
    @Expose
    private String jan;

    @ColumnInfo(name = "ISBN")
    @SerializedName("ISBN")
    @Expose
    private String isbn;

    @ColumnInfo(name = "MPN")
    @SerializedName("MPN")
    @Expose
    private String mpn;

    @ColumnInfo(name = "Location")
    @SerializedName("Location")
    @Expose
    private String location;

    @ColumnInfo(name = "Price")
    @SerializedName("Price")
    @Expose
    private Double price;

    @ColumnInfo(name = "Quantity")
    @SerializedName("Quantity")
    @Expose
    private int quantity;


    @ColumnInfo(name = "maxQuantity")
    @Expose
    private int maxQuantity;

    @ColumnInfo(name = "MinimumQty")
    @SerializedName("MinimumQty")
    @Expose
    private int minimumQty;

    @ColumnInfo(name = "PrimaryImage")
    @SerializedName("PrimaryImage")
    @Expose
    private String primaryImage;

    @ColumnInfo(name = "Image1")
    @SerializedName("Image1")
    @Expose
    private String image1;

    @ColumnInfo(name = "Image2")
    @SerializedName("Image2")
    @Expose
    private String image2;

    @ColumnInfo(name = "Image3")
    @SerializedName("Image3")
    @Expose
    private String image3;

    @ColumnInfo(name = "SortOder")
    @SerializedName("SortOder")
    @Expose
    private Integer sortOder;

    @ColumnInfo(name = "Status")
    @SerializedName("Status")
    @Expose
    private Boolean status;

    @ColumnInfo(name = "getActucalPrice")
    @Expose
    private Double getActucalPrice;



    @ColumnInfo(name = "specialInstructions")
    @Expose
    private String specialInstructions;


    @ColumnInfo(name = "RequriesShipping")
    @Expose
    private String RequriesShipping;





    @ColumnInfo(name = "StoreDeliveryFee")
    @Expose
    private Double StoreDeliveryFee;


    @ColumnInfo(name = "StoreDeliveryEstimatedTime")
    @Expose
    private String StoreDeliveryEstimatedTime;


    public Double getStoreDeliveryFee() {
        return StoreDeliveryFee;
    }

    public void setStoreDeliveryFee(Double storeDeliveryFee) {
        StoreDeliveryFee = storeDeliveryFee;
    }

    public String getStoreDeliveryEstimatedTime() {
        return StoreDeliveryEstimatedTime;
    }

    public void setStoreDeliveryEstimatedTime(String storeDeliveryEstimatedTime) {
        StoreDeliveryEstimatedTime = storeDeliveryEstimatedTime;
    }

    @ColumnInfo(name = "BaseUrl")
    @Expose
    private String BaseUrl;



    @ColumnInfo(name = "DateAvailable")
    @Expose
    private String DateAvailable;


    @ColumnInfo(name = "ProductOutOfStockName")
    @Expose
    private String ProductOutOfStockName;


    @ColumnInfo(name = "ProductOptions")
    @Expose
    private int ProductOptions;

    @TypeConverters(ConverterProductImages.class)
    @ColumnInfo(name = "ProductImages")
    @Expose
    private List<ProductImages> ProductImages;


    @TypeConverters(ConvertedOptionDetailData.class)
    @ColumnInfo(name = "orderDetailData")
    @Expose
    private List<OptionDetailedData> orderDetailData;


    private boolean last;
    private boolean first;

    @Ignore
    private String catName;

    @TypeConverters(ConverterProductImages.class)
    public List<ProductImages> getProductImages() {
        return ProductImages;
    }

    @TypeConverters(ConverterProductImages.class)
    public void setProductImages(List<ProductImages> productImages) {
        ProductImages = productImages;
    }


    public boolean getIsWishList() {
        if(IsWishList == null){
            return false;
        }
        return IsWishList;
    }


    public int getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(int maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public List<OptionDetailedData> getOrderDetailData() {
        return orderDetailData;
    }

    public void setOrderDetailData(List<OptionDetailedData> orderDetailData) {
        this.orderDetailData = orderDetailData;
    }


    public String getRequriesShipping() {
        return RequriesShipping;
    }

    public void setRequriesShipping(String requriesShipping) {
        RequriesShipping = requriesShipping;
    }

    public void setIsWishList(boolean isWishList) {
        IsWishList = isWishList;
    }

    public String getBaseUrl() {
        return BaseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        BaseUrl = baseUrl;
    }

    public String getDateAvailable() {
        return DateAvailable;
    }

    public void setDateAvailable(String dateAvailable) {
        DateAvailable = dateAvailable;
    }

    public String getProductOutOfStockName() {
        return ProductOutOfStockName;
    }

    public void setProductOutOfStockName(String productOutOfStockName) {
        ProductOutOfStockName = productOutOfStockName;
    }

    public int getProductOptions() {
        return ProductOptions;
    }

    public void setProductOptions(int productOptions) {
        ProductOptions = productOptions;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getcatName() {
        return catName;
    }

    public void setcatName(String catName) {
        this.catName = catName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }


    public boolean isLast() {
        return last;
    }

    public void setLast(boolean last) {
        this.last = last;
    }

    public boolean isFirst() {
        return first;
    }

    public void setFirst(boolean first) {
        this.first = first;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public Product() {

    }

    public Product(Integer id, Integer categoryId, Integer subCategoryId, String productName, Double price, int quantity, int minimumQty, String primaryImage, String image1, String image2, String image3, Integer sortOder, Double getActucalPrice) {
        this.id = id;
        this.categoryId = categoryId;
        this.subCategoryId = subCategoryId;
        this.productName = productName;
        this.price = price;
        this.quantity = quantity;
        this.minimumQty = minimumQty;
        this.primaryImage = primaryImage;
        this.image1 = image1;
        this.image2 = image2;
        this.image3 = image3;
        this.sortOder = sortOder;
        this.getActucalPrice = getActucalPrice;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Integer subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {

        if(price == null){
            price = 00.0;
        }

        roundfloat(price, 2);
        return Double.parseDouble(String.format("%.2f", price));
    }

    public static double roundfloat(Double d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.doubleValue();
    }


    public void setPrice(Double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMinimumQty() {
        if(minimumQty == 0){
            return 1;
        }
        return minimumQty;
    }

    public void setMinimumQty(int minimumQty) {
        this.minimumQty = minimumQty;
    }

    public String getPrimaryImage() {
        return primaryImage;
    }

    public void setPrimaryImage(String primaryImage) {
        this.primaryImage = primaryImage;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public Integer getSortOder() {
        return sortOder;
    }

    public void setSortOder(Integer sortOder) {
        this.sortOder = sortOder;
    }

    public Double getGetActucalPrice() {
        return getActucalPrice;
    }

    public void setActucalPrice(Double getActucalPrice) {
        this.getActucalPrice = getActucalPrice;
    }

    public String getProductDesc() {
        return productDesc == null ? "" : productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getUpc() {
        return upc;
    }

    public void setUpc(String upc) {
        this.upc = upc;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getJan() {
        return jan;
    }

    public void setJan(String jan) {
        this.jan = jan;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getMpn() {
        return mpn;
    }

    public void setMpn(String mpn) {
        this.mpn = mpn;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public void setGetActucalPrice(Double getActucalPrice) {
        this.getActucalPrice = getActucalPrice;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }


    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }


    public Double getmultipliedByQuanty() {
        return quantity * getActucalPrice;
    }

    public Double setPriceByQuantity(Double price) {
        return this.price = price;
    }


    @Override
    public boolean equals(@Nullable @org.jetbrains.annotations.Nullable Object obj) {
        if (obj instanceof Product) {
            Product p = (Product) obj;
            return this.id.equals(p.getId());
        } else
            return false;
    }
}
