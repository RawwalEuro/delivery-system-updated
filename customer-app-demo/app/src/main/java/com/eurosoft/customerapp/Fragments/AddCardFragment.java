package com.eurosoft.customerapp.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.ModelStripe.ModelStripe;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.FourDigitCardFormatWatcher;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.ConfirmPaymentIntentParams;
import com.stripe.android.model.PaymentIntent;
import com.stripe.android.model.PaymentMethodCreateParams;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AddCardFragment extends BottomSheetDialogFragment {
    private BottomSheetListener mListener;
    private EditText editTextCardNumber;
    private EditText etCvv;
    private EditText expiryEt;
    private RelativeLayout btnAddCard;
    private Stripe stripe;
    private String paymentIntentClientSecret;
    private TextView payNow;
    private SweetAlertDialog dialog;
    CardInputWidget cardInputWidget;
    private BottomSheetListener bottomSheetListener;
    Context context;
    String total = "";
    private TextView totalTv;
    private MasterPojo masterPojo;
    private TextView paynow, totalBill;


    public AddCardFragment(Context context, String total, MasterPojo masterPojo, BottomSheetListener bottomSheetListener) {
        this.context = context;
        this.bottomSheetListener = bottomSheetListener;
        this.total = total;
        this.masterPojo = masterPojo;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_card_bottom_sheet, container, false);


        stripe = new Stripe(
                getApplicationContext(),
                Objects.requireNonNull(Constants.Stripe_PK)
        );
        init(v);
        totalTv.setText(total + "");

        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
        return v;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void showSuccessgMsg(String title, String message) {
        if (dialog != null) {
            dialog.dismiss();
        }
        Toast.makeText(getApplicationContext(), masterPojo.getTransCompletedSucess(), Toast.LENGTH_LONG).show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void showErrorMessage(String title, String message) {
        if (dialog != null) {
            dialog.dismiss();
        }
        dialog = new SweetAlertDialog(getApplicationContext(), SweetAlertDialog.ERROR_TYPE);
        dialog.setTitleText(title)
                .setContentText(message)
//                                .setCancelText("Exit!")
                .setConfirmText(masterPojo.getoK())
                .showCancelButton(false)
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
//                                        new InitializeAppDb(mContext, true).execute();
                    }
                })
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
//                                        mContext.finish();
                    }
                })
                .show();
    }


    private void init(View v) {
        editTextCardNumber = v.findViewById(R.id.cardET);
        etCvv = v.findViewById(R.id.etCvv);
        expiryEt = v.findViewById(R.id.expiryEt);
        btnAddCard = v.findViewById(R.id.btnAddCard);
        cardInputWidget = v.findViewById(R.id.cardInputWidget);
        totalTv = v.findViewById(R.id.total);
        payNow = v.findViewById(R.id.paynow);
        totalBill = v.findViewById(R.id.totalBill);
        paynow = v.findViewById(R.id.paynow);

        totalBill.setText(masterPojo.getTotalBill());
        paynow.setText(masterPojo.getPayNow());


        editTextCardNumber.addTextChangedListener(new FourDigitCardFormatWatcher());
        expiryEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {


                if (editable.length() > 0 && (editable.length() % 3) == 0) {
                    final char c = editable.charAt(editable.length() - 1);
                    if ('/' == c) {
                        editable.delete(editable.length() - 1, editable.length());
                    }
                }
                if (editable.length() > 0 && (editable.length() % 3) == 0) {
                    char c = editable.charAt(editable.length() - 1);
                    if (Character.isDigit(c) && TextUtils.split(editable.toString(), String.valueOf("/")).length <= 2) {
                        editable.insert(editable.length() - 1, String.valueOf("/"));
                    }
                }

            }
        });

        btnAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                    Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                    return;
                }

                payNow.setText(masterPojo.getPleaseWait());
                /*ObjectAnimator colorAnim = ObjectAnimator.ofInt(payNow, "textColor", Color.WHITE, Color.GREEN);
                colorAnim.setDuration(1000);
                colorAnim.setEvaluator(new ArgbEvaluator());
                colorAnim.setRepeatCount(ValueAnimator.INFINITE);
                colorAnim.setRepeatMode(ValueAnimator.REVERSE);
                colorAnim.start();*/


                final Handler handler = new Handler();
                Runnable runnable = new Runnable() {

                    int count = 0;

                    @Override
                    public void run() {
                        count++;

                        if (count == 1) {
                            payNow.setText(masterPojo.getPleaseWait() + ".");
                        } else if (count == 2) {
                            payNow.setText(masterPojo.getPleaseWait() + "..");
                        } else if (count == 3) {
                            payNow.setText(masterPojo.getPleaseWait() + "...");
                        }

                        if (count == 3)
                            count = 0;
                        handler.postDelayed(this, 500);
                    }
                };
                handler.postDelayed(runnable, 500);
                checkOut();

            }
        });
    }

    private void checkOut() {

        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }


        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    ModelStripe stripeModel = new ModelStripe();
                    stripeModel.defaultClientId = "5057";
                    stripeModel.ReceiptEmail = "rawallpervaiz@gmail.com";
                    stripeModel.Amount = "1.1";
                    stripeModel.Currency = "GBP";
                    //    stripeModel.Stripe_PK = Config.Stripe_PK;
                    stripeModel.MerchantSecondaryAPIKey = Constants.Stripe_SK;
                    Gson gson = new Gson();
                    String jsonString = gson.toJson(stripeModel);


                    OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
                    clientBuilder.connectTimeout(8, TimeUnit.SECONDS);

                    clientBuilder.readTimeout(8, TimeUnit.SECONDS);
                    OkHttpClient client = clientBuilder.build();
                    String url = "http://eurosofttech-api.co.uk/paymentgateway/api/gateway/createstripepaymentintent";
                    RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonString);
                    Request request = new Request.Builder()
                            .url(url)
                            .post(requestBody)
                            .build();
                    Response rawResponse = client.newCall(request).execute();
                    String response = rawResponse.body().string();
                    if (response != null && !response.startsWith("false") && !response.equals("")) {
                        try {
                            paymentIntentClientSecret = response.replace("\"", "");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    PaymentMethodCreateParams params = cardInputWidget.getPaymentMethodCreateParams();
                                    if (params != null) {
                                        ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams.createWithPaymentMethodCreateParams(params, paymentIntentClientSecret);
                                        final Context context = getApplicationContext();
                                        stripe = new Stripe(context, Constants.Stripe_PK);
                                        stripe.confirmPayment(getActivity(), confirmParams);
                                        Log.e("Sucess", "Sucess");

                                        bottomSheetListener.onButtonClicked("Verifying your card");
                                    }
                                }
                            });
                        } catch (Exception e) {
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Log.e("Failure", "Catch");
                                    // dialog.dismiss();
                                    //showErrorMessage("Payment Failed", "Unable to process payment, Please try again later");
                                }
                            });
                        }
                    } else {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Log.e("Failure", "Failure");

                                //dialog.dismiss();
                                //showErrorMessage("Payment Failed", "Unable to process payment, Please try again later");
                            }
                        });
                    }
                } catch (Exception e) {
                }


            }
        }).start();

        //bottomSheetListener.onButtonClicked("");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Handle the result of stripe.confirmPayment
        stripe.onPaymentResult(requestCode, data, new PaymentResultCallback(getActivity()));
    }


    private class PaymentResultCallback implements ApiResultCallback<PaymentIntentResult> {
        private WeakReference<Activity> activityRef;

        PaymentResultCallback(Activity fragment) {
            activityRef = new WeakReference<>(fragment);
        }


        @Override
        public void onSuccess(PaymentIntentResult result) {
            final Activity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                // Payment completed successfully
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String finalResult = gson.toJson(paymentIntent);
                try {

                    JSONObject jsonObject = new JSONObject(finalResult);
                    JSONObject subObj = jsonObject.getJSONObject("paymentMethod");
                    String transId = jsonObject.getString("id");
//                    JSONObject cardObj=jsonObject.getJSONObject("card");
//                    String CardCountry=cardObj.getString("country");
                    if (bottomSheetListener != null) {
                        bottomSheetListener.onButtonClicked("success==" + transId);
                    }

                } catch (Exception e) {
                    if (bottomSheetListener != null) {
                        bottomSheetListener.onButtonClicked("failed==" + e.getMessage());
                    }
                }
                showSuccessgMsg(
                        masterPojo.getPaymentCompleted(),
                        gson.toJson(paymentIntent)

                );
            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed
                showErrorMessage(
                        masterPojo.getPaymentFailed(),
                        Objects.requireNonNull(paymentIntent.getLastPaymentError()).getMessage()

                );
            }
        }

        @Override
        public void onError(Exception e) {
            final Activity activity = activityRef.get();
            if (activity == null) {
                return;
            }
            if (bottomSheetListener != null) {
                bottomSheetListener.onButtonClicked("failed==" + e.getMessage());
            }
            // Payment request failed – allow retrying using the same payment method
            showErrorMessage(masterPojo.getPaymentFailed(), e.toString());
        }
    }


    public interface BottomSheetListener {
        void onButtonClicked(String text);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (BottomSheetListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement BottomSheetListener");
        }
    }
}
