package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceOrder {
    @SerializedName("BookingTypeId")
    @Expose
    private final int BookingTypeId;

    @SerializedName("dropoffAddress")
    @Expose
    private String dropoffAddress;
    @SerializedName("dropofflocationtypeid")
    @Expose
    private String dropofflocationtypeid;
    @SerializedName("dropoffLocationType")
    @Expose
    private String dropoffLocationType;
    @SerializedName("dropoffLatitude")
    @Expose
    private Double dropoffLatitude;
    @SerializedName("dropoffLongitude")
    @Expose
    private Double dropoffLongitude;
    @SerializedName("orderTime")
    @Expose
    private String orderTime;
    @SerializedName("customerId")
    @Expose
    private Integer customerId;
    @SerializedName("orderDate")
    @Expose
    private String orderDate;
    @SerializedName("DeliveryTime")
    @Expose
    private String DeliveryTime;


    @SerializedName("Instruction")
    @Expose
    private String Instruction;

    @SerializedName("subTotal")
    @Expose
    private Double subTotal;
    @SerializedName("shippingCharges")
    @Expose
    private Double shippingCharges;
    @SerializedName("totalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("paymentTypeId")
    @Expose
    private Integer paymentTypeId;
    @SerializedName("orderStatus")
    @Expose
    private Integer orderStatus;

    @SerializedName("StoreId")
    @Expose
    private Integer StoreId;

    @SerializedName("orderDetailData")
    @Expose
    private List<OrderDetails> orderDetailData = null;


    @SerializedName("transId")
    @Expose
    private String transId;


    @SerializedName("ServiceCharges")
    @Expose
    private Double ServiceCharges;


    @SerializedName("TipCharges")
    @Expose
    private Double TipCharges;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public int getBookingTypeId() {
        return BookingTypeId;
    }

    public PlaceOrder(String dropoffAddress, String dropofflocationtypeid, String dropoffLocationType, Double dropoffLatitude, Double dropoffLongitude,
                      String orderTime, Integer customerId, String orderDate, Double subTotal, Double shippingCharges, Double totalAmount,String transId,
                      Integer paymentTypeId, Integer orderStatus, List<OrderDetails> orderDetailData, String DeliveryTime, String instruction, int BookingTypeId, int StoreId,Double serviceCharges, Double tipCharges) {
        this.dropoffAddress = dropoffAddress;
        this.dropofflocationtypeid = dropofflocationtypeid;
        this.dropoffLocationType = dropoffLocationType;
        this.dropoffLatitude = dropoffLatitude;
        this.dropoffLongitude = dropoffLongitude;
        this.orderTime = orderTime;
        this.customerId = customerId;
        this.orderDate = orderDate;
        this.subTotal = subTotal;
        this.shippingCharges = shippingCharges;
        this.totalAmount = totalAmount;
        this.paymentTypeId = paymentTypeId;
        this.orderStatus = orderStatus;
        this.orderDetailData = orderDetailData;
        this.DeliveryTime = DeliveryTime;
        this.Instruction = instruction;
        this.BookingTypeId = BookingTypeId;
        this.StoreId = StoreId;
        this.transId = transId;
        this.ServiceCharges = serviceCharges;
        this.TipCharges = tipCharges;
    }


    public Double getServiceCharges() {
        return ServiceCharges;
    }

    public void setServiceCharges(Double serviceCharges) {
        ServiceCharges = serviceCharges;
    }

    public Double getTipCharges() {
        return TipCharges;
    }

    public void setTipCharges(Double tipCharges) {
        TipCharges = tipCharges;
    }

    public Integer getStoreId() {
        return StoreId;
    }

    public void setStoreId(Integer storeId) {
        StoreId = storeId;
    }

    public String getDeliveryTime() {
        return DeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        DeliveryTime = deliveryTime;
    }

    public String getInstruction() {
        return Instruction;
    }

    public void setInstruction(String instruction) {
        Instruction = instruction;
    }

    public String getDropoffAddress() {
        return dropoffAddress;
    }

    public void setDropoffAddress(String dropoffAddress) {
        this.dropoffAddress = dropoffAddress;
    }

    public String getDropofflocationtypeid() {
        return dropofflocationtypeid;
    }

    public void setDropofflocationtypeid(String dropofflocationtypeid) {
        this.dropofflocationtypeid = dropofflocationtypeid;
    }

    public String getDropoffLocationType() {
        return dropoffLocationType;
    }

    public void setDropoffLocationType(String dropoffLocationType) {
        this.dropoffLocationType = dropoffLocationType;
    }

    public Double getDropoffLatitude() {
        return dropoffLatitude;
    }

    public void setDropoffLatitude(Double dropoffLatitude) {
        this.dropoffLatitude = dropoffLatitude;
    }

    public Double getDropoffLongitude() {
        return dropoffLongitude;
    }

    public void setDropoffLongitude(Double dropoffLongitude) {
        this.dropoffLongitude = dropoffLongitude;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Double shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Integer paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public List<OrderDetails> getOrderDetailData() {
        return orderDetailData;
    }

    public void setOrderDetailData(List<OrderDetails> orderDetailData) {
        this.orderDetailData = orderDetailData;
    }


}
