package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Adapters.AdapterHome;
import com.eurosoft.customerapp.Adapters.AdapterNestedHome;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Store;
import com.fxn.stash.Stash;

import java.util.List;

public class ActivitySeeMore extends AppCompatActivity {

    private MasterPojo masterPojo;
    private RelativeLayout navIcon;
    private TextView titleRestruant,categoryRestruant,openRestruant;
    private RecyclerView rvSeeMore;
    private AdapterNestedHome adapterNestedHome;
    private AppSettings appSettings;
    private List<Store> storeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_more);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        getBundle();
        initViews();
    }

    private void getBundle() {
        Bundle b = getIntent().getExtras();
        storeList = (List<Store>) b.getSerializable("storeList");
    }

    private void initViews() {
        navIcon = findViewById(R.id.navIcon);
        titleRestruant = findViewById(R.id.titleRestruant);
        rvSeeMore = findViewById(R.id.rvSeeMore);
        openRestruant = findViewById(R.id.openRestruant);

       setData();

        navIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setData() {
        if(storeList == null || storeList.size() == 0){
            return;
        }

        openRestruant.setText(storeList.size() + " stores available");
        titleRestruant.setText("Details");

        rvSeeMore.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapterNestedHome = new AdapterNestedHome(getApplicationContext(), storeList,appSettings,masterPojo);
        rvSeeMore.setAdapter(adapterNestedHome);

    }
}