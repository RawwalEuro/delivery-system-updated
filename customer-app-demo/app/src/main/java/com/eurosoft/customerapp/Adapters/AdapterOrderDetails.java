package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterOrderDetails  extends RecyclerView.Adapter<AdapterOrderDetails.ViewHolder>{

  private List<Product> productList;
  private Context context;

  public  AdapterOrderDetails(Context context, List<Product> products){
    this.context = context;
    this.productList = products;
  }


  @Override
  public AdapterOrderDetails.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {

    View view = LayoutInflater.from(context).inflate(R.layout.adapter_order_details, parent, false);
    return new AdapterOrderDetails.ViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull @NotNull AdapterOrderDetails.ViewHolder holder, int position) {
    Product product = productList.get(position);
    holder.pricing.setText(Converters.roundfloat(product.getGetActucalPrice()) + "");
    holder.productName.setText(product.getProductName() + "");
    holder.value.setText(product.getQuantity() + "");

  }

  @Override
  public int getItemCount() {
    return productList.size();
  }

  public class ViewHolder extends RecyclerView.ViewHolder {
    TextView value,productName,pricing;

    public ViewHolder(@NonNull @NotNull View itemView) {
      super(itemView);

      value = itemView.findViewById(R.id.value);
      productName = itemView.findViewById(R.id.productName);
      pricing = itemView.findViewById(R.id.pricingDetails);

    }
  }
}
