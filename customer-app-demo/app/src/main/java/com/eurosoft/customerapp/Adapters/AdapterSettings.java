package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.Languages;
import com.eurosoft.customerapp.R;
import com.fxn.stash.Stash;

import java.util.List;

public class AdapterSettings extends RecyclerView.Adapter<AdapterSettings.ViewHolder> {

    private Context mCtx;
    private List<Languages> listLanguage;
    private OnItemClickListener mOnItemClickListener;
    int row_index=-1;

    public AdapterSettings (Context mCtx, List<Languages> langList, int row_index, OnItemClickListener onItemClicked ){
        this.mCtx = mCtx;
        this.listLanguage = langList;
        this.mOnItemClickListener = onItemClicked;
        this.row_index = row_index;
    }


    @Override
    public AdapterSettings.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Stash.init(mCtx);
        View view = LayoutInflater.from(mCtx).inflate(R.layout.adapter_settings, parent, false);


        return new AdapterSettings.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterSettings.ViewHolder holder, int position) {

        Languages languages = listLanguage.get(position);
        holder.langName.setText(languages.getLanguage());
        holder.langCode.setText(languages.getCulture());

        if (row_index == position) {
            holder.rlBg.setBackgroundColor(Color.parseColor("#EA8948"));
            holder.langName.setTextColor(Color.parseColor("#FFFFFF"));
            holder.langCode.setTextColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.rlBg.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.langName.setTextColor(Color.parseColor("#000000"));
            holder.langCode.setTextColor(Color.parseColor("#000000"));
        }

        holder.rlBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Stash.put(Constants.SELECTED_LANG_POS,holder.getAdapterPosition());
                mOnItemClickListener.onItemClick(v,holder.getAdapterPosition());
                row_index=holder.getAdapterPosition();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listLanguage.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView langName,langCode;
        private CheckBox checkBox;
        private RelativeLayout rlBg;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            langName = itemView.findViewById(R.id.langName);
            langCode = itemView.findViewById(R.id.langCode);
            checkBox = itemView.findViewById(R.id.checkBox);
            rlBg = itemView.findViewById(R.id.rlBg);
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}
