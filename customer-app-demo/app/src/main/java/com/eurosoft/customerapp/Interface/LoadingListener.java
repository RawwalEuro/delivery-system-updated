package com.eurosoft.customerapp.Interface;

public interface LoadingListener {
    void loadingStarted();
    void loadingFinished();
}
