package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Adapters.AdapterRestruantMenu;
import com.eurosoft.customerapp.Adapters.AdapterSubCategory;
import com.eurosoft.customerapp.Fragments.AddProductDialog;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.SubCategoryWithProducts;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.Footer.FooterBarLayout;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityRestruantMenu extends AppCompatActivity implements View.OnClickListener {

    private AppBarLayout appBarlayout;
    private LinearLayout toolbarItemLL;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private RecyclerView rvMenu;
    LinearLayoutManager linearLayoutManager;
    ArrayList<String> arrayList = new ArrayList<>();
    AdapterRestruantMenu adapter;
    AdapterSubCategory adapterSubCategory;
    private boolean isUserScrolling = false;
    private boolean isListGoingUp = true;
    private RelativeLayout btnCart;
    private String id = "0";
    private ImageView bckPress;
    private boolean hasSubCategory;
    private Context context;
    private ArrayList<Product> productArrayList = new ArrayList<>();
    private ArrayList<String> listTabLayout = new ArrayList<>();
    private RelativeLayout rlNoData;
    private FooterBarLayout footerCart;
    private List<Product> productsFromDB = new ArrayList<>();
    private User currentUser;
    private TextView count, totalAmount;

    private FrameLayout frameCart;
    private TextView cart_badge;
    private ArrayList<TabIndexSize> tabIndexesArray;
    private String restruantName = "";
    private TextView title;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private AppSettings appSettings;
    private EditText searchEt;
    private ArrayList<Product> products;
    private ArrayList<Product> listProductWithoutSubCategory = new ArrayList<>();
    private RelativeLayout searchBarRl;
    private AddProductDialog bottomSheet;
    private TextView noItemFound;
    private MasterPojo masterPojo;
    private ProductDetailFragment bottomSheetProductDetails;
    private Store storeObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restruant_menu);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        initViews();
        getData();

    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }

    private void setDataWithOutCat() {
        progressVisiblityVisible();
        tabLayout.setVisibility(View.GONE);

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<Product>> call = apiService.getSubCategoriesProductsById(id,currentUser.getId(),currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<Product>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Product>> call, Response<CustomWebResponse<Product>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    rvMenu.setLayoutManager(linearLayoutManager);
                    rvMenu.setHasFixedSize(true);
                    listProductWithoutSubCategory = response.body().getData();

                    if (listProductWithoutSubCategory == null || listProductWithoutSubCategory.size() == 0) {
                        searchBarRl.setVisibility(View.GONE);
                        rlNoData.setVisibility(View.VISIBLE);
                        rvMenu.setVisibility(View.GONE);
                        progressVisiblityGone();
                        return;
                    } else {
                        searchBarRl.setVisibility(View.VISIBLE);
                        rlNoData.setVisibility(View.GONE);
                        rvMenu.setVisibility(View.VISIBLE);
                        progressVisiblityGone();
                    }

                    adapter = new AdapterRestruantMenu(context, response.body().getData(), appSettings, new AdapterRestruantMenu.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, Product product) {
                            switch (view.getId()) {
                                case R.id.restruant_menu:

                                   /* product.setId(adapter.getClickedPosId());
                                    Log.e("ProductId",product.getId() + "");*/

                                    //product.setId(Stash.getInt(Constants.HOLDER_POSITON));
                                    if (product.getProductOptions() != 0) {


                                        bottomSheetProductDetails = new ProductDetailFragment(ActivityRestruantMenu.this, product, new ProductDetailFragment.BottomSheetListener() {
                                            @Override
                                            public void onButtonClicked(boolean value) {
                                                bottomSheetProductDetails.dismiss();
                                                getCartDetails();
                                                Stash.put(Constants.PERMENANT_STORE,Stash.getObject(Constants.TEMPORARY_STORE, Store.class));
                                                storeObject = (Store) Stash.getObject(Constants.PERMENANT_STORE, Store.class);
                                                Log.e("StoreName",storeObject.getStoreName());
                                            }
                                        });
                                        bottomSheetProductDetails.show(getSupportFragmentManager(), "addCardFragment");


                                        /*Intent intent = new Intent(ActivityRestruantMenu.this, ProductDetailsActivity.class);
                                        Gson gson = new Gson();
                                        String myJson = gson.toJson(product);
                                        intent.putExtra("myjson", myJson);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);*/
                                        return;
                                    }

                                    bottomSheet = new AddProductDialog(ActivityRestruantMenu.this, product, new AddProductDialog.BottomSheetListener() {
                                        @Override
                                        public void onButtonClicked(boolean value) {
                                            bottomSheet.dismiss();
                                            getCartDetails();

                                            Stash.put(Constants.PERMENANT_STORE,Stash.getObject(Constants.TEMPORARY_STORE, Store.class));
                                            storeObject = (Store) Stash.getObject(Constants.PERMENANT_STORE, Store.class);
                                            Log.e("StoreName",storeObject.getStoreName());
                                        }
                                    });
                                    bottomSheet.show(getSupportFragmentManager(), "addCardFragment");




                                    break;

                            }
                        }
                    });
                    rvMenu.setAdapter(adapter);
                    progressVisiblityGone();
                    if (response.body().getData().isEmpty() || response.body().getData() == null || response.body().getData().size() == 0) {
                        rlNoData.setVisibility(View.VISIBLE);
                        progressVisiblityGone();
                    }
                    getCartDetails();
                }
            }


            @Override
            public void onFailure(Call<CustomWebResponse<Product>> call, Throwable t) {
                progressVisiblityGone();
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });


    }

    private void setDataWithCategories() {
        progressVisiblityVisible();
        tabLayout.setVisibility(View.VISIBLE);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<SubCategoryWithProducts>> call = apiService.getSubCategoriesProductsByIdTab(id,currentUser.getId(),currentUser.getToken());
        call.enqueue(new Callback<CustomWebResponse<SubCategoryWithProducts>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<SubCategoryWithProducts>> call, Response<CustomWebResponse<SubCategoryWithProducts>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    products = new ArrayList<>();
                    tabIndexesArray = new ArrayList<>();
                    for (int i = 0; i < response.body().getData().size(); i++) {
                        TabLayout.Tab tab = tabLayout.newTab();
                        if (response.body().getData().get(i).getProduct() == null || response.body().getData().get(i).getProduct().isEmpty()) {
                            rlNoData.setVisibility(View.VISIBLE);
                            searchBarRl.setVisibility(View.GONE);
                            progressVisiblityGone();
                            return;
                        }

                        searchBarRl.setVisibility(View.VISIBLE);
                        for (int j = 0; j < response.body().getData().get(i).getProduct().size(); j++) {
                            products.add(response.body().getData().get(i).getProduct().get(j));

                            if (j == response.body().getData().get(i).getProduct().size() - 1) {
                                products.get(products.size() - 1).setLast(true);
                            }
                            if (j == 0) {
                                tab.setTag(products.size() - 1);
                                products.get(products.size() - 1).setcatName(response.body().getData().get(i).getSubCategoryName());
                                products.get(products.size() - 1).setFirst(true);
                            }
                        }
                        tab.setText(response.body().getData().get(i).getSubCategoryName() + "");

                        tabLayout.addTab(tab);

                        if (response.body().getData().size() == 1) {
                            tabLayout.setVisibility(View.GONE);
                        }
                        progressVisiblityGone();
                    }

                    rvMenu.setLayoutManager(linearLayoutManager);
                    rvMenu.setHasFixedSize(true);

                    if (products.size() == 0) {
                        searchBarRl.setVisibility(View.GONE);
                    } else {
                        searchBarRl.setVisibility(View.VISIBLE);
                    }

                    adapterSubCategory = new AdapterSubCategory(context, products, appSettings, new AdapterSubCategory.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            switch (view.getId()) {

                                case R.id.restruant_menu:


                                    Product product = products.get(position);

                                    if (product.getProductOptions() != 0) {


                                        Intent intent = new Intent(ActivityRestruantMenu.this, ProductDetailFragment.class);
                                        Gson gson = new Gson();
                                        String myJson = gson.toJson(product);
                                        intent.putExtra("myjson", myJson);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }


                                    bottomSheet = new AddProductDialog(ActivityRestruantMenu.this, product, new AddProductDialog.BottomSheetListener() {
                                        @Override
                                        public void onButtonClicked(boolean value) {
                                            bottomSheet.dismiss();
                                            getCartDetails();
                                            Stash.put(Constants.PERMENANT_STORE,Stash.getObject(Constants.TEMPORARY_STORE, Store.class));
                                            storeObject = (Store) Stash.getObject(Constants.PERMENANT_STORE, Store.class);
                                            Log.e("StoreName",storeObject.getStoreName());
                                        }
                                    });
                                    bottomSheet.show(getSupportFragmentManager(), "addCardFragment");


                                    break;


                            }

                        }
                    });

                    rvMenu.setAdapter(adapterSubCategory);
                    tabLayout.setOnTouchListener(new View.OnTouchListener() {
                        @Override
                        public boolean onTouch(View v, MotionEvent event) {
                            isUserScrolling = false;
                            return false;
                        }
                    });
                    tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                        @Override
                        public void onTabSelected(TabLayout.Tab tab) {
                            if (isUserScrolling) {
                                return;
                            }

                            int position = tab.getPosition();
                            if (position == 0) {
                                rvMenu.smoothScrollToPosition(0);
                            } else {
                                rvMenu.smoothScrollToPosition((int) tab.getTag());
                                rvMenu.computeVerticalScrollOffset();
                            }
                        }

                        @Override
                        public void onTabUnselected(TabLayout.Tab tab) {

                        }

                        @Override
                        public void onTabReselected(TabLayout.Tab tab) {

                        }
                    });

                    rvMenu.addOnScrollListener(new RecyclerView.OnScrollListener() {

                        @Override
                        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                                isUserScrolling = true;
                            }
//                            if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
//
//                                if (isListGoingUp) {
//                                    //my recycler view is actually inverted so I have to write this condition instead
//                                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1 == products.size()) {
//                                        Handler handler = new Handler();
//                                        handler.postDelayed(new Runnable() {
//                                            @Override
//                                            public void run() {
//                                                if (isListGoingUp) {
//                                                    if (linearLayoutManager.findLastCompletelyVisibleItemPosition() + 1 == arrayList.size()) {
//                                                        //  Toast.makeText(ActivityRestruantMenu.this, "exeute something", Toast.LENGTH_SHORT).show();
//                                                    }
//                                                }
//                                            }
//                                        }, 50);
//                                        //waiting for 50ms because when scrolling down from top, the variable isListGoingUp is still true until the onScrolled method is executed
//                                    }
//                                }
//                            }
                            if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                                isUserScrolling = false;

                            }

                        }

                        @Override
                        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            int itemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                            if (isUserScrolling) {
                                if (itemPosition == 0) { //  item position of uses
                                    TabLayout.Tab tab = tabLayout.getTabAt(0);
                                    tab.select();
                                    tabLayout.requestLayout();
                                } else {
                                    if (tabLayout.getTabCount() > 1) {
                                        for (int i = 1; i < tabLayout.getTabCount(); i++) {
                                            TabLayout.Tab tab = tabLayout.getTabAt(i);
                                            if (i < tabLayout.getTabCount() - 1) {
                                                if (itemPosition >= (int) (tabLayout.getTabAt(i).getTag()) && itemPosition < (int) (tabLayout.getTabAt(i + 1).getTag())) {

                                                    tab.select();
                                                    tabLayout.requestLayout();
                                                    break;
                                                }
                                            } else {
                                                if (itemPosition >= (int) (tabLayout.getTabAt(i).getTag())) {
                                                    tab.select();
                                                    tabLayout.requestLayout();
                                                }

                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });

                    if (response.body().getData().isEmpty() || response.body().getData() == null || response.body().getData().size() == 0) {
                        rlNoData.setVisibility(View.VISIBLE);
                        progressVisiblityGone();
                    }
                    getCartDetails();
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<SubCategoryWithProducts>> call, Throwable
                    t) {
                // Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                progressVisiblityGone();
            }
        });
    }


    private void initViews() {
        linearLayoutManager = new LinearLayoutManager(this);
        context = getApplicationContext();
        appBarlayout = findViewById(R.id.appBarlayout);
        toolbarItemLL = findViewById(R.id.toolbarItemLL);
        toolbar = findViewById(R.id.toolbar);
        tabLayout = findViewById(R.id.tabLayout);
        rvMenu = findViewById(R.id.rvMenu);
        btnCart = findViewById(R.id.btnCart);
        bckPress = findViewById(R.id.bckPress);
        rlNoData = findViewById(R.id.rlNoData);
        bckPress = findViewById(R.id.bckPress);
        footerCart = findViewById(R.id.footerCart);
        totalAmount = findViewById(R.id.total);
        count = findViewById(R.id.count);
        frameCart = findViewById(R.id.frameCart);
        cart_badge = findViewById(R.id.cart_badge);
        title = findViewById(R.id.titleLocation);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        searchEt = findViewById(R.id.searchEt);
        searchBarRl = findViewById(R.id.searchBarRl);
        noItemFound = findViewById(R.id.noItemFound);

        searchEt.setHint(masterPojo.getSearch());
        noItemFound.setText(masterPojo.getNoProductsAvailable());


        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                try {
                    filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        footerCart.setOnClickListener(this);
        btnCart.setOnClickListener(this);

        bckPress.setOnClickListener(this);
        btnCart.setOnClickListener(this);

        frameCart.setOnClickListener(this);


    }


    private void filter(String text) {
        ArrayList<Product> filteredList = new ArrayList<>();

        if (hasSubCategory) {
            for (Product item : products) {
                if (item.getProductName().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
            adapterSubCategory.filterList(filteredList);


            if (adapterSubCategory.getProductList().size() == 0) {
                rlNoData.setVisibility(View.VISIBLE);
                rvMenu.setVisibility(View.GONE);
            } else {
                rlNoData.setVisibility(View.GONE);
                rvMenu.setVisibility(View.VISIBLE);
            }
        } else {


            for (Product item : listProductWithoutSubCategory) {
                if (item.getProductName().toLowerCase().contains(text.toLowerCase())) {
                    filteredList.add(item);
                }
            }
            adapter.filterList(filteredList);
            if (adapter.getProductList().size() == 0) {
                rlNoData.setVisibility(View.VISIBLE);
                rvMenu.setVisibility(View.GONE);
            } else {
                rlNoData.setVisibility(View.GONE);
                rvMenu.setVisibility(View.VISIBLE);
            }
        }
    }


    private void collapseHeaderVisiblity() {
        appBarlayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    //  Collapsed
                    toolbar.setVisibility(View.VISIBLE);
                    toolbarItemLL.setVisibility(View.VISIBLE);

                } else {
                    //Expanded
                    toolbar.setVisibility(View.GONE);
                    toolbarItemLL.setVisibility(View.GONE);

                }
            }
        });
    }


    private void getData() {
        if (getIntent().getStringExtra("id") != null || getIntent().getStringExtra("id").isEmpty()) {
            id = getIntent().getStringExtra("id");
            hasSubCategory = getIntent().getExtras().getBoolean("IsSubCategory");
            restruantName = getIntent().getExtras().getString("restruantName");
            title.setText(restruantName);
            callApi();
            return;
        }
    }

    private void callApi() {

        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            progressVisiblityGone();
            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }

        if (hasSubCategory) {
            setDataWithCategories();
            return;
        }
        setDataWithOutCat();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bckPress:
                finish();
                break;

            case R.id.frameCart:
                Intent intent = new Intent(ActivityRestruantMenu.this, ActivityReviewPayment.class);
                startActivity(intent);
                break;


            case R.id.btnCart:

                Intent intent1 = new Intent(ActivityRestruantMenu.this, ActivityReviewPayment.class);
                startActivity(intent1);

                break;

        }
    }


    private void getCartDetails() {
        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {

            @Override
            protected List<Product> doInBackground(Void... voids) {
                productsFromDB = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productsFromDB;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productsFromDB == null || productsFromDB.size() == 0) {
                    footerCart.setVisibility(View.GONE);
                    frameCart.setVisibility(View.GONE);
                } else {
                    footerCart.setVisibility(View.VISIBLE);
                    calculateTotal(productsFromDB);
                    count.setText(productsFromDB.size() + "");
                    frameCart.setVisibility(View.VISIBLE);
                    cart_badge.setText(productsFromDB.size() + "");


                }


            }
        }

        GetCartItem gt = new GetCartItem();
        gt.execute();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartDetails();

    }

    public void calculateTotal(List<Product> products) {
        int i = 0;
        double total = 0;

        if (products.isEmpty() || products == null) {
            totalAmount.setText("");
            return;
        }

        while (i < products.size()) {
            total = total + Double.valueOf(products.get(i).getPrice());
            i++;
        }
        totalAmount.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(total));
        count.setText(products.size() + "");

    }
}