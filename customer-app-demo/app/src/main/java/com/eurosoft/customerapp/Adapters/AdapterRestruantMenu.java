package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;
import com.fxn.stash.Stash;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterRestruantMenu extends RecyclerView.Adapter<AdapterRestruantMenu.ViewHolder> {


    private Context mCtx;
    private ArrayList<Product> productList;
    private AppSettings appSetting;
    private OnItemClickListener mOnItemClickListener;
    int clickedPosId = 0;

    public void setClickedPosId(int clickedPosId) {
        this.clickedPosId = clickedPosId;
    }

    public int getClickedPosId() {
        return clickedPosId;
    }

    public AdapterRestruantMenu(Context mCtx, ArrayList<Product> productArrayList, AppSettings appSettings, OnItemClickListener onItemClickListener) {
        this.mCtx = mCtx;
        this.productList = productArrayList;
        this.appSetting = appSettings;
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public AdapterRestruantMenu.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.adapter_restruant_menu, parent, false);
        Stash.init(mCtx);
        return new AdapterRestruantMenu.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterRestruantMenu.ViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.productName.setText(product.getProductName() + "");
        holder.productDesc.setText(product.getProductDesc() + "");

        Stash.put(Constants.HOLDER_POSITON,product.getId());
        Log.e("ProductName",product.getProductName());
        Log.e("ProductId",product.getId() + "");

        try {
            holder.productPrice.setText(appSetting.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(product.getPrice()) + " ");
            Picasso.get().load(product.getBaseUrl() + product.getId() + "/" + product.getPrimaryImage()).placeholder(R.color.grey).into(holder.loadImage);

        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.restruant_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, product);

            }
        });

    }


    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void filterList(ArrayList<Product> filteredList) {
        productList = filteredList;
        notifyDataSetChanged();

    }


    public ArrayList<Product> getProductList() {
        return productList;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, Product product);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout restruant_menu;
        TextView productName, productDesc, productPrice;
        private ImageView loadImage;

        public ViewHolder(View itemView) {
            super(itemView);
            restruant_menu = itemView.findViewById(R.id.restruant_menu);
            productName = itemView.findViewById(R.id.productName);
            productDesc = itemView.findViewById(R.id.productDesc);
            productPrice = itemView.findViewById(R.id.productPrice);
            loadImage = itemView.findViewById(R.id.loadImage);
        }
    }
}