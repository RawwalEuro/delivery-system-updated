package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Model.MasterPojo;
import com.fxn.stash.Stash;

public class ActivityOrderPlaced extends AppCompatActivity {

    private RelativeLayout btnAddCard;
    private TextView continueBtn,thankyou,orderConfirmed;
    private MasterPojo masterPojo;
    private String orderId = "";
    private TextView orderIdTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_placed);
        Stash.init(getApplicationContext());

        orderId = getIntent().getStringExtra(Constants.ORDER_ID);


        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);



        btnAddCard = findViewById(R.id.btnAddCard);
        continueBtn = findViewById(R.id.continueBtn);
        thankyou = findViewById(R.id.thankyou);
        orderConfirmed = findViewById(R.id.orderConfirmed);
        orderIdTxt = findViewById(R.id.orderId);

        orderConfirmed.setText(masterPojo.getOrderConfirmed());
        thankyou.setText(masterPojo.getThankYou());
        continueBtn.setText(masterPojo.getContinuee());

        if(orderId != null || !orderId.equalsIgnoreCase("")){
            orderIdTxt.setText("Your order ID is " + orderId);
        } else {
            orderIdTxt.setVisibility(View.INVISIBLE);
        }

        btnAddCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityOrderPlaced.this,ActivityPastOrders.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
    }
}