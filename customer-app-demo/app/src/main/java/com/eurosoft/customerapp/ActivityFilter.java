package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appyvet.materialrangebar.RangeBar;
import com.eurosoft.customerapp.Adapters.AdapterDietory;
import com.eurosoft.customerapp.Adapters.AdapterSortFilter;
import com.eurosoft.customerapp.Model.FilterBody;
import com.eurosoft.customerapp.Model.FilterDetailDatum;
import com.eurosoft.customerapp.Model.HomeAllDetails;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.fxn.stash.Stash;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class ActivityFilter extends AppCompatActivity {
    private RecyclerView sortRv, dietaryRv;
    private ArrayList<String> stringsFilter = new ArrayList<>();
    private AdapterSortFilter adapterSortFilter;
    private AdapterDietory adapterDietaryFilter;
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager linearLayoutManagerHor;
    private ArrayList<String> lstDietary = new ArrayList<>();
    private ImageView closeBtn;
    private LinearLayout btnFilter, llSort, llDietory, llFreeDelivery, llPriceRange;
    private TextView headingFilter, sort, priceRange, dietry, filter;
    private CheckBox checkBox;
    private MasterPojo masterPojo;
    private HomeAllDetails homeAllDetails;
    private RangeBar rangebar;
    FilterBody filterBody;
    int minRange = 0;
    int maxRange = 100;
    String sortId = "";
    ArrayList<FilterDetailDatum> filterDetailData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        filterBody = new FilterBody();
        filterBody.setSortId(null);
        filterBody.setFreeDelivery(true);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManagerHor = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        Stash.init(getApplicationContext());
        initViews();
        getData();
        setUpSortRv();
        setUpDietaryRv();
        setUpPriceRange();
    }

    private void setUpPriceRange() {

        if (homeAllDetails.getFilterList().get(1) != null) {
            priceRange.setText(homeAllDetails.getFilterList().get(1).getName());
            List<String> listItems = new ArrayList<String>();
            for (int i = 0; i < homeAllDetails.getFilterList().get(1).getFilterDetailData().size(); i++) {
                listItems.add(homeAllDetails.getFilterList().get(1).getFilterDetailData().get(i).getName());
            }

            final CharSequence[] charSequenceItems = listItems.toArray(new CharSequence[listItems.size()]);
            rangebar.setTickBottomLabels(charSequenceItems);

            rangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
                @Override
                public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {

                    minRange = Integer.parseInt(leftPinValue);
                    maxRange = Integer.parseInt(rightPinValue);

                }

                @Override
                public void onTouchStarted(RangeBar rangeBar) {

                }

                @Override
                public void onTouchEnded(RangeBar rangeBar) {

                }
            });
        } else {

        }


    }

    private void getData() {
        String filterName = getIntent().getStringExtra("filterName");
        Gson gson = new Gson();
        homeAllDetails = gson.fromJson(getIntent().getStringExtra("homeAllDetails"), HomeAllDetails.class);


        if (filterName == null || filterName.equalsIgnoreCase("")) {
            return;
        }

        if (homeAllDetails == null) {
            return;
        }
        switch (filterName) {
            case "All":
                showAllFilters();
                break;

            case "Sort":
                showFilterSort();
                break;


            case "Price Range":
                showPriceRange();
                break;

            case "Max Delivery Fees":
                showDeliveryFees();
                break;


            case "Dietary":
                showDietry();
                break;


            default:
                showAllFilters();
                break;

        }


    }

    private void showPriceRange() {
        llDietory.setVisibility(View.GONE);
        llPriceRange.setVisibility(View.VISIBLE);
        llFreeDelivery.setVisibility(View.GONE);
        llSort.setVisibility(View.GONE);
    }


    private void showDeliveryFees() {
        llDietory.setVisibility(View.GONE);
        llPriceRange.setVisibility(View.GONE);
        llFreeDelivery.setVisibility(View.VISIBLE);
        llSort.setVisibility(View.GONE);
    }

    private void showDietry() {
        llDietory.setVisibility(View.VISIBLE);
        llPriceRange.setVisibility(View.GONE);
        llFreeDelivery.setVisibility(View.GONE);
        llSort.setVisibility(View.GONE);
    }

    private void showFilterSort() {
        llDietory.setVisibility(View.GONE);
        llPriceRange.setVisibility(View.GONE);
        llFreeDelivery.setVisibility(View.GONE);
        llSort.setVisibility(View.VISIBLE);
    }

    private void showAllFilters() {
        llDietory.setVisibility(View.VISIBLE);
        llPriceRange.setVisibility(View.VISIBLE);
        llFreeDelivery.setVisibility(View.VISIBLE);
        llSort.setVisibility(View.VISIBLE);
    }

    private void setUpDietaryRv() {


        if (homeAllDetails.getFilterList().get(3) != null) {

            dietry.setText(homeAllDetails.getFilterList().get(3).getName());
            adapterDietaryFilter = new AdapterDietory(this, homeAllDetails.getFilterList().get(3).getFilterDetailData(), new AdapterDietory.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    switch (view.getId()) {
                        case R.id.llBg:
                            //filterBody.setDietary(homeAllDetails.getFilterList().get(3).getFilterDetailData().get(position).getName());
                            break;
                    }
                }
            });
            dietaryRv.setLayoutManager(linearLayoutManagerHor);
            dietaryRv.setAdapter(adapterDietaryFilter);
        } else {

        }


    }

    private void setUpSortRv() {


        if (homeAllDetails.getFilterList().get(0) != null) {
            sort.setText(homeAllDetails.getFilterList().get(0).getName());

            adapterSortFilter = new AdapterSortFilter(this, homeAllDetails.getFilterList().get(0).getFilterDetailData(), new AdapterSortFilter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {

                    switch (view.getId()) {
                        case R.id.llBg:
                            sortId = homeAllDetails.getFilterList().get(0).getFilterDetailData().get(position).getName();
                            filterBody.setSortId(sortId);

                            break;
                    }
                }
            });
            sortRv.setLayoutManager(linearLayoutManager);
            sortRv.setAdapter(adapterSortFilter);
        } else {

        }

    }

    private void initViews() {
        sortRv = findViewById(R.id.sortRv);
        dietaryRv = findViewById(R.id.dietaryRv);
        closeBtn = findViewById(R.id.closeBtn);
        btnFilter = findViewById(R.id.btnFilter);
        llSort = findViewById(R.id.llSort);
        llDietory = findViewById(R.id.llDietory);
        llFreeDelivery = findViewById(R.id.llFreeDelivery);
        llPriceRange = findViewById(R.id.llPriceRange);
        rangebar = findViewById(R.id.rangebar1);

        //Language
        headingFilter = findViewById(R.id.headingFilter);
        sort = findViewById(R.id.sort);
        priceRange = findViewById(R.id.priceRange);
        checkBox = findViewById(R.id.checkBox);
        dietry = findViewById(R.id.dietry);
        filter = findViewById(R.id.filter);

        headingFilter.setText(masterPojo.getFilter());
        sort.setText(masterPojo.getSort());
        priceRange.setText(masterPojo.getPriceRange());
        checkBox.setText(masterPojo.getDeliveryFee());
        dietry.setText(masterPojo.getDietary());
        filter.setText(masterPojo.getFilter());


        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    filterBody.setFreeDelivery(true);
                } else {
                    filterBody.setFreeDelivery(false);
                }
            }
        });

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                    Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                    return;
                }
                filterBody.setToPrice(maxRange);
                filterBody.setFromPrice(minRange);

                if (adapterDietaryFilter.getFilterDietryList() == null || adapterDietaryFilter.getFilterDietryList().size() == 0) {
                    filterBody.setDietary(null);
                } else {
                    StringBuilder filterDietry = new StringBuilder();
                    for (int i = 0; i < adapterDietaryFilter.getFilterDietryList().size(); i++) {
                        if (i == 0) {
                            filterDietry.append(adapterDietaryFilter.getFilterDietryList().get(i).getName());
                        } else {
                            filterDietry.append("," + adapterDietaryFilter.getFilterDietryList().get(i).getName());
                        }
                    }
                    filterBody.setDietary(filterDietry.toString());
                }

                Intent intent = getIntent();
                intent.putExtra("filterKey", (Serializable) filterBody);
                setResult(1, intent);
                finish();
            }
        });
    }


}