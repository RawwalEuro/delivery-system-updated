package com.eurosoft.customerapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Adapters.AdapterCheckOutProducts;
import com.eurosoft.customerapp.Fragments.AddCardFragment;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.LocationModel;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Order;
import com.eurosoft.customerapp.Model.OrderDetails;
import com.eurosoft.customerapp.Model.OrderPlaced;
import com.eurosoft.customerapp.Model.PlaceOrder;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.eurosoft.customerapp.services.OrderStatusService;
import com.fxn.stash.Stash;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.rilixtech.widget.countrycodepicker.Country;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;
import com.stripe.android.ApiResultCallback;
import com.stripe.android.PaymentIntentResult;
import com.stripe.android.Stripe;
import com.stripe.android.model.PaymentIntent;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

public class ActivityCheckOut extends AppCompatActivity implements AddCardFragment.BottomSheetListener, View.OnClickListener, OnMapReadyCallback {

    private RadioGroup radioGroupAddress;
    private RadioButton currentLoc, homeLoc, workLoc;
    private LocationModel homelocation;
    private LocationModel workLocation;
    private String address = "";
    private Stripe stripe;
    private ViewDialog alertDialoge;
    private TextView deliveryAddress, totalPrice, delivery, subtotal;
    private String transId = "";
    private AddCardFragment bottomSheet;
    private int payBy = 2;
    private RadioGroup radioGroup;
    private RelativeLayout editAddress;
    private static final int PLACES_ACTIVITY_REQUEST_CODE = 0;
    private RelativeLayout checkOutRl;
    private List<Product> productArrayListl = new ArrayList<>();
    private User currentUser;
    private RelativeLayout rlProgressBar;
    private RelativeLayout mainRl;
    private RecyclerView cartDetailsRv;
    private CircularProgressBar circularProgressBar;
    private AdapterCheckOutProducts adapterCheckOutProducts;
    private AppSettings appSettings;
    private LinearLayoutManager linearLayoutManagerHor;
    private ViewDialogAddNumber alertAddNumber;
    private CountryCodePicker ccp;
    private Order order;
    private ImageView bckBtn;
    private GoogleMap mMap;
    private NestedScrollView nestedScroll;
    private EditText addInstructions;
    private RelativeLayout rootView;
    private MasterPojo masterPojo;
    private RadioButton cash, card;
    private LinearLayout llInstructions;
    private TextView titleLocation, appName, deliveryAddressTitle, paymentMethod, orderSummary, deliveryHeading, placeOrder, totalHeading, totalFinal;
    private RadioGroup radioGroupTip;
    private RadioButton radioButtonEdit;
    private int tip = 0;
    private Double serviceCharges = 0.0;
    private ViewDialogTip tipDialogue;
    private Store storeObject;
    private RelativeLayout rlServicesCharges, rlTip;
    private TextView servicesChargesValue, servicesChargesTv, tipValueTv, tipTv;
    private CardView cardViewTip;
    private TextView remove;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        linearLayoutManagerHor = new LinearLayoutManager(this);
        initViews();
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        alertDialoge = new ViewDialog();
        tipDialogue = new ViewDialogTip();
        alertAddNumber = new ViewDialogAddNumber();
        stripe = new Stripe(
                getApplicationContext(),
                Objects.requireNonNull(Constants.Stripe_PK)
        );

        getCartDetails();
        setUpLocUser();
        if (productArrayListl.size() == 0) {
        } else {
            appName.setText(productArrayListl.get(0).getStoreName() + "");
        }
        storeObject = (Store) Stash.getObject(Constants.PERMENANT_STORE, Store.class);
        if (storeObject.isServiceChargesEnable()) {
            //  Toasty.success(getApplicationContext(), storeObject.getServiceCharges() + "", Toasty.LENGTH_SHORT).show();
            serviceCharges = storeObject.getServiceCharges();
            servicesChargesValue.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(serviceCharges) + "");
            rlServicesCharges.setVisibility(View.VISIBLE);
        } else {
            rlServicesCharges.setVisibility(View.GONE);
            serviceCharges = 0.0;
        }

        if (storeObject.getTip()) {
            tipValueTv.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat((double) tip) + "");
            cardViewTip.setVisibility(View.VISIBLE);
            rlTip.setVisibility(View.VISIBLE);
        } else {
            cardViewTip.setVisibility(View.GONE);
            rlTip.setVisibility(View.GONE);
        }

        remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                radioGroupTip.clearCheck();
                tip = 0;

                tipValueTv.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat((double) tip) + "");
                calculateTotal();
                checkTipifZero();
                radioButtonEdit.setText("Edit");

            }
        });
        calculateTotal();
    }


    private void initViews() {
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        radioGroupAddress = findViewById(R.id.radioGroupAddress);


        deliveryAddress = findViewById(R.id.deliveryAddress);
        checkOutRl = findViewById(R.id.checkOutRl);
        editAddress = findViewById(R.id.editAddress);
        radioGroup = findViewById(R.id.radioGroup);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        totalPrice = findViewById(R.id.total);
        totalPrice = findViewById(R.id.total);
        subtotal = findViewById(R.id.subtotal);
        delivery = findViewById(R.id.delivery);
        bckBtn = findViewById(R.id.bckBtn);
        cartDetailsRv = findViewById(R.id.cartDetailsRv);
        nestedScroll = findViewById(R.id.nestedScroll);
        rootView = findViewById(R.id.rootView);
        llInstructions = findViewById(R.id.llInstructions);
        nestedScroll = findViewById(R.id.nestedScroll);
        radioGroupTip = findViewById(R.id.radioGroupTip);
        radioButtonEdit = findViewById(R.id.radio3);
        remove = findViewById(R.id.remove);

        rlServicesCharges = findViewById(R.id.rlServicesCharges);
        servicesChargesValue = findViewById(R.id.servicesChargesValue);
        servicesChargesTv = findViewById(R.id.servicesCharges);
        cardViewTip = findViewById(R.id.cardViewTip);

        rlTip = findViewById(R.id.rlTip);
        tipValueTv = findViewById(R.id.tipValueTv);
        tipTv = findViewById(R.id.tipTv);


        //Language
        titleLocation = findViewById(R.id.titleLocation);
        appName = findViewById(R.id.appName);
        deliveryAddressTitle = findViewById(R.id.deliveryAddressTitle);
        currentLoc = findViewById(R.id.currentLoc);
        homeLoc = findViewById(R.id.homeLoc);
        workLoc = findViewById(R.id.workLoc);
        paymentMethod = findViewById(R.id.paymentMethod);
        cash = findViewById(R.id.cash);
        card = findViewById(R.id.card);
        orderSummary = findViewById(R.id.orderSummary);
        deliveryHeading = findViewById(R.id.deliveryHeading);
        totalHeading = findViewById(R.id.totalHeading);
        addInstructions = findViewById(R.id.addInstructions);
        totalFinal = findViewById(R.id.totalFinal);
        placeOrder = findViewById(R.id.placeOrder);

        titleLocation.setText(masterPojo.getCheckOut());
        appName.setText(masterPojo.getAppName());
        deliveryAddressTitle.setText(masterPojo.getDeliveryAddress());
        currentLoc.setText(masterPojo.getUseMyLocation());
        homeLoc.setText(masterPojo.getHome());
        workLoc.setText(masterPojo.getWork());
        paymentMethod.setText(masterPojo.getPaymentMethod());
        cash.setText(masterPojo.getPayByCash());
        card.setText(masterPojo.getPayByCard());
        orderSummary.setText(masterPojo.getOrderSummary());
        deliveryHeading.setText(masterPojo.getDeliveryFee());
        totalHeading.setText(masterPojo.getSubTotal());
        addInstructions.setHint(masterPojo.getAddInstructions());
        totalHeading.setText(masterPojo.getTotal());
        placeOrder.setText(masterPojo.getPlaceOrder());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        homeLoc.setOnClickListener(this);
        radioGroupAddress.setOnClickListener(this);
        checkOutRl.setOnClickListener(this);
        editAddress.setOnClickListener(this);
        bckBtn.setOnClickListener(this);

        llInstructions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showKeyboard(addInstructions, getApplicationContext());

                nestedScroll.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nestedScroll.fullScroll(ScrollView.FOCUS_DOWN);
                        addInstructions.requestFocus();
                    }
                }, 200);
            }
        });


        radioButtonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tipDialogue.showDialog(ActivityCheckOut.this);
            }
        });

        radioGroupTip.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio0:
                        tip = 2;
                        radioButtonEdit.setText("Edit");
                        tipValueTv.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat((double) tip) + "");
                        calculateTotal();
                        checkTipifZero();
                        break;


                    case R.id.radio1:

                        tip = 3;
                        radioButtonEdit.setText("Edit");
                        tipValueTv.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat((double) tip) + "");
                        calculateTotal();
                        checkTipifZero();
                        break;


                    case R.id.radio2:
                        tip = 4;
                        radioButtonEdit.setText("Edit");
                        tipValueTv.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat((double) tip) + "");
                        calculateTotal();
                        checkTipifZero();
                        break;


                    case R.id.radio3:
                        //tipDialogue.showDialog(ActivityCheckOut.this);
                        break;

                }
            }
        });


    }

    private void checkTipifZero() {
        if (tip == 0) {
            remove.setVisibility(View.GONE);
        } else {
            remove.setVisibility(View.VISIBLE);
        }
    }

    public static void showKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }


    private void setUpRecylerCheckOut() {
        adapterCheckOutProducts = new AdapterCheckOutProducts(this, productArrayListl, appSettings);
        cartDetailsRv.setLayoutManager(linearLayoutManagerHor);
        cartDetailsRv.setAdapter(adapterCheckOutProducts);
        calculateTotal();
    }

    @Override
    public void onButtonClicked(String text) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.checkOutRl:

                if (productArrayListl.size() == 0 || productArrayListl == null) {
                    Toast.makeText(getApplicationContext(), masterPojo.getCartEmpty(), LENGTH_LONG).show();
                    return;
                }

                Double calculatedTotal = calculateTotalD();
                Double fullTotal = calculatedTotal + tip + serviceCharges;
                if (fullTotal < storeObject.getMinimumOrderLimit()) {
                    Toasty.error(getApplicationContext(), "Total amount is less than store minimum limit", Toasty.LENGTH_SHORT).show();
                    return;
                }

                if (payBy == 1) {

                    if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                        progressVisiblityGone();
                        Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                        return;
                    }

                    callApiPlaceOrder();

                } else {
                    bottomSheet = new AddCardFragment(this, appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(calculateTotalD() + tip + serviceCharges) + "", masterPojo, new AddCardFragment.BottomSheetListener() {
                        @Override
                        public void onButtonClicked(String text) {

                            Toast.makeText(getApplicationContext(), text, LENGTH_LONG).show();
                            progressVisiblityVisible();
                            bottomSheet.dismiss();
                            //callApiPlaceOrder();
                        }
                    });
                    bottomSheet.show(getSupportFragmentManager(), "addCardFragment");
                }
                break;


            case R.id.editAddress:
                Intent i = new Intent(ActivityCheckOut.this, PlacePickerActivity.class);
                startActivityForResult(i, PLACES_ACTIVITY_REQUEST_CODE);
                break;


            case R.id.bckBtn:
                finish();
                break;
        }
    }


    private void setUpLocUser() {

        homelocation = (LocationModel) Stash.getObject(Constants.USER_HOME, LocationModel.class);
        workLocation = (LocationModel) Stash.getObject(Constants.USER_WORK, LocationModel.class);


        if (homelocation == null || homelocation.getPlaceName().isEmpty() || homelocation.getPlaceName().equalsIgnoreCase("")) {
            homeLoc.setVisibility(View.GONE);
        } else {
            homeLoc.setVisibility(View.VISIBLE);
        }


        if (workLocation == null || workLocation.getPlaceName().isEmpty() || workLocation.getPlaceName().equalsIgnoreCase("")) {
            workLoc.setVisibility(View.GONE);
        } else {
            workLoc.setVisibility(View.VISIBLE);
        }


        if (Stash.getString(Constants.USER_CURRENT_ADDRESS) == null || Stash.getString(Constants.USER_CURRENT_ADDRESS).equalsIgnoreCase("")) {
            currentLoc.setText(masterPojo.getSelectDeliveryAddress());
        } else {
            currentLoc.setText(Stash.getString(Constants.USER_CURRENT_ADDRESS));

        }


        radioGroupAddress.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.currentLoc:
                        address = deliveryAddress.getText().toString();
                        editAddress.setEnabled(true);
                        break;


                    case R.id.deliveryAddress:

                        Intent in = new Intent(ActivityCheckOut.this, PlacePickerActivity.class);
                        startActivityForResult(in, PLACES_ACTIVITY_REQUEST_CODE);
                        address = deliveryAddress.getText().toString();

                        break;


                    case R.id.workLoc:
                        editAddress.setEnabled(false);
                        LocationModel locationModel = (LocationModel) Stash.getObject(Constants.USER_WORK, LocationModel.class);
                        address = locationModel.getPlaceName();

                        setUpPin(locationModel.getLatitude(), locationModel.getLongitude());

                        break;


                    case R.id.homeLoc:
                        editAddress.setEnabled(false);
                        LocationModel locationWork = (LocationModel) Stash.getObject(Constants.USER_HOME, LocationModel.class);
                        address = locationWork.getPlaceName();
                        setUpPin(locationWork.getLatitude(), locationWork.getLongitude());
                        break;

                }
            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.cash:
                        payBy = 1;
                        break;


                    case R.id.card:
                        payBy = 2;
                        break;


                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACES_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                LocationModel locationModel = (LocationModel) data.getExtras().getSerializable("keyName");
                alertDialoge.showDialog(ActivityCheckOut.this, locationModel.getPlaceName());
                Stash.put(Constants.USER_LAT, locationModel.getLatitude());
                Stash.put(Constants.USER_LONG, locationModel.getLongitude());
                Log.e("AR-longitude", Stash.getString(Constants.USER_LONG));
                Log.e("AR-latitude", Stash.getString(Constants.USER_LAT));
            }
        }

        stripe.onPaymentResult(requestCode, data, new ActivityCheckOut.PaymentResultCallback(ActivityCheckOut.this));
    }

    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        mMap = googleMap;


        if (Stash.getString(Constants.USER_LAT) == null || Stash.getString(Constants.USER_LAT).equalsIgnoreCase("") && Stash.getString(Constants.USER_LONG) == null || Stash.getString(Constants.USER_LONG).equalsIgnoreCase("")) {
            return;
        } else {
            try {// Same
                String lat = (Stash.getString(Constants.USER_LAT)); // Same
                String longitude = (Stash.getString(Constants.USER_LONG)); // Same
                Double latitde = Double.parseDouble(lat);
                Double longitudes = Double.parseDouble(longitude);

                setUpPin(latitde, longitudes);
            } catch (NumberFormatException e) {
                // p did not contain a valid double
            }


        }

    }

    private void setUpPin(Double lat, Double longitude) {
        if (mMap == null) {
            return;
        }


        if (lat == null || longitude == null) {
            return;
        }
        mMap.clear();
        LatLng selectedLoc = new LatLng(lat, longitude);

        mMap.addMarker(new
                MarkerOptions().position(selectedLoc).title(masterPojo.getDeliveryAddress()));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, longitude), 16.0f));
    }


    private class PaymentResultCallback implements ApiResultCallback<PaymentIntentResult> {
        private WeakReference<Activity> activityRef;

        PaymentResultCallback(Activity fragment) {
            activityRef = new WeakReference<>(fragment);
        }


        @Override
        public void onSuccess(PaymentIntentResult result) {
            final Activity activity = activityRef.get();
            if (activity == null) {
                return;
            }


            PaymentIntent paymentIntent = result.getIntent();
            PaymentIntent.Status status = paymentIntent.getStatus();
            if (status == PaymentIntent.Status.Succeeded) {
                // Payment completed successfully
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                String finalResult = gson.toJson(paymentIntent);
                try {

                    JSONObject jsonObject = new JSONObject(finalResult);
                    // JSONObject subObj = jsonObject.getJSONObject("paymentMethod");
                    transId = jsonObject.getString("id");

                } catch (Exception e) {

                }
                bottomSheet.dismiss();

                callApiPlaceOrder();

            } else if (status == PaymentIntent.Status.RequiresPaymentMethod) {
                // Payment failed
                bottomSheet.dismiss();


            }
        }

        @Override
        public void onError(Exception e) {
            final Activity activity = activityRef.get();
            if (activity == null) {
                return;
            }

            // Payment request failed – allow retrying using the same payment method
        }
    }


    public class ViewDialog {

        public void showDialog(Activity activity, String placeName) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_add_street);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView selectedLoc = (TextView) dialog.findViewById(R.id.selectedLoc);
            selectedLoc.setVisibility(View.GONE);

            Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btnLeave);
            EditText addStreetEt = (EditText) dialog.findViewById(R.id.addStreet);
            TextView fetchedAddress = (TextView) dialog.findViewById(R.id.fetchedAddress);


            Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btnSave);
            dialogBtn_cancel.setText(masterPojo.getSkip());
            dialogBtn_okay.setText(masterPojo.getSave());
            fetchedAddress.setText(masterPojo.getAdddoorno());

            dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentLoc.setText(placeName);
                    address = placeName;
                    dialog.dismiss();
                }
            });

            dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();

                    if (addStreetEt.getText().toString().equalsIgnoreCase("")) {
                        Toasty.error(getApplicationContext(), masterPojo.getAddStreet(), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        currentLoc.setText(placeName + " " + masterPojo.getDoorNo() + addStreetEt.getText().toString());
                        address = placeName;
                        dialog.dismiss();
                    }

                }
            });

            dialog.show();
        }
    }


    public class ViewDialogTip {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_add_tip);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));


            Button btnAdd = (Button) dialog.findViewById(R.id.btnAdd);
            EditText addTipEdit = (EditText) dialog.findViewById(R.id.addTipEdit);
            TextView currency = (TextView) dialog.findViewById(R.id.currency);
            currency.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "");
            addTipEdit.setText(tip + "");

            Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btnSave);
            btnAdd.setText(masterPojo.getDone());

            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (addTipEdit.getText().toString().trim().equalsIgnoreCase("")) {
                        Toasty.warning(getApplicationContext(), "Add tip", Toasty.LENGTH_SHORT).show();
                        return;
                    }

                    if (addTipEdit.getText().toString().equalsIgnoreCase("0")) {
                        Toasty.warning(getApplicationContext(), "Tip can't be zero", Toasty.LENGTH_SHORT).show();
                        return;
                    } else {
                        tip = Integer.parseInt((addTipEdit.getText().toString().trim()));
                        radioButtonEdit.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat((double) tip) + "");
                        tipValueTv.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat((double) tip) + "");
                        calculateTotal();
                        dialog.dismiss();
                        checkTipifZero();
                    }
                }
            });


            dialog.show();
        }
    }


    private void getCartDetails() {


        productArrayListl = Stash.getArrayList("productList", Product.class);
        if (productArrayListl == null || productArrayListl.size() == 0) {
            return;
        } else {
            setUpRecylerCheckOut();
        }
    }


    public void calculateTotal() {
        int i = 0;
        double total = 0;

        if (productArrayListl.isEmpty() || productArrayListl == null) {
            totalPrice.setText("");
            delivery.setText("");
            subtotal.setText("");
            return;
        }

        while (i < productArrayListl.size()) {
            total = total + Double.valueOf(productArrayListl.get(i).getPrice());
            i++;
        }

        if (productArrayListl.get(0).getStoreDeliveryFee() != null) {
            delivery.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(productArrayListl.get(0).getStoreDeliveryFee()));
            double totalValue = total + productArrayListl.get(0).getStoreDeliveryFee() + tip + serviceCharges;
            totalPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(total));
            subtotal.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(totalValue));
        } else {
            delivery.setText("--");
            totalPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(total) + tip + serviceCharges);
            subtotal.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(total));
        }
    }

    public double calculateTotalD() {
        int i = 0;
        double total = 0;
        if (productArrayListl.isEmpty() || productArrayListl == null) {
            return 0.0;
        }

        while (i < productArrayListl.size()) {
            total = total + Double.valueOf(productArrayListl.get(i).getPrice());
            i++;
        }
        return total + productArrayListl.get(0).getStoreDeliveryFee();

    }


    private String getCurrentTime() {
        String currentTime = new SimpleDateFormat("HH:mm", Locale.getDefault()).format(new Date());
        return currentTime;
    }

    private void callApiPlaceOrder() {

       /* if (rlProgressBar.getVisibility() == View.VISIBLE) {
            return;
        }*/


        checkOutRl.setEnabled(false);

        if (productArrayListl.size() == 0 || productArrayListl == null) {
            Toast.makeText(getApplicationContext(), masterPojo.getCartEmpty(), LENGTH_LONG).show();
            return;
        }

        Collections.sort(productArrayListl, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                Product p1 = (Product) o1;
                Product p2 = (Product) o2;
                return p1.getStoreId() - (p2.getStoreId());
            }
        });

        if (currentUser.getCustomerPhoneNumber().equalsIgnoreCase("") || currentUser.getCustomerPhoneNumber() == null
                || currentUser.getCustomerPhoneNumber().equalsIgnoreCase("123")) {
            alertAddNumber.showDialog(ActivityCheckOut.this);
            return;
        }


        String latitude;
        String longitude;
        progressVisiblityVisible();

        ArrayList<OrderDetails> listOrderDetails = new ArrayList<>();

        for (int i = 0; i < productArrayListl.size(); i++) {
            Product product = productArrayListl.get(i);
            OrderDetails orderDetails = new OrderDetails(product.getId(), product.getGetActucalPrice(), product.getQuantity(), product.getGetActucalPrice() * product.getQuantity(), product.getProductName(), product.getSpecialInstructions(), product.getOrderDetailData());
            listOrderDetails.add(orderDetails);
        }

        latitude = Stash.getString(Constants.USER_LAT);
        longitude = Stash.getString(Constants.USER_LONG);


        Log.e("latitude", Stash.getString(Constants.USER_LAT));
        Log.e("longitude", Stash.getString(Constants.USER_LONG));


        if (latitude == null || longitude == null) {
            Toast.makeText(getApplicationContext(), "Please add delivery address", LENGTH_LONG).show();
            return;
        }

        if (latitude.equalsIgnoreCase("") || longitude.equalsIgnoreCase("")) {
            Toast.makeText(getApplicationContext(), "Please add delivery address", LENGTH_LONG).show();
            return;
        }


        Double calculatedTotal = calculateTotalD();

        if (currentLoc.isChecked()) {
            address = currentLoc.getText().toString();
        }

        Double deliveryFeees = productArrayListl.get(0).getStoreDeliveryFee();
        if (deliveryFeees == null) {
            deliveryFeees = 00.00;
        } else {
            deliveryFeees = productArrayListl.get(0).getStoreDeliveryFee();

        }

        String deliveryTime = productArrayListl.get(0).getStoreDeliveryEstimatedTime();

        if (deliveryTime == null || deliveryTime.equalsIgnoreCase("")) {
            deliveryTime = "30 mins";
        } else {
            deliveryTime = productArrayListl.get(0).getStoreDeliveryEstimatedTime();
        }


        PlaceOrder placeOrder = new PlaceOrder(address + "", "7", "address",
                Double.parseDouble(latitude), Double.parseDouble(longitude), getCurrentTime(), currentUser.getId(), parseDateToddMMyyyy(Constants.DATE_FORMAT_WITHOUT), calculatedTotal - deliveryFeees, deliveryFeees, calculatedTotal + tip + serviceCharges, transId,
                payBy, 15, listOrderDetails, deliveryTime, addInstructions.getText().toString(), 2, productArrayListl.get(0).getStoreId(), Double.valueOf(serviceCharges), Double.valueOf(tip));


        if (placeOrder.getOrderDetailData().size() == 0 || placeOrder.getOrderDetailData().isEmpty()) {
            progressVisiblityGone();
            return;
        }

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<OrderPlaced> call = apiService.placeOrder(placeOrder, currentUser.getToken());
        call.enqueue(new Callback<OrderPlaced>() {
            @Override
            public void onResponse(Call<OrderPlaced> call, Response<OrderPlaced> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    checkOutRl.setEnabled(true);
                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    checkOutRl.setEnabled(true);
                    progressVisiblityGone();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    checkOutRl.setEnabled(true);

                    String fetchedResponse = response.body().getData();
                    String[] splited_text = fetchedResponse.split(",");
                    String orderNum = splited_text[0];
                    String orderId = splited_text[1];

                    Stash.put(Constants.ORDER_ID, orderId);
                    order = new Order(calculatedTotal, calculatedTotal,
                            0.0, orderId, String.valueOf(currentUser.getId()), Stash.getInt(Constants.PRODUCT_STORE_ID),
                            Stash.getString(Constants.PRODUCT_STORE_NAME), "Cash", getProductNames(productArrayListl), parseDateToddMMyyyy(Constants.DATE_FORMAT), 1, productArrayListl,
                            Integer.parseInt(orderId), transId, payBy);
                    addOrderAsync(order);
                    progressVisiblityGone();
                    Toasty.success(getApplicationContext(), masterPojo.getOrderPlacedSucess(), LENGTH_LONG).show();
                    productArrayListl.clear();

                    //getOrdersByUserId();
                    Stash.put("frgToLoad", "0");
                    Intent intent = new Intent(ActivityCheckOut.this, ActivityOrderPlaced.class);
                    intent.putExtra(Constants.ORDER_ID, orderNum);
                    startActivity(intent);
                    startService();
                    delAllProductAsync(currentUser.getId());
                }
            }


            @Override
            public void onFailure(Call<OrderPlaced> call, Throwable t) {
                //checkOutRl.setEnabled(true);
                progressVisiblityGone();
                Toast.makeText(ActivityCheckOut.this, t.getMessage(), LENGTH_LONG).show();
            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
        rlProgressBar.bringToFront();
        disableEnableControls(false, mainRl);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
        disableEnableControls(true, mainRl);

    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    private void startService() {
        Intent serviceIntent = new Intent(this, OrderStatusService.class);
        serviceIntent.setAction(Constants.STARTFOREGROUND_ACTION);
        Stash.clear(Constants.ORDER_STATUS);
        serviceIntent.putExtra("inputExtra", "We are looking for your updates");
        ContextCompat.startForegroundService(this, serviceIntent);
    }


    private void delAllProductAsync(int userId) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .delAllUsersById(userId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                calculateTotal();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    private void addOrderAsync(Order order) {
        class AddOrder extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .orderDao()
                        .insert(order);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        AddOrder st = new AddOrder();
        st.execute();
    }

    public class ViewDialogAddNumber {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.enter_user_number);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            EditText addNumber = (EditText) dialog.findViewById(R.id.addNumber);

            addNumber.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btnSave);
            ccp = (CountryCodePicker) dialog.findViewById(R.id.ccp);

            ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                @Override
                public void onCountrySelected(Country selectedCountry) {

                }
            });

            dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();

                    if (addNumber.getText().toString().equalsIgnoreCase("")) {
                        Toasty.error(getApplicationContext(), masterPojo.getNumberNotAdded(), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        callApiUpdateProfile(currentUser,
                                addNumber.getText().toString());
                        dialog.dismiss();

                    }
                }
            });

            dialog.show();
        }
    }

    private void callApiUpdateProfile(User currentUser, String addNumber) {
        progressVisiblityVisible();

        if (addNumber.startsWith("0")) {
            String phoneRemoved = addNumber.substring(1);
            currentUser.setCustomerPhoneNumber(phoneRemoved);
        } else {
            currentUser.setCustomerPhoneNumber(addNumber);
        }
        currentUser.setToken(currentUser.getToken());
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<User>> call = apiService.updateProfile(currentUser, currentUser.getToken());

        call.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {
                if (response.code() != 200 || response.body() == null) {
                    currentUser.setCustomerPhoneNumber("");
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    currentUser.setCustomerPhoneNumber("");

                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toasty.LENGTH_LONG).show();
                    progressVisiblityGone();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    Stash.put(Constants.USER, response.body().getData());
                    progressVisiblityGone();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {
                progressVisiblityGone();
                currentUser.setCustomerPhoneNumber("");
            }
        });

    }


    public static String parseDateToddMMyyyy(String format) {
        SimpleDateFormat outputFormat = new SimpleDateFormat(format);
        Date date = null;
        String str = null;

        try {
            date = Calendar.getInstance().getTime();
            str = outputFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return str;
    }

    private String getProductNames(List<Product> products) {
        String productNames = "";
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < products.size(); i++) {
            String nameProduct = products.get(i).getProductName() + " ";
            str.append(nameProduct);
        }
        productNames = str.toString();

        return productNames;
    }
}