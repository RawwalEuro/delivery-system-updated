package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetails extends ListItem {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("OrderMasterId")
    @Expose
    private Integer orderMasterId;
    @SerializedName("StoreId")
    @Expose
    private Integer storeId;
    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("Quantity")
    @Expose
    private Double quantity;
    @SerializedName("TotalPrice")
    @Expose
    private Double totalPrice;
    @SerializedName("specialInstructions")
    @Expose
    private String specialInstructions;
    @SerializedName("Status")
    @Expose
    private Object status;
    @SerializedName("OrderOptionDetailDataCount")
    @Expose
    private Integer orderOptionDetailDataCount;
    @SerializedName("OrderDetailOptions")
    @Expose
    private Object orderDetailOptions;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOrderMasterId() {
        return orderMasterId;
    }

    public void setOrderMasterId(Integer orderMasterId) {
        this.orderMasterId = orderMasterId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return (storeName == null || storeName.isEmpty() ? "" : storeName);
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Integer getOrderOptionDetailDataCount() {
        return orderOptionDetailDataCount;
    }

    public void setOrderOptionDetailDataCount(Integer orderOptionDetailDataCount) {
        this.orderOptionDetailDataCount = orderOptionDetailDataCount;
    }

    public Object getOrderDetailOptions() {
        return orderDetailOptions;
    }

    public void setOrderDetailOptions(Object orderDetailOptions) {
        this.orderDetailOptions = orderDetailOptions;
    }
}
