package com.eurosoft.customerapp;

import static com.facebook.FacebookSdk.getApplicationContext;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Adapters.AdapterProductVariations;
import com.eurosoft.customerapp.Fragments.AddProductDialog;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.OptionDetailedData;
import com.eurosoft.customerapp.Model.OptionsList;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.ProductVariant;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Model.ValidationProductVariant;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailFragment extends BottomSheetDialogFragment implements View.OnClickListener {

    private BottomSheetListener bottomSheetListener;
    private TextView addToCart;
    private ImageView decrement, increment;
    private EditText quantityValue;
    private int quantity = 1;
    private Product product;
    private TextView productPrice, productName, productDesc;
    private User currentUser;
    private RelativeLayout rlProgressBar;
    private LinearLayout mainRl;
    private CircularProgressBar circularProgressBar;
    private int i = 0;
    private ImageView headerImage;
    double setAddedValues = 0.0;
    double finalPrice = 0.0;
    private RelativeLayout headerRl;

    private FrameLayout frameCart;
    private TextView cart_badge;
    private List<Product> productsFromDB = new ArrayList<>();
    private ImageView bckIcon;
    private EditText specialInstructionEt;
    private AppSettings appSettings;
    private RecyclerView variationRv;
    private AdapterProductVariations adapterProductVariations;
    private List<OptionsList> optionsLists = new ArrayList<>();
    private ArrayList<ValidationProductVariant> lstProductVariant = new ArrayList<>();
    private ViewDialogClearCart alertDialogCart;
    private TextView special_guide, guide_text;
    private MasterPojo masterPojo;
    private double priceToAddSubtract = 0.0;
    private int maxCount = 20;
    private LinearLayout llEdit;
    private NestedScrollView scrollView;
    Context context;
    private ImageView favouriteIv,unFavourite;


    public ProductDetailFragment(Context context, Product product, BottomSheetListener bottomSheetListener) {
        this.context = context;
        this.product = product;
        this.bottomSheetListener = bottomSheetListener;
    }


    public ProductDetailFragment() {

    }

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_product_details, container, false);
        Stash.init(getContext().getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        alertDialogCart = new ViewDialogClearCart();
        init(v);
        frameCart.setVisibility(View.INVISIBLE);
        getDataProduct();
        getCartDetails();
        return v;
    }

    public interface BottomSheetListener {
        void onButtonClicked(boolean value);
    }


    @Override
    public void onResume() {
        super.onResume();
        getCartDetails();
    }

    private void getDataProduct() {
        /*Gson gson = new Gson();
        product = gson.fromJson(getActivity().getIntent().getStringExtra("myjson"), Product.class);*/
        if (product == null) {
            Log.e("Return", "Return");
            return;
        } else {


            Log.e("Comimng", "Comimng");
            setData(product);

            if (!AppNetWorkStatus.getInstance(getActivity().getApplicationContext()).isOnline()) {
                progressVisiblityGone();
                Toasty.warning(getActivity().getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                return;
            }

            if (product.getProductOptions() != 0) {
                callApiGetProductOptions();
            } else {

            }
        }
        finalPrice = product.getPrice();
    }

    private void callApiGetProductOptions() {
        progressVisiblityVisible();

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<OptionsList>> call = apiService.getProductVariations(product.getId(), currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<OptionsList>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<OptionsList>> call, Response<CustomWebResponse<OptionsList>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getActivity().getApplicationContext(), response.body().getMessage());
                    progressVisiblityGone();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {

                    if (response.body().getData().size() == 0) {
                        progressVisiblityGone();
                        return;
                    } else {
                        progressVisiblityGone();
                        for (int i = 0; i < response.body().getData().size(); i++) {
                            if (response.body().getData().get(i).getTypeName().equalsIgnoreCase("RadioButton")) {
                                OptionsList optionsList = response.body().getData().get(i);
                                optionsList.setType(OptionsList.RADIO_TYPE);
                                optionsLists.add(optionsList);
                                ValidationProductVariant validationProductVariant = new ValidationProductVariant(response.body().getData().get(i).getName(), new ArrayList<>(), OptionsList.RADIO_TYPE, response.body().getData().get(i).getIsOptionRequried(),2);
                                lstProductVariant.add(validationProductVariant);
                            } else {
                                OptionsList optionsList = response.body().getData().get(i);
                                optionsList.setType(OptionsList.CHECK_BOX_TYPE);
                                optionsLists.add(optionsList);
                                ValidationProductVariant validationProductVariant = new ValidationProductVariant(response.body().getData().get(i).getName(), new ArrayList<>(), OptionsList.CHECK_BOX_TYPE, response.body().getData().get(i).getIsOptionRequried(),2);
                                lstProductVariant.add(validationProductVariant);
                            }

                        }


                        adapterProductVariations = new AdapterProductVariations(getActivity().getApplicationContext(), lstProductVariant, optionsLists, appSettings, 50.0, new AdapterProductVariations.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position, Double price, boolean isAdd, boolean isUnChecked, String operator, Double priceToDeduct, boolean isRadioDeduct,List<OptionsList> optionsLists,int parentPostion,int selectedCheckBoxCount) {
                                switch (view.getId()) {
                                    case R.id.rlParent:

                                        break;


                                    case R.id.radioBtn:

                                        setPricing(price, true, isUnChecked, operator, priceToDeduct, isRadioDeduct);
                                        break;


                                    case R.id.checkBox:

                                       /* if (optionsLists.get(parentPostion).getSelectionLimit() == selectedCheckBoxCount) {
                                            Toasty.warning(context, "Can't add more than " + optionsLists.get(parentPostion).getSelectionLimit(), Toasty.LENGTH_SHORT).show();
                                            return;
                                        }*/
                                        setPricing(price, false, isUnChecked, operator, priceToDeduct, isRadioDeduct);
                                        break;

                                }
                            }
                        });

                        variationRv.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
                        variationRv.setAdapter(adapterProductVariations);

                    }
                }
            }





            @Override
            public void onFailure(Call<CustomWebResponse<OptionsList>> call, Throwable t) {
                //  Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                progressVisiblityGone();
            }
        });

    }


    private void setData(Product product) {

        quantity = product.getMinimumQty();

        productName.setText(product.getProductName() + "");
        productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(product.getPrice()) + "" + "");
        productDesc.setText(product.getProductDesc() + "");
        quantityValue.setText(String.valueOf(quantity));

        maxCount = product.getQuantity();


        Log.e("WIshList",product.getIsWishList() + "");


        if(product.getIsWishList()){
            unFavourite.setVisibility(View.VISIBLE);
            favouriteIv.setVisibility(View.GONE);
        } else {
            favouriteIv.setVisibility(View.VISIBLE);
            unFavourite.setVisibility(View.GONE);
        }

        try {
            Picasso.get().load(product.getBaseUrl() + product.getId() + "/" + product.getPrimaryImage()).placeholder(R.color.grey).into(headerImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init(View view) {

        Stash.init(getContext());
        increment = view.findViewById(R.id.increment);
        decrement = view.findViewById(R.id.decrement);
        quantityValue = view.findViewById(R.id.value);
        productName = view.findViewById(R.id.name);
        productDesc = view.findViewById(R.id.desc);
        productPrice = view.findViewById(R.id.price);
        headerImage = view.findViewById(R.id.headerImage);
        headerRl = view.findViewById(R.id.headerRl);
        bckIcon = view.findViewById(R.id.bckIcon);
        variationRv = view.findViewById(R.id.variationRv);
        llEdit = view.findViewById(R.id.llEdit);
        scrollView = view.findViewById(R.id.scrollView);
        favouriteIv = view.findViewById(R.id.favouriteIv);
        unFavourite = view.findViewById(R.id.unFavourite);

        //Language
        specialInstructionEt = view.findViewById(R.id.specialInstructionEt);
        special_guide = view.findViewById(R.id.special_guide);
        guide_text = view.findViewById(R.id.guide_text);
        addToCart = view.findViewById(R.id.addToCart);


        specialInstructionEt.setHint(" " + masterPojo.getAddSomething());
        guide_text.setText(masterPojo.getAvoidAnything());
        special_guide.setText(masterPojo.getSpecialInstructions());
        addToCart.setText(masterPojo.getAddToCart());

        rlProgressBar = view.findViewById(R.id.rlProgressBar);
        circularProgressBar = view.findViewById(R.id.circularProgressBar);
        mainRl = view.findViewById(R.id.mainRl);

        frameCart = view.findViewById(R.id.frameCart);
        cart_badge = view.findViewById(R.id.cart_badge);
        frameCart.setOnClickListener(this);


        addToCart.setOnClickListener(this);
        increment.setOnClickListener(this);
        decrement.setOnClickListener(this);
        quantityValue.setOnClickListener(this);
        addToCart.setOnClickListener(this);
        bckIcon.setOnClickListener(this);

        headerRl.bringToFront();


        favouriteIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callApiAddToFavourties(currentUser.getId(), product.getId(), true);

            }
        });

        unFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callApiAddToFavourties(currentUser.getId(), product.getId(), false);
            }
        });
        variationRv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState); 

            }
        });


        llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showKeyboard(specialInstructionEt, getActivity().getApplicationContext());

                scrollView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
                        specialInstructionEt.requestFocus();
                    }
                }, 500);


            }
        });


    }

    private void callApiAddToFavourties(int customerId, int productId, boolean isLike) {
        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterPojo>> call = apiService.addToWishlist(customerId, productId, isLike,currentUser.getToken());

        call.enqueue(new Callback<WebResponse<MasterPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterPojo>> call, Response<WebResponse<MasterPojo>> response) {
                if (response.code() != 200 || response.body() == null) {

                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {

                    if(isLike){
                        addWishListUi();
                    } else {
                        removeWishListUI();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<MasterPojo>> call, Throwable t) {
                Toasty.warning(getApplicationContext(), "Something went wrong", Toasty.LENGTH_SHORT).show();
            }
        });

    }

    private void removeWishListUI() {
        favouriteIv.setVisibility(View.VISIBLE);
        unFavourite.setVisibility(View.GONE);
    }

    private void addWishListUi() {
        unFavourite.setVisibility(View.VISIBLE);
        favouriteIv.setVisibility(View.GONE);
    }

    public static void showKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addToCart:
                // addProductToCart();
                if (!appSettings.getWebsiteSettingData().isMultipleResturentOrderAllow()) {
                    if (productsFromDB.size() == 0) {
                        addProductToCart();
                    } else if (productsFromDB != null || productsFromDB.size() == 0) {
                        if (product.getStoreId() == productsFromDB.get(0).getStoreId()) {
                            addProductToCart();
                        } else {
                            alertDialogCart.showDialog(getActivity());
                            return;
                        }
                    }
                } else {
                    addProductToCart();
                }
//                Intent intent = new Intent(ProductDetailsActivity.this, ActivityReviewPayment.class);
//                startActivity(intent);
                break;

            case R.id.increment:

                incrementValues();

                break;


            case R.id.decrement:

                try {
                    setDecrement();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;

            case R.id.bckIcon:
                bottomSheetListener.onButtonClicked(true);
                break;

            case R.id.frameCart:
                startActivity(new Intent(getActivity(), ActivityReviewPayment.class));
                break;

        }
    }

    public void setPricing(Double pricing, boolean isReplace, boolean isUnChecked, String checkOperation, double priceToDeuct, boolean isRadioDeduct) {

        if (checkOperation.equalsIgnoreCase("1")) {
            product.setPrice(pricing);
            productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(product.getPrice() + setAddedValues) + "");
            finalPrice = product.getPrice() + setAddedValues;
            return;
        } else if (isRadioDeduct) {
            setAddedValues = setAddedValues - priceToDeuct;
            setAddedValues = setAddedValues + pricing;
            finalPrice = product.getPrice() + setAddedValues;

            productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(finalPrice) + "");
            return;
        } else if (checkOperation.equalsIgnoreCase("0") && !isUnChecked) {
            setAddedValues = setAddedValues + pricing;
            productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(product.getPrice() + setAddedValues) + "");
            finalPrice = product.getPrice() + setAddedValues;
            return;
        } else if (isUnChecked && !isRadioDeduct) {
            setAddedValues = setAddedValues - pricing;
            productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(product.getPrice() + setAddedValues) + "");
            finalPrice = product.getPrice() + setAddedValues;
            return;
        } else {

        }
    }

    private void incrementValues() {
        if (quantity >= product.getQuantity()) {
            Toast.makeText(getActivity().getApplicationContext(), product.getQuantity() + " is the max quantity for this product", Toast.LENGTH_LONG).show();
            return;
        }
        quantity++;
        quantityValue.setText(String.valueOf(quantity));

    }

    private void setDecrement() {

        /*if (quantity >= product.getMinimumQty()) {
            //progressVisiblityGone();
            Toast.makeText(getActivity().getApplicationContext(), product.getMinimumQty() + " is the minimum quantity for this product", Toast.LENGTH_LONG).show();
            quantity = product.getMinimumQty();
            return;
        }*/

        if (quantity == 0) {
            //progressVisiblityGone();
            quantity = 1;
            return;
        }
        quantityValue.setText(String.valueOf(quantity--));
    }

    private void addProductToCart() {
        if (quantityValue.getText().toString().equalsIgnoreCase("") || quantityValue.getText().toString() == null) {
            Toast.makeText(getActivity().getApplicationContext(), masterPojo.getAddQuantity(), Toast.LENGTH_SHORT).show();
            return;
        }


        quantity = Integer.parseInt(quantityValue.getText().toString());


        if (quantity == 0) {
            Toast.makeText(getActivity().getApplicationContext(), masterPojo.getAddQuantity(), Toast.LENGTH_SHORT).show();
            return;
        }
        if (product == null) {
            Toast.makeText(getActivity().getApplicationContext(), masterPojo.getCantAddProduct(), Toast.LENGTH_SHORT).show();
            return;
        }


        if (product.getProductOptions() != 0) {

            try {
                if (adapterProductVariations.getLstOptionDetailData().size() != 0) {
                    adapterProductVariations.getLstOptionDetailData().clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (adapterProductVariations.getValidationProductVariant() != null) {
                for (int j = 0; j < adapterProductVariations.getValidationProductVariant().size(); j++) {

                    if (adapterProductVariations.getValidationProductVariant().get(j).isRequired()) {

                        if (adapterProductVariations.getValidationProductVariant().get(j) == null || adapterProductVariations.getValidationProductVariant().get(j).getLstDataObject().size() == 0) {
                            Toast.makeText(getActivity().getApplicationContext(), adapterProductVariations.getValidationProductVariant().get(j).getName() + " " + masterPojo.getNotSelected(), Toast.LENGTH_SHORT).show();
                            return;
                        } else {

                            if (adapterProductVariations.getValidationProductVariant().get(j).getType() == OptionsList.RADIO_TYPE) {
                                ProductVariant productVariant = adapterProductVariations.getValidationProductVariant().get(j).getLstDataObject().get(0);
                                OptionDetailedData optionDetailedData = new OptionDetailedData(productVariant.getOptionMasterId(),
                                        productVariant.getOptionId(), productVariant.getOptionValue(), productVariant.getOptionPrice(), productVariant.getProductId());
                                adapterProductVariations.getLstOptionDetailData().add(optionDetailedData);
                            } else if (adapterProductVariations.getValidationProductVariant().get(j).getType() == OptionsList.CHECK_BOX_TYPE) {

                                for (int k = 0; k < adapterProductVariations.getValidationProductVariant().get(j).getLstDataObject().size(); k++) {
                                    ProductVariant productVariant = adapterProductVariations.getValidationProductVariant().get(j).getLstDataObject().get(k);
                                    OptionDetailedData optionDetailedData = new OptionDetailedData(productVariant.getOptionMasterId(), productVariant.getOptionId(), productVariant.getOptionValue(), productVariant.getOptionPrice(), productVariant.getProductId());
                                    adapterProductVariations.getLstOptionDetailData().add(optionDetailedData);
                                }
                            }
                        }
                    } else {
                        if (adapterProductVariations.getValidationProductVariant().get(j).getType() == OptionsList.RADIO_TYPE) {
                            ProductVariant productVariant = adapterProductVariations.getValidationProductVariant().get(j).getLstDataObject().get(0);
                            OptionDetailedData optionDetailedData = new OptionDetailedData(productVariant.getOptionMasterId(),
                                    productVariant.getOptionId(), productVariant.getOptionValue(), productVariant.getOptionPrice(), productVariant.getProductId());
                            adapterProductVariations.getLstOptionDetailData().add(optionDetailedData);
                        } else if (adapterProductVariations.getValidationProductVariant().get(j).getType() == OptionsList.CHECK_BOX_TYPE) {
                            for (int k = 0; k < adapterProductVariations.getValidationProductVariant().get(j).getLstDataObject().size(); k++) {
                                ProductVariant productVariant = adapterProductVariations.getValidationProductVariant().get(j).getLstDataObject().get(k);
                                OptionDetailedData optionDetailedData = new OptionDetailedData(productVariant.getOptionMasterId(),
                                        productVariant.getOptionId(), productVariant.getOptionValue(), productVariant.getOptionPrice(), productVariant.getProductId());
                                adapterProductVariations.getLstOptionDetailData().add(optionDetailedData);
                            }
                        }
                    }
                }
                productAddedToCart();
            } else {
                productAddedToCart();
            }

        } else {
            productAddedToCart();
        }


    }

    private void productAddedToCart() {
        for (int i = 0; i < productsFromDB.size(); i++) {
            for (int y = i + 1; y <= productsFromDB.size(); y++) {
                boolean check = productsFromDB.get(i).getId().equals(product.getId());

                Product product = productsFromDB.get(i);

                if (check) {
                    if (product.getProductOptions() != 0) {
                        product.setName(product.getProductName());
                        product.setMaxQuantity(maxCount);
                        product.setMinimumQty(product.getMinimumQty());
                        product.setQuantity(quantity);
                        product.setActucalPrice(finalPrice);
                        product.setPrice(finalPrice * quantity);
                        product.setStoreName(product.getStoreName());
                        product.setStoreId(product.getStoreId());
                        product.setSpecialInstructions(specialInstructionEt.getText().toString() + " ");
                        if (product.getProductOptions() != 0) {
                            product.setOrderDetailData(adapterProductVariations.getLstOptionDetailData());
                        } else {
                            product.setOrderDetailData(new ArrayList<>());
                        }
                        updateProductAsync(product);
                        bottomSheetListener.onButtonClicked(true);
                        return;
                    }
                }
            }
        }


        Product productToAdd = product;
        productToAdd.setName(product.getProductName());
        productToAdd.setQuantity(quantity);
        productToAdd.setMaxQuantity(maxCount);
        productToAdd.setMinimumQty(product.getMinimumQty());
        productToAdd.setActucalPrice(finalPrice);
        productToAdd.setStoreId(product.getStoreId());
        productToAdd.setPrice(finalPrice * quantity);
        productToAdd.setStoreName(product.getStoreName());
        productToAdd.setUserid(currentUser.getId());
        product.setSpecialInstructions(specialInstructionEt.getText().toString() + " ");
        if (product.getProductOptions() != 0) {
            product.setOrderDetailData(adapterProductVariations.getLstOptionDetailData());
        } else {
            product.setOrderDetailData(new ArrayList<>());
        }
//            product=productToAdd;
        addProductAsync(productToAdd);
        bottomSheetListener.onButtonClicked(true);
        //finish();
    }


    private void updateProductAsync(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
                        .productDao()
                        .update(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                progressVisiblityGone();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    private void getCartDetails() {
        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {

            @Override
            protected List<Product> doInBackground(Void... voids) {
                productsFromDB = DatabaseClient
                        .getInstance(getActivity().getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productsFromDB;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productsFromDB == null || productsFromDB.size() == 0) {
                    frameCart.setVisibility(View.INVISIBLE);
                } else {

                    frameCart.setVisibility(View.VISIBLE);
                    cart_badge.setText(productsFromDB.size() + "");
                }
            }
        }

        GetCartItem gt = new GetCartItem();
        gt.execute();

    }

    private void addProductAsync(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
                        .productDao()
                        .insert(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //progressVisiblityGone();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    private void deleteProduct(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
                        .productDao()
                        .delete(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //progressVisiblityGone();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    public class ViewDialogClearCart {
        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_same_restruant);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            Button btnClear = (Button) dialog.findViewById(R.id.btnClear);
            Button cancel = (Button) dialog.findViewById(R.id.cancel);
            btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delAllProductAsync(currentUser.getId());
                    dialog.dismiss();
                    Toasty.success(getActivity().getApplicationContext(), masterPojo.getCartClearSucess(), Toasty.LENGTH_LONG).show();
                    getCartDetails();
                    addProductToCart();
                }
            });


            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    private void delAllProductAsync(int userId) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
                        .productDao()
                        .delAllUsersById(userId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }
        AddProduct st = new AddProduct();
        st.execute();
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }

    private boolean isStatusSame(List<OptionDetailedData> orderDetailData) {
        for (int i = 0; i < productsFromDB.size(); i++) {
            if (productsFromDB.get(i).getOrderDetailData().equals(product.getOrderDetailData())) {
                return true;
            }
        }
        return false;
    }


}

