package com.eurosoft.customerapp.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.OrderModel;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AdapterPastActiveOrders extends RecyclerView.Adapter<AdapterPastActiveOrders.ViewHolder> {

    private Context ctx;
    List<OrderModel> orderArrayList;
    private AdapterCartItems.OnItemClickListener mOnItemClickListener;
    private AppSettings appSettings;
    private MasterPojo masterPojo;


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
    

    public AdapterPastActiveOrders(Context ctx, List<OrderModel> orderArrayList, AppSettings appSetting, MasterPojo masterPojo, AdapterCartItems.OnItemClickListener onItemClickListener) {
        this.ctx = ctx;
        this.orderArrayList = orderArrayList;
        this.mOnItemClickListener = onItemClickListener;
        this.appSettings = appSetting;
        this.masterPojo = masterPojo;
    }

    public AdapterPastActiveOrders(Context ctx, List<OrderModel> orderArrayList) {
        this.ctx = ctx;
        this.orderArrayList = orderArrayList;

    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.adapter_past_orders, parent, false);
        ViewHolder vh = new ViewHolder(v);


        vh.cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, vh.getAdapterPosition());
            }
        });


        vh.details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, vh.getAdapterPosition());
            }
        });


        vh.returnOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, vh.getAdapterPosition());
            }
        });

        return vh;


    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

        OrderModel order = orderArrayList.get(position);

        holder.orderId.setText(order.getOrderNo());
        holder.orderIdHeading.setText(masterPojo.getOrderId());

        if (order.getOrderStatusId() == OrderModel.Pending) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.red)));
            holder.cancel.setText(masterPojo.getCancel());
            holder.status.setText(masterPojo.getPending());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.VISIBLE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));
        }

        if (order.getOrderStatusId() == OrderModel.Waiting) {
            holder.status.setText(masterPojo.getPending());
            holder.cancel.setClickable(true);
            holder.cancel.setFocusable(true);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.red)));
            holder.cancel.setText(masterPojo.getCancel());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.GONE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));

        }


        if (order.getOrderStatusId() == OrderModel.Cancelled) {
            Log.e("ID",order.getOrderStatusId()+"");
            Log.e("Status",masterPojo.getCancelled()+"");
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.red)));
            holder.cancel.setText(masterPojo.getCancelled());
            holder.status.setText(masterPojo.getCancelled());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.GONE);
            holder.dot.setColorFilter(ContextCompat.getColor(ctx, R.color.red));

        }


        if (order.getOrderStatusId() == OrderModel.Accept) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.accepted)));
            holder.cancel.setText(masterPojo.getAccepted());
            holder.status.setText(masterPojo.getActive());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.GONE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));

        }


          if (order.getOrderStatusId() == OrderModel.AcceptPending) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.accepted)));
            holder.cancel.setText(masterPojo.getAccepted());
            holder.status.setText(masterPojo.getActive());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.GONE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));

        }

        if (order.getOrderStatusId() == OrderModel.Completed) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.grey)));
            holder.cancel.setText(masterPojo.getCompleted());
            holder.status.setText(masterPojo.getCompleted());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.VISIBLE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));
        }


        if (order.getOrderStatusId() == OrderModel.GotoCustomer) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(true);
            holder.cancel.setFocusable(true);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.onroute)));
            holder.cancel.setText(masterPojo.getTrack());
            holder.status.setText(masterPojo.getOnRoute());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.GONE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));
        }


        if (order.getOrderStatusId() == OrderModel.Arrive) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.black)));
            holder.cancel.setText(masterPojo.getArrived());
            holder.status.setText(masterPojo.getActive());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.VISIBLE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));
        }

        if (order.getOrderStatusId() == OrderModel.Deliver) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.delivered)));
            holder.cancel.setText(masterPojo.getDelivered());
            holder.status.setText(masterPojo.getDelivered());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.VISIBLE);
            holder.returnOrderBtn.setVisibility(View.VISIBLE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));
        }

        if (order.getOrderStatusId() == OrderModel.Collect) {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(false);
            holder.cancel.setFocusable(false);
            holder.cancel.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.collected)));
            holder.cancel.setText(masterPojo.getCollected());
            holder.status.setText(masterPojo.getCollected());
            holder.cancel.setTextColor(ctx.getResources().getColor(R.color.white));
            holder.rlLayoutActive.setVisibility(View.VISIBLE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));
        } else {
            holder.cancel.setVisibility(View.VISIBLE);
            holder.cancel.setClickable(true);
            holder.rlLayoutActive.setVisibility(View.VISIBLE);
            holder.dot.setBackgroundTintList(ColorStateList.valueOf(ctx.getResources().getColor(R.color.main_green_color)));
        }

        if (order.getProductList() == null || order.getProductList().isEmpty()) {
        } else {
            holder.storeName.setText(order.getProductList().get(0).getStoreName() + "");
        }


        int count = order.getProductList().size();

        if (count <= 0) {
            if (order.getProductList() == null || order.getProductList().isEmpty()) {

            } else {
                holder.deals.setText(order.getProductList().get(0).getStoreName() + ",");
            }
        } else {
            if (order.getProductList() == null || order.getProductList().isEmpty()) {
            } else {
                holder.deals.setText(order.getProductList().get(0).getProductName() + " and " + count + " more" + "");
            }
        }
        holder.date.setText(order.getOrderDate() + " " + order.getOrderTime());
        holder.total.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(order.getTotalAmount()));
    }

    private String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }


    public String convertDateFormat(String date) throws ParseException {


        //current date format
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss");
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy");
        String finalDate = dateFormat2.format(objDate);


        return finalDate;
    }


    public String convertTime(String date) throws ParseException {

        //current date format
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm:ss");
        Date objDate = dateFormat.parse(date);
        //Expected date format
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("hh:mm");
        String finalDate = dateFormat2.format(objDate);

        return finalDate;
    }

    @Override
    public int getItemCount() {
        return orderArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView storeName, cancel, details, date, deals, total, status, orderId,orderIdHeading,returnOrderBtn;
        private LinearLayout rlLayoutActive;
        private ImageView dot;


        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            storeName = itemView.findViewById(R.id.storeName);
            cancel = itemView.findViewById(R.id.cancel);
            details = itemView.findViewById(R.id.details);
            deals = itemView.findViewById(R.id.deals);
            date = itemView.findViewById(R.id.date);
            total = itemView.findViewById(R.id.total);
            status = itemView.findViewById(R.id.status);
            orderId = itemView.findViewById(R.id.orderId);
            rlLayoutActive = itemView.findViewById(R.id.rlLayoutActive);
            dot = itemView.findViewById(R.id.dot);
            returnOrderBtn = itemView.findViewById(R.id.returnOrderBtn);
            orderIdHeading = itemView.findViewById(R.id.orderIdHeading);

        }
    }
}
