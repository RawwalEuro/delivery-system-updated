package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Filter implements Serializable {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("TypeName")
    @Expose
    private String typeName;
    @SerializedName("SortOrder")
    @Expose
    private Integer sortOrder;
    @SerializedName("FilterDetailData")
    @Expose
    private List<FilterDetailDatum> filterDetailData = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public List<FilterDetailDatum> getFilterDetailData() {
        return filterDetailData;
    }

    public void setFilterDetailData(List<FilterDetailDatum> filterDetailData) {
        this.filterDetailData = filterDetailData;
    }

}
