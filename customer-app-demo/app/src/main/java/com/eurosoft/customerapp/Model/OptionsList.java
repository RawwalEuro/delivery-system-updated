package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OptionsList {


    public static final int RADIO_TYPE=0;
    public static final int CHECK_BOX_TYPE=1;

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("OptionsId")
    @Expose
    private Integer optionsId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Type")
    @Expose
    private Integer type;
    @SerializedName("TypeName")
    @Expose
    private String typeName;
    @SerializedName("IsOptionRequried")
    @Expose
    private Boolean isOptionRequried;


    @SerializedName("OptionDesc")
    @Expose
    private String OptionDesc;


    @SerializedName("SelectionLimit")
    @Expose
    private int SelectionLimit;
    @SerializedName("OptionDetailData")
    @Expose
    private List<OptionDetailedData> optionDetailData = null;


    public String getOptionDesc() {
        return OptionDesc;
    }

    public void setOptionDesc(String optionDesc) {
        OptionDesc = optionDesc;
    }

    public int getSelectionLimit() {
        if(SelectionLimit == 0){
            return 2;
        }
        return SelectionLimit;
    }

    public void setSelectionLimit(int selectionLimit) {
        SelectionLimit = selectionLimit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getOptionsId() {
        return optionsId;
    }

    public void setOptionsId(Integer optionsId) {
        this.optionsId = optionsId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public Boolean getIsOptionRequried() {
        return isOptionRequried;
    }

    public void setIsOptionRequried(Boolean isOptionRequried) {
        this.isOptionRequried = isOptionRequried;
    }

    public List<OptionDetailedData> getOptionDetailData() {
        return optionDetailData;
    }

    public void setOptionDetailData(List<OptionDetailedData> optionDetailData) {
        this.optionDetailData = optionDetailData;
    }


}
