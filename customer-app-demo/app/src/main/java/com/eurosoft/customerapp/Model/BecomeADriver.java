package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BecomeADriver implements Serializable {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("vehNo")
    @Expose
    private String vehNo;
    @SerializedName("vehColor")
    @Expose
    private String vehColor;
    @SerializedName("vehOwner")
    @Expose
    private String vehOwner;
    @SerializedName("vehMake")
    @Expose
    private String vehMake;
    @SerializedName("vehModel")
    @Expose
    private String vehModel;
    @SerializedName("vehType")
    @Expose
    private Integer vehType;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("requestedDate")
    @Expose
    private String requestedDate;





    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getVehNo() {
        return vehNo;
    }

    public void setVehNo(String vehNo) {
        this.vehNo = vehNo;
    }

    public String getVehColor() {
        return vehColor;
    }

    public void setVehColor(String vehColor) {
        this.vehColor = vehColor;
    }

    public String getVehOwner() {
        return vehOwner;
    }

    public void setVehOwner(String vehOwner) {
        this.vehOwner = vehOwner;
    }

    public String getVehMake() {
        return vehMake;
    }

    public void setVehMake(String vehMake) {
        this.vehMake = vehMake;
    }

    public String getVehModel() {
        return vehModel;
    }

    public void setVehModel(String vehModel) {
        this.vehModel = vehModel;
    }

    public Integer getVehType() {
        return vehType;
    }

    public void setVehType(Integer vehType) {
        this.vehType = vehType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

}
