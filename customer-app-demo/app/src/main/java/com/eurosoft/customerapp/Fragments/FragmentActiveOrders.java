package com.eurosoft.customerapp.Fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.eurosoft.customerapp.ActivityOrderDetails;
import com.eurosoft.customerapp.ActivityPastOrders;
import com.eurosoft.customerapp.ActivityTrackingOrder;
import com.eurosoft.customerapp.Adapters.AdapterCartItems;
import com.eurosoft.customerapp.Adapters.AdapterPastActiveOrders;
import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Data;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.OrderModel;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.eurosoft.customerapp.services.OrderStatusService;
import com.fxn.stash.Stash;
import com.google.gson.Gson;

import java.util.Collections;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class FragmentActiveOrders extends Fragment implements View.OnClickListener {

    private User currentUser;
    private List<OrderModel> orderList;
    private RecyclerView rvOrders;
    private LinearLayoutManager layoutManager;
    private AdapterPastActiveOrders mAdapter;
    private LinearLayout rlLayout;
    private RelativeLayout rlNoData;
    private AppSettings appSettings;
    private ActivityPastOrders activityPastOrders;
    private TextView noOrderFound;
    private MasterPojo masterPojo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_active, container, false);
        Stash.init(getActivity());
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
//        getOrdersByUserId();
        init(view);
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(broadcastReceiver, new IntentFilter(OrderStatusService.receiver));
        getOrdersByUserId();
    }


    @Override
    public void onStop() {
        super.onStop();
        try {
            if (broadcastReceiver == null)
                return;
            getActivity().unregisterReceiver(broadcastReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            getOrdersByUserId();

        }
    };

    private void init(View view) {
        rvOrders = (RecyclerView) view.findViewById(R.id.rvOrders);
        rlLayout = (LinearLayout) view.findViewById(R.id.mainRl);
        rlNoData = (RelativeLayout) view.findViewById(R.id.rlNoData);
        noOrderFound = (TextView) view.findViewById(R.id.noOrderFound);


        noOrderFound.setText(masterPojo.getNoOrdersFound());

        final SwipeRefreshLayout pullToRefresh = view.findViewById(R.id.pullToRefresh);

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getOrdersByUserId(); // your code
                pullToRefresh.setRefreshing(false);
            }
        });
    }

    private void getOrdersByUserId() {

        if (!AppNetWorkStatus.getInstance(getActivity()).isOnline()) {
            ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
            Toasty.warning(getActivity(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<OrderModel>> call = apiService.getAllActivieNPastOrders(currentUser.getId(), 0,currentUser.getToken());
       // ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingStarted();
        call.enqueue(new Callback<CustomWebResponse<OrderModel>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<OrderModel>> call, Response<CustomWebResponse<OrderModel>> response) {
                if (response.code() != 200 || response.body() == null) {
                    //((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                    return;
                }
                if (response.body().getSuccess()) {

                    if(response.body().getData() == null){
                        //((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                        rlNoData.setVisibility(View.VISIBLE);
                       // ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                        rvOrders.setVisibility(View.GONE);
                        return;
                    }

                    if (response.body().getData().size() == 0) {
                        rlNoData.setVisibility(View.VISIBLE);
                       // ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                        rvOrders.setVisibility(View.GONE);
                    } else {
                        orderList = response.body().getData();
                        rlNoData.setVisibility(View.GONE);
                       // ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                        setUpRecycler();
                        rvOrders.setVisibility(VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<OrderModel>> call, Throwable t) {

                // ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
            }
        });

    }


    private void setUpRecycler() {
        rvOrders.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        rvOrders.setLayoutManager(layoutManager);

        Collections.reverse(orderList);

        mAdapter = new AdapterPastActiveOrders(getActivity(), orderList, appSettings,masterPojo, new AdapterCartItems.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.cancel:


                        if (orderList.get(position).getOrderStatusId() == OrderModel.GotoCustomer) {

                            Intent intent = new Intent(getActivity(), ActivityTrackingOrder.class);
                            Gson gson = new Gson();
                            String myJson = gson.toJson(orderList.get(position));
                            intent.putExtra("myjson", myJson);

                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getActivity().startActivity(intent);
                            return;
                        } else if (orderList.get(position).getOrderStatusId() == OrderModel.Accept) {
                        }

                        else if (orderList.get(position).getOrderStatusId() == OrderModel.Arrive) {
                        }
                        else if (orderList.get(position).getOrderStatusId() == OrderModel.Completed) {
                        }

                        else if (orderList.get(position).getOrderStatusId() == OrderModel.Collect) {
                        }


                        else {

                            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(masterPojo.getSureWantToCancel())
                                    .setConfirmText(masterPojo.getYes())

                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog
                                                    .setTitleText(masterPojo.getCanceled())
                                                    .setContentText(null)
                                                    .setConfirmClickListener(null)
                                                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                            sDialog.dismissWithAnimation();


                                            callApiCancelOrder(orderList.get(position), orderList.get(position).getId().toString(), 1, position);
                                        }
                                    })

                                    .setCancelText("No")
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismissWithAnimation();
                                        }
                                    })
                                    .show();

                        }

                        break;

                    case R.id.details:
                        Intent intent = new Intent(getActivity(), ActivityOrderDetails.class);
                        Gson gson = new Gson();
                        String myJson = gson.toJson(orderList.get(position));
                        intent.putExtra("myjson", myJson);

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getActivity().startActivity(intent);


                        break;

                }
            }
        });
        rvOrders.setAdapter(mAdapter);
    }

    private void checkIfDataExixts() {
        if (orderList == null || orderList.size() == 0) {
            rlNoData.setVisibility(View.VISIBLE);
            return;
        } else {
            rlLayout.setVisibility(View.VISIBLE);

        }
    }

    private void callApiCancelOrder(OrderModel order, String orderId, int status, int position) {

        if (!AppNetWorkStatus.getInstance(getActivity()).isOnline()) {
            ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
            Toasty.warning(getActivity(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }

       // ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingStarted();
        mAdapter.notifyDataSetChanged();
        orderList.remove(position);
        mAdapter.notifyItemRemoved(position);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<Data>> call = apiService.cancelOrderByOrderId(orderId, 3,currentUser.getToken());
        Toasty.success(getActivity(), masterPojo.getOrderCancelledSucess(), Toasty.LENGTH_LONG).show();

        call.enqueue(new Callback<WebResponse<Data>>() {
            @Override
            public void onResponse(Call<WebResponse<Data>> call, Response<WebResponse<Data>> response) {
                if (response.code() != 200 || response.body() == null) {
                    ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                    return;
                }
                if (!response.body().getSuccess()) {
                    Toasty.error(getActivity(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();
                    ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    //addProductAsync(order, position);
                    ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
                    getOrdersByUserId();

                }
            }

            @Override
            public void onFailure(Call<WebResponse<Data>> call, Throwable t) {
                //Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
                ((ActivityPastOrders)getActivity()).getActivityPastOrders().loadingFinished();
            }
        });


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }
    }
}
