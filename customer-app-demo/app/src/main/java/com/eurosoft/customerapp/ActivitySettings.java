package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Adapters.AdapterSettings;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Languages;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySettings extends AppCompatActivity implements View.OnClickListener {

    private MasterPojo masterPojo;
    private AppSettings appSettings;
    private List<Languages> languagesList = new ArrayList<>();
    private RelativeLayout rlProgressBar;
    private RelativeLayout mainRl;
    private CircularProgressBar circularProgressBar;
    private ImageView bckPress;
    private RecyclerView rlChangeLanguage;
    private TextView settingHeader;
    private AdapterSettings adapterSettings;
    private LinearLayoutManager layoutManager;
    private int selectedIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Stash.init(this);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        selectedIndex = (int) Stash.getInt(Constants.SELECTED_LANG_POS);
        languagesList = appSettings.getLanguages();
        initViews();
    }

    private void initViews() {
        mainRl = findViewById(R.id.mainRl);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        bckPress = findViewById(R.id.bckPress);
        rlChangeLanguage = findViewById(R.id.rlChangeLanguage);
        settingHeader = findViewById(R.id.settingHeader);

        settingHeader.setText(masterPojo.getSettings());

        bckPress.setOnClickListener(this);


        adapterSettings = new AdapterSettings(this, languagesList, selectedIndex, new AdapterSettings.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {

                    case R.id.rlBg:


                        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                            return;
                        }

                        callApiSetLangauge(languagesList.get(position).getCulture());
                        break;

                }
            }
        });

        rlChangeLanguage.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(ActivitySettings.this);
        rlChangeLanguage.setLayoutManager(layoutManager);
        rlChangeLanguage.setAdapter(adapterSettings);
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
//        mainRl.setEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bckPress:
                finish();
                break;

        }
    }

    private void callApiSetLangauge(String code) {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterPojo>> call = apiService.setLanguage(code);

        call.enqueue(new Callback<WebResponse<MasterPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterPojo>> call, Response<WebResponse<MasterPojo>> response) {

                if (response.code() != 200 || !response.isSuccessful()) {
                    progressVisiblityGone();
                    return;
                }


                if (response.code() == 200 && response.isSuccessful()) {
                    try {
                        Stash.put(Constants.IS_LANGUAGE_SELECTED, true);
                        Stash.put(Constants.MASTER_POJO, response.body().getData());
                        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
                        progressVisiblityGone();
                        // setData(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onFailure(Call<WebResponse<MasterPojo>> call, Throwable t) {
                progressVisiblityGone();
            }
        });


    }
}