package com.eurosoft.customerapp.Utils.Footer;

import android.view.View;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.appbar.AppBarLayout;

public class FooterBarBehaviour extends CoordinatorLayout.Behavior<FooterBarLayout> {


  //This is called to determine which views this behavior depends on
  @Override
  public boolean layoutDependsOn(CoordinatorLayout parent,
                                 FooterBarLayout child,
                                 View dependency) {
    //We are watching changes in the AppBarLayout
    return dependency instanceof AppBarLayout;
  }

  //This is called for each change to a dependent view
  @Override
  public boolean onDependentViewChanged(CoordinatorLayout parent,
                                        FooterBarLayout child,
                                        View dependency) {
    int offset = -dependency.getTop();
    child.setTranslationY(offset);
    return true;
  }
}
