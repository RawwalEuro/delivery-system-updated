package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.FilterDetailDatum;
import com.eurosoft.customerapp.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterSortFilter extends RecyclerView.Adapter<AdapterSortFilter.ViewHolder> {


    private Context context;
    private List<FilterDetailDatum> filterDetailData;
    private OnItemClickListener mOnItemClickListener;
    private int row_index = -1;

    public AdapterSortFilter(Context ctx, List<FilterDetailDatum> filterDetailDatumArrayList, OnItemClickListener onItemClickListener) {
        context = ctx;
        filterDetailData = filterDetailDatumArrayList;
        mOnItemClickListener = onItemClickListener;
    }


    @Override
    public AdapterSortFilter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_filter_sort, parent, false);
        final AdapterSortFilter.ViewHolder viewHolder = new AdapterSortFilter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterSortFilter.ViewHolder holder, int position) {
        FilterDetailDatum filterDetailDatum = filterDetailData.get(position);
        holder.sortText.setText(filterDetailDatum.getName());
        Picasso.get().load(filterDetailDatum.getBaseUrl() + filterDetailDatum.getIcon()).placeholder(R.color.grey).into(holder.iconSort);

        holder.llBg.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition());
                row_index = position;
                notifyDataSetChanged();

            }
        });

        if (row_index == position) {
            holder.llBg.setBackgroundResource(R.drawable.drawable_curved_rectangle);
            holder.sortText.setTextColor(Color.parseColor("#000000"));
        } else {
            holder.llBg.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.sortText.setTextColor(Color.parseColor("#000000"));
        }
    }

    @Override
    public int getItemCount() {
        return filterDetailData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView sortText;
        private ImageView iconSort;
        private LinearLayout llBg;


        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            sortText = itemView.findViewById(R.id.sortText);
            iconSort = itemView.findViewById(R.id.iconSort);
            llBg = itemView.findViewById(R.id.llBg);

        }
    }


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}
