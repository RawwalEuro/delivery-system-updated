package com.eurosoft.customerapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;

import com.eurosoft.customerapp.Adapters.AdapterHomeCategories;
import com.eurosoft.customerapp.Adapters.AdapterHomeFilter;
import com.eurosoft.customerapp.Fragments.FragmentLeftMenu;
import com.eurosoft.customerapp.Interface.FilterListener;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Filter;
import com.eurosoft.customerapp.Model.FilterBody;
import com.eurosoft.customerapp.Model.HomeAllDetails;
import com.eurosoft.customerapp.Model.LocationModel;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Order;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.StoreList;
import com.eurosoft.customerapp.Model.StoreTagsData;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.eurosoft.customerapp.Utils.ServiceTools;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.eurosoft.customerapp.services.GoogleService;
import com.eurosoft.customerapp.services.OrderStatusService;
import com.facebook.FacebookSdk;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Adapters.AdapterHome;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.fxn.stash.Stash;


import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.hosseiniseyro.apprating.AppRatingDialog;
import com.hosseiniseyro.apprating.listener.RatingDialogListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.eurosoft.customerapp.services.GoogleService.str_receiver;
import static com.eurosoft.customerapp.services.OrderStatusService.receiver;

public class HomeActivity extends AppCompatActivity implements RatingDialogListener, FilterListener,FragmentLeftMenu.onCLoseDrawer {

    private CircularProgressBar progressCircle;
    private RelativeLayout rlHome;
    private RelativeLayout rlProgressBar;
    private RecyclerView homeRecycler;
    private AdapterHome adapterHome;
    private User currentUser;
    private TextView userName;
    private RelativeLayout navIcon;
    Geocoder geocoder;
    boolean boolean_permission;
    private static final int REQUEST_PERMISSIONS = 100;
    private HomeActivity mContext;
    private TextView locationCurrent;
    private static final int REQUEST_LOCATION = 99;
    private static final int PLACES_ACTIVITY_REQUEST_CODE = 0;
    private DrawerLayout mDrawer;
    private RelativeLayout slectLocation;
    private Order order;
    private Boolean isVisible = false;
    private AppSettings appSettings;
    private RecyclerView filterRecycler;
    private AdapterHomeFilter adapterHomeFilter;
    private AdapterHomeCategories adapterHomeCategories;
    ArrayList<Filter> stringsFilter = new ArrayList<>();
    ArrayList<StoreTagsData> storeTagsDataArrayList = new ArrayList<>();
    ArrayList<String> stringCatList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private LinearLayoutManager linearLayoutManagerCat;
    private RecyclerView catRecycler;
    private ImageView asapBtn;
    private int initialId = 0;
    private RelativeLayout rlNoData;
    private SwipeRefreshLayout pullToRefresh;
    private MasterPojo masterPojo;
    private TextView noStore;
    private HomeAllDetails homeAllDetails;
    public final static int REQ_CODE_FILTER = 1;
    private RelativeLayout cart;
    private List<Product> productsFromDB = new ArrayList<>();
    private TextView cart_badge;
    private boolean isClickedCatFirst = false;
    private ImageView filter;
    private CardView searchRL;
    private AppBarLayout appBar;
    private CoordinatorLayout coordinator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        linearLayoutManagerCat = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        FacebookSdk.sdkInitialize(getApplicationContext());
        init();


        askPermission();
        runProgressBar();
        getCurrentLat();
        getCartDetails();
        //checkIfActiveOrdersExist();

        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            disableProgressBar();
            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        } else {
            callApiHomeDetails();
        }


        asapBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
               /* new SingleDateAndTimePickerDialog.Builder(HomeActivity.this)
                        .bottomSheet()
                        .curved()
                        .mainColor(getColor(R.color.black))
                        .displayHours(true)
                        .displayMinutes(true)
                        .todayText("Now")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                // Retrieve the SingleDateAndTimePicker
                            }


                        })
                        .titleTextColor(getColor(R.color.black))
                        .title("SCHEDULE DELIVERY")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                            }
                        }).display();*/
            }
        });



        homeRecycler.setOnScrollListener(new RecyclerView.OnScrollListener() {
            int scrollDy = 0;
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                scrollDy += dy;
                if (!recyclerView.canScrollVertically(1) && dy > 0)
                {
                  //  collapseToolbar();
                    //scrolled to BOTTOM
                }else if (!recyclerView.canScrollVertically(-1) && dy < 0)
                {
                    //scrolled to TOP
                   // expandToolbar();
                }
            }
        });




        locationCurrent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, PlacePickerActivity.class);
                startActivityForResult(i, PLACES_ACTIVITY_REQUEST_CODE);
            }
        });

        slectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(HomeActivity.this, PlacePickerActivity.class);
                startActivityForResult(i, PLACES_ACTIVITY_REQUEST_CODE);
            }
        });

        navIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mDrawer.openDrawer(GravityCompat.START, true);

                /*Toast.makeText(getApplicationContext(), "Logged Out", Toast.LENGTH_LONG).show();
                Stash.clear(Constants.isLoggenIn);
                Stash.clear(Constants.USER);
                Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);*/
            }
        });
        mDrawer.setScrimColor(getResources().getColor(android.R.color.transparent));
        //setDrawerAnimation();

        getSupportFragmentManager().beginTransaction().replace(R.id.navigation_container, FragmentLeftMenu.newInstance()).commitAllowingStateLoss();
    }

    private void checkIfActiveOrdersExist() {
        if (Stash.getBoolean(Constants.IS_ACTIVE_ORDERS_PRESENT)) {
            try {
                Boolean isServiceRunning = ServiceTools.isServiceRunning(
                        HomeActivity.this,
                        OrderStatusService.class);

                if (!isServiceRunning) {
                    try {
                        Intent serviceIntent = new Intent(this, OrderStatusService.class);
                        serviceIntent.setAction(Constants.STARTFOREGROUND_ACTION);
                        Stash.clear(Constants.ORDER_STATUS);
                        serviceIntent.putExtra("inputExtra", "We are looking for your updates");
                        ContextCompat.startForegroundService(this, serviceIntent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    private void askPermission() {
        if (Build.VERSION.SDK_INT >= 23) {

            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getApplicationContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                android.app.AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
                builder.setTitle(masterPojo.getLocationPermission())
                        .setMessage(masterPojo.getManualLocationPermission())
                        .setPositiveButton(masterPojo.getoK(), new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
                                requestPermissions(new String[]{
                                                Manifest.permission.ACCESS_FINE_LOCATION,
                                                Manifest.permission.ACCESS_COARSE_LOCATION},
                                        REQUEST_LOCATION);
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }

        }
    }

    public void startService() {
        Intent serviceIntent = new Intent(this, OrderStatusService.class);
        serviceIntent.putExtra("inputExtra", "We are looking for your updates");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(this, serviceIntent);
        } else {
            startService(serviceIntent);
        }
    }


    private void getCurrentLat() {
        try {
            mContext = this;
            geocoder = new Geocoder(this, Locale.getDefault());
            fn_permission();
            Intent intent = new Intent(getApplicationContext(), GoogleService.class);
            startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void fn_permission() {
        if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION);
            ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION);
            ActivityCompat.requestPermissions(HomeActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    REQUEST_PERMISSIONS);

        } else {
            boolean_permission = true;
        }
    }


    private void getCartDetails() {
        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {
            @Override
            protected List<Product> doInBackground(Void... voids) {
                productsFromDB = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productsFromDB;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productsFromDB == null || productsFromDB.size() == 0) {
                    cart.setVisibility(View.INVISIBLE);
                } else {
                    cart.setVisibility(View.VISIBLE);
                    cart_badge.setText(productsFromDB.size() + "");
                }
            }
        }
        GetCartItem gt = new GetCartItem();
        gt.execute();
    }

    private void init() {
        mContext = this;
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        progressCircle = findViewById(R.id.circularProgressBar);
        rlHome = findViewById(R.id.homeLayout);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        homeRecycler = findViewById(R.id.homeRecycler);
        filterRecycler = findViewById(R.id.filterRecycler);
        catRecycler = findViewById(R.id.catRecycler);
        navIcon = findViewById(R.id.navIcon);
        locationCurrent = findViewById(R.id.locationCurrent);
        slectLocation = findViewById(R.id.slectLocation);
        mDrawer = findViewById(R.id.drawer_layout);
        asapBtn = findViewById(R.id.asapBtn);
        rlNoData = findViewById(R.id.rlNoData);
        pullToRefresh = findViewById(R.id.pullToRefresh);
        noStore = findViewById(R.id.noStore);
        cart = findViewById(R.id.cart);
        cart_badge = findViewById(R.id.cart_badge);
        filter = findViewById(R.id.filter);
        searchRL = findViewById(R.id.searchRL);
        appBar = findViewById(R.id.appBarlayout);
        coordinator = findViewById(R.id.coordinator);



        searchRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,ActivityMainSearch.class));
            }
        });

        filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stringsFilter == null || stringsFilter.size() == 0)
                    return;

                Intent intent = new Intent(getBaseContext(), ActivityFilter.class);
                Gson gson = new Gson();
                String homeAllDetailsObj = gson.toJson(homeAllDetails);
                intent.putExtra("filterName", stringsFilter.get(0).getName());
                intent.putExtra("homeAllDetails", homeAllDetailsObj);
                startActivityForResult(intent, REQ_CODE_FILTER);
            }
        });

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this, ActivityReviewPayment.class));
            }
        });

        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initialId = 0;
                if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()){
                    Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                } else {
                    callApiGetStoresByTagId(initialId);
                    pullToRefresh.setRefreshing(false);
                }

               /* if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                    disableProgressBar();
                    Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                    pullToRefresh.setRefreshing(false);
                    return;
                } else {
                    if (stringsFilter.size() != 0)
                        stringsFilter.clear();


                    callApiHomeDetails(); // your code
                    pullToRefresh.setRefreshing(false);

                }*/
            }
        });

        if (Stash.getString(Constants.USER_CURRENT_ADDRESS) == null || Stash.getString(Constants.USER_CURRENT_ADDRESS).equalsIgnoreCase("")) {
        } else {
            locationCurrent.setText(Stash.getString(Constants.USER_CURRENT_ADDRESS));
        }

        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            disableProgressBar();
            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }

        callApiAppSettings();


    }
    public void collapseToolbar() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBar.getLayoutParams();
        final AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        if (behavior != null) {
            ValueAnimator valueAnimator = ValueAnimator.ofInt();
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    behavior.setTopAndBottomOffset((Integer) animation.getAnimatedValue());
                    appBar.requestLayout();
                }
            });
            valueAnimator.setIntValues(0, -900);
            valueAnimator.setDuration(700);
            valueAnimator.start();
        }
    }

    public void expandToolbar(){
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBar.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        behavior = (AppBarLayout.Behavior) params.getBehavior();
        if(behavior!=null) {
            behavior.onNestedFling(coordinator, appBar, null, 0, -10000, false);
        }
    }

    private void setCatRecycler(ArrayList<StoreTagsData> storeTagsDataArrayList) {

        StoreTagsData storeTagsData = new StoreTagsData();
        storeTagsData.setId(0);
        storeTagsData.setName("All Stores");
        storeTagsData.setBaseUrl("http://onlinedeliverysystem.co.uk/EcommerceSystem/Content/Images/StoreTagIcon/");
        storeTagsData.setIcon("AllStoreTag.png");
        storeTagsData.setSortOrder(1);

        storeTagsDataArrayList.add(0, storeTagsData);


        adapterHomeCategories = new AdapterHomeCategories(this, storeTagsDataArrayList, new AdapterHomeCategories.OnItemClickListenerCat() {
            @Override
            public void onItemClick(View view, int position) {

                switch (view.getId()) {
                    case R.id.parentLL:

                        if (storeTagsDataArrayList == null || storeTagsDataArrayList.size() == 0) {
                            return;
                        }

                        initialId = storeTagsDataArrayList.get(position).getId();

                        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                            disableProgressBar();
                            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                            return;
                        }

                        isClickedCatFirst = true;
                        callApiGetStoresByTagId(initialId);

                        break;
                }
            }
        });
        catRecycler.setLayoutManager(linearLayoutManagerCat);
        catRecycler.setAdapter(adapterHomeCategories);
    }

    private void callApiGetStoresByTagId(int initialId) {
        runProgressBar();
        Store store = new Store(initialId);
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<StoreList>> call = apiService.getAllStoresByTag(store, currentUser.getToken());
        call.enqueue(new Callback<CustomWebResponse<StoreList>>() {
                         @Override
                         public void onResponse(Call<CustomWebResponse<StoreList>> call, Response<CustomWebResponse<StoreList>> response) {

                             // noStore.setText(response.message());
                             if (response.code() != 200 || response.body() == null) {
                                 disableProgressBar();
                                 return;
                             }
                             if (!response.body().getSuccess()) {
                                 disableProgressBar();
                                 return;
                             }
                             if (response.isSuccessful() && response.code() == 200) {
                                 homeRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                 adapterHome = new AdapterHome(getApplicationContext(), response.body().getData(), appSettings, masterPojo, new AdapterHome.OnItemClickListener() {
                                     @Override
                                     public void onItemClick(View view, int position, List<Store> store) {
                                         Intent intent = new Intent(HomeActivity.this,ActivitySeeMore.class);
                                         intent.putExtra("storeList",(Serializable) store);
                                         startActivity(intent);
                                     }
                                 });
                                 homeRecycler.setAdapter(adapterHome);
                                 if (response.body().getData().size() == 0) {
                                     rlNoData.setVisibility(View.VISIBLE);

                                 } else {
                                     rlNoData.setVisibility(View.GONE);
                                     homeRecycler.setVisibility(View.VISIBLE);
                                 }
                                 disableProgressBar();
                             }
                         }


                         @Override
                         public void onFailure(Call<CustomWebResponse<StoreList>> call, Throwable t) {
                             disableProgressBar();
                         }
                     }
        );

    }


    private void callApiAppSettings() {
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<AppSettings>> call = apiService.getAppSettings();
        call.enqueue(new Callback<WebResponse<AppSettings>>() {
                         @Override
                         public void onResponse(Call<WebResponse<AppSettings>> call, Response<WebResponse<AppSettings>> response) {
                             if (response.code() != 200 || response.body() == null) {
                                 return;
                             }
                             if (!response.body().getSuccess()) {
                                 return;
                             }
                             if (response.isSuccessful() && response.code() == 200) {
                                 Stash.put(Constants.APP_SETTINGS, response.body().getData());
                                 appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
                             }
                         }


                         @Override
                         public void onFailure(Call<WebResponse<AppSettings>> call, Throwable t) {

                         }
                     }
        );

    }

    @Override
    public void onBackPressed() {
        if (isClickedCatFirst) {
            initialId = 0;
            if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                disableProgressBar();
                Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                // pullToRefresh.setRefreshing(false);
                return;
            } else {
                if (stringsFilter.size() != 0)
                    stringsFilter.clear();
                if (storeTagsDataArrayList.size() != 0)
                    storeTagsDataArrayList.clear();
                callApiHomeDetails(); // your code
                isClickedCatFirst = false;
                return;
            }
        } else {
            finishAffinity();
        }
        super.onBackPressed();
    }

    private void runProgressBar() {
        rlProgressBar.setVisibility(View.VISIBLE);
        progressCircle.setIndeterminateMode(true);
        disableEnableControls(false, rlHome);
    }


    private void disableProgressBar() {
        rlProgressBar.setVisibility(View.GONE);
        rlHome.setVisibility(View.VISIBLE);
        progressCircle.setIndeterminateMode(true);
        disableEnableControls(true, rlHome);
    }


    private void callApiHomeDetails() {
        noStore.setText(masterPojo.getNoStoresAvailable());
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<HomeAllDetails>> call = apiService.homeAllDetails(currentUser.getToken());

        call.enqueue(new Callback<WebResponse<HomeAllDetails>>() {
            @Override
            public void onResponse(Call<WebResponse<HomeAllDetails>> call, Response<WebResponse<HomeAllDetails>> response) {
                if (response.code() != 200 || response.body() == null) {
                    disableProgressBar();
                    pullToRefresh.setRefreshing(false);
                    return;
                }

                if (!response.body().getSuccess()) {
                    disableProgressBar();
                    pullToRefresh.setRefreshing(false);
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    pullToRefresh.setRefreshing(false);
                    disableProgressBar();
                    homeAllDetails = response.body().getData();
                    for (int i = 0; i < response.body().getData().getFilterList().size(); i++) {
                        stringsFilter.add(response.body().getData().getFilterList().get(i));
                    }
                    for (int i = 0; i < response.body().getData().getStoreTagsDataList().size(); i++) {
                        storeTagsDataArrayList.add(response.body().getData().getStoreTagsDataList().get(i));
                    }
                    setUpFilterRecycler(stringsFilter);
                    setCatRecycler(storeTagsDataArrayList);
                    callApiGetStoresByTagId(initialId);


                    navIcon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDrawer.openDrawer(GravityCompat.START, true);

                /*Toast.makeText(getApplicationContext(), "Logged Out", Toast.LENGTH_LONG).show();
                Stash.clear(Constants.isLoggenIn);
                Stash.clear(Constants.USER);
                Intent i = new Intent(HomeActivity.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);*/
                        }
                    });
                    mDrawer.setScrimColor(getResources().getColor(android.R.color.transparent));
                    //setDrawerAnimation();

                    getSupportFragmentManager().beginTransaction().replace(R.id.navigation_container, FragmentLeftMenu.newInstance()).commitAllowingStateLoss();
                }
            }

            @Override
            public void onFailure(Call<WebResponse<HomeAllDetails>> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                disableProgressBar();
            }
        });
    }


    private void setUpFilterRecycler(ArrayList<Filter> stringsFilterList) {
        Filter filter = new Filter();
        filter.setName("");
        stringsFilterList.add(0,
                filter);

        adapterHomeFilter = new AdapterHomeFilter(this, stringsFilterList, new AdapterHomeFilter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                switch (view.getId()) {
                    case R.id.rlFilterBg:

                        if (stringsFilterList == null || stringsFilterList.size() == 0)
                            return;

                        Intent intent = new Intent(getBaseContext(), ActivityFilter.class);
                        Gson gson = new Gson();
                        String homeAllDetailsObj = gson.toJson(homeAllDetails);
                        intent.putExtra("filterName", stringsFilterList.get(position).getName());
                        intent.putExtra("homeAllDetails", homeAllDetailsObj);
                        startActivityForResult(intent, REQ_CODE_FILTER);
                        break;
                }
            }
        });
       /* filterRecycler.setLayoutManager(linearLayoutManager);
        filterRecycler.setAdapter(adapterHomeFilter);*/
    }

    private void callApiGetStoresByFilter(FilterBody filterBody) {
        runProgressBar();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<StoreList>> call = apiService.searchStoreByFilters(filterBody, currentUser.getToken());
        call.enqueue(new Callback<CustomWebResponse<StoreList>>() {
                         @Override
                         public void onResponse(Call<CustomWebResponse<StoreList>> call, Response<CustomWebResponse<StoreList>> response) {
                             if (response.code() != 200 || response.body() == null) {
                                 disableProgressBar();
                                 return;
                             }
                             if (!response.body().getSuccess()) {
                                 disableProgressBar();
                                 return;
                             }
                             if (response.isSuccessful() && response.code() == 200) {

                                 if (response.body().getData() == null) {
                                     rlNoData.setVisibility(View.VISIBLE);
                                     homeRecycler.setVisibility(View.GONE);
                                     disableProgressBar();
                                     return;
                                 }
                                 homeRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                                 adapterHome = new AdapterHome(getApplicationContext(), response.body().getData(), appSettings, masterPojo, new AdapterHome.OnItemClickListener() {
                                     @Override
                                     public void onItemClick(View view, int position,List<Store> storeList) {
                                         Intent intent = new Intent(HomeActivity.this,ActivitySeeMore.class);
                                         intent.putExtra("storeList",(Serializable) storeList);
                                         startActivity(intent);
                                     }
                                 });
                                 homeRecycler.setAdapter(adapterHome);


                                 if (response.body().getData().size() == 0) {
                                     rlNoData.setVisibility(View.VISIBLE);
                                     homeRecycler.setVisibility(View.GONE);
                                 } else {
                                     rlNoData.setVisibility(View.GONE);
                                     homeRecycler.setVisibility(View.VISIBLE);
                                 }
                                 disableProgressBar();
                             }
                         }


                         @Override
                         public void onFailure(Call<CustomWebResponse<StoreList>> call, Throwable t) {
                             disableProgressBar();
                         }
                     }
        );

    }


    @Override
    protected void onResume() {
        super.onResume();
        getCartDetails();
        registerReceiver(broadcastReceiver, new IntentFilter(str_receiver));
        registerReceiver(broadcastReceiver, new IntentFilter(receiver));
        boolean isRefresh = Stash.getBoolean(Constants.IS_FILTER);
        if (!Stash.getBoolean(Constants.IS_FILTER)) {
            return;
        } else {
            callApiGetStoresByTagId(0);
            Stash.put(Constants.IS_FILTER, false);
        }
        askPermission();
    }


    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSIONS:
                if (requestCode == REQUEST_LOCATION) {
                    if (grantResults.length >= 1
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        // We can now safely use the API we requested access to
                        boolean_permission = true;
                        getCurrentLat();
                        fn_permission();
                        // startLocationUpdates();
                    } else {
                    }
                    break;
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACES_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                LocationModel locationModel = (LocationModel) data.getExtras().getSerializable("keyName");
                locationCurrent.setText(locationModel.getPlaceName());
            }
        } else if (requestCode == REQ_CODE_FILTER) {
            if (data == null) {
                return;
            }
            FilterBody filterBody = (FilterBody) data.getExtras().getSerializable("filterKey");
            callApiGetStoresByFilter(filterBody);
        }
    }


    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equalsIgnoreCase(str_receiver)) {
                double latitude = Double.valueOf(intent.getStringExtra("latutide"));
                double longitude = Double.valueOf(intent.getStringExtra("longitude"));

                List<Address> addresses = null;
                if (locationCurrent.getText().toString().length() > 5) {
                    // stopService(intent);
                    return;
                }

                try {
                    addresses = geocoder.getFromLocation(latitude, longitude, 1);
                    String cityName = addresses.get(0).getAddressLine(0);
                    String stateName = addresses.get(0).getAddressLine(1);
                    String countryName = addresses.get(0).getAddressLine(2);
                    Stash.put(Constants.USER_CURRENT_ADDRESS, addresses.get(0).getAddressLine(0) + "");
                    Stash.put(Constants.USER_LAT, latitude);
                    Stash.put(Constants.USER_LONG, longitude);
                    locationCurrent.setText(addresses.get(0).getAddressLine(0) + "");

                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } else {
                Gson gson = new Gson();
                order = gson.fromJson(getIntent().getStringExtra("orderObject"), Order.class);

                order = (Order) Stash.getObject(Constants.ORDER_REVIEW, Order.class);

                if (order == null) {
                    return;
                } else {
                    if (order.getStatus() == 4 && !order.isFeedbackGiven()) {
                        showDialog(order);
                    } else {

                    }
                }
            }
        }
    };


    private void showDialog(Order order) {


        if (isVisible == true) {
            return;
        } else {
            new AppRatingDialog.Builder()
                    .setPositiveButtonText(masterPojo.getSubmit())
                    .setNegativeButtonText(masterPojo.getSkip())
                    .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                    .setDefaultRating(2)
                    .setThreshold(5)
                    .setTitle(masterPojo.getTotalBill() + Converters.roundfloat(order.getSubTotal()) + "\n" + masterPojo.getOrderId() + order.getOrderPlacedId())
                    .setDescription(masterPojo.getSelectStars())
                    .setCommentInputEnabled(true)
                    .setDefaultComment(masterPojo.getGoodExperience())
                    .setStarColor(R.color.colorPrimary)
                    .setNoteDescriptionTextColor(R.color.grey)
                    .setTitleTextColor(R.color.black)
                    .setDescriptionTextColor(R.color.black)
                    .setHint(masterPojo.getPleaseWrite())
                    .setHintTextColor(R.color.colorPrimary)
                    .setCommentTextColor(R.color.black)
                    .setCommentBackgroundColor(R.color.light_greu)
                    .setDialogBackgroundColor(R.color.white)
                    .setCancelable(false)
                    .setCancelable(false)
                    .setCanceledOnTouchOutside(false)
                    .create(HomeActivity.this)
                    .show();
            isVisible = true;
        }


    }


    @Override
    public void onNegativeButtonClicked() {
        isVisible = false;
    }

    @Override
    public void onNeutralButtonClicked() {
        isVisible = false;
    }

    @Override
    public void onPositiveButtonClickedWithComment(int i, @NotNull String s) {
        // callApiGiveReview(String.valueOf(i), s, 1, order);
        isVisible = false;
    }

    @Override
    public void onPositiveButtonClickedWithoutComment(int i) {
        isVisible = false;
    }


    /*private void callApiGiveReview(String rating, String feedback, int type, Order order) {
        Feedback feedbackReview = new Feedback(0, order.getOrderId(), type, rating, feedback, false, parseDateToddMMyyyy(Constants.DATE_FORMAT_WITHOUT));
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<Feedback>> call = apiService.orderFeedback(feedbackReview);
        call.enqueue(new Callback<WebResponse<Feedback>>() {
            @Override
            public void onResponse(Call<WebResponse<Feedback>> call, Response<WebResponse<Feedback>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (!response.body().getSuccess()) {

                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toasty.LENGTH_LONG).show();
                    return;
                }
                if (response.isSuccessful() && response.code() == 200) {
                    order.setFeedbackGiven(true);
                    Toasty.success(getApplicationContext(), response.body().getMessage(), Toasty.LENGTH_LONG).show();
                    insertOrder(order);
                }
            }

            @Override
            public void onFailure(Call<WebResponse<Feedback>> call, Throwable t) {

            }
        });
    }*/


    private void insertOrder(Order order) {
        class InsertOrderByStatus extends AsyncTask<Void, Void, Void> {
            @Override
            protected Void doInBackground(Void... voids) {
                DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .orderDao()
                        .update(order);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }


        }
        InsertOrderByStatus gt = new InsertOrderByStatus();
        gt.execute();
    }

    @Override
    public void onFilterClicked(FilterBody filterBody) {


    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    @Override
    public void sendEventCloseDrawer(boolean closeDrawer) {
        mDrawer.closeDrawers();
    }
}