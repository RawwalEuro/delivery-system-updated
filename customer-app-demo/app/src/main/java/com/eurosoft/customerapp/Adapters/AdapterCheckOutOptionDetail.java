package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.OptionDetailedData;
import com.eurosoft.customerapp.R;

import java.util.List;

public class AdapterCheckOutOptionDetail extends RecyclerView.Adapter<AdapterCheckOutOptionDetail.ViewHolder> {


    private Context context;
    private List<OptionDetailedData> optionDetailedDataList;

    public AdapterCheckOutOptionDetail(Context ctx, List<OptionDetailedData> list) {
        context = ctx;
        optionDetailedDataList = list;
    }

    @Override
    public AdapterCheckOutOptionDetail.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_checkout_summary, parent, false);
        final AdapterCheckOutOptionDetail.ViewHolder viewHolder = new AdapterCheckOutOptionDetail.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterCheckOutOptionDetail.ViewHolder holder, int position) {
        OptionDetailedData optionDetailedData = optionDetailedDataList.get(position);
        holder.optionName.setText(optionDetailedData.getOptionValue());
    }

    @Override
    public int getItemCount() {
        return optionDetailedDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView optionName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            optionName = itemView.findViewById(R.id.optionName);
        }
    }
}
