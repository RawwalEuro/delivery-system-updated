package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.FilterDetailDatum;
import com.eurosoft.customerapp.R;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AdapterDietory extends RecyclerView.Adapter<AdapterDietory.ViewHolder> {


    private Context context;
    private List<FilterDetailDatum> dietryList;
    private AdapterDietory.OnItemClickListener mOnItemClickListener;
    private int row_index = -1;
    private List<FilterDetailDatum> filterDietryList = new ArrayList();

    public AdapterDietory(Context ctx, List<FilterDetailDatum> listDietry, AdapterDietory.OnItemClickListener onItemClickListener) {
        context = ctx;
        dietryList = listDietry;
        mOnItemClickListener = onItemClickListener;
    }


    @Override
    public AdapterDietory.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_dietary, parent, false);
        final AdapterDietory.ViewHolder viewHolder = new AdapterDietory.ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterDietory.ViewHolder holder, int position) {
        FilterDetailDatum filterDetailDatum = dietryList.get(position);
        holder.dietaryText.setText(filterDetailDatum.getName());

        Picasso.get().load(filterDetailDatum.getBaseUrl() + filterDetailDatum.getIcon()).placeholder(R.color.grey).into(holder.iconSort);

        holder.llBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (filterDetailDatum.getSelected()) {
                    filterDetailDatum.setSelected(false);
                    try {
                        filterDietryList.remove(position);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                } else {
                    filterDetailDatum.setSelected(true);
                    filterDietryList.add(filterDetailDatum);
                }


                mOnItemClickListener.onItemClick(v, holder.getAdapterPosition());
                row_index = position;
                notifyDataSetChanged();

            }
        });

        if (filterDetailDatum.getSelected()) {
            holder.llBg.setBackgroundResource(R.drawable.drawable_curved_rectangle);
            holder.dietaryText.setTextColor(Color.parseColor("#000000"));
        } else if (!filterDetailDatum.getSelected()) {
            holder.llBg.setBackgroundColor(Color.parseColor("#ffffff"));
            holder.dietaryText.setTextColor(Color.parseColor("#000000"));
        }
    }

    @Override
    public int getItemCount() {
        return dietryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dietaryText;
        private ImageView iconSort;
        private LinearLayout llBg;


        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            dietaryText = itemView.findViewById(R.id.dietaryText);
            iconSort = itemView.findViewById(R.id.iconSort);
            llBg = itemView.findViewById(R.id.llBg);

        }
    }


    public List<FilterDetailDatum> getFilterDietryList() {

        return filterDietryList;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}
