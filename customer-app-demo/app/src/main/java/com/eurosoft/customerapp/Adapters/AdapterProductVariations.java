package com.eurosoft.customerapp.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.AdapterCheckBoxSelect;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.CheckedListSelected;
import com.eurosoft.customerapp.Model.OptionDetailedData;
import com.eurosoft.customerapp.Model.OptionsList;
import com.eurosoft.customerapp.Model.ProductVariant;
import com.eurosoft.customerapp.Model.ValidationProductVariant;
import com.eurosoft.customerapp.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class AdapterProductVariations extends RecyclerView.Adapter {


    private Context context;
    private List<OptionsList> optionsLists;
    private OnItemClickListener mOnItemClickListener;
    private AppSettings appSetting;
    public int lastCheckedPosition = -1;
    int copyOfLastCheckedPosition = 0;
    private AdapterRadioBtnSelect adapterRadioBtnSelect;
    private AdapterCheckBoxSelect adapterCheckBoxSelect;
    private OptionsList optionsList;
    private ArrayList<ValidationProductVariant> validationProductVariant;
    private List<OptionDetailedData> lstOptionDetailData = new ArrayList<>();
    private List<OptionDetailedData> templstOptionDetailData = new ArrayList<>();
    private List<OptionsList> optionsListsTemp = new ArrayList<>();
    int currentPosition = 0;
    double productPrice = 0.0;


    public List<OptionsList> getOptionsLists() {
        return optionsLists;
    }

    public AdapterProductVariations(Context ctx, ArrayList<ValidationProductVariant> validationProductVariants, List<OptionsList> optionsList, AppSettings appSettings, Double productValue, OnItemClickListener onItemClickListener) {
        context = ctx;
        validationProductVariant = validationProductVariants;
        optionsLists = optionsList;
        mOnItemClickListener = onItemClickListener;
        appSetting = appSettings;
        productPrice = productValue;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case OptionsList.RADIO_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_product_rdtbn_variations, parent, false);
                return new RadioBtnViewHolder(view);
            case OptionsList.CHECK_BOX_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_check_box_products, parent, false);
                return new CheckBoxViewHolder(view);
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {

        switch (optionsLists.get(position).getType()) {
            case 0:
                return OptionsList.RADIO_TYPE;
            case 1:
                return OptionsList.CHECK_BOX_TYPE;

            default:
                return -1;
        }
    }


    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int holderPosition) {


        optionsList = optionsLists.get(holderPosition);

        if (optionsList != null) {
            switch (optionsList.getType()) {

                case OptionsList.RADIO_TYPE:
                    ((RadioBtnViewHolder) holder).variationName.setText(optionsList.getName());
                    adapterRadioBtnSelect = new AdapterRadioBtnSelect(context, optionsList.getOptionDetailData(), appSetting, new AdapterRadioBtnSelect.OnItemClickListener() {

                        @Override
                        public void onItemClick(View view, int position, int lastPos) {

                            switch (view.getId()) {

                                case R.id.rlParent:
                                    break;


                                case R.id.radioBtn:

                                    OptionDetailedData optionDetailedData = optionsLists.get(holderPosition).getOptionDetailData().get(position);

                                    adapterRadioBtnSelect.notifyItemChanged(adapterRadioBtnSelect.getCopyOfLastCheckedPosition());
                                    adapterRadioBtnSelect.notifyItemChanged(adapterRadioBtnSelect.getLastCheckedPosition());


                                    if (lastPos == -1) {
                                        OptionDetailedData optionDetaile = new OptionDetailedData(optionDetailedData.getOptionMasterId(),
                                                optionDetailedData.getId(), optionDetailedData.getValue(), optionDetailedData.getPrice(),
                                                optionDetailedData.getProductId());
                                        lstOptionDetailData.add(optionDetaile);
                                        mOnItemClickListener.onItemClick(view, holder.getAdapterPosition(), optionDetailedData.getPrice(), true, false, optionDetailedData.getPriceOperator(), 0.0, false, optionsLists, holderPosition, 0);

                                        ProductVariant productVariant = new ProductVariant();
                                        productVariant.setChildPosition(position);
                                        productVariant.setParentPosition(holderPosition);
                                        productVariant.setOptionMasterId(optionDetailedData.getOptionMasterId());
                                        productVariant.setOptionId(optionDetailedData.getId());
                                        productVariant.setOptionValue(optionDetailedData.getValue());
                                        productVariant.setOptionPrice(optionDetailedData.getPrice());
                                        productVariant.setProductId(optionDetailedData.getProductId());
                                        validationProductVariant.get(holderPosition).getLstDataObject().add(0, productVariant);
                                    } else {
                                        //OptionDetailedData lastCheckedItem = optionsList.getOptionDetailData().get(lastPos);

                                        OptionDetailedData optionDetaile = new OptionDetailedData(optionDetailedData.getOptionMasterId(),
                                                optionDetailedData.getId(), optionDetailedData.getValue(), optionDetailedData.getPrice(),
                                                optionDetailedData.getProductId());
                                        lstOptionDetailData.add(optionDetaile);


                                        mOnItemClickListener.onItemClick(view, holder.getAdapterPosition(), optionsLists.get(holderPosition).getOptionDetailData().get(position).getPrice(), true, false, optionDetailedData.getPriceOperator(), optionsLists.get(holderPosition).getOptionDetailData().get(lastPos).getPrice(), true, optionsLists, holderPosition, 0);


                                        ProductVariant productVariant = new ProductVariant();
                                        productVariant.setChildPosition(position);
                                        productVariant.setParentPosition(holderPosition);
                                        productVariant.setOptionMasterId(optionDetailedData.getOptionMasterId());
                                        productVariant.setOptionId(optionDetailedData.getId());
                                        productVariant.setOptionValue(optionDetailedData.getValue());
                                        productVariant.setOptionPrice(optionDetailedData.getPrice());
                                        productVariant.setProductId(optionDetailedData.getProductId());

                                        validationProductVariant.get(holderPosition).getLstDataObject().add(0, productVariant);

                                      /*  for (int i = 0; i < validationProductVariant.get(holderPosition).getLstDataObject().size(); i++) {
                                            if (validationProductVariant.get(holderPosition).getLstDataObject().get(i).getOptionValue().equalsIgnoreCase(lastCheckedItem.getOptionValue())) {
                                                validationProductVariant.get(holderPosition).getLstDataObject().remove(i);
                                                mOnItemClickListener.onItemClick(view, holder.getAdapterPosition(), optionDetailedData.getPrice(), true, true, optionDetailedData.getPriceOperator());
                                                return;
                                            }
                                        }*/

                                    }
                                    break;
                            }
                        }
                    });
                    ((RadioBtnViewHolder) holder).radioBtnRv.setLayoutManager(new LinearLayoutManager(context));
                    ((RadioBtnViewHolder) holder).radioBtnRv.setAdapter(adapterRadioBtnSelect);


                    if (optionsList.getIsOptionRequried() == true) {
                        ((RadioBtnViewHolder) holder).viewRequired.setVisibility(View.VISIBLE);
                    } else {
                        ((RadioBtnViewHolder) holder).viewRequired.setVisibility(View.GONE);
                    }
                    break;


                case OptionsList.CHECK_BOX_TYPE:

                    int selectedCounts = 0;

                    for (int i = 0; i < validationProductVariant.get(holderPosition).getLstDataObject().size(); i++) {
                        Log.e("Validation Product", validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected() + "");
                        if (validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected()) {
                            selectedCounts = selectedCounts + 1;
                        }
                    }

                    if (optionsLists.get(holderPosition).getSelectionLimit() == selectedCounts) {
                        Toasty.warning(context, "Can't add more than " + optionsLists.get(holderPosition).getSelectionLimit(), Toasty.LENGTH_SHORT).show();
                        return;
                    }

                    ((CheckBoxViewHolder) holder).variationName.setText(optionsList.getName());







                    adapterCheckBoxSelect = new AdapterCheckBoxSelect(context, optionsList.getOptionDetailData(), appSetting, optionsLists.get(holderPosition).getSelectionLimit(), selectedCounts, new AdapterCheckBoxSelect.OnItemClickListener() {

                        @Override
                        public void onItemClick(View view, int position) {

                            switch (view.getId()) {

                                case R.id.checkBox:

                                    int selectedCountss = 0;
                                    for (int i = 0; i < validationProductVariant.get(holderPosition).getLstDataObject().size(); i++) {
                                        Log.e("Validation Product", validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected() + "");
                                        if (validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected()) {
                                            selectedCountss = selectedCountss + 1;
                                        }
                                    }

                                    if(selectedCountss >= 2){
                                        Toasty.warning(context,"Can't more than 2 items",Toasty.LENGTH_SHORT).show();
                                        return;
                                    }


                                    OptionDetailedData optionDetailedData = optionsLists.get(holderPosition).getOptionDetailData().get(position);
                                    OptionDetailedData optionDetaile = new OptionDetailedData(optionDetailedData.getOptionMasterId(),
                                            optionDetailedData.getId(), optionDetailedData.getValue(), optionDetailedData.getPrice(),
                                            optionDetailedData.getProductId());
                                    lstOptionDetailData.add(optionDetaile);


                                    // Log.e("Limit",optionsLists.get(holderPosition).getSelectionLimit()+ "Limit");


                                    if (adapterCheckBoxSelect.isChecked()) {
                                        if (validationProductVariant.get(holderPosition).getLstDataObject().size() == 0) {
                                            addingItemCheckBox(optionDetailedData, position, holderPosition);


                                            int selectedCount = 0;
                                            if (validationProductVariant.size() == 0) {
                                                selectedCount = 0;

                                            } else {
                                                for (int i = 0; i < validationProductVariant.get(holderPosition).getLstDataObject().size(); i++) {
                                                    Log.e("Validation Product", validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected() + "");
                                                    if (validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected()) {
                                                        selectedCount = selectedCount + 1;
                                                    }
                                                }
                                                Log.e("selectedCount", selectedCount + "");
                                            }

                                            if (optionsLists.get(holderPosition).getSelectionLimit() == selectedCount) {
                                                Toasty.warning(context, "Can't add more than " + optionsLists.get(holderPosition).getSelectionLimit(), Toasty.LENGTH_SHORT).show();
                                                return;
                                            }
                                            mOnItemClickListener.onItemClick(view, holder.getAdapterPosition(), optionDetailedData.getPrice(), true, false, optionDetailedData.getPriceOperator(), 0.0, false, optionsLists, holderPosition, selectedCount);


                                        } else {
                                            for (int i = 0; i < validationProductVariant.get(holderPosition).getLstDataObject().size(); i++) {
                                                if (validationProductVariant.get(holderPosition).getLstDataObject().get(i).getOptionValue().equalsIgnoreCase(optionDetailedData.getOptionValue())) {
                                                    return;
                                                }
                                            }

                                            ProductVariant productVariant = new ProductVariant();
                                            productVariant.setChildPosition(position);
                                            productVariant.setParentPosition(holderPosition);
                                            productVariant.setOptionMasterId(optionDetailedData.getOptionMasterId());
                                            productVariant.setOptionId(optionDetailedData.getId());
                                            productVariant.setOptionValue(optionDetailedData.getValue());
                                            productVariant.setSetSelected(true);
                                            productVariant.setOptionPrice(optionDetailedData.getPrice());
                                            productVariant.setProductId(optionDetailedData.getProductId());
                                            validationProductVariant.get(holderPosition).getLstDataObject().add(productVariant);
                                            int selectedCount = 0;


                                            for (int i = 0; i < validationProductVariant.get(holderPosition).getLstDataObject().size(); i++) {
                                                Log.e("Validation Product", validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected() + "");
                                                if (validationProductVariant.get(holderPosition).getLstDataObject().get(i).getSetSelected()) {
                                                    selectedCount = selectedCount + 1;
                                                }
                                            }
                                            Log.e("selectedCount", selectedCount + "");


                                            mOnItemClickListener.onItemClick(view, holder.getAdapterPosition(), optionDetailedData.getPrice(), true, false, optionDetailedData.getPriceOperator(), 0.0, false, optionsLists, holderPosition, selectedCount);

                                        }

                                    } else {

                                        for (int i = 0; i < validationProductVariant.get(holderPosition).getLstDataObject().size(); i++) {
                                            if (validationProductVariant.get(holderPosition).getLstDataObject().get(i).getOptionValue().equalsIgnoreCase(optionDetaile.getOptionValue())) {
                                                validationProductVariant.get(holderPosition).getLstDataObject().remove(i);
                                                int selectedCount = 0;
                                                for (int j = 0; j < validationProductVariant.get(holderPosition).getLstDataObject().size(); j++) {
                                                    if (validationProductVariant.get(holderPosition).getLstDataObject().get(j).getSetSelected()) {
                                                        selectedCount = selectedCount + 1;
                                                    }
                                                }
                                                Log.e("selectedCount", selectedCount + "");

                                                mOnItemClickListener.onItemClick(view, holder.getAdapterPosition(), optionDetailedData.getPrice(), true, true, optionDetailedData.getPriceOperator(), 0.0, false, optionsLists, holderPosition, selectedCount);
                                                return;
                                            }
                                        }
                                    }
                                    break;


                            }
                        }
                    });
                    ((CheckBoxViewHolder) holder).checkBoxRv.setLayoutManager(new LinearLayoutManager(context));
                    ((CheckBoxViewHolder) holder).checkBoxRv.setAdapter(adapterCheckBoxSelect);
                    if (optionsList.getIsOptionRequried() == true) {
                        ((CheckBoxViewHolder) holder).viewRequired.setVisibility(View.VISIBLE);
                    } else {
                        ((CheckBoxViewHolder) holder).viewRequired.setVisibility(View.GONE);
                    }

                    break;
            }
        }
    }

    private void addingItemCheckBox(OptionDetailedData optionDetailedData, int position, int holderPosition) {
        OptionDetailedData optionDetaile = new OptionDetailedData(optionDetailedData.getOptionMasterId(),
                optionDetailedData.getId(), optionDetailedData.getValue(), optionDetailedData.getPrice(),
                optionDetailedData.getProductId());
        lstOptionDetailData.add(optionDetaile);

        ProductVariant productVariant = new ProductVariant();
        productVariant.setChildPosition(position);
        productVariant.setParentPosition(holderPosition);
        productVariant.setOptionMasterId(optionDetailedData.getOptionMasterId());
        productVariant.setOptionId(optionDetailedData.getId());
        productVariant.setOptionValue(optionDetailedData.getValue());
        productVariant.setOptionPrice(optionDetailedData.getPrice());
        productVariant.setSetSelected(true);
        productVariant.setProductId(optionDetailedData.getProductId());
        validationProductVariant.get(holderPosition).getLstDataObject().add(productVariant);
    }

    @Override
    public int getItemCount() {
        return optionsLists.size();
    }

    public class RadioBtnViewHolder extends RecyclerView.ViewHolder {

        private RadioButton radioBtn;
        private TextView variantPrice, variationName;
        private RelativeLayout rlParent;
        private RecyclerView radioBtnRv;
        private ImageView viewRequired, circle;

        public RadioBtnViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            variationName = itemView.findViewById(R.id.variationName);
            viewRequired = itemView.findViewById(R.id.view_required);
            radioBtnRv = itemView.findViewById(R.id.radioBtnRv);
        }
    }


    public class CheckBoxViewHolder extends RecyclerView.ViewHolder {

        private CheckBox checkBox;
        private TextView variantPrice, variationName;
        private RelativeLayout rlParent;
        private RecyclerView radioBtnRv;
        private ImageView viewRequired;
        private RecyclerView checkBoxRv;

        public CheckBoxViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            variationName = itemView.findViewById(R.id.variationName);
            viewRequired = itemView.findViewById(R.id.view_required);
            checkBoxRv = itemView.findViewById(R.id.checkBoxRv);
        }
    }


    public int getCopyOfLastCheckedPosition() {
        return copyOfLastCheckedPosition;
    }

    public int getLastCheckedPosition() {
        return lastCheckedPosition;
    }

    public List<OptionDetailedData> getLstOptionDetailData() {
        return lstOptionDetailData;
    }


    public ArrayList<ValidationProductVariant> getValidationProductVariant() {
        return validationProductVariant;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position, Double price, boolean isAdd, boolean isUnChecked, String operator, Double priceToDeduct, boolean isRadioDeduct, List<OptionsList> optionsLists, int parentPosition, int selectedCount);
    }
}
