package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.droidnet.DroidListener;
import com.droidnet.DroidNet;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Utils.HttpsTrustManager;
import com.eurosoft.customerapp.Utils.LocationService.LocationEnable;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity implements DroidListener {
    private final int SPLASH_DISPLAY_LENGTH = 3000;
    private ImageView logo;
    private Animation slideAnimation;
    private DroidNet mDroidNet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Stash.init(this);
        DroidNet.init(this);
        mDroidNet = DroidNet.getInstance();
        mDroidNet.addInternetConnectivityListener(this);
        logo = findViewById(R.id.logo);
        slideAnimation = AnimationUtils.loadAnimation(this, R.anim.side_slide);
        logo.startAnimation(slideAnimation);
    }


    private void callApiSetLangauge(String code) {

        HttpsTrustManager.allowAllSSL();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterPojo>> call = apiService.setLanguage(code);

        call.enqueue(new Callback<WebResponse<MasterPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterPojo>> call, Response<WebResponse<MasterPojo>> response) {

                if (response.code() == 200 && response.isSuccessful()) {
                    try {
                        Stash.put(Constants.MASTER_POJO, response.body().getData());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }


            }

            @Override
            public void onFailure(Call<WebResponse<MasterPojo>> call, Throwable t) {

            }
        });


    }

    private void callApiAppSettings() {
        HttpsTrustManager.allowAllSSL();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<AppSettings>> call = apiService.getAppSettings();

        call.enqueue(new Callback<WebResponse<AppSettings>>() {
                         @Override
                         public void onResponse(Call<WebResponse<AppSettings>> call, Response<WebResponse<AppSettings>> response) {
                             if (response.code() != 200 || response.body() == null) {
                                 return;
                             }

                             if (!response.body().getSuccess()) {
                                 return;
                             }

                             if (response.isSuccessful() && response.code() == 200) {
                                 Stash.put(Constants.APP_SETTINGS, response.body().getData());
                                 AppSettings appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);


                                 new Handler().postDelayed(new Runnable() {
                                     @Override
                                     public void run() {
                                         if (!Stash.getBoolean(Constants.IS_SIGN_UP_COMPLETED, true) && (!Stash.getBoolean(Constants.isLoggenIn))) {
                                             startActivity(new Intent(SplashActivity.this, OtpVerifyActivity.class));
                                         } else if (!Stash.getBoolean(Constants.isLoggenIn)) {
                                             Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                                             startActivity(mainIntent);
                                             finish();
                                         } else {

                                             if (LocationEnable.isLocationEnabled(getApplicationContext())) {
                                                 Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                                                 startActivity(mainIntent);
                                                 finish();
                                                 return;
                                             } else if(!LocationEnable.isLocationEnabled(SplashActivity.this)){
                                                 Intent mainIntentI = new Intent(SplashActivity.this, ActivityTurnOnLocation.class);
                                                 startActivity(mainIntentI);
                                                 finish();
                                             } else {

                                             }
                                         }
                                     }
                                 }, SPLASH_DISPLAY_LENGTH);

                             }
                         }


                         @Override
                         public void onFailure(Call<WebResponse<AppSettings>> call, Throwable t) {

                             if (!Stash.getBoolean(Constants.IS_SIGN_UP_COMPLETED, true) && (!Stash.getBoolean(Constants.isLoggenIn))) {
                                 startActivity(new Intent(SplashActivity.this, OtpVerifyActivity.class));
                             } else if (!Stash.getBoolean(Constants.isLoggenIn)) {
                                 Intent mainIntent = new Intent(SplashActivity.this, LoginActivity.class);
                                 startActivity(mainIntent);
                                 finish();
                             } else {
                                 Log.e("IsEnable",LocationEnable.isLocationEnabled(SplashActivity.this) + "");

                                 if (LocationEnable.isLocationEnabled(SplashActivity.this)) {
                                     Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                                     startActivity(mainIntent);
                                     finish();
                                 } else if(!LocationEnable.isLocationEnabled(SplashActivity.this)){
                                     Intent mainIntentI = new Intent(SplashActivity.this, ActivityTurnOnLocation.class);
                                     startActivity(mainIntentI);
                                     finish();
                                 } else {

                                 }
                             }

                         }
                     }
        );

    }

    @Override
    public void onInternetConnectivityChanged(boolean isConnected) {
        if (isConnected) {
            //do Stuff with internet
            try {
                if (!AppNetWorkStatus.getInstance(this).isOnline()) {
                    Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                    return;
                } else {
                    callApiAppSettings();
                    if (!Stash.getBoolean(Constants.IS_LANGUAGE_SELECTED)) {
                        if (!AppNetWorkStatus.getInstance(this).isOnline()) {
                            Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                            return;
                        } else {
                            callApiSetLangauge("en-GB");
                        }
                    }
                }

            } catch (Exception exception) {
                Log.e("Exception", exception.getMessage());
            }

        } else {
            Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
        }
    }


}