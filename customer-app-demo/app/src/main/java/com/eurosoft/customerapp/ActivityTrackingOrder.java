package com.eurosoft.customerapp;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.eurosoft.customerapp.Adapters.AdapterOrderDetails;
import com.eurosoft.customerapp.Adapters.MyRecyclerAdapter;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.DriverTracking;
import com.eurosoft.customerapp.Model.Header;
import com.eurosoft.customerapp.Model.ListItem;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Order;
import com.eurosoft.customerapp.Model.OrderModel;
import com.eurosoft.customerapp.Model.ProductDetails;
import com.eurosoft.customerapp.Model.TrackDriver;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.Directions.DirectionsJSONParser;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.maps.model.RoundCap;
import com.google.gson.Gson;
import com.hosseiniseyro.apprating.AppRatingDialog;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import android.view.animation.LinearInterpolator;
import android.view.animation.Interpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityTrackingOrder extends AppCompatActivity implements OnMapReadyCallback, View.OnClickListener, Runnable {

    private static final int ROUND = 2;
    private ImageView closeBtn;
    private LatLng oldLocation;
    private Marker marker;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private boolean isMarkerRotating;
    private Polyline blackPolyline, greyPolyLine;
    private PolylineOptions polylineOptions, blackPolylineOptions;

    private TextView orderId, orderStatus, cash, subTotal, deliveryFee, totalBill;
    private TextView restruantName;
    private RecyclerView cartRv;
    private OrderModel order;
    private AdapterOrderDetails adapterOrderDetails;
    private ImageView close;
    private List<Order> orderList = new ArrayList<>();
    private User currentUser;
    private LinearLayout rlTransId;
    private TextView transId;
    private FrameLayout bckIcon;
    private boolean isFirstTime = true;
    private MasterPojo masterPojo;

    private RelativeLayout rlProgressBar;
    private RelativeLayout mainRl;
    private CircularProgressBar circularProgressBar;
    private CoordinatorLayout coordinator;
    private FrameLayout frameLayout;
    LatLng[] dummyLatLng = new LatLng[]{

            new LatLng(24.865425, 67.078554),
            new LatLng(24.865048326240174, 67.0776306439026),
            new LatLng(24.86454286926562, 67.07632247716069),
            new LatLng(24.86355241315495, 67.0743512051464),
            new LatLng(24.862651991630287, 67.07172584982672)
    };

    Handler handler = new Handler();
    Runnable runnable;
    int delay = 10 * 1000;
    private ActivityTrackingOrder mContext;
    ArrayList markerPoints = new ArrayList();
    ArrayList<Marker> markers = new ArrayList();
    private String duration = "";
    private TextView estimateTime;
    private TrackDriver trackDriver;
    int i = 0;
    private DriverTracking driverTracking;
    private LinearLayout orderStatusLL;
    private TextView orderStatusTv;
    private ImageView closeView;
    private AppSettings appSettings;
    private TextView estimateHeading, transactionId, paymentMethod, total, inclusive_tax;
    private RelativeLayout llEstimated;
    private TextView deliveryHeading, deliveryAddress;
    private TextView servicesChargesValue,tipValueTv;
    private RelativeLayout rlServicesCharges,rlTip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tracking_order);
        mContext = this;
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);

        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        initViews();
        getData();
        // Get the SupportMapFragment and request notification when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        oldLocation = new LatLng(24.88197937443171, 67.1116698263267);
        progressVisiblityVisible();
        // showDialog();

    }


    @Override
    protected void onResume() {
        super.onResume();

        handler.postDelayed(runnable = new Runnable() {
            public void run() {
                //do something

            }
        }, delay);

        super.onResume();
    }

    @Override
    protected void onPause() {
        handler.removeCallbacks(runnable);

        handler.removeCallbacksAndMessages(null);
        super.onPause();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bckIcon:
                finish();
                break;


            case R.id.closeView:
                finish();
                break;

        }
    }

    private void initViews() {
        //closeBtn = findViewById(R.id.closeBtn);
        // closeBtn.setOnClickListener(this);


        cartRv = findViewById(R.id.cartRv);
        cash = findViewById(R.id.cash);
        llEstimated = findViewById(R.id.llEstimated);


        totalBill = findViewById(R.id.totalBill);
        close = findViewById(R.id.close);
        rlTransId = findViewById(R.id.rlTransId);
        transId = findViewById(R.id.transId);
        bckIcon = findViewById(R.id.bckIcon);

        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        mainRl = findViewById(R.id.mainRl);
        coordinator = findViewById(R.id.coordinator);
        orderStatusLL = findViewById(R.id.orderStatusLL);
        orderStatusTv = findViewById(R.id.orderStatus);
        closeView = findViewById(R.id.closeView);
        frameLayout = findViewById(R.id.FrameLayout1);
        deliveryHeading = findViewById(R.id.deliveryHeading);
        deliveryAddress = findViewById(R.id.deliveryAddress);

        servicesChargesValue = findViewById(R.id.servicesChargesValue);
        tipValueTv = findViewById(R.id.tipValueTv);
        rlServicesCharges = findViewById(R.id.rlServicesCharges);
        rlTip = findViewById(R.id.rlTip);


        //Language
        estimateHeading = findViewById(R.id.estimateHeading);
        estimateTime = findViewById(R.id.estimateTime);
        orderId = findViewById(R.id.orderId);
        restruantName = findViewById(R.id.restruantName);
        transactionId = findViewById(R.id.transactionId);
        paymentMethod = findViewById(R.id.paymentMethod);
        subTotal = findViewById(R.id.subTotal);
        deliveryFee = findViewById(R.id.deliveryFee);
        total = findViewById(R.id.total);
        inclusive_tax = findViewById(R.id.inclusive_tax);

        estimateHeading.setText(masterPojo.getEstimateHeading());
        estimateTime.setText(masterPojo.getEstimateTime());
        orderId.setText(masterPojo.getOrderDetails());
        restruantName.setText(masterPojo.getRestruantName());
        transactionId.setText(masterPojo.getTransactionId());
        paymentMethod.setText(masterPojo.getPaymentMethod());
        subTotal.setText(masterPojo.getSubTotal());
        deliveryFee.setText(masterPojo.getDeliveryFee());
        total.setText(masterPojo.getTotal());
        inclusive_tax.setText(masterPojo.getInclusiveTax());
        deliveryHeading.setText(masterPojo.getDeliveryAddress());

        close.setOnClickListener(this);
        closeView.setOnClickListener(this);
        bckIcon.setOnClickListener(this);


    }


    private void getData() {
        Gson gson = new Gson();
        order = gson.fromJson(getIntent().getStringExtra("myjson"), OrderModel.class);
        if (order == null) {
            progressVisiblityGone();
            return;
        } else {
            progressVisiblityGone();
            setData(order);

        }
    }


    private void setData(OrderModel order) {
        // orderId.setText(order.getOrderPlacedId() + "");
        restruantName.setText(order.getProductList().get(0).getStoreName() + "");

        subTotal.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(order.getSubTotal()) + "");
        deliveryFee.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(order.getShippingCharges()) + "");
        totalBill.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(order.getTotalAmount()) + "");
        deliveryAddress.setText(order.getDropoffAddress());
        if (!order.getDeliveryTime().equalsIgnoreCase("")) {
            estimateTime.setText(order.getDeliveryTime() + " mins");
            estimateTime.setVisibility(View.VISIBLE);
        } else {
            estimateTime.setVisibility(View.INVISIBLE);
        }


        if (order.getPaymentTypeName().equalsIgnoreCase("Cash")) {
            cash.setText(masterPojo.getCash());
            rlTransId.setVisibility(View.GONE);

        } else if (order.getPaymentTypeName().equalsIgnoreCase("Card")) {
            cash.setText(masterPojo.getCard());
            rlTransId.setVisibility(View.GONE);
            //transId.setText(order.getTransId() + "");
        }



        if(order.getTipValue() == null || order.getTipValue() == 00.00){
            rlTip.setVisibility(View.GONE);
        } else {
            rlTip.setVisibility(View.VISIBLE);
            tipValueTv.setText(Converters.roundfloat(order.getTipValue()) + "");
        }

        if(order.getServiceCharges() == null || order.getServiceCharges() == 00.00){
            rlServicesCharges.setVisibility(View.GONE);
        } else {
            rlServicesCharges.setVisibility(View.VISIBLE);
            servicesChargesValue.setText(Converters.roundfloat(order.getServiceCharges()) + "");
        }

        setStickyRecyler();

    }

    private void setStickyRecyler() {
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(this, getList(), appSettings, new MyRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }
        });
        cartRv.setLayoutManager(new LinearLayoutManager(this));
        cartRv.setAdapter(adapter);


    }


    private ArrayList<ListItem> getList() {


        ArrayList<ListItem> arrayList = new ArrayList<>();


        List<ProductDetails> productsList = order.getProductList();///order products List
        for (int j = 0; j < productsList.size(); j++) {
            Header header = new Header();
            if (j == 0) {//header set

                header.setHeader(productsList.get(j).getStoreName());
                arrayList.add(header);
                arrayList.add(productsList.get(j));
            } else {
                if (productsList.get(j - 1).getStoreId() != productsList.get(j).getStoreId()) {
                    header.setHeader(productsList.get(j).getStoreName());
                    arrayList.add(header);
                }
                arrayList.add(productsList.get(j));
            }
        }

        return arrayList;
    }

    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        mMap = googleMap;
//        "DriverLatitude": 51.62144503498237,
//                "DriverLongitude": -3.943645879626274

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                .target(new LatLng(51.62144503498237, -3.943645879626274))
                .zoom(15.5f)
                .build()));
        // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(51.62144503498237,-3.943645879626274), 15));
        handler.postDelayed(this, 5000);
        //callApiDriverDetails();

    }

    private void setupRoute(Double originLat, Double originLong, Double driverLat, Double driverLong) {
        MarkerOptions originOpt = new MarkerOptions();
        MarkerOptions destinationOpt = new MarkerOptions();
        LatLng origin = new LatLng(originLat, originLong);
        LatLng dest = new LatLng(driverLat, driverLong);
        markerPoints.add(origin);
        markerPoints.add(dest);

//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dest, 15));
        originOpt.position(origin);
        destinationOpt.position(dest);
        // Add new marker to the Google Map Android API V2
        originOpt.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        destinationOpt.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));


        if (markerPoints.size() >= 2) {
            LatLng originOrder = (LatLng) markerPoints.get(0);
            LatLng destintation = (LatLng) markerPoints.get(1);


            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(originOrder, destintation);


            Log.e("URL", url);

            DownloadTask downloadTask = new DownloadTask();

            // Start downloading json data from Google Directions API
            downloadTask.execute(url);

        }
    }


    public void moveCarOnMapOffline(LatLng newLocation) {
//        oldLocation latlng

        if (oldLocation != null && oldLocation.latitude != newLocation.latitude) {
            if (marker == null) {
                Bitmap bitmap = getMarkerBitmapFromView(R.drawable.avcar);
                if (bitmap == null) {
                    marker = mMap.addMarker(new MarkerOptions().position(newLocation)
                            .flat(true)
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                } else {
                    oldLocation = (LatLng) markerPoints.get(0);
                    marker = mMap.addMarker(new MarkerOptions().position(oldLocation)
                            .flat(true)
                            .icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
                }
            }
            try {
                rotateMarker(marker, (float) bearingBetweenLocations(marker.getPosition(), newLocation));
                animateMarkerNew(newLocation, marker);
            } catch (Exception e) {
            }
            oldLocation = newLocation;
        } else {

            if (marker == null) {
                Bitmap bitmap = getMarkerBitmapFromView(R.drawable.avcar);
                if (bitmap == null) {
                    marker = mMap.addMarker(new MarkerOptions().position(newLocation)
                            .flat(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.avcar)));
                } else {
                    marker = mMap.addMarker(new MarkerOptions().position(newLocation)
                            .flat(true)
                            .icon(BitmapDescriptorFactory.fromBitmap(bitmap)));
                }

                LatLngBounds.Builder builder = new LatLngBounds.Builder();

//the include method will calculate the min and max bound.
                builder.include(oldLocation);
                builder.include(newLocation);

                LatLngBounds bounds = builder.build();

                int width = getResources().getDisplayMetrics().widthPixels;
                int height = getResources().getDisplayMetrics().heightPixels;
                int padding = (int) (width * 0.10); // offset from edges of the map 10% of screen

                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                mMap.animateCamera(cu);
                mMap.animateCamera(cu);
               /* mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(marker.getPosition())
                        .zoom(15)
                        .build()));*/
            }
            oldLocation = newLocation;


        }


    }


    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        markerImageView.setImageResource(resId);
        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }


    private Float bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {
        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;
        double dLon = (long2 - long1);
        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);
        double brng = Math.atan2(y, x);
        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;
        return (float) brng;
    }


    private void rotateMarker(final Marker marker, final float toRotation) {
        if (!isMarkerRotating) {
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();
            final float startRotation = marker.getRotation();
            final long duration = 1000;
            final Interpolator interpolator = new LinearInterpolator();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    isMarkerRotating = true;
                    long elapsed = SystemClock.uptimeMillis() - start;
                    float t = interpolator.getInterpolation((float) elapsed / duration);
                    float rot = t * toRotation + (1 - t) * startRotation;
                    marker.setRotation(-rot > 180 ? rot / 2 : rot);
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 50);
                    } else {
                        isMarkerRotating = false;
                    }
                }
            });
        }
    }


    private void animateMarkerNew(final LatLng destination, final Marker marker) {
        try {
            if (marker != null) {
                final LatLng startPosition = marker.getPosition();
                final LatLng endPosition = new LatLng(destination.latitude, destination.longitude);
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .target(marker.getPosition())
                        .zoom(mMap.getCameraPosition().zoom)
                        .build()));
                final float startRotation = marker.getRotation();
                LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();
                ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
                valueAnimator.setDuration(8000); // duration 3 second
                valueAnimator.setInterpolator(new LinearInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        try {

                            float v = animation.getAnimatedFraction();
                            LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                            marker.setPosition(newPosition);
                            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                    .target(newPosition)
                                    .zoom(15.5f)
                                    .build()));


                        } catch (Exception ex) {
                            //I don't care atm..
//                            movingDrivers.remove(marker.getTag().toString());
                        }
                    }
                });
                valueAnimator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                });
                valueAnimator.start();
            }
        } catch (Exception e) {
        }
    }

    Marker destMarker = null;

    @Override
    public void run() {


        // mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dest, 15));
        callApiDriverDetails();
        handler.postDelayed(ActivityTrackingOrder.this, delay);
    }

    private void callApiDriverDetails() {


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<DriverTracking>> call = apiService.trackOrderByCustomerAndOrderId(currentUser.getId(), order.getId(), currentUser.getToken());

        call.enqueue(new Callback<WebResponse<DriverTracking>>() {
            @Override
            public void onResponse(Call<WebResponse<DriverTracking>> call, Response<WebResponse<DriverTracking>> response) {


                if (response.body().getData() == null) {
                    progressVisiblityGone();
                    return;
                }


                if (response.isSuccessful() && response.code() == 200) {


                    driverTracking = response.body().getData();
                    isFirstTime = false;

                    if (driverTracking.getDropoffLatitude() == null || driverTracking.getDropoffLatitude().equals("")) {
                        Toast.makeText(getApplicationContext(), masterPojo.getUnableToFindRoute(), Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (driverTracking.getDropoffLongitude() == null || driverTracking.getDropoffLongitude().equals("")) {
                        Toast.makeText(getApplicationContext(), masterPojo.getUnableToFindRoute(), Toast.LENGTH_LONG).show();
                        return;
                    }


                    if (driverTracking.getDriverLongitude() == null || driverTracking.getDriverLongitude().equals("")) {
                        Toast.makeText(getApplicationContext(), masterPojo.getUnableToFindRoute(), Toast.LENGTH_LONG).show();
                        return;
                    }


                    if (driverTracking.getDriverLatitude() == null || driverTracking.getDriverLatitude().equals("")) {
                        Toast.makeText(getApplicationContext(), masterPojo.getUnableToFindRoute(), Toast.LENGTH_LONG).show();
                        return;
                    }


                    if (driverTracking.getOrderStatus().equalsIgnoreCase("Completed")) {
                        hideMapWithStatus(masterPojo.getCompleted());
                        return;
                    }

                    if (driverTracking.getOrderStatus().equalsIgnoreCase("Delivered")) {
                        hideMapWithStatus(masterPojo.getDelivered());
                        return;
                    }

                    if (driverTracking.getOrderStatus().equalsIgnoreCase("Arrived")) {
                        hideMapWithStatus(masterPojo.getArrived());
                        return;
                    }


                    if (mMap != null) {
                       /* if (destMarker == null) {
                            LatLng latLng = new LatLng(driverTracking.getDropoffLatitude(),driverTracking.getDropoffLongitude());

                            destMarker = mMap.addMarker(new MarkerOptions().position(latLng)
                                    .flat(false)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                        }*/


                        if (driverTracking.getDriverLatitude() == 0 || driverTracking.getDriverLongitude() == 0) {
                            progressVisiblityGone();
                            Toast.makeText(getApplicationContext(), masterPojo.getUnableToFindRoute(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        setupRoute(driverTracking.getDriverLatitude(), driverTracking.getDriverLongitude(), driverTracking.getDropoffLatitude(), driverTracking.getDropoffLongitude());


                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<DriverTracking>> call, Throwable t) {
                progressVisiblityGone();

            }
        });


    }

    private void hideMapWithStatus(String text) {
        frameLayout.setVisibility(View.GONE);
        orderStatusLL.setVisibility(View.VISIBLE);
        orderStatusTv.setText(text);
        llEstimated.setVisibility(View.GONE);
    }


    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }


    private ValueAnimator polylineFlasher(GoogleMap mMap, List<LatLng> polyLineList) {
        try {
            Collections.reverse(polyLineList);
            polylineOptions = new PolylineOptions();
            polylineOptions.color(getApplication().getResources().getColor(R.color.black));
            polylineOptions.width(5);
            polylineOptions.startCap(new RoundCap());
            polylineOptions.endCap(new RoundCap());
            polylineOptions.jointType(ROUND);
            polylineOptions.addAll(polyLineList);
            greyPolyLine = mMap.addPolyline(polylineOptions);
            blackPolylineOptions = new PolylineOptions();
            blackPolylineOptions.width(7);
            blackPolylineOptions.color(getApplicationContext().getResources().getColor(R.color.colorPrimary));
            blackPolylineOptions.startCap(new RoundCap());
            blackPolylineOptions.endCap(new RoundCap());
            blackPolylineOptions.jointType(ROUND);
            blackPolylineOptions.addAll(polyLineList);
            blackPolyline = mMap.addPolyline(blackPolylineOptions);
            ValueAnimator polylineAnimator = ValueAnimator.ofInt(0, 100);
            polylineAnimator.setDuration(1000);
            polylineAnimator.setRepeatCount(ValueAnimator.INFINITE);
            polylineAnimator.setInterpolator(new LinearInterpolator());
            polylineAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    List<LatLng> points = greyPolyLine.getPoints();
                    int percentValue = (int) valueAnimator.getAnimatedValue();
                    int size = points.size();
                    int newPoints = (int) (size * (percentValue / 100.0f));
                    List<LatLng> p = points.subList(0, newPoints);
                    blackPolyline.setPoints(p);
                }
            });
            polylineAnimator.start();
            return polylineAnimator;
        } catch (Exception e) {
            return null;
        }
    }


    private void progressVisiblityVisible() {

        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);

    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }

            JSONObject jsonObject = null;
            try {

                String distance = "";
                duration = "";
                jsonObject = new JSONObject(data);

                JSONArray routesArray = jsonObject.getJSONArray("routes");

                JSONObject route = routesArray.getJSONObject(0);
                JSONArray legs = route.getJSONArray("legs");
                JSONObject leg = legs.getJSONObject(0);
                JSONObject distanceObj = leg.getJSONObject("distance");
                JSONObject durationObject = leg.getJSONObject("duration");
                distance = distanceObj.getString("value");

                duration = durationObject.getString("text");

                setText(estimateTime, duration);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

            if (duration == null || duration.isEmpty()) {
            } else {
                estimateTime.setText(duration + "");
            }

            if (driverTracking == null) {

                return;
            }


            Log.e("Getting", "Getting");
            LatLng latLng = new LatLng(driverTracking.getDriverLatitude(), driverTracking.getDriverLongitude());
            moveCarOnMapOffline(latLng);
        }
    }


    private void setText(final TextView text, final String duration) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                estimateTime.setText(duration);
            }
        });
    }


    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);


                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {

                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {

                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.GREEN);
                lineOptions.geodesic(true);

            }
            /*if (polyline != null) {
                polyline.remove();
            }*/


            if (lineOptions == null) {

                //  Toast.makeText(getApplicationContext(),"Unable to draw route",Toast.LENGTH_LONG).show();
                progressVisiblityGone();
                return;
            }

            polyline = mMap.addPolyline(lineOptions);

            progressVisiblityGone();

        }
    }

    Polyline polyline = null;

    private String getDirectionsUrl(LatLng origin, LatLng dest) {


        if (origin.latitude == 0) {
            return "";
        }

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String key = "key=" + appSettings.getWebsiteSettingData().getMapapi();
        ;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + key;
        ;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private void showDialog() {
        new AppRatingDialog.Builder()
                .setPositiveButtonText("Submit")
                .setNegativeButtonText("Cancel")
                .setNeutralButtonText("Later")
                .setNoteDescriptions(Arrays.asList("Very Bad", "Not good", "Quite ok", "Very Good", "Excellent !!!"))
                .setDefaultRating(2)
                .setThreshold(5)
                .setTitle("Rate your experience")
                .setDescription("Please select some stars and give your feedback")
                .setCommentInputEnabled(true)
                .setDefaultComment("I had a very good experience!")
                .setStarColor(R.color.colorPrimary)
                .setNoteDescriptionTextColor(R.color.grey)
                .setTitleTextColor(R.color.black)
                .setDescriptionTextColor(R.color.black)
                .setHint("Please write your comment here ...")
                .setHintTextColor(R.color.colorPrimary)
                .setCommentTextColor(R.color.black)
                .setCommentBackgroundColor(R.color.grey)
                .setDialogBackgroundColor(R.color.white)
                .setCancelable(false)
                .setCanceledOnTouchOutside(false)
                .create(ActivityTrackingOrder.this)
                .show();
    }

}