package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasterPojo {

   /* private String authFailed = "Authentication Failed";
    private String authSucess = "Authentication Success";
    private String signInSucess = "Signed In Success";
    private String signInFailed = "Signed In Failed";
    private String emptyEmail = "Email is empty!";
    private String invalidEmail = "Not a valid Email!";
    private String passwordIsEmpty = "Password is empty!";
    private String loginSucess = "Login Successfully";
    private String userAlreadyExist = "User Already Exist";
    private String welcome = "Welcome";
    private String signInToContinie = "Sign in to continue";
    private String emailAddress = "Email Address";
    private String password = "Password";
    private String forgetPassword = "Forget Password?";
    private String login = "Login";
    private String orLoginWith = "or login with";
    private String haveAccount = "Don't have account?";
    private String signUp = "Sign Up";
    private String headerForgetPass = "Enter email\n to reset password";
    private String submit = "Submit";
    private String cancel = "Cancel";

    //SignUp
    private String signUpWithEmai = "Sign up with your email and password";
    private String firstName = "First Name";
    private String lastName = "Last Name";
    private String mobileNumber = "Mobile Number";
    private String createAccount = "Create Account";
    private String alreadyHaveAcc = "Already have account?";
    private String firstNameEmpty = "Fist name is empty";
    private String lastNameEmpty = "Last name is empty";
    private String numberIsEmpty = "Number is empty";
    private String numberToShort = "Number too short";
    private String passWordEmpty = "Password is empty";
    private String passStrong = "Password must be strong";
    private String authCodeSent = "Auth code is sent to your number";

    //OtpVerify
    private String verfication = "Verification";
    private String verficationDesc = "We sent 4 digits code to your mobile number\nplease enter the code";
    private String confirm = "Confirm";
    private String didntReceive = "Didn\'t receive the code?";
    private String resend = "Resend";
    private String pleaseEnterOtp = "Please enter OTP";
    private String secsRemaining = "seconds remaining: ";


    //Home
    private String locationPermission = "Location Permission Required";
    private String manualLocationPermission = "Location access is required to auto detect your location otherwise you have to select your location manually.";
    private String oK = "OK";
    private String skip = "Skip";
    private String totalBill = "Total bill : ";
    private String orderId = "Order ID EC - ";
    private String selectStars = "Please select some stars and give your feedback";
    private String goodExperience = "I had a very good experience!";
    private String pleaseWrite = "Please write your comment here ...";
    private String noStore = "No Store Found";
    private String orderWhatYouLove = "Order what you love";

    //PlacePickerActivity
    private String unableToFindAddress = "Unable to find address. Please try again";
    private String done = "Done";

    //FragmentLeftMenu
    private String logutQustion = "Are you sure you want to logout?";
    private String yes = "Yes";
    private String loggedOut = "Logged Out!";
    private String no = "No";
    private String loggOut = "Logged Out";
    private String orders = "Orders";
    private String Profile = "Profile";
    private String logOut = "Logout";
    private String activeOrders = "Active Orders";
    private String pastOrders = "Past Orders";

    //ActiveOrders
    private String noOrdersFound = "No Orders Found";
    private String sureWantToCancel = "Are sure you want to cancel this?";
    private String canceled = "Canceled!";
    private String orderCancelledSucess = "Order Cancelled Sucessfully !";

    //AdapterPastActiveOrders
    private String pending = "Pending!";
    private String cancelled = "Cancelled";
    private String accepted = "Accepted";
    private String active = "Active";
    private String completed = "Completed";
    private String track = "Track";
    private String onRoute = "On Route";
    private String arrived = "Arrived";
    private String delivered = "Delivered";
    private String collected = "Collected";

    //ActivityOrderDetails
    private String orderDetails = "Order Details";
    private String estimateHeading = "Estimated Delivery";
    private String estimateTime = "45 Mins";
    private String transactionId = "Transaction Id";
    private String paymentMethod = "Payment Method";
    private String subTotal = "Sub Total";
    private String deliveryFee = "Delivery Fee";
    private String total = "Total";
    private String inclusiveTax = "(Include Tax)";

    //ActivityTracking
    private String restruantName = "Artigiano Restaurant";
    private String cash = "Cash";
    private String card = "Card";
    private String unableToFindRoute = "Unable to find route,driver location unavailable";

    //ActivityProfile
    private String profile = "Profile";
    private String save = "Save";
    private String addWork = "Add Work Address";
    private String addHome = "Add Home Address";
    private String phone = "Phone";
    private String youSure = "Are you sure?";
    private String wannaDelt = "You want to delete this address";
    private String work = "Work";
    private String home = "Home";
    private String addStreet = "You haven't added your street";
    private String doorNo = "Door No";

    //ActivityAllCategorie
    private String allCatrgories = "All Categories";
    private String search = "Search";
    private String textNoResultFound = "No Result Found";

    //ActivityRestruantMenu
    private String noItemFound = "No Item Found";

    //ActiivityProductDetails
    private String specialInstructions = "Special Instructions";
    private String avoidAnything = "Please let us know if we need to avoid anything";
    private String addSomething = "e.g Add something";
    private String required = "Required";
    private String addToCart = "Add To Cart";
    private String addQuantity = "Please Add Quantity";
    private String cantAddProduct = "Can't add this product";
    private String notSelected = "is not selected";
    private String cartClearSucess = "Cart cleared successfully!";

    //AddCardFragment
    private String transCompletedSucess = " Transaction Completed Successfully!";
    private String pleaseWait = " Please wait";
    private String payNow = " Pay Now";
    private String paymentCompleted = " Payment completed";
    private String paymentFailed = " Payment failed";

    //ActivityReviewPayment
    private String cart = " Cart";
    private String appName = " Papa Jol\'s";
    private String reviewPaymentNAddress = "Review Payment & Address";
    private String continueShopping = "Continue Shopping";
    private String cartEmpty = "Your cart is empty";
    private String mins =  " mins";

    //ActivityOrderPlaced
    private String continuee =  " Continue";
    private String orderConfirmed =  " Your order has been placed and\nwe will confirm you shortly.";
    private String thankYou =  " Thank you for order";

    //ActivityCheckOut
    private String checkOut =  "CheckOut";
    private String deliveryAddress =  "Delivery Address";
    private String useMyLocation =  "Use my current location";
    private String payByCash =  "Pay By Cash";
    private String payByCard =  "Pay By Debt / Credit Card";
    private String orderSummary =  "Order Summary";
    private String addInstructions =  "Add Instructions";
    private String placeOrder =  "Place Order";
    private String selectDeliveryAddress =  "Select your delivery address";
    private String orderPlacedSucess =  "Order placed sucessfully";
    private String numberNotAdded =  "You haven't added your number";


    //ActivityFilter
    private String filter =  "Filter";
    private String sort =  "Sort";
    private String priceRange =  "Price Range";
    private String dietary =  "Dietary";


    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public String getDietary() {
        return dietary;
    }

    public void setDietary(String dietary) {
        this.dietary = dietary;
    }

    public String getNumberNotAdded() {
        return numberNotAdded;
    }

    public void setNumberNotAdded(String numberNotAdded) {
        this.numberNotAdded = numberNotAdded;
    }

    public String getOrderPlacedSucess() {
        return orderPlacedSucess;
    }

    public void setOrderPlacedSucess(String orderPlacedSucess) {
        this.orderPlacedSucess = orderPlacedSucess;
    }

    public String getSelectDeliveryAddress() {
        return selectDeliveryAddress;
    }

    public void setSelectDeliveryAddress(String selectDeliveryAddress) {
        this.selectDeliveryAddress = selectDeliveryAddress;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getUseMyLocation() {
        return useMyLocation;
    }

    public void setUseMyLocation(String useMyLocation) {
        this.useMyLocation = useMyLocation;
    }

    public String getPayByCash() {
        return payByCash;
    }

    public void setPayByCash(String payByCash) {
        this.payByCash = payByCash;
    }

    public String getPayByCard() {
        return payByCard;
    }

    public void setPayByCard(String payByCard) {
        this.payByCard = payByCard;
    }

    public String getOrderSummary() {
        return orderSummary;
    }

    public void setOrderSummary(String orderSummary) {
        this.orderSummary = orderSummary;
    }

    public String getAddInstructions() {
        return addInstructions;
    }

    public void setAddInstructions(String addInstructions) {
        this.addInstructions = addInstructions;
    }

    public String getPlaceOrder() {
        return placeOrder;
    }

    public void setPlaceOrder(String placeOrder) {
        this.placeOrder = placeOrder;
    }

    public String getContinuee() {
        return continuee;
    }

    public void setContinuee(String continuee) {
        this.continuee = continuee;
    }

    public String getOrderConfirmed() {
        return orderConfirmed;
    }

    public void setOrderConfirmed(String orderConfirmed) {
        this.orderConfirmed = orderConfirmed;
    }

    public String getThankYou() {
        return thankYou;
    }

    public void setThankYou(String thankYou) {
        this.thankYou = thankYou;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getReviewPaymentNAddress() {
        return reviewPaymentNAddress;
    }

    public void setReviewPaymentNAddress(String reviewPaymentNAddress) {
        this.reviewPaymentNAddress = reviewPaymentNAddress;
    }

    public String getContinueShopping() {
        return continueShopping;
    }

    public void setContinueShopping(String continueShopping) {
        this.continueShopping = continueShopping;
    }

    public String getCartEmpty() {
        return cartEmpty;
    }

    public void setCartEmpty(String cartEmpty) {
        this.cartEmpty = cartEmpty;
    }

    public String getPaymentCompleted() {
        return paymentCompleted;
    }

    public void setPaymentCompleted(String paymentCompleted) {
        this.paymentCompleted = paymentCompleted;
    }

    public String getPaymentFailed() {
        return paymentFailed;
    }

    public void setPaymentFailed(String paymentFailed) {
        this.paymentFailed = paymentFailed;
    }

    public String getPayNow() {
        return payNow;
    }

    public void setPayNow(String payNow) {
        this.payNow = payNow;
    }

    public String getTransCompletedSucess() {
        return transCompletedSucess;
    }

    public void setTransCompletedSucess(String transCompletedSucess) {
        this.transCompletedSucess = transCompletedSucess;
    }

    public String getPleaseWait() {
        return pleaseWait;
    }

    public void setPleaseWait(String pleaseWait) {
        this.pleaseWait = pleaseWait;
    }

    public String getAddQuantity() {
        return addQuantity;
    }

    public void setAddQuantity(String addQuantity) {
        this.addQuantity = addQuantity;
    }

    public String getCantAddProduct() {
        return cantAddProduct;
    }

    public void setCantAddProduct(String cantAddProduct) {
        this.cantAddProduct = cantAddProduct;
    }

    public String getNotSelected() {
        return notSelected;
    }

    public void setNotSelected(String notSelected) {
        this.notSelected = notSelected;
    }

    public String getCartClearSucess() {
        return cartClearSucess;
    }

    public void setCartClearSucess(String cartClearSucess) {
        this.cartClearSucess = cartClearSucess;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getAddToCart() {
        return addToCart;
    }

    public void setAddToCart(String addToCart) {
        this.addToCart = addToCart;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public String getAvoidAnything() {
        return avoidAnything;
    }

    public void setAvoidAnything(String avoidAnything) {
        this.avoidAnything = avoidAnything;
    }

    public String getAddSomething() {
        return addSomething;
    }

    public void setAddSomething(String addSomething) {
        this.addSomething = addSomething;
    }

    public String getNoItemFound() {
        return noItemFound;
    }

    public void setNoItemFound(String noItemFound) {
        this.noItemFound = noItemFound;
    }

    public String getTextNoResultFound() {
        return textNoResultFound;
    }

    public void setTextNoResultFound(String textNoResultFound) {
        this.textNoResultFound = textNoResultFound;
    }

    public String getAllCatrgories() {
        return allCatrgories;
    }

    public void setAllCatrgories(String allCatrgories) {
        this.allCatrgories = allCatrgories;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getDoorNo() {
        return doorNo;
    }

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    public String getAddStreet() {
        return addStreet;
    }

    public void setAddStreet(String addStreet) {
        this.addStreet = addStreet;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getYouSure() {
        return youSure;
    }

    public void setYouSure(String youSure) {
        this.youSure = youSure;
    }

    public String getWannaDelt() {
        return wannaDelt;
    }

    public void setWannaDelt(String wannaDelt) {
        this.wannaDelt = wannaDelt;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddWork() {
        return addWork;
    }

    public void setAddWork(String addWork) {
        this.addWork = addWork;
    }

    public String getAddHome() {
        return addHome;
    }

    public void setAddHome(String addHome) {
        this.addHome = addHome;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getUnableToFindRoute() {
        return unableToFindRoute;
    }

    public void setUnableToFindRoute(String unableToFindRoute) {
        this.unableToFindRoute = unableToFindRoute;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getRestruantName() {
        return restruantName;
    }

    public void setRestruantName(String restruantName) {
        this.restruantName = restruantName;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getEstimateHeading() {
        return estimateHeading;
    }

    public void setEstimateHeading(String estimateHeading) {
        this.estimateHeading = estimateHeading;
    }

    public String getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(String estimateTime) {
        this.estimateTime = estimateTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getInclusiveTax() {
        return inclusiveTax;
    }

    public void setInclusiveTax(String inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getAccepted() {
        return accepted;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getOnRoute() {
        return onRoute;
    }

    public void setOnRoute(String onRoute) {
        this.onRoute = onRoute;
    }

    public String getArrived() {
        return arrived;
    }

    public void setArrived(String arrived) {
        this.arrived = arrived;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getCollected() {
        return collected;
    }

    public void setCollected(String collected) {
        this.collected = collected;
    }

    public String getSureWantToCancel() {
        return sureWantToCancel;
    }

    public void setSureWantToCancel(String sureWantToCancel) {
        this.sureWantToCancel = sureWantToCancel;
    }

    public String getCanceled() {
        return canceled;
    }

    public void setCanceled(String canceled) {
        this.canceled = canceled;
    }

    public String getOrderCancelledSucess() {
        return orderCancelledSucess;
    }

    public void setOrderCancelledSucess(String orderCancelledSucess) {
        this.orderCancelledSucess = orderCancelledSucess;
    }

    public String getNoOrdersFound() {
        return noOrdersFound;
    }

    public void setNoOrdersFound(String noOrdersFound) {
        this.noOrdersFound = noOrdersFound;
    }

    public String getPastOrders() {
        return pastOrders;
    }

    public void setPastOrders(String pastOrders) {
        this.pastOrders = pastOrders;
    }

    public String getActiveOrders() {
        return activeOrders;
    }

    public void setActiveOrders(String activeOrders) {
        this.activeOrders = activeOrders;
    }

    public String getLogutQustion() {
        return logutQustion;
    }

    public void setLogutQustion(String logutQustion) {
        this.logutQustion = logutQustion;
    }

    public String getYes() {
        return yes;
    }

    public void setYes(String yes) {
        this.yes = yes;
    }

    public String getLoggedOut() {
        return loggedOut;
    }

    public void setLoggedOut(String loggedOut) {
        this.loggedOut = loggedOut;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getLoggOut() {
        return loggOut;
    }

    public void setLoggOut(String loggOut) {
        this.loggOut = loggOut;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getProfile() {
        return Profile;
    }

    public void setProfile(String profile) {
        Profile = profile;
    }

    public String getLogOut() {
        return logOut;
    }

    public void setLogOut(String logOut) {
        this.logOut = logOut;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    public String getUnableToFindAddress() {
        return unableToFindAddress;
    }

    public void setUnableToFindAddress(String unableToFindAddress) {
        this.unableToFindAddress = unableToFindAddress;
    }

    public String getOrderWhatYouLove() {
        return orderWhatYouLove;
    }

    public void setOrderWhatYouLove(String orderWhatYouLove) {
        this.orderWhatYouLove = orderWhatYouLove;
    }

    public String getNoStore() {
        return noStore;
    }

    public void setNoStore(String noStore) {
        this.noStore = noStore;
    }

    public String getLocationPermission() {
        return locationPermission;
    }

    public void setLocationPermission(String locationPermission) {
        this.locationPermission = locationPermission;
    }

    public String getManualLocationPermission() {
        return manualLocationPermission;
    }

    public void setManualLocationPermission(String manualLocationPermission) {
        this.manualLocationPermission = manualLocationPermission;
    }

    public String getoK() {
        return oK;
    }

    public void setoK(String oK) {
        this.oK = oK;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(String totalBill) {
        this.totalBill = totalBill;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSelectStars() {
        return selectStars;
    }

    public void setSelectStars(String selectStars) {
        this.selectStars = selectStars;
    }

    public String getGoodExperience() {
        return goodExperience;
    }

    public void setGoodExperience(String goodExperience) {
        this.goodExperience = goodExperience;
    }

    public String getPleaseWrite() {
        return pleaseWrite;
    }

    public void setPleaseWrite(String pleaseWrite) {
        this.pleaseWrite = pleaseWrite;
    }

    public String getSecsRemaining() {
        return secsRemaining;
    }

    public void setSecsRemaining(String secsRemaining) {
        this.secsRemaining = secsRemaining;
    }

    public String getVerfication() {
        return verfication;
    }

    public void setVerfication(String verfication) {
        this.verfication = verfication;
    }

    public String getVerficationDesc() {
        return verficationDesc;
    }

    public void setVerficationDesc(String verficationDesc) {
        this.verficationDesc = verficationDesc;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getDidntReceive() {
        return didntReceive;
    }

    public void setDidntReceive(String didntReceive) {
        this.didntReceive = didntReceive;
    }

    public String getResend() {
        return resend;
    }

    public void setResend(String resend) {
        this.resend = resend;
    }

    public String getPleaseEnterOtp() {
        return pleaseEnterOtp;
    }

    public void setPleaseEnterOtp(String pleaseEnterOtp) {
        this.pleaseEnterOtp = pleaseEnterOtp;
    }

    public String getFirstNameEmpty() {
        return firstNameEmpty;
    }

    public void setFirstNameEmpty(String firstNameEmpty) {
        this.firstNameEmpty = firstNameEmpty;
    }

    public String getLastNameEmpty() {
        return lastNameEmpty;
    }

    public void setLastNameEmpty(String lastNameEmpty) {
        this.lastNameEmpty = lastNameEmpty;
    }

    public String getNumberIsEmpty() {
        return numberIsEmpty;
    }

    public void setNumberIsEmpty(String numberIsEmpty) {
        this.numberIsEmpty = numberIsEmpty;
    }

    public String getNumberToShort() {
        return numberToShort;
    }

    public void setNumberToShort(String numberToShort) {
        this.numberToShort = numberToShort;
    }

    public String getPassWordEmpty() {
        return passWordEmpty;
    }

    public void setPassWordEmpty(String passWordEmpty) {
        this.passWordEmpty = passWordEmpty;
    }

    public String getPassStrong() {
        return passStrong;
    }

    public void setPassStrong(String passStrong) {
        this.passStrong = passStrong;
    }

    public String getAuthCodeSent() {
        return authCodeSent;
    }

    public void setAuthCodeSent(String authCodeSent) {
        this.authCodeSent = authCodeSent;
    }

    public String getSignUpWithEmai() {
        return signUpWithEmai;
    }

    public void setSignUpWithEmai(String signUpWithEmai) {
        this.signUpWithEmai = signUpWithEmai;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCreateAccount() {
        return createAccount;
    }

    public void setCreateAccount(String createAccount) {
        this.createAccount = createAccount;
    }

    public String getAlreadyHaveAcc() {
        return alreadyHaveAcc;
    }

    public void setAlreadyHaveAcc(String alreadyHaveAcc) {
        this.alreadyHaveAcc = alreadyHaveAcc;
    }

    public String getHeaderForgetPass() {
        return headerForgetPass;
    }

    public void setHeaderForgetPass(String headerForgetPass) {
        this.headerForgetPass = headerForgetPass;
    }

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getWelcome() {
        return welcome;
    }

    public void setWelcome(String welcome) {
        this.welcome = welcome;
    }

    public String getSignInToContinie() {
        return signInToContinie;
    }

    public void setSignInToContinie(String signInToContinie) {
        this.signInToContinie = signInToContinie;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getForgetPassword() {
        return forgetPassword;
    }

    public void setForgetPassword(String forgetPassword) {
        this.forgetPassword = forgetPassword;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getOrLoginWith() {
        return orLoginWith;
    }

    public void setOrLoginWith(String orLoginWith) {
        this.orLoginWith = orLoginWith;
    }

    public String getHaveAccount() {
        return haveAccount;
    }

    public void setHaveAccount(String haveAccount) {
        this.haveAccount = haveAccount;
    }

    public String getSignUp() {
        return signUp;
    }

    public void setSignUp(String signUp) {
        this.signUp = signUp;
    }

    public String getAuthSucess() {
        return authSucess;
    }

    public void setAuthSucess(String authSucess) {
        this.authSucess = authSucess;
    }

    public String getAuthFailed() {
        return authFailed;
    }

    public void setAuthFailed(String authFailed) {
        this.authFailed = authFailed;
    }

    public String getSignInSucess() {
        return signInSucess;
    }

    public void setSignInSucess(String signInSucess) {
        this.signInSucess = signInSucess;
    }

    public String getSignInFailed() {
        return signInFailed;
    }

    public void setSignInFailed(String signInFailed) {
        this.signInFailed = signInFailed;
    }

    public String getEmptyEmail() {
        return emptyEmail;
    }

    public void setEmptyEmail(String emptyEmail) {
        this.emptyEmail = emptyEmail;
    }

    public String getInvalidEmail() {
        return invalidEmail;
    }

    public void setInvalidEmail(String invalidEmail) {
        this.invalidEmail = invalidEmail;
    }

    public String getPasswordIsEmpty() {
        return passwordIsEmpty;
    }

    public void setPasswordIsEmpty(String passwordIsEmpty) {
        this.passwordIsEmpty = passwordIsEmpty;
    }

    public String getLoginSucess() {
        return loginSucess;
    }

    public void setLoginSucess(String loginSucess) {
        this.loginSucess = loginSucess;
    }

    public String getUserAlreadyExist() {
        return userAlreadyExist;
    }

    public void setUserAlreadyExist(String userAlreadyExist) {
        this.userAlreadyExist = userAlreadyExist;
    }*/


    @SerializedName("authFailed")
    @Expose
    private String authFailed;
    @SerializedName("authSucess")
    @Expose
    private String authSucess;
    @SerializedName("signInSucess")
    @Expose
    private String signInSucess;
    @SerializedName("signInFailed")
    @Expose
    private String signInFailed;
    @SerializedName("emptyEmail")
    @Expose
    private String emptyEmail;
    @SerializedName("invalidEmail")
    @Expose
    private String invalidEmail;
    @SerializedName("passwordIsEmpty")
    @Expose
    private String passwordIsEmpty;
    @SerializedName("loginSucess")
    @Expose
    private String loginSucess;
    @SerializedName("userAlreadyExist")
    @Expose
    private String userAlreadyExist;
    @SerializedName("welcome")
    @Expose
    private String welcome;
    @SerializedName("signInToContinie")
    @Expose
    private String signInToContinie;
    @SerializedName("emailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("forgetPassword")
    @Expose
    private String forgetPassword;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("orLoginWith")
    @Expose
    private String orLoginWith;
    @SerializedName("haveAccount")
    @Expose
    private String haveAccount;
    @SerializedName("signUp")
    @Expose
    private String signUp;
    @SerializedName("headerForgetPass")
    @Expose
    private String headerForgetPass;
    @SerializedName("submit")
    @Expose
    private String submit;
    @SerializedName("cancel")
    @Expose
    private String cancel;
    @SerializedName("signUpWithEmail")
    @Expose
    private String signUpWithEmail;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("mobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("createAccount")
    @Expose
    private String createAccount;
    @SerializedName("alreadyHaveAcc")
    @Expose
    private String alreadyHaveAcc;
    @SerializedName("firstNameEmpty")
    @Expose
    private String firstNameEmpty;
    @SerializedName("lastNameEmpty")
    @Expose
    private String lastNameEmpty;
    @SerializedName("numberIsEmpty")
    @Expose
    private String numberIsEmpty;
    @SerializedName("numberToShort")
    @Expose
    private String numberToShort;
    @SerializedName("passWordEmpty")
    @Expose
    private String passWordEmpty;
    @SerializedName("passStrong")
    @Expose
    private String passStrong;
    @SerializedName("authCodeSent")
    @Expose
    private String authCodeSent;
    @SerializedName("verfication")
    @Expose
    private String verfication;
    @SerializedName("verficationDesc")
    @Expose
    private String verficationDesc;
    @SerializedName("confirm")
    @Expose
    private String confirm;
    @SerializedName("didntReceive")
    @Expose
    private String didntReceive;
    @SerializedName("resend")
    @Expose
    private String resend;
    @SerializedName("pleaseEnterOtp")
    @Expose
    private String pleaseEnterOtp;
    @SerializedName("secsRemaining")
    @Expose
    private String secsRemaining;
    @SerializedName("locationPermission")
    @Expose
    private String locationPermission;
    @SerializedName("manualLocationPermission")
    @Expose
    private String manualLocationPermission;
    @SerializedName("oK")
    @Expose
    private String oK;
    @SerializedName("skip")
    @Expose
    private String skip;
    @SerializedName("totalBill")
    @Expose
    private String totalBill;
    @SerializedName("OrderId")
    @Expose
    private String orderId;
    @SerializedName("selectStars")
    @Expose
    private String selectStars;
    @SerializedName("goodExperience")
    @Expose
    private String goodExperience;
    @SerializedName("pleaseWrite")
    @Expose
    private String pleaseWrite;
    @SerializedName("noStore")
    @Expose
    private String noStore;
    @SerializedName("orderWhatYouLove")
    @Expose
    private String orderWhatYouLove;
    @SerializedName("unableToFindAddress")
    @Expose
    private String unableToFindAddress;
    @SerializedName("done")
    @Expose
    private String done;
    @SerializedName("logutQustion")
    @Expose
    private String logutQustion;
    @SerializedName("yes")
    @Expose
    private String yes;
    @SerializedName("loggedOut")
    @Expose
    private String loggedOut;
    @SerializedName("no")
    @Expose
    private String no;
    @SerializedName("loggOut")
    @Expose
    private String loggOut;
    @SerializedName("orders")
    @Expose
    private String orders;
    @SerializedName("logOut")
    @Expose
    private String logOut;
    @SerializedName("activeOrders")
    @Expose
    private String activeOrders;
    @SerializedName("pastOrders")
    @Expose
    private String pastOrders;
    @SerializedName("noOrdersFound")
    @Expose
    private String noOrdersFound;
    @SerializedName("sureWantToCancel")
    @Expose
    private String sureWantToCancel;
    @SerializedName("canceled")
    @Expose
    private String canceled;
    @SerializedName("orderCancelledSucess")
    @Expose
    private String orderCancelledSucess;
    @SerializedName("pending")
    @Expose
    private String pending;
    @SerializedName("cancelled")
    @Expose
    private String cancelled;
    @SerializedName("accepted")
    @Expose
    private String accepted;
    @SerializedName("active")
    @Expose
    private String active;
    @SerializedName("completed")
    @Expose
    private String completed;
    @SerializedName("track")
    @Expose
    private String track;
    @SerializedName("onRoute")
    @Expose
    private String onRoute;
    @SerializedName("arrived")
    @Expose
    private String arrived;
    @SerializedName("delivered")
    @Expose
    private String delivered;
    @SerializedName("collected")
    @Expose
    private String collected;
    @SerializedName("orderDetails")
    @Expose
    private String orderDetails;
    @SerializedName("estimateHeading")
    @Expose
    private String estimateHeading;
    @SerializedName("estimateTime")
    @Expose
    private String estimateTime;
    @SerializedName("transactionId")
    @Expose
    private String transactionId;
    @SerializedName("paymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("subTotal")
    @Expose
    private String subTotal;
    @SerializedName("deliveryFee")
    @Expose
    private String deliveryFee;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("inclusiveTax")
    @Expose
    private String inclusiveTax;
    @SerializedName("restruantName")
    @Expose
    private String restruantName;
    @SerializedName("cash")
    @Expose
    private String cash;
    @SerializedName("card")
    @Expose
    private String card;
    @SerializedName("unableToFindRoute")
    @Expose
    private String unableToFindRoute;
    @SerializedName("profile")
    @Expose
    private String profile;
    @SerializedName("save")
    @Expose
    private String save;
    @SerializedName("addWork")
    @Expose
    private String addWork;
    @SerializedName("addHome")
    @Expose
    private String addHome;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("youSure")
    @Expose
    private String youSure;
    @SerializedName("wannaDelt")
    @Expose
    private String wannaDelt;
    @SerializedName("work")
    @Expose
    private String work;
    @SerializedName("home")
    @Expose
    private String home;
    @SerializedName("addStreet")
    @Expose
    private String addStreet;
    @SerializedName("doorNo")
    @Expose
    private String doorNo;
    @SerializedName("allCatrgories")
    @Expose
    private String allCatrgories;
    @SerializedName("search")
    @Expose
    private String search;
    @SerializedName("textNoResultFound")
    @Expose
    private String textNoResultFound;
    @SerializedName("noItemFound")
    @Expose
    private String noItemFound;
    @SerializedName("specialInstructions")
    @Expose
    private String specialInstructions;
    @SerializedName("avoidAnything")
    @Expose
    private String avoidAnything;
    @SerializedName("addSomething")
    @Expose
    private String addSomething;
    @SerializedName("required")
    @Expose
    private String required;
    @SerializedName("addToCart")
    @Expose
    private String addToCart;
    @SerializedName("addQuantity")
    @Expose
    private String addQuantity;
    @SerializedName("cantAddProduct")
    @Expose
    private String cantAddProduct;
    @SerializedName("notSelected")
    @Expose
    private String notSelected;
    @SerializedName("cartClearSucess")
    @Expose
    private String cartClearSucess;
    @SerializedName("transCompletedSucess")
    @Expose
    private String transCompletedSucess;
    @SerializedName("pleaseWait")
    @Expose
    private String pleaseWait;
    @SerializedName("payNow")
    @Expose
    private String payNow;
    @SerializedName("paymentCompleted")
    @Expose
    private String paymentCompleted;
    @SerializedName("paymentFailed")
    @Expose
    private String paymentFailed;
    @SerializedName("cart")
    @Expose
    private String cart;
    @SerializedName("appName")
    @Expose
    private String appName;
    @SerializedName("reviewPaymentNAddress")
    @Expose
    private String reviewPaymentNAddress;
    @SerializedName("continueShopping")
    @Expose
    private String continueShopping;
    @SerializedName("cartEmpty")
    @Expose
    private String cartEmpty;
    @SerializedName("mins")
    @Expose
    private String mins;
    @SerializedName("continuee")
    @Expose
    private String continuee;
    @SerializedName("orderConfirmed")
    @Expose
    private String orderConfirmed;
    @SerializedName("thankYou")
    @Expose
    private String thankYou;
    @SerializedName("checkOut")
    @Expose
    private String checkOut;
    @SerializedName("deliveryAddress")
    @Expose
    private String deliveryAddress;
    @SerializedName("useMyLocation")
    @Expose
    private String useMyLocation;
    @SerializedName("payByCash")
    @Expose
    private String payByCash;
    @SerializedName("payByCard")
    @Expose
    private String payByCard;
    @SerializedName("orderSummary")
    @Expose
    private String orderSummary;
    @SerializedName("addInstructions")
    @Expose
    private String addInstructions;
    @SerializedName("placeOrder")
    @Expose
    private String placeOrder;
    @SerializedName("selectDeliveryAddress")
    @Expose
    private String selectDeliveryAddress;
    @SerializedName("orderPlacedSucess")
    @Expose
    private String orderPlacedSucess;
    @SerializedName("numberNotAdded")
    @Expose
    private String numberNotAdded;
    @SerializedName("filter")
    @Expose
    private String filter;
    @SerializedName("sort")
    @Expose
    private String sort;
    @SerializedName("priceRange")
    @Expose
    private String priceRange;
    @SerializedName("dietary")
    @Expose
    private String dietary;

    @SerializedName("settings")
    @Expose
    private String settings;

    @SerializedName("noCategoriesAvailable")
    @Expose
    private String noCategoriesAvailable;


    @SerializedName("noProductsAvailable")
    @Expose
    private String noProductsAvailable;


    @SerializedName("noStoresAvailable")
    @Expose
    private String noStoresAvailable;



    @SerializedName("adddoorno")
    @Expose
    private String adddoorno;



    public String getAdddoorno() {
        return adddoorno;
    }

    public void setAdddoorno(String adddoorno) {
        this.adddoorno = adddoorno;
    }

    public String getNoCategoriesAvailable() {
        return noCategoriesAvailable;
    }

    public void setNoCategoriesAvailable(String noCategoriesAvailable) {
        this.noCategoriesAvailable = noCategoriesAvailable;
    }

    public String getNoProductsAvailable() {
        return noProductsAvailable;
    }

    public void setNoProductsAvailable(String noProductsAvailable) {
        this.noProductsAvailable = noProductsAvailable;
    }

    public String getNoStoresAvailable() {
        return noStoresAvailable;
    }

    public void setNoStoresAvailable(String noStoresAvailable) {
        this.noStoresAvailable = noStoresAvailable;
    }

    public String getSettings() {
        return settings;
    }

    public void setSettings(String settings) {
        this.settings = settings;
    }

    public String getAuthFailed() {
        return authFailed;
    }

    public void setAuthFailed(String authFailed) {
        this.authFailed = authFailed;
    }

    public String getAuthSucess() {
        return authSucess;
    }

    public void setAuthSucess(String authSucess) {
        this.authSucess = authSucess;
    }

    public String getSignInSucess() {
        return signInSucess;
    }

    public void setSignInSucess(String signInSucess) {
        this.signInSucess = signInSucess;
    }

    public String getSignInFailed() {
        return signInFailed;
    }

    public void setSignInFailed(String signInFailed) {
        this.signInFailed = signInFailed;
    }

    public String getEmptyEmail() {
        return emptyEmail;
    }

    public void setEmptyEmail(String emptyEmail) {
        this.emptyEmail = emptyEmail;
    }

    public String getInvalidEmail() {
        return invalidEmail;
    }

    public void setInvalidEmail(String invalidEmail) {
        this.invalidEmail = invalidEmail;
    }

    public String getPasswordIsEmpty() {
        return passwordIsEmpty;
    }

    public void setPasswordIsEmpty(String passwordIsEmpty) {
        this.passwordIsEmpty = passwordIsEmpty;
    }

    public String getLoginSucess() {
        return loginSucess;
    }

    public void setLoginSucess(String loginSucess) {
        this.loginSucess = loginSucess;
    }

    public String getUserAlreadyExist() {
        return userAlreadyExist;
    }

    public void setUserAlreadyExist(String userAlreadyExist) {
        this.userAlreadyExist = userAlreadyExist;
    }

    public String getWelcome() {
        return welcome;
    }

    public void setWelcome(String welcome) {
        this.welcome = welcome;
    }

    public String getSignInToContinie() {
        return signInToContinie;
    }

    public void setSignInToContinie(String signInToContinie) {
        this.signInToContinie = signInToContinie;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getForgetPassword() {
        return forgetPassword;
    }

    public void setForgetPassword(String forgetPassword) {
        this.forgetPassword = forgetPassword;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getOrLoginWith() {
        return orLoginWith;
    }

    public void setOrLoginWith(String orLoginWith) {
        this.orLoginWith = orLoginWith;
    }

    public String getHaveAccount() {
        return haveAccount;
    }

    public void setHaveAccount(String haveAccount) {
        this.haveAccount = haveAccount;
    }

    public String getSignUp() {
        return signUp;
    }

    public void setSignUp(String signUp) {
        this.signUp = signUp;
    }

    public String getHeaderForgetPass() {
        return headerForgetPass;
    }

    public void setHeaderForgetPass(String headerForgetPass) {
        this.headerForgetPass = headerForgetPass;
    }

    public String getSubmit() {
        return submit;
    }

    public void setSubmit(String submit) {
        this.submit = submit;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getSignUpWithEmail() {
        return signUpWithEmail;
    }

    public void setSignUpWithEmail(String signUpWithEmail) {
        this.signUpWithEmail = signUpWithEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCreateAccount() {
        return createAccount;
    }

    public void setCreateAccount(String createAccount) {
        this.createAccount = createAccount;
    }

    public String getAlreadyHaveAcc() {
        return alreadyHaveAcc;
    }

    public void setAlreadyHaveAcc(String alreadyHaveAcc) {
        this.alreadyHaveAcc = alreadyHaveAcc;
    }

    public String getFirstNameEmpty() {
        return firstNameEmpty;
    }

    public void setFirstNameEmpty(String firstNameEmpty) {
        this.firstNameEmpty = firstNameEmpty;
    }

    public String getLastNameEmpty() {
        return lastNameEmpty;
    }

    public void setLastNameEmpty(String lastNameEmpty) {
        this.lastNameEmpty = lastNameEmpty;
    }

    public String getNumberIsEmpty() {
        return numberIsEmpty;
    }

    public void setNumberIsEmpty(String numberIsEmpty) {
        this.numberIsEmpty = numberIsEmpty;
    }

    public String getNumberToShort() {
        return numberToShort;
    }

    public void setNumberToShort(String numberToShort) {
        this.numberToShort = numberToShort;
    }

    public String getPassWordEmpty() {
        return passWordEmpty;
    }

    public void setPassWordEmpty(String passWordEmpty) {
        this.passWordEmpty = passWordEmpty;
    }

    public String getPassStrong() {
        return passStrong;
    }

    public void setPassStrong(String passStrong) {
        this.passStrong = passStrong;
    }

    public String getAuthCodeSent() {
        return authCodeSent;
    }

    public void setAuthCodeSent(String authCodeSent) {
        this.authCodeSent = authCodeSent;
    }

    public String getVerfication() {
        return verfication;
    }

    public void setVerfication(String verfication) {
        this.verfication = verfication;
    }

    public String getVerficationDesc() {
        return verficationDesc;
    }

    public void setVerficationDesc(String verficationDesc) {
        this.verficationDesc = verficationDesc;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getDidntReceive() {
        return didntReceive;
    }

    public void setDidntReceive(String didntReceive) {
        this.didntReceive = didntReceive;
    }

    public String getResend() {
        return resend;
    }

    public void setResend(String resend) {
        this.resend = resend;
    }

    public String getPleaseEnterOtp() {
        return pleaseEnterOtp;
    }

    public void setPleaseEnterOtp(String pleaseEnterOtp) {
        this.pleaseEnterOtp = pleaseEnterOtp;
    }

    public String getSecsRemaining() {
        return secsRemaining;
    }

    public void setSecsRemaining(String secsRemaining) {
        this.secsRemaining = secsRemaining;
    }

    public String getLocationPermission() {
        return locationPermission;
    }

    public void setLocationPermission(String locationPermission) {
        this.locationPermission = locationPermission;
    }

    public String getManualLocationPermission() {
        return manualLocationPermission;
    }

    public void setManualLocationPermission(String manualLocationPermission) {
        this.manualLocationPermission = manualLocationPermission;
    }

    public String getoK() {
        return oK;
    }

    public void setoK(String oK) {
        this.oK = oK;
    }

    public String getSkip() {
        return skip;
    }

    public void setSkip(String skip) {
        this.skip = skip;
    }

    public String getTotalBill() {
        return totalBill;
    }

    public void setTotalBill(String totalBill) {
        this.totalBill = totalBill;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getSelectStars() {
        return selectStars;
    }

    public void setSelectStars(String selectStars) {
        this.selectStars = selectStars;
    }

    public String getGoodExperience() {
        return goodExperience;
    }

    public void setGoodExperience(String goodExperience) {
        this.goodExperience = goodExperience;
    }

    public String getPleaseWrite() {
        return pleaseWrite;
    }

    public void setPleaseWrite(String pleaseWrite) {
        this.pleaseWrite = pleaseWrite;
    }

    public String getNoStore() {
        return noStore;
    }

    public void setNoStore(String noStore) {
        this.noStore = noStore;
    }

    public String getOrderWhatYouLove() {
        return orderWhatYouLove;
    }

    public void setOrderWhatYouLove(String orderWhatYouLove) {
        this.orderWhatYouLove = orderWhatYouLove;
    }

    public String getUnableToFindAddress() {
        return unableToFindAddress;
    }

    public void setUnableToFindAddress(String unableToFindAddress) {
        this.unableToFindAddress = unableToFindAddress;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    public String getLogutQustion() {
        return logutQustion;
    }

    public void setLogutQustion(String logutQustion) {
        this.logutQustion = logutQustion;
    }

    public String getYes() {
        return yes;
    }

    public void setYes(String yes) {
        this.yes = yes;
    }

    public String getLoggedOut() {
        return loggedOut;
    }

    public void setLoggedOut(String loggedOut) {
        this.loggedOut = loggedOut;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getLoggOut() {
        return loggOut;
    }

    public void setLoggOut(String loggOut) {
        this.loggOut = loggOut;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public String getLogOut() {
        return logOut;
    }

    public void setLogOut(String logOut) {
        this.logOut = logOut;
    }

    public String getActiveOrders() {
        return activeOrders;
    }

    public void setActiveOrders(String activeOrders) {
        this.activeOrders = activeOrders;
    }

    public String getPastOrders() {
        return pastOrders;
    }

    public void setPastOrders(String pastOrders) {
        this.pastOrders = pastOrders;
    }

    public String getNoOrdersFound() {
        return noOrdersFound;
    }

    public void setNoOrdersFound(String noOrdersFound) {
        this.noOrdersFound = noOrdersFound;
    }

    public String getSureWantToCancel() {
        return sureWantToCancel;
    }

    public void setSureWantToCancel(String sureWantToCancel) {
        this.sureWantToCancel = sureWantToCancel;
    }

    public String getCanceled() {
        return canceled;
    }

    public void setCanceled(String canceled) {
        this.canceled = canceled;
    }

    public String getOrderCancelledSucess() {
        return orderCancelledSucess;
    }

    public void setOrderCancelledSucess(String orderCancelledSucess) {
        this.orderCancelledSucess = orderCancelledSucess;
    }

    public String getPending() {
        return pending;
    }

    public void setPending(String pending) {
        this.pending = pending;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    public String getAccepted() {
        return accepted;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public String getTrack() {
        return track;
    }

    public void setTrack(String track) {
        this.track = track;
    }

    public String getOnRoute() {
        return onRoute;
    }

    public void setOnRoute(String onRoute) {
        this.onRoute = onRoute;
    }

    public String getArrived() {
        return arrived;
    }

    public void setArrived(String arrived) {
        this.arrived = arrived;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getCollected() {
        return collected;
    }

    public void setCollected(String collected) {
        this.collected = collected;
    }

    public String getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(String orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getEstimateHeading() {
        return estimateHeading;
    }

    public void setEstimateHeading(String estimateHeading) {
        this.estimateHeading = estimateHeading;
    }

    public String getEstimateTime() {
        return estimateTime;
    }

    public void setEstimateTime(String estimateTime) {
        this.estimateTime = estimateTime;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public String getDeliveryFee() {
        return deliveryFee;
    }

    public void setDeliveryFee(String deliveryFee) {
        this.deliveryFee = deliveryFee;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getInclusiveTax() {
        return inclusiveTax;
    }

    public void setInclusiveTax(String inclusiveTax) {
        this.inclusiveTax = inclusiveTax;
    }

    public String getRestruantName() {
        return restruantName;
    }

    public void setRestruantName(String restruantName) {
        this.restruantName = restruantName;
    }

    public String getCash() {
        return cash;
    }

    public void setCash(String cash) {
        this.cash = cash;
    }

    public String getCard() {
        return card;
    }

    public void setCard(String card) {
        this.card = card;
    }

    public String getUnableToFindRoute() {
        return unableToFindRoute;
    }

    public void setUnableToFindRoute(String unableToFindRoute) {
        this.unableToFindRoute = unableToFindRoute;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getAddWork() {
        return addWork;
    }

    public void setAddWork(String addWork) {
        this.addWork = addWork;
    }

    public String getAddHome() {
        return addHome;
    }

    public void setAddHome(String addHome) {
        this.addHome = addHome;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getYouSure() {
        return youSure;
    }

    public void setYouSure(String youSure) {
        this.youSure = youSure;
    }

    public String getWannaDelt() {
        return wannaDelt;
    }

    public void setWannaDelt(String wannaDelt) {
        this.wannaDelt = wannaDelt;
    }

    public String getWork() {
        return work;
    }

    public void setWork(String work) {
        this.work = work;
    }

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getAddStreet() {
        return addStreet;
    }

    public void setAddStreet(String addStreet) {
        this.addStreet = addStreet;
    }

    public String getDoorNo() {
        return doorNo;
    }

    public void setDoorNo(String doorNo) {
        this.doorNo = doorNo;
    }

    public String getAllCatrgories() {
        return allCatrgories;
    }

    public void setAllCatrgories(String allCatrgories) {
        this.allCatrgories = allCatrgories;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getTextNoResultFound() {
        return textNoResultFound;
    }

    public void setTextNoResultFound(String textNoResultFound) {
        this.textNoResultFound = textNoResultFound;
    }

    public String getNoItemFound() {
        return noItemFound;
    }

    public void setNoItemFound(String noItemFound) {
        this.noItemFound = noItemFound;
    }

    public String getSpecialInstructions() {
        return specialInstructions;
    }

    public void setSpecialInstructions(String specialInstructions) {
        this.specialInstructions = specialInstructions;
    }

    public String getAvoidAnything() {
        return avoidAnything;
    }

    public void setAvoidAnything(String avoidAnything) {
        this.avoidAnything = avoidAnything;
    }

    public String getAddSomething() {
        return addSomething;
    }

    public void setAddSomething(String addSomething) {
        this.addSomething = addSomething;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getAddToCart() {
        return addToCart;
    }

    public void setAddToCart(String addToCart) {
        this.addToCart = addToCart;
    }

    public String getAddQuantity() {
        return addQuantity;
    }

    public void setAddQuantity(String addQuantity) {
        this.addQuantity = addQuantity;
    }

    public String getCantAddProduct() {
        return cantAddProduct;
    }

    public void setCantAddProduct(String cantAddProduct) {
        this.cantAddProduct = cantAddProduct;
    }

    public String getNotSelected() {
        return notSelected;
    }

    public void setNotSelected(String notSelected) {
        this.notSelected = notSelected;
    }

    public String getCartClearSucess() {
        return cartClearSucess;
    }

    public void setCartClearSucess(String cartClearSucess) {
        this.cartClearSucess = cartClearSucess;
    }

    public String getTransCompletedSucess() {
        return transCompletedSucess;
    }

    public void setTransCompletedSucess(String transCompletedSucess) {
        this.transCompletedSucess = transCompletedSucess;
    }

    public String getPleaseWait() {
        return pleaseWait;
    }

    public void setPleaseWait(String pleaseWait) {
        this.pleaseWait = pleaseWait;
    }

    public String getPayNow() {
        return payNow;
    }

    public void setPayNow(String payNow) {
        this.payNow = payNow;
    }

    public String getPaymentCompleted() {
        return paymentCompleted;
    }

    public void setPaymentCompleted(String paymentCompleted) {
        this.paymentCompleted = paymentCompleted;
    }

    public String getPaymentFailed() {
        return paymentFailed;
    }

    public void setPaymentFailed(String paymentFailed) {
        this.paymentFailed = paymentFailed;
    }

    public String getCart() {
        return cart;
    }

    public void setCart(String cart) {
        this.cart = cart;
    }

    public String getAppName() {

        if(appName != null && appName.contains("\\")){
            return appName.replace("\\","");
        }
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getReviewPaymentNAddress() {
        return reviewPaymentNAddress;
    }

    public void setReviewPaymentNAddress(String reviewPaymentNAddress) {
        this.reviewPaymentNAddress = reviewPaymentNAddress;
    }

    public String getContinueShopping() {
        return continueShopping;
    }

    public void setContinueShopping(String continueShopping) {
        this.continueShopping = continueShopping;
    }

    public String getCartEmpty() {
        return cartEmpty;
    }

    public void setCartEmpty(String cartEmpty) {
        this.cartEmpty = cartEmpty;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public String getContinuee() {
        return continuee;
    }

    public void setContinuee(String continuee) {
        this.continuee = continuee;
    }

    public String getOrderConfirmed() {
        return orderConfirmed;
    }

    public void setOrderConfirmed(String orderConfirmed) {
        this.orderConfirmed = orderConfirmed;
    }

    public String getThankYou() {
        return thankYou;
    }

    public void setThankYou(String thankYou) {
        this.thankYou = thankYou;
    }

    public String getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(String checkOut) {
        this.checkOut = checkOut;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getUseMyLocation() {
        return useMyLocation;
    }

    public void setUseMyLocation(String useMyLocation) {
        this.useMyLocation = useMyLocation;
    }

    public String getPayByCash() {
        return payByCash;
    }

    public void setPayByCash(String payByCash) {
        this.payByCash = payByCash;
    }

    public String getPayByCard() {
        return payByCard;
    }

    public void setPayByCard(String payByCard) {
        this.payByCard = payByCard;
    }

    public String getOrderSummary() {
        return orderSummary;
    }

    public void setOrderSummary(String orderSummary) {
        this.orderSummary = orderSummary;
    }

    public String getAddInstructions() {
        return addInstructions;
    }

    public void setAddInstructions(String addInstructions) {
        this.addInstructions = addInstructions;
    }

    public String getPlaceOrder() {
        return placeOrder;
    }

    public void setPlaceOrder(String placeOrder) {
        this.placeOrder = placeOrder;
    }

    public String getSelectDeliveryAddress() {
        return selectDeliveryAddress;
    }

    public void setSelectDeliveryAddress(String selectDeliveryAddress) {
        this.selectDeliveryAddress = selectDeliveryAddress;
    }

    public String getOrderPlacedSucess() {
        return orderPlacedSucess;
    }

    public void setOrderPlacedSucess(String orderPlacedSucess) {
        this.orderPlacedSucess = orderPlacedSucess;
    }

    public String getNumberNotAdded() {
        return numberNotAdded;
    }

    public void setNumberNotAdded(String numberNotAdded) {
        this.numberNotAdded = numberNotAdded;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public String getDietary() {
        return dietary;
    }

    public void setDietary(String dietary) {
        this.dietary = dietary;
    }
}
