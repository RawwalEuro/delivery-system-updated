package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductVariant {

    @SerializedName("OptionMasterId")
    @Expose
    private Integer optionMasterId;

    @SerializedName("OptionId")
    @Expose
    private Integer optionId;


    @SerializedName("OptionValue")
    @Expose
    private String OptionValue;

    @SerializedName("OptionPrice")
    @Expose
    private Double OptionPrice;


    @SerializedName("ProductId")
    @Expose
    private Integer productId;

    @SerializedName("parentPosition")
    @Expose
    private Integer parentPosition;


    @SerializedName("childPosition")
    @Expose
    private Integer childPosition;

    @SerializedName("isRequired")
    @Expose
    private Boolean isRequired;


    @SerializedName("setSelected")
    @Expose
    private Boolean setSelected;


    public Boolean getSetSelected() {
        if(setSelected == null){
            return  false;
        }
        return setSelected;
    }

    public void setSetSelected(Boolean setSelected) {
        this.setSelected = setSelected;
    }

    public Boolean getRequired() {
        return isRequired;
    }

    public void setRequired(Boolean required) {
        isRequired = required;
    }

    public Integer getOptionMasterId() {
        return optionMasterId;
    }

    public void setOptionMasterId(Integer optionMasterId) {
        this.optionMasterId = optionMasterId;
    }

    public Integer getOptionId() {
        return optionId;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    public String getOptionValue() {
        return OptionValue;
    }

    public void setOptionValue(String optionValue) {
        OptionValue = optionValue;
    }

    public Double getOptionPrice() {
        return OptionPrice;
    }

    public void setOptionPrice(Double optionPrice) {
        OptionPrice = optionPrice;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getParentPosition() {
        return parentPosition;
    }

    public void setParentPosition(Integer parentPosition) {
        this.parentPosition = parentPosition;
    }

    public Integer getChildPosition() {
        return childPosition;
    }

    public void setChildPosition(Integer childPosition) {
        this.childPosition = childPosition;
    }
}
