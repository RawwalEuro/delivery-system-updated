package com.eurosoft.customerapp;

public class TabIndexSize {

  private int tabIndex;
  private int listSize;

  public TabIndexSize(int tabIndex, int listSize) {
    this.tabIndex = tabIndex;
    this.listSize = listSize;
  }

  public int getTabIndex() {
    return tabIndex;
  }

  public void setTabIndex(int tabIndex) {
    this.tabIndex = tabIndex;
  }

  public int getListSize() {
    return listSize;
  }

  public void setListSize(int listSize) {
    this.listSize = listSize;
  }
}
