package com.eurosoft.customerapp;

import static android.view.View.VISIBLE;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Adapters.AdapterSearchAll;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.SearchItemProduct;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMainSearch extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText searchEt;
    private RecyclerView rvSearchAll;
    private ImageView bckIcon;
    private MasterPojo masterPojo;
    private AppSettings appSettings;
    private User currentUser;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private ArrayList<SearchItemProduct> arrayListData;
    private AdapterSearchAll adapterSearchAll;
    private RelativeLayout rlNoDataFound;
    private ProductDetailFragment bottomSheetProductDetails;
    private Store storeObject;
    private ImageView cart;
    private TextView cart_badge;
    private List<Product> productsFromDB = new ArrayList<>();
    private TextView titleLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_search);
        Stash.init(getApplicationContext());
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        initViews();
        getCartDetails();

        cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityMainSearch.this, ActivityReviewPayment.class));
            }
        });
    }

    private void initViews() {
        searchEt = findViewById(R.id.searchEt);
        rvSearchAll = findViewById(R.id.rvSearchAll);
        bckIcon = findViewById(R.id.bckPress);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        rlNoDataFound = findViewById(R.id.rlNoDataFound);
        titleLocation = findViewById(R.id.titleLocation);

        cart = findViewById(R.id.cart);
        cart_badge = findViewById(R.id.cart_badge);

        bckIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            rvSearchAll.setVisibility(View.GONE);
                        }
                    }, 50);
                    return;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() == 0) {
                        rvSearchAll.setVisibility(View.GONE);
                        return;
                    }

                    if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                        progressVisiblityGone();
                        Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                        return;
                    }

                    if (s.length() <= 3) {
                        rvSearchAll.setVisibility(View.GONE);
                        return;
                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            filterByApi(s.toString());
                        }
                    }, 500);

                    //filter(s.toString());
                } catch (Exception e) {
                    Log.e("Catch", e.getMessage() + "");
                    e.printStackTrace();
                }
            }
        });


    }


    private void getCartDetails() {
        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {
            @Override
            protected List<Product> doInBackground(Void... voids) {
                productsFromDB = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productsFromDB;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productsFromDB == null || productsFromDB.size() == 0) {
                    cart.setVisibility(View.INVISIBLE);
                    cart_badge.setVisibility(View.INVISIBLE);
                } else {
                    cart.setVisibility(View.VISIBLE);
                    cart_badge.setVisibility(View.VISIBLE);
                    cart_badge.setText(productsFromDB.size() + "");
                }
            }
        }
        GetCartItem gt = new GetCartItem();
        gt.execute();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCartDetails();
    }


    public void onPause() {
        super.onPause();

    }

    private void filterByApi(String text) {
        if (text.isEmpty() || text == null) {
            rvSearchAll.setVisibility(View.GONE);
            return;
        }

        if (text.length() == 0) {
            rvSearchAll.setVisibility(View.GONE);
            return;
        }

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<SearchItemProduct>> call = apiService.searchProducts(null + "", text, currentUser.getId(), currentUser.getToken());
        call.enqueue(new Callback<CustomWebResponse<SearchItemProduct>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<SearchItemProduct>> call, Response<CustomWebResponse<SearchItemProduct>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {


                    arrayListData = response.body().getData();
                    rvSearchAll.setVisibility(VISIBLE);
                    rvSearchAll.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    adapterSearchAll = new AdapterSearchAll(getApplicationContext(), arrayListData, appSettings, new AdapterSearchAll.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position, Product product) {
                            bottomSheetProductDetails = new ProductDetailFragment(ActivityMainSearch.this, product, new ProductDetailFragment.BottomSheetListener() {
                                @Override
                                public void onButtonClicked(boolean value) {
                                    bottomSheetProductDetails.dismiss();
                                    Stash.put(Constants.PERMENANT_STORE, Stash.getObject(Constants.TEMPORARY_STORE, Store.class));
                                    storeObject = (Store) Stash.getObject(Constants.PERMENANT_STORE, Store.class);
                                    Toasty.success(getApplicationContext(), "Product Added Sucessfully", Toasty.LENGTH_SHORT).show();
                                    getCartDetails();
                                }
                            });
                            bottomSheetProductDetails.show(getSupportFragmentManager(), "addCardFragment");
                        }
                    });
                    if (arrayListData.size() == 0 && rvSearchAll.getVisibility() == VISIBLE) {
                        //rlNoDataFound.setVisibility(VISIBLE);
                    } else {
                        rlNoDataFound.setVisibility(View.GONE);
                    }
                    rvSearchAll.setAdapter(adapterSearchAll);
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<SearchItemProduct>> call, Throwable t) {

            }
        });


    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }


}