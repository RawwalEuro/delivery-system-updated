package com.eurosoft.customerapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Model.LocationModel;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.InputValidatorHelper;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.fxn.stash.Stash;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.rilixtech.widget.countrycodepicker.CountryCodePicker;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityProfile extends AppCompatActivity implements View.OnClickListener {

    private EditText userNameEt, emailEt, phoneEt, userLastNameEt;
    private User currentUser;
    private RelativeLayout navIcon;
    private ImageView editBtn;
    private Button btnSave;
    private InputValidatorHelper inputValidatorHelper;
    private RelativeLayout rlProgressBar;
    private RelativeLayout mainRl;
    private CircularProgressBar circularProgressBar;
    private LinearLayout llHome, llWork;
    private static final int HOME_REQUEST_CODE = 0;
    private static final int WORK_REQUEST_CODE = 1;
    private TextView homeTitle, homeAddress, workTitle, workAddress;
    private LocationModel homelocation, workLocation;
    private ImageView delteHome, delteWork;
    private ViewDialog alertDialoge;
    private CountryCodePicker ccp;
    private TextView profileHeading, firstName, lastName, phone, email;
    private MasterPojo masterPojo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        alertDialoge = new ViewDialog();
        inputValidatorHelper = new InputValidatorHelper();
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        initViews();
        setData(currentUser);
    }


    private void initViews() {
        userNameEt = findViewById(R.id.userNameEt);
        emailEt = findViewById(R.id.emailEt);
        phoneEt = findViewById(R.id.phoneEt);
        userLastNameEt = findViewById(R.id.userLastNameEt);
        navIcon = findViewById(R.id.navIcon);
        editBtn = findViewById(R.id.editBtn);
        mainRl = findViewById(R.id.mainRl);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        llHome = findViewById(R.id.llHome);
        llWork = findViewById(R.id.llWork);
        homeAddress = findViewById(R.id.homeAddress);

        workAddress = findViewById(R.id.workAddress);
        delteHome = findViewById(R.id.delteHome);
        delteWork = findViewById(R.id.delteWork);
        ccp = findViewById(R.id.ccp);


        //Language
        profileHeading = findViewById(R.id.profileHeading);
        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        homeTitle = findViewById(R.id.homeTitle);
        workTitle = findViewById(R.id.workTitle);
        btnSave = findViewById(R.id.btnSave);


        profileHeading.setText(masterPojo.getProfile());
        firstName.setText(masterPojo.getFirstName());
        lastName.setText(masterPojo.getLastName());
        email.setText(masterPojo.getEmailAddress());
        phone.setText(masterPojo.getPhone());
        homeTitle.setText(masterPojo.getAddHome());
        workTitle.setText(masterPojo.getAddWork());
        btnSave.setText(masterPojo.getSave());

        navIcon.setOnClickListener(this);
        editBtn.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        llHome.setOnClickListener(this);
        llWork.setOnClickListener(this);
        delteHome.setOnClickListener(this);
        delteWork.setOnClickListener(this);

        phoneEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }


    private void setData(User user) {
        if (user.getFirstName() == null || user.getFirstName().equalsIgnoreCase("")) {
            userNameEt.setText("");
        } else {
            userNameEt.setText(user.getFirstName());
        }

        if (user.getLastName() == null || user.getLastName().equalsIgnoreCase("")) {
            userLastNameEt.setText("");
        } else {
            userLastNameEt.setText(user.getLastName());
        }

        emailEt.setText(user.getCustomerEmail());

        homelocation = (LocationModel) Stash.getObject(Constants.USER_HOME, LocationModel.class);
        workLocation = (LocationModel) Stash.getObject(Constants.USER_WORK, LocationModel.class);

        setUpHomeLocation(homelocation);
        setUpWorkLocation(workLocation);
        if (user.getCustomerPhoneNumber() == null || user.getCustomerPhoneNumber().isEmpty() || user.getCustomerPhoneNumber().equalsIgnoreCase("123")) {
            phoneEt.setText("");
          //  ccp.setVisibility(View.GONE);
            return;
        } else {
            phoneEt.setText(user.getCustomerPhoneNumber());
           // ccp.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.navIcon:
                finish();
                break;


            case R.id.editBtn:
                setEditable(true);
                break;


            case R.id.btnSave:
                validateFields();
                break;


            case R.id.llHome:
                Intent i = new Intent(ActivityProfile.this, PlacePickerActivity.class);
                startActivityForResult(i, HOME_REQUEST_CODE);
                break;


            case R.id.llWork:
                Intent ii = new Intent(ActivityProfile.this, PlacePickerActivity.class);
                startActivityForResult(ii, WORK_REQUEST_CODE);
                break;


            case R.id.delteWork:
                deleteAddress(1);
                break;


            case R.id.delteHome:
                deleteAddress(0);
                break;

        }
    }

    private void deleteAddress(int toDelete) {
        if (toDelete == 0) {
            deleteDialog(0);
        } else if (toDelete == 1) {
            deleteDialog(1);

        }

    }

    private void deleteDialog(int toDelete) {
        new SweetAlertDialog(ActivityProfile.this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText( masterPojo.getYouSure())
                .setContentText(masterPojo.getWannaDelt())
                .setConfirmText(masterPojo.getYes())

                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog
                                .setTitleText(masterPojo.getCancelled())
                                .setContentText(null)
                                .setConfirmClickListener(null)
                                .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                        if (toDelete == 0) {
                            Stash.clear(Constants.USER_HOME);
                            homelocation = (LocationModel) Stash.getObject(Constants.USER_HOME, LocationModel.class);
                            setUpHomeLocation(homelocation);
                        } else if (toDelete == 1) {
                            Stash.clear(Constants.USER_WORK);
                            workLocation = (LocationModel) Stash.getObject(Constants.USER_WORK, LocationModel.class);
                            setUpWorkLocation(workLocation);
                        }
                        sDialog.dismissWithAnimation();


                    }
                })

                .setCancelText(masterPojo.getNo())
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                })
                .show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HOME_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                LocationModel locationModel = (LocationModel) data.getExtras().getSerializable("keyName");
                alertDialoge.showDialog(ActivityProfile.this, locationModel, true);
            }
        } else if (requestCode == WORK_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                LocationModel locationModel = (LocationModel) data.getExtras().getSerializable("keyName");
                alertDialoge.showDialog(ActivityProfile.this, locationModel, false);
            }
        }

    }

    private void setUpWorkLocation(LocationModel locationModel) {

        if (locationModel == null || locationModel.getPlaceName().isEmpty() || locationModel.getPlaceName().equalsIgnoreCase("")) {
            workTitle.setText(masterPojo.getAddWork());
            workAddress.setVisibility(View.GONE);
            delteWork.setVisibility(View.GONE);
            return;
        } else {
            workTitle.setText(masterPojo.getWork());
            workAddress.setVisibility(View.VISIBLE);
            delteWork.setVisibility(View.VISIBLE);
            workAddress.setText(locationModel.getPlaceName());
            Stash.put(Constants.USER_WORK, locationModel);
        }
    }

    private void setUpHomeLocation(LocationModel locationModel) {

        if (locationModel == null || locationModel.getPlaceName().isEmpty() || locationModel.getPlaceName().equalsIgnoreCase("")) {
            homeTitle.setText(masterPojo.getAddHome());
            homeAddress.setVisibility(View.GONE);
            delteHome.setVisibility(View.GONE);
            return;
        }
        homeTitle.setText(masterPojo.getHome());
        homeAddress.setVisibility(View.VISIBLE);
        delteHome.setVisibility(View.VISIBLE);
        homeAddress.setText(locationModel.getPlaceName());
        Stash.put(Constants.USER_HOME, locationModel);
    }

    private void validateFields() {


        if (!AppNetWorkStatus.getInstance(this).isOnline()) {
            progressVisiblityGone();
            Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }

        String userName = userNameEt.getText().toString();
        String userEmail = emailEt.getText().toString().replace(" ", "");
        String phone = phoneEt.getText().toString();
        String userLastName = userLastNameEt.getText().toString();



        if (inputValidatorHelper.isNullOrEmpty(userName)) {
            Toasty.warning(this, masterPojo.getFirstNameEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(userLastName)) {
            Toasty.warning(this, masterPojo.getLastNameEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (inputValidatorHelper.isNullOrEmpty(userEmail)) {
            Toasty.warning(this, masterPojo.getEmptyEmail(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (!inputValidatorHelper.isValidEmailOrNot(userEmail)) {
            Toasty.warning(this, masterPojo.getInvalidEmail(), Toast.LENGTH_SHORT, true).show();
            return;
        }


        if (inputValidatorHelper.isNullOrEmpty(phone)) {
            Toasty.warning(this, masterPojo.getNumberIsEmpty(), Toast.LENGTH_SHORT, true).show();
            return;
        }

        currentUser.setFirstName(userName);
        currentUser.setLastName(userLastName);
        currentUser.setCustomerEmail(userEmail);


        if (phone.startsWith("0")) {
            String phoneRemoved = phone.substring(1);
            currentUser.setCustomerPhoneNumber("+" +  phoneRemoved);
        } else {
            currentUser.setCustomerPhoneNumber("+" + phone);
        }
        callApiUpdateProfile(currentUser);
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setEnabled(true);
    }

    private void callApiUpdateProfile(User currentUser) {
        currentUser.setToken(currentUser.getToken());
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<User>> call = apiService.updateProfile(currentUser,currentUser.getToken());


        call.enqueue(new Callback<WebResponse<User>>() {
            @Override
            public void onResponse(Call<WebResponse<User>> call, Response<WebResponse<User>> response) {


                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage(), Toasty.LENGTH_LONG).show();
                    setEditable(false);
                    progressVisiblityGone();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    Stash.put(Constants.USER, response.body().getData());
                    setData(response.body().getData());
                    progressVisiblityGone();
                    setEditable(false);
                }
            }

            @Override
            public void onFailure(Call<WebResponse<User>> call, Throwable t) {

            }
        });

    }

    private void setEditable(Boolean bool) {
        userNameEt.setEnabled(bool);
        emailEt.setEnabled(bool);
        phoneEt.setEnabled(bool);
        userNameEt.setClickable(bool);
        userLastNameEt.setClickable(bool);
        userLastNameEt.setEnabled(bool);
        emailEt.setClickable(bool);
        phoneEt.setClickable(bool);
        if (bool) {
            userNameEt.requestFocus();
            userNameEt.setSelection(userNameEt.getText().length());
            editBtn.setVisibility(View.GONE);
            btnSave.setVisibility(View.VISIBLE);
        } else {
            editBtn.setVisibility(View.VISIBLE);
            btnSave.setVisibility(View.GONE);
        }
    }


    public class ViewDialog {

        public void showDialog(Activity activity, LocationModel locationModel, boolean isHome) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_add_street);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            TextView selectedLoc = (TextView) dialog.findViewById(R.id.selectedLoc);
            TextView fetchedAddress = (TextView) dialog.findViewById(R.id.fetchedAddress);

            fetchedAddress.setText(masterPojo.getAdddoorno());

            Button dialogBtn_cancel = (Button) dialog.findViewById(R.id.btnLeave);
            EditText addStreetEt = (EditText) dialog.findViewById(R.id.addStreet);
            Button dialogBtn_okay = (Button) dialog.findViewById(R.id.btnSave);
            dialogBtn_cancel.setText(masterPojo.getSkip());
            dialogBtn_okay.setText(masterPojo.getSave());

            if (isHome)
                selectedLoc.setText(masterPojo.getHome());
            else
                selectedLoc.setText(masterPojo.getWork());

            dialogBtn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isHome) {
                        setUpHomeLocation(locationModel);
                        dialog.dismiss();
                    } else {
                        setUpWorkLocation(locationModel);
                        dialog.dismiss();
                    }

                    dialog.dismiss();
                }
            });


            dialogBtn_okay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(getApplicationContext(),"Okay" ,Toast.LENGTH_SHORT).show();

                    if (addStreetEt.getText().toString().equalsIgnoreCase("") ||  (addStreetEt.getText().toString().startsWith(" ")) ) {
                        Toasty.error(getApplicationContext(), masterPojo.getAddStreet(), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    else {
                        if (isHome) {
                            locationModel.setPlaceName(locationModel.getPlaceName() + "\n" + masterPojo.getDoorNo() + " " + addStreetEt.getText().toString());
                            setUpHomeLocation(locationModel);
                            dialog.dismiss();
                        } else {
                            locationModel.setPlaceName(locationModel.getPlaceName() + "\n" + masterPojo.getDoorNo() + " " + addStreetEt.getText().toString());
                            setUpWorkLocation(locationModel);
                            dialog.dismiss();
                        }
                    }
                }
            });

            dialog.show();
        }
    }

}