package com.eurosoft.customerapp.Utils.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.eurosoft.customerapp.Model.CardJudoModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class SharedPreferenceHelper {
  private final static String PREF_FILE = "PREF";

  /**
   * Set a string shared preference
   * @param key - Key to set shared preference
   * @param value - Value for the key
   */
  static void setSharedPreferenceString(Context context, String key, String value){
    SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
    SharedPreferences.Editor editor = settings.edit();
    editor.putString(key, value);
    editor.apply();
  }

  /**
   * Set a integer shared preference
   * @param key - Key to set shared preference
   * @param value - Value for the key
   */
  static void setSharedPreferenceInt(Context context, String key, int value){
    SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
    SharedPreferences.Editor editor = settings.edit();
    editor.putInt(key, value);
    editor.apply();
  }

  /**
   * Set a Boolean shared preference
   * @param key - Key to set shared preference
   * @param value - Value for the key
   */
  static void setSharedPreferenceBoolean(Context context, String key, boolean value){
    SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
    SharedPreferences.Editor editor = settings.edit();
    editor.putBoolean(key, value);
    editor.apply();
  }

  /**
   * Get a string shared preference
   * @param key - Key to look up in shared preferences.
   * @param defValue - Default value to be returned if shared preference isn't found.
   * @return value - String containing value of the shared preference if found.
   */
  static String getSharedPreferenceString(Context context, String key, String defValue){
    SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
    return settings.getString(key, defValue);
  }

  /**
   * Get a integer shared preference
   * @param key - Key to look up in shared preferences.
   * @param defValue - Default value to be returned if shared preference isn't found.
   * @return value - String containing value of the shared preference if found.
   */
  static int getSharedPreferenceInt(Context context, String key, int defValue){
    SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
    return settings.getInt(key, defValue);
  }

  /**
   * Get a boolean shared preference
   * @param key - Key to look up in shared preferences.
   * @param defValue - Default value to be returned if shared preference isn't found.
   * @return value - String containing value of the shared preference if found.
   */
  static boolean getSharedPreferenceBoolean(Context context, String key, boolean defValue){
    SharedPreferences settings = context.getSharedPreferences(PREF_FILE, 0);
    return settings.getBoolean(key, defValue);
  }

  public void putJudoCardModelArrayList(Context mContext,ArrayList<CardJudoModel> cardDetailsModelArrayList) {
    try {
      SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
      SharedPreferences.Editor edit = sp.edit();
      Gson gson = new Gson();
      String json = gson.toJson(cardDetailsModelArrayList);
      edit.putString("card_list_judo", json);
      edit.apply();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public int getIntVal(String key,Context mContext) {
    return PreferenceManager.getDefaultSharedPreferences(mContext).getInt(key, 0);
  }
  public void putIntVal(String key, int val,Context mContext) {
    PreferenceManager.getDefaultSharedPreferences(mContext).edit().putInt(key, val).commit();
  }



  //TODO

  public void saveCardJudoModel(CardJudoModel receipt,Context context) {
    CardJudoModel cardJudoModel = new CardJudoModel();
    cardJudoModel.setToken(receipt.getToken());
    cardJudoModel.setConsumerToken(receipt.getConsumerToken());
    cardJudoModel.setConsumerReference(receipt.getConsumerReference());
    cardJudoModel.setEndDate(receipt.getEndDate());
    cardJudoModel.setLastFour(receipt.getLastFour());
    cardJudoModel.setCardLabel(receipt.getCardLabel());
    Object obj = cardJudoModel;
    Gson gson = new Gson();
    String json = gson.toJson(obj);
    PreferenceManager.getDefaultSharedPreferences(context)
            .edit()
            .putString("Judo-CardJUDO-TokenReceipt" + "CUSTOMER_ID", json)
            .commit();
  }

  public ArrayList<CardJudoModel> getJudoCardList(Context mContext) {
    SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(mContext);
    Gson gson = new Gson();
    String json = sp.getString("card_list_judo", "");
    Type type = new TypeToken<ArrayList<CardJudoModel>>() {
    }.getType();
    if (json != null || json.equals("null") || json.length() > 0) {
      return gson.fromJson(json, type);
    } else {
      return null;
    }
  }



  //TODO
  public CardJudoModel getCardJudoModel(Context mContext) {
    Gson gson = new Gson();
    String json = PreferenceManager.getDefaultSharedPreferences(mContext).getString("Judo-CardJUDO-TokenReceipt" + "CustomerId", null);
    CardJudoModel obj = gson.fromJson(json, CardJudoModel.class);
    return obj;
  }



  //TODO
  public void removeLastReciept(Context mContext) {
    try {
      PreferenceManager.getDefaultSharedPreferences(mContext)
              .edit()
              .remove("Judo-CardJUDO-TokenReceipt" + "CustomerId")
              .apply();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  public void removeJudoCardModelArrayList(Context mContext) {
    try {
      PreferenceManager.getDefaultSharedPreferences(mContext)
              .edit()
              .remove("card_list_judo")
              .commit();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}