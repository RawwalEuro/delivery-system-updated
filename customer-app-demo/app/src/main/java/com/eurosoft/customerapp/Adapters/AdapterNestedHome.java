package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.ActivityAllCategories;
import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.StoreList;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;
import com.fxn.stash.Stash;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class AdapterNestedHome extends RecyclerView.Adapter<AdapterNestedHome.ViewHolder> {

    private Context context;
    private List<Store> storesArrayList;
    private MasterPojo masterPojo;
    private AppSettings appSetting;

    public AdapterNestedHome(Context ctx, List<Store> storesList, AppSettings appSettings, MasterPojo masterPojoo) {
        context = ctx;
        storesArrayList = storesList;
        appSetting = appSettings;
        masterPojo = masterPojoo;
    }


    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_home, parent, false);
        Stash.init(context);
        return new AdapterNestedHome.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {
        Store store = storesArrayList.get(position);

        if (!store.getIsStoreOpen()) {
            holder.rlClosed.setVisibility(View.GONE);
        } else {
            holder.rlClosed.setVisibility(View.VISIBLE);
            holder.bgRl.setAlpha(0.6f);
            holder.rlClosed.setTranslationZ(90);
        }

        Stash.put(Constants.IS_STORE_OPEN, store.getIsStoreOpen());
        holder.storeTitle.setText(storesArrayList.get(position).getStoreName());
        holder.storeDesc.setText(store.getStoreTypeName());
        holder.deliveryFee.setText(masterPojo.getDeliveryFee() + " " + appSetting.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(store.getStoreDeliveryFee()) + "");
        holder.deliveryTime.setText(store.getStoreDeliveryEstimatedTime() + "");
        holder.rating.setRating(Math.round(store.getStoreRating()));
        holder.ratingCount.setText(Math.round(store.getStoreRating()) + "");
        holder.minimumOrder.setText("Min Order " + appSetting.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(store.getMinimumOrderLimit()) + "");
        Picasso.get().load(store.getStoreBaseUrl() + store.getStoreImage()).placeholder(R.drawable.burger_image).into(holder.storesImage);
        Picasso.get().load(store.getStoreBaseUrl() + store.getStoreLogo()).placeholder(R.drawable.papa_jols).into(holder.storeLogo);

    }

    private void disableEnableControls(boolean enable, ViewGroup vg) {
        for (int i = 0; i < vg.getChildCount(); i++) {
            View child = vg.getChildAt(i);
            child.setEnabled(enable);
            if (child instanceof ViewGroup) {
                disableEnableControls(enable, (ViewGroup) child);
            }
        }
    }

    @Override
    public int getItemCount() {
        return storesArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView bgRl;
        private ImageView storesImage;
        private TextView storeTitle, storeDesc, deliveryFee, minimumOrder, ratingCount, deliveryTime;
        private RelativeLayout rlClosed;
        private CircleImageView storeLogo;
        private RatingBar rating;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            bgRl = itemView.findViewById(R.id.bgRl);
            storeTitle = itemView.findViewById(R.id.storeTitle);
            storeDesc = itemView.findViewById(R.id.storeDesc);
            storesImage = itemView.findViewById(R.id.storesImage);
            deliveryFee = itemView.findViewById(R.id.deliveryFee);
            minimumOrder = itemView.findViewById(R.id.minimumOrder);
            ratingCount = itemView.findViewById(R.id.ratingCount);
            deliveryTime = itemView.findViewById(R.id.deliveryTime);
            rlClosed = itemView.findViewById(R.id.rlClosed);
            storeLogo = itemView.findViewById(R.id.storeLogo);
            rating = itemView.findViewById(R.id.rating);


            bgRl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    if (!storesArrayList.get(pos).getIsStoreOpen()) {
                        Intent intent = new Intent(v.getContext(), ActivityAllCategories.class);
                        Stash.put(Constants.TEMPORARY_STORE, storesArrayList.get(pos));
                        Gson gson = new Gson();
                        String storesObject = gson.toJson(storesArrayList.get(pos));
                        intent.putExtra("storesObject", storesObject);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        v.getContext().startActivity(intent);

                        return;
                    } else {
                        Toasty.warning(context.getApplicationContext(), "Store is closed at this moment", Toasty.LENGTH_SHORT).show();
                    }
                }
            });
        }

    }
}
