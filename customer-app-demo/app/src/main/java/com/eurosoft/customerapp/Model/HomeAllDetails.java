package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class HomeAllDetails implements Serializable {
  @SerializedName("FilterList")
  @Expose
  private List<Filter> filterList = null;
  @SerializedName("StoreTagData")
  @Expose
  private List<StoreTagsData> storeTagsDataList = null;
  @SerializedName("StoreTypeList")
  @Expose
  private List<StoreType> storeTypeList = null;
  @SerializedName("StoreData")
  @Expose
  private List<Store> storeList = null;

  public List<Filter> getFilterList() {
    return filterList;
  }

  public void setFilterList(List<Filter> filterList) {
    this.filterList = filterList;
  }

  public List<StoreTagsData> getStoreTagsDataList() {
    return storeTagsDataList;
  }

  public void setStoreTagsDataList(List<StoreTagsData> storeTagsDataList) {
    this.storeTagsDataList = storeTagsDataList;
  }

  public List<StoreType> getStoreTypeList() {
    return storeTypeList;
  }

  public void setStoreTypeList(List<StoreType> storeTypeList) {
    this.storeTypeList = storeTypeList;
  }

  public List<Store> getStoreList() {
    return storeList;
  }

  public void setStoreList(List<Store> storeList) {
    this.storeList = storeList;
  }
}


