package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdapterCartItems extends RecyclerView.Adapter<AdapterCartItems.ViewHolder> {

    private Context ctx;
    private List<Product> productArrayList;
    private OnItemClickListener mOnItemClickListener;
    private AppSettings appSettings;


    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }


    public interface ListAction {
        public void onItemEdit(int position);
    }


    public AdapterCartItems(Context context, List<Product> products, AppSettings appSetting, OnItemClickListener onItemClickListener) {
        ctx = context;
        productArrayList = products;
        mOnItemClickListener = onItemClickListener;
        appSettings = appSetting;
    }

    @Override
    public AdapterCartItems.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adapter_cart_item, parent, false);
        final ViewHolder viewHolder = new ViewHolder(view);

        viewHolder.productName.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });


        viewHolder.increment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());

            }
        });

        viewHolder.decrement.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());

            }
        });

        viewHolder.removeItem.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());

            }
        });


        viewHolder.crossRl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });

        viewHolder.parentLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(viewHolder.llEdit.getVisibility() == View.GONE){
                    Transition transition = new Slide(Gravity.LEFT);
                    transition.setDuration(600);
                    transition.addTarget(R.id.llEdit);
                    TransitionManager.beginDelayedTransition(parent, transition);
                    viewHolder.llEdit.setVisibility(View.VISIBLE);

                } else {
                    Transition transition = new Slide(Gravity.LEFT);
                    transition.setDuration(600);
                    transition.addTarget(R.id.llEdit);
                    TransitionManager.beginDelayedTransition(parent, transition);
                    viewHolder.llEdit.setVisibility(View.GONE);
                }
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterCartItems.ViewHolder holder, int position) {
        Product product = productArrayList.get(position);
        holder.productName.setText(product.getProductName() + "");
        holder.productPricing.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(product.getPrice()) + "");
        holder.value.setText(product.getQuantity() + "");
        holder.counter.setText(product.getQuantity() + "");
    }

    @Override
    public int getItemCount() {
        return productArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView productName, value, productPricing, counter;
        private ImageView removeItem, increment, decrement;
        private RelativeLayout crossRl;
        private RelativeLayout llEdit, parentLL;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);

            productName = itemView.findViewById(R.id.productName);
            productPricing = itemView.findViewById(R.id.pricing);

            value = itemView.findViewById(R.id.value);
            increment = itemView.findViewById(R.id.increment);
            decrement = itemView.findViewById(R.id.decrement);
            removeItem = itemView.findViewById(R.id.cross);
            parentLL = itemView.findViewById(R.id.parentLL);
            crossRl = itemView.findViewById(R.id.crossRl);
            llEdit = itemView.findViewById(R.id.llEdit);
            counter = itemView.findViewById(R.id.counter);
        }
    }
}
