package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetails {
    @SerializedName("productId")
    @Expose
    private Integer productId;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("totalPrice")
    @Expose
    private Double totalPrice;

    @SerializedName("productName")
    @Expose
    private String productName;

    @SerializedName("specialInstructions")
    @Expose
    private String specialInstructions;


    @SerializedName("ProductOptions")
    @Expose
    private List<OptionDetailedData> ProductOptions;

    public OrderDetails(Integer productId, Double price, Integer quantity, Double totalPrice, String productName,String special,List<OptionDetailedData> optionDetailedData) {
        this.productId = productId;
        this.price = price;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.productName = productName;
        this.specialInstructions = special;
        this.ProductOptions = optionDetailedData;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }


}
