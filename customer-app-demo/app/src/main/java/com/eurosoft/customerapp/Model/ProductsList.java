package com.eurosoft.customerapp.Model;

import androidx.room.ColumnInfo;

import java.io.Serializable;
import java.util.List;

public class ProductsList implements Serializable {

    @ColumnInfo(name = "productItems")
    private List<Product> productList;

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }
}
