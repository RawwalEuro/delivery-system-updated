package com.eurosoft.customerapp.Fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.eurosoft.customerapp.Constants;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.NetworkUtils.WebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class AddProductDialog extends BottomSheetDialogFragment {

    Context context;
    private BottomSheetListener bottomSheetListener;
    private Product product;
    private ImageView productImage, increment, decrement;
    private TextView addToCart;
    private TextView productName, price, desc, value;
    private int quantity = 1;
    private AppSettings appSettings;
    private List<Product> productsFromDB = new ArrayList<>();
    private User currentUser;
    private ViewDialogClearCart alertDialogCart;
    private MasterPojo masterPojo;
    private int maxCount = 20;
    private ImageView favouriteIv, unFavourite;


    public AddProductDialog(Context context, Product product, BottomSheetListener bottomSheetListener) {
        this.context = context;
        this.product = product;
        this.bottomSheetListener = bottomSheetListener;
    }


    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_product_details, container, false);
        Stash.init(getActivity());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);

        init(v);
        alertDialogCart = new ViewDialogClearCart();


        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle);
        getCartDetails();
        return v;
    }

    private void init(View v) {

        productImage = v.findViewById(R.id.productImage);
        productName = v.findViewById(R.id.productName);
        price = v.findViewById(R.id.price);
        desc = v.findViewById(R.id.desc);
        decrement = v.findViewById(R.id.decrement);
        increment = v.findViewById(R.id.increment);
        value = v.findViewById(R.id.value);
        addToCart = v.findViewById(R.id.addToCart);


        favouriteIv = v.findViewById(R.id.favouriteIv);
        unFavourite = v.findViewById(R.id.unFavourite);


        favouriteIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callApiAddToFavourties(currentUser.getId(), product.getId(), true);


            }
        });

        unFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callApiAddToFavourties(currentUser.getId(), product.getId(), false);

            }
        });

        addToCart.setText(masterPojo.getAddToCart());


        try {
            final int radius = 25;
            final int margin = 0;
            final Transformation transformation = new RoundedCornersTransformation(radius, margin);
            Picasso.get().load(product.getBaseUrl() + product.getId() + "/" + product.getPrimaryImage()).transform(transformation).into(productImage);
            // Picasso.get().load(product.getBaseUrl() + product.getId() + "/" + product.getPrimaryImage()).placeholder(R.color.grey).into(productImage);

            quantity = product.getMinimumQty();
            maxCount = product.getQuantity();


        } catch (Exception e) {
            e.printStackTrace();
        }

        productName.setText(product.getProductName());
        price.setText(appSettings.getRegionSettingData().getCurrencySymbol() + "" + Converters.roundfloat(product.getPrice()) + "");
        desc.setText(product.getProductDesc() + "");
        value.setText(quantity + "");
        Log.e("WIshList",product.getIsWishList() + "");


        if(product.getIsWishList()){
            unFavourite.setVisibility(View.VISIBLE);
            favouriteIv.setVisibility(View.GONE);
        } else {
            favouriteIv.setVisibility(View.VISIBLE);
            unFavourite.setVisibility(View.GONE);
        }


        decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDecrement();
            }
        });


        increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incrementValues();
            }
        });

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!appSettings.getWebsiteSettingData().isMultipleResturentOrderAllow()) {
                    if (productsFromDB.size() == 0) {
                        addProductToCart();
                    } else if (productsFromDB != null || productsFromDB.size() == 0) {
                        if (product.getStoreId() == productsFromDB.get(0).getStoreId()) {
                            addProductToCart();
                        } else {
                            alertDialogCart.showDialog(getActivity());
                            return;
                        }
                    }
                } else {
                    addProductToCart();
                }
            }
        });
    }

    private void removeWishListUI() {
        favouriteIv.setVisibility(View.VISIBLE);
        unFavourite.setVisibility(View.GONE);
    }

    private void addWishListUi() {
        unFavourite.setVisibility(View.VISIBLE);
        favouriteIv.setVisibility(View.GONE);
    }

    private void callApiAddToFavourties(int customerId, int productId, boolean isLike) {
        if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
            Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }


        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<WebResponse<MasterPojo>> call = apiService.addToWishlist(customerId, productId, isLike,currentUser.getToken());

        call.enqueue(new Callback<WebResponse<MasterPojo>>() {
            @Override
            public void onResponse(Call<WebResponse<MasterPojo>> call, Response<WebResponse<MasterPojo>> response) {
                if (response.code() != 200 || response.body() == null) {

                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {

                    if(isLike){
                        addWishListUi();
                    } else {
                        removeWishListUI();
                    }
                }
            }

            @Override
            public void onFailure(Call<WebResponse<MasterPojo>> call, Throwable t) {
                Toasty.warning(getApplicationContext(), "Something went wrong", Toasty.LENGTH_SHORT).show();
            }
        });

    }


    private void addProductToCart() {

        if (value.getText().toString().equalsIgnoreCase("") || value.getText().toString() == null) {
            Toast.makeText(getApplicationContext(), masterPojo.getAddQuantity(), Toast.LENGTH_LONG).show();
            return;
        }


        quantity = Integer.parseInt(value.getText().toString());


        if (quantity == 0) {
            Toast.makeText(getApplicationContext(), masterPojo.getAddQuantity(), Toast.LENGTH_LONG).show();
            return;
        }
        if (product == null) {
            Toast.makeText(getApplicationContext(), masterPojo.getCantAddProduct(), Toast.LENGTH_LONG).show();
            return;
        }


        for (int i = 0; i < productsFromDB.size(); i++) {
            for (int y = i + 1; y <= productsFromDB.size(); y++) {
                boolean check = productsFromDB.get(i).getId().equals(product.getId());
                if (check) {
                    Product product = productsFromDB.get(i);
                    product.setName(product.getProductName());
                    product.setQuantity(product.getQuantity() + quantity);
                    product.setMaxQuantity(maxCount);
                    product.setMinimumQty(product.getMinimumQty());
                    product.setPrice(product.getGetActucalPrice() * product.getQuantity());
                    product.setStoreId(product.getStoreId());
                    product.setStoreName(product.getStoreName());
                    product.setSpecialInstructions(" ");
                    if (product.getProductOptions() != 0) {
                        product.setOrderDetailData(new ArrayList<>());
                    } else {
                        product.setOrderDetailData(new ArrayList<>());
                    }
                    updateProductAsync(product);
                    bottomSheetListener.onButtonClicked(true);

                    product.setQuantity(maxCount);
                    return;
                }
            }
        }


        Product productToAdd = product;
        Log.e("Store", product.getStoreName());
        productToAdd.setName(product.getProductName());
        productToAdd.setQuantity(quantity);
        productToAdd.setMaxQuantity(maxCount);
        productToAdd.setMinimumQty(product.getMinimumQty());
        productToAdd.setActucalPrice(product.getPrice());
        productToAdd.setStoreId(product.getStoreId());
        productToAdd.setPrice(productToAdd.getGetActucalPrice() * quantity);
        productToAdd.setStoreName(product.getStoreName());
        productToAdd.setUserid(currentUser.getId());
        product.setSpecialInstructions(" ");
        if (product.getProductOptions() != 0) {
            product.setOrderDetailData(new ArrayList<>());
        } else {
            product.setOrderDetailData(new ArrayList<>());
        }

        addProductAsync(productToAdd);

        bottomSheetListener.onButtonClicked(true);
        product.setQuantity(maxCount);
    }


    private void incrementValues() {
        if (quantity >= product.getQuantity()) {
            Toast.makeText(getApplicationContext(), product.getQuantity() + " is the max quantity for this product", Toast.LENGTH_LONG).show();
            return;
        }
        quantity++;
        value.setText(String.valueOf(quantity));

    }

    private void setDecrement() {
        if (quantity == 0) {
            //progressVisiblityGone();
            quantity = 1;
            return;
        }

       /* if (quantity == Integer.parseInt(value.getText().toString())) {
            //progressVisiblityGone();
            Toast.makeText(getApplicationContext(), product.getMinimumQty() + " is the minimum quantity for this product", Toast.LENGTH_LONG).show();

            quantity = product.getMinimumQty();
            return;
        }*/
        value.setText(String.valueOf(quantity--));
    }


    private void getCartDetails() {

        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {

            @Override
            protected List<Product> doInBackground(Void... voids) {
                productsFromDB = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productsFromDB;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productsFromDB == null || productsFromDB.size() == 0) {

                }


            }
        }

        GetCartItem gt = new GetCartItem();
        gt.execute();

    }


    private void updateProductAsync(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .update(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }

    private void addProductAsync(Product product) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .insert(product);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                //progressVisiblityGone();
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    public class ViewDialogClearCart {
        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity, R.style.Theme_Dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.dialog_same_restruant);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            Button btnClear = (Button) dialog.findViewById(R.id.btnClear);
            Button cancel = (Button) dialog.findViewById(R.id.cancel);
            btnClear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    delAllProductAsync(currentUser.getId());
                    dialog.dismiss();
                    Toasty.success(getApplicationContext(), masterPojo.getCartClearSucess(), Toasty.LENGTH_LONG).show();
                    //addProductToCart();
                    getCartDetails();

                }
            });


            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    private void delAllProductAsync(int userId) {
        class AddProduct extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... voids) {
                //creating a task
                //adding to database
                DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                        .productDao()
                        .delAllUsersById(userId);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
            }
        }

        AddProduct st = new AddProduct();
        st.execute();
    }


    public interface BottomSheetListener {
        void onButtonClicked(boolean value);
    }
}
