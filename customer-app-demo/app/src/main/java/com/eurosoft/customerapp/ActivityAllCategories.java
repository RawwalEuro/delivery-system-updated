package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.eurosoft.customerapp.Adapters.AdapterGetCategoriesByStoreId;
import com.eurosoft.customerapp.Adapters.AdapterSearchAll;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Categories;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Order;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.SearchItemProduct;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.google.android.material.appbar.AppBarLayout;
import com.google.gson.Gson;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class ActivityAllCategories extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvAllRestruants;
    private ArrayList<String> allRestruantsList = new ArrayList<>();
    private AdapterGetCategoriesByStoreId adapter;
    private String id = "0";
    private ArrayList<Categories> categoriesArrayList = new ArrayList<>();
    private FrameLayout bckIcon;
    private Store store;
    private ImageView imageHeader;
    private TextView restruantName, restruantlocation, restruantitem, deliveryTime, rating;
    private FrameLayout frameCart;
    private TextView cart_badge;
    private List<Product> productsFromDB = new ArrayList<>();
    private User currentUser;
    private List<Order> orderList;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private EditText searchEt;
    private RelativeLayout searchBarRl;
    private RelativeLayout rlNoData;
    private MasterPojo masterPojo;
    private TextView allCategories;
    private TextView textNoResultFound;
    private RecyclerView rvSearchAll;
    private AdapterSearchAll adapterSearchAll;
    private AppSettings appSettings;
    private ArrayList<SearchItemProduct> arrayListData = new ArrayList<>();
    private AppBarLayout appBar;
    private ProductDetailFragment bottomSheetProductDetails;
    private Store storeObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_restruants);
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        init();
        getDataProduct();
        getCartDetails();
        //getId();
    }

    public void collapseToolbar() {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBar.getLayoutParams();
        final AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        if (behavior != null) {
            ValueAnimator valueAnimator = ValueAnimator.ofInt();
            valueAnimator.setInterpolator(new DecelerateInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    behavior.setTopAndBottomOffset((Integer) animation.getAnimatedValue());
                    appBar.requestLayout();
                }
            });
            valueAnimator.setIntValues(0, -900);
            valueAnimator.setDuration(700);
            valueAnimator.start();
        }
    }

    private void init() {
        rvAllRestruants = findViewById(R.id.rvAllRestruants);
        rvSearchAll = findViewById(R.id.rvSearchAll);
        bckIcon = findViewById(R.id.bckIcon);
        appBar = findViewById(R.id.appBarlayout);
        imageHeader = findViewById(R.id.toolbar_background);
        restruantName = findViewById(R.id.restruantName);
        restruantlocation = findViewById(R.id.restruantlocation);
        restruantitem = findViewById(R.id.restruantitem);
        deliveryTime = findViewById(R.id.deliveryTime);
        rating = findViewById(R.id.rating);
        searchBarRl = findViewById(R.id.searchBarRl);

        frameCart = findViewById(R.id.frameCart);
        cart_badge = findViewById(R.id.cart_badge);

        rlNoData = findViewById(R.id.rlNoDataFound);
        frameCart.setOnClickListener(this);

        //Language
        searchEt = findViewById(R.id.searchEt);
        allCategories = findViewById(R.id.allCategories);
        textNoResultFound = findViewById(R.id.textNoResultFound);
        searchEt.setHint(masterPojo.getSearch());
        allCategories.setText(masterPojo.getAllCatrgories());
        textNoResultFound.setText(masterPojo.getNoCategoriesAvailable());
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        bckIcon.setOnClickListener(this);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        searchEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    collapseToolbar();
                }
            }
        });

        searchEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            rvSearchAll.setVisibility(View.GONE);
                            rvAllRestruants.setVisibility(VISIBLE);
                        }
                    }, 2000);
                    return;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    if (s.length() == 0) {
                        rvSearchAll.setVisibility(View.GONE);
                        rvAllRestruants.setVisibility(VISIBLE);
                        return;
                    }

                    if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                        progressVisiblityGone();
                        Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                        return;
                    }
                    filterByApi(s.toString());
                    //filter(s.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void filterByApi(String text) {
        if (text.isEmpty() || text == null) {
            rvSearchAll.setVisibility(View.GONE);
            rvAllRestruants.setVisibility(VISIBLE);
            return;
        }

        if (text.length() == 0) {
            rvSearchAll.setVisibility(View.GONE);
            rvAllRestruants.setVisibility(VISIBLE);
            return;
        }

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<SearchItemProduct>> call = apiService.searchProducts(store.getId() + "", text,currentUser.getId(), currentUser.getToken());
        call.enqueue(new Callback<CustomWebResponse<SearchItemProduct>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<SearchItemProduct>> call, Response<CustomWebResponse<SearchItemProduct>> response) {
                if (response.code() != 200 || response.body() == null) {
                    return;
                }
                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    arrayListData = response.body().getData();
                    rvSearchAll.setVisibility(VISIBLE);
                    rvAllRestruants.setVisibility(View.GONE);
                    rvSearchAll.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    try {
                        adapterSearchAll = new AdapterSearchAll(getApplicationContext(), arrayListData, appSettings, new AdapterSearchAll.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position, Product product) {
                                bottomSheetProductDetails.dismiss();
                                Stash.put(Constants.PERMENANT_STORE, Stash.getObject(Constants.TEMPORARY_STORE, Store.class));
                                storeObject = (Store) Stash.getObject(Constants.PERMENANT_STORE, Store.class);
                                Toasty.success(getApplicationContext(), "Product Added Sucessfully", Toasty.LENGTH_SHORT).show();
                            }
                        });
                        bottomSheetProductDetails.show(getSupportFragmentManager(), "addCardFragment");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (arrayListData.size() == 0 && rvSearchAll.getVisibility() == VISIBLE) {
                        rlNoData.setVisibility(VISIBLE);
                    } else {
                        rlNoData.setVisibility(View.GONE);
                    }
                    rvSearchAll.setAdapter(adapterSearchAll);
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<SearchItemProduct>> call, Throwable t) {

            }
        });


    }

    private void filter(String text) {
        ArrayList<Categories> filteredList = new ArrayList<>();
        for (Categories item : categoriesArrayList) {
            if (item.getCategoryName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }
        adapter.filterList(filteredList);
    }

    private void getId() {
        if (getIntent().getStringExtra("id") != null || getIntent().getStringExtra("id").isEmpty()) {
            id = getIntent().getStringExtra("id");
            callApiGetByStoreId(Integer.parseInt(id));
            return;
        }
    }


    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }

    private void callApiGetByStoreId(int id) {
        if (!AppNetWorkStatus.getInstance(this).isOnline()) {
            progressVisiblityGone();
            Toasty.warning(this, "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
            return;
        }
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<Categories>> call = apiService.getAllCatByStoreId(id, currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<Categories>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<Categories>> call, Response<CustomWebResponse<Categories>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    progressVisiblityGone();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {

                    if (response.body().getData().size() == 0) {
                        progressVisiblityGone();
                        searchBarRl.setVisibility(View.GONE);
                        rlNoData.setVisibility(View.VISIBLE);
                        searchBarRl.setNestedScrollingEnabled(false);
                        return;
                    }
                    categoriesArrayList = response.body().getData();
                    if (categoriesArrayList.size() == 0) {
                        searchBarRl.setVisibility(View.GONE);
                        rlNoData.setVisibility(View.VISIBLE);
                        searchBarRl.setNestedScrollingEnabled(false);
                        return;
                    } else {
                        searchBarRl.setVisibility(View.VISIBLE);
                        rlNoData.setVisibility(View.GONE);
                        adapter = new AdapterGetCategoriesByStoreId(ActivityAllCategories.this, categoriesArrayList);
                        rvAllRestruants.setLayoutManager(new LinearLayoutManager(ActivityAllCategories.this));
                        rvAllRestruants.setAdapter(adapter);
                        rlNoData.setVisibility(View.GONE);
                        searchBarRl.setNestedScrollingEnabled(true);
                        progressVisiblityGone();
                    }
                }
            }


            @Override
            public void onFailure(Call<CustomWebResponse<Categories>> call, Throwable t) {
                //Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                progressVisiblityGone();
            }
        });

    }

    private void getDataProduct() {
        Gson gson = new Gson();
        store = gson.fromJson(getIntent().getStringExtra("storesObject"), Store.class);
        if (store == null) {
            return;
        } else {
            Picasso.get().load(store.getStoreBaseUrl() + store.getStoreImage()).into(imageHeader);
            callApiGetByStoreId(store.getId());
            restruantName.setText(store.getStoreName() + "");
            restruantlocation.setText(store.getStoreAddress() + "");
            restruantitem.setText("--" + "");
            deliveryTime.setText(store.getStoreDeliveryEstimatedTime());
            rating.setText(5 + "");
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        getCartDetails();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bckIcon:
                finish();
                //collapseToolbar();
                break;


            case R.id.frameCart:
                Intent intent = new Intent(ActivityAllCategories.this, ActivityReviewPayment.class);
                startActivity(intent);
                break;
        }
    }

    private void getCartDetails() {
        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {
            @Override
            protected List<Product> doInBackground(Void... voids) {
                productsFromDB = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productsFromDB;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productsFromDB == null || productsFromDB.size() == 0) {
                    frameCart.setVisibility(View.INVISIBLE);
                } else {
                    frameCart.setVisibility(View.VISIBLE);
                    cart_badge.setText(productsFromDB.size() + "");
                }
            }
        }
        GetCartItem gt = new GetCartItem();
        gt.execute();
    }
}