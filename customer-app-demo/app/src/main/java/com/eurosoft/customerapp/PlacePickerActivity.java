package com.eurosoft.customerapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Adapters.AdapterSearchLocations;
import com.eurosoft.customerapp.Model.LocationModel;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.SearchAddress;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Utils.GeocoderHelperClass;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.AppNetWorkStatus;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.fxn.stash.Stash;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class PlacePickerActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int REQUEST_LOCATION = 99;
    public static final int REQUEST_CHECK_SETTINGS = 1021;

    private GoogleApiClient mGoogleApiClient;
    private SweetAlertDialog mDialog;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private GoogleMap mMap;
    private Handler mCameraHandler;
    private LocationCallback locationCallback;
    private SupportMapFragment mapFragment;
    private TextView setArrTv;
    ImageView topIv;
    ImageView backIv;
    int indexEditTextPosition;
    private CardView doneBtn;
    private TextView setFromTv;
    private RecyclerView searchItemRv;
    private EditText searchEdit;
    private AdapterSearchLocations adapterSearch;
    private ImageView placeHolder;

    private int previousLength = 0;
    private boolean backSpace = false;
    private List<SearchAddress> arrayListData = new ArrayList<>();
    private LocationModel locationModel;
    private MasterPojo masterPojo;

    private Runnable mNewCameraMoveCallback = new Runnable() {

        @Override
        public void run() {
            //searchEdit.setText("Getting Location ..");
            if (Geocoder.isPresent() && isNetworkAvailable()) {
                CameraPosition position = mMap.getCameraPosition();

                new GeocoderHelperClass(getApplicationContext()).setResultInterface(new GeocoderHelperClass.SetResult() {

                    @Override
                    public void onGetResult(List<Address> list) {

                        if (list != null && list.size() > 0) {

                            if (list != null && !list.isEmpty()) {
                                searchEdit.setText(list.get(0).getAddressLine(0));
                                searchEdit.setSelection(searchEdit.getEditableText().toString().length());
                                locationModel = new LocationModel();
                                locationModel.setPlaceName(list.get(0).getAddressLine(0));
                                locationModel.setLatitude(list.get(0).getLatitude());
                                locationModel.setLongitude(list.get(0).getLongitude());
                                Stash.put(Constants.USER_SELECTED_ADDRESS, list.get(0).getAddressLine(0));
                                if (list == null || list.equals("null") || list.equals("")) {
                                    searchEdit.setText(masterPojo.getUnableToFindAddress());
                                    searchEdit.setSelection(searchEdit.getEditableText().toString().length());
                                }
                                return;
                            }
                        }
                    }
                }).execute(position.target.latitude, position.target.longitude);
            }
        }
    };
    private TextView textDone;
    private User currentUser;


    // private AddressModel addressModel = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_picker);
        mCameraHandler = new Handler();
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER,User.class);
        init();

        //initData();

        listener();


    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            stopLocationUpdates();
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected())
                mGoogleApiClient.disconnect();
        } catch (Exception e) {
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ContextCompat.checkSelfPermission(PlacePickerActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            fusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext());
            fusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), location -> {
                if (location != null) {
                    try {
                        stopLocationUpdates();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("TAG", "onConnected: ELSE : " + location);
                }
            }).addOnFailureListener(e -> {
            });
        }
    }

    public static void hideSoftKeyboard(Activity activity) {

        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager == null || activity.getCurrentFocus().getWindowToken() == null) {
            return;
        }
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length >= 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                startLocationUpdates();
            } else {
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK) {
//            mDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
//            mDialog.setTitleText("Please wait .. ");
//            mDialog.setContentText("You will be taking to your current location");
//            mDialog.setCancelable(false);
//            mDialog.show();
            startLocationUpdates();
        }
    }

    boolean isAddressSelected = false;

    private void init() {
        backIv = findViewById(R.id.backIv);
        topIv = findViewById(R.id.topIv);
        doneBtn = findViewById(R.id.doneBtn);
        setFromTv = findViewById(R.id.setFromTv);
        setArrTv = findViewById(R.id.setArrTv);
        searchItemRv = findViewById(R.id.searchItemRv);
        searchEdit = findViewById(R.id.searchEt);
        placeHolder = findViewById(R.id.myLoc);
        textDone = findViewById(R.id.textDone);
        textDone.setText(masterPojo.getDone());

        searchEdit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    isAddressSelected = false;
                }
                return false;
            }
        });
        searchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                previousLength = s.length();

                if (count == 0) {
                    searchItemRv.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                backSpace = previousLength > s.length();

                if (backSpace) {
                    isAddressSelected = false;
                    // do your stuff ...
                }

                if (count >= 3) {

                }

                if (count == 0) {
                    searchItemRv.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (isAddressSelected) {
                    return;
                }

                if (!AppNetWorkStatus.getInstance(getApplicationContext()).isOnline()) {
                    Toasty.warning(getApplicationContext(), "Please check your internet!!!!", Toasty.LENGTH_SHORT).show();
                    return;
                }


                if (s.toString().length() >= 3) {
                    selectedStr = s.toString();
                    if (handler != null) {
                        handler.removeCallbacks(m_runnable);
                        handler.postDelayed(m_runnable, 500);
                    }
                }
            }
        });


        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);


    }

    Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if (msg.what == 2) {
                filter(selectedStr);
            }
        }
    };
    String selectedStr = "";
    Runnable m_runnable = new Runnable() {
        @Override
        public void run() {
            Message message = new Message();
            message.what = 2;
            handler.sendMessage(message);
        }
    };

    private void filter(String text) {

        if (text.isEmpty() || text == null) {
            searchItemRv.setVisibility(View.GONE);
            return;
        }

        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<SearchAddress>> call = apiService.searchAddressPrefix(text,currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<SearchAddress>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<SearchAddress>> call, Response<CustomWebResponse<SearchAddress>> response) {
                if (response.code() != 200 || response.body() == null) {

                    return;
                }
                if (!response.body().getSuccess()) {

                    Toasty.error(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {

                    arrayListData = response.body().getData();
                    searchItemRv.setVisibility(VISIBLE);
                    searchItemRv.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


                    adapterSearch = new AdapterSearchLocations(getApplicationContext(), response.body().getData(), new AdapterSearchLocations.OnItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            switch (view.getId()) {
                                case R.id.locationText:
                                    isAddressSelected = true;
                                    SearchAddress datum = arrayListData.get(position);
                                    Toast.makeText(getApplicationContext(), datum.getAddressLine1(), Toast.LENGTH_LONG);
                                    searchItemRv.setVisibility(View.GONE);
                                    searchEdit.setText(datum.getAddressLine1() + "");
                                    searchEdit.setSelection(searchEdit.getEditableText().toString().length());
                                    // hideSoftKeyboard(getActivity());



                                    if (arrayListData.get(position).getLatitude() == null || arrayListData.get(position).getLongitude().equals("")) {
                                        return;

                                    }

                                    if (arrayListData.get(position).getLongitude() == null || arrayListData.get(position).getLongitude().equals("")) {
                                        return;
                                    }
                                    else {
                                        try {

                                            locationModel = new LocationModel();
                                            locationModel.setPlaceName(datum.getAddressLine1());
                                            locationModel.setLatitude(datum.getLatitude());
                                            locationModel.setLongitude(datum.getLongitude());
                                            Stash.put(Constants.USER_SELECTED_ADDRESS,datum.getAddressLine1());
                                            Stash.put(Constants.USER_CURRENT_ADDRESS,datum.getAddressLine1());

                                            Log.e("PickerLat",datum.getLatitude() + "");
                                            Log.e("PickerLong",datum.getLongitude() + "");
                                            Stash.put(Constants.USER_LAT,datum.getLatitude());
                                            Stash.put(Constants.USER_LONG,datum.getLongitude());


                                            Log.e("LatStash",Stash.getString(Constants.USER_LAT) + "");
                                            Log.e("LongStash",Stash.getString(Constants.USER_LONG) + "");
                                            Intent intent = getIntent();
                                            intent.putExtra("keyName", (Serializable) locationModel);
                                            setResult(RESULT_OK, intent);
                                            finish();


                                            //moveCameraAnimation(arrayListData.get(position).getLatitude(), arrayListData.get(position).getLongitude());
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }


                                    }
                                    break;
                            }
                        }
                    });
                    searchItemRv.setAdapter(adapterSearch);
                }
            }

            @Override
            public void onFailure(Call<CustomWebResponse<SearchAddress>> call, Throwable t) {
                //Toast.makeText(PlacePickerActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void moveCameraAnimation(Double latitude, Double longitude) {
        LatLng coordinate = new LatLng(latitude, longitude); //Store these lat lng values somewhere. These should be constant.
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                coordinate, 15);
        mMap.animateCamera(location);
    }

    private void listener() {
        backIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        doneBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Put the String to pass back into an Intent and close this activity

                if (locationModel == null) {
                    finish();
                    return;
                } else {
                    Intent intent = getIntent();
                    intent.putExtra("keyName", (Serializable) locationModel);

                    Log.e("CursorLat",locationModel.getLatitude() + "");
                    Log.e("CursorLong",locationModel.getLongitude() + "");

                    Stash.put(Constants.USER_LAT,locationModel.getLatitude());
                    Stash.put(Constants.USER_LONG,locationModel.getLongitude());
                    Stash.put(Constants.USER_CURRENT_ADDRESS,locationModel.getPlaceName());


                    Log.e("CursorLatStash",Stash.getString(Constants.USER_LAT) + "");
                    Log.e("CursorLongStash",Stash.getString(Constants.USER_LONG) + "");
                    setResult(RESULT_OK, intent);
                    finish();
                }

            }
        });

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                try {
                    mMap = googleMap;

                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(PlacePickerActivity.this, R.raw.map_style));
                    if (Build.VERSION.SDK_INT >= 23) {

                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                            builder.setTitle(masterPojo.getLocationPermission())
                                    .setMessage(masterPojo.getManualLocationPermission())
                                    .setPositiveButton(masterPojo.getoK(), new DialogInterface.OnClickListener() {
                                        @RequiresApi(api = Build.VERSION_CODES.M)
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            requestPermissions(new String[]{
                                                            Manifest.permission.ACCESS_FINE_LOCATION,
                                                            Manifest.permission.ACCESS_COARSE_LOCATION},
                                                    REQUEST_LOCATION);
                                        }
                                    })
                                    .setCancelable(false)
                                    .create()
                                    .show();
                        }
                        mMap.setMyLocationEnabled(true);
                        moveCameraAnimation(Double.parseDouble(Stash.getString(Constants.USER_LAT)), Double.parseDouble(Stash.getString(Constants.USER_LONG)));

                        mCameraHandler.removeCallbacks(mNewCameraMoveCallback);
                        mCameraHandler.postDelayed(mNewCameraMoveCallback, 0);

                    }

                    mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                        @Override
                        public void onCameraIdle() {
                            mCameraHandler.removeCallbacks(mNewCameraMoveCallback);
                            mCameraHandler.postDelayed(mNewCameraMoveCallback, 0);
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                        String provider = manager.getBestProvider(new Criteria(), true);

                        if (provider != null) {

                            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                Location loc = manager.getLastKnownLocation(provider);
                                CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 10);
                                mMap.animateCamera(update);
                            }
                        }

                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

                mMap.getUiSettings().setCompassEnabled(false);
                checkLocationIsEnabled();
            }
        });

        placeHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    String provider = manager.getBestProvider(new Criteria(), true);


                    if (provider == null) {
                        return;
                    }

                    if (provider != null) {

                        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }


                        Location loc = manager.getLastKnownLocation(provider);
                        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 16);
                        mMap.animateCamera(update);

                        mMap.getUiSettings().setCompassEnabled(false);
                        checkLocationIsEnabled();
                    }

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

        });


        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (mDialog != null) {
                    mDialog.dismissWithAnimation();
                    mDialog.dismiss();
                }

                if (locationResult == null) {
                    return;
                }

                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    Log.d("TAG", "onLocationResult: " + location.getLatitude() + " " + location.getLongitude());

                    if (location != null) {
                        stopLocationUpdates();
                        if (getLocationMode() != 1) {

                            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13);
                            mMap.animateCamera(update);
                        } else {
                            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(51.509865, -0.118092), 13);
                            mMap.animateCamera(update);
                        }

                        mCameraHandler.removeCallbacks(mNewCameraMoveCallback);
                        mCameraHandler.postDelayed(mNewCameraMoveCallback, 0);

                        break;
                    }
                }
            }
        };
    }


    private void moveCameraToCurrentLocation() {

        if (mMap == null) {
            return;
        } else {
            try {
                LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                String provider = manager.getBestProvider(new Criteria(), true);


                if (provider == null) {
                    return;
                }

                if (provider != null) {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Location loc = manager.getLastKnownLocation(provider);
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(new LatLng(loc.getLatitude(), loc.getLongitude()), 16);
                    mMap.animateCamera(update);

                    mMap.getUiSettings().setCompassEnabled(false);
                    checkLocationIsEnabled();
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    private void checkLocationIsEnabled() {
        if (mGoogleApiClient == null) {
            buildGoogleApiClient();
        }
        // Check for google map installation
        LocationSettingsRequest locationSettingsRequest = new LocationSettingsRequest.Builder().addLocationRequest(LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)).build();

        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, locationSettingsRequest).setResultCallback((ResultCallback<Result>) result -> {
            final Status status = result.getStatus();
            if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED && status.hasResolution()) {

                try {
                    status.startResolutionForResult(PlacePickerActivity.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    private void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (fusedLocationClient != null) {
            fusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, null);
        }
    }

    private void stopLocationUpdates() {
        try {
            if (locationCallback != null) {
                fusedLocationClient.removeLocationUpdates(locationCallback);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = manager.getActiveNetworkInfo();

        return info != null && info.isConnected();
    }


    public int getLocationMode() {
        try {
            return Settings.Secure.getInt(this.getContentResolver(), Settings.Secure.LOCATION_MODE);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    private PlacePickerActivity getActivity() {
        return this;
    }


}