package com.eurosoft.customerapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.R;
import com.eurosoft.customerapp.Utils.Converters;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class AdapterRelatedProducts extends RecyclerView.Adapter<AdapterRelatedProducts.ViewHolder> {


    private Context context;
    private ArrayList<Product> productArrayList;
    private OnItemClickListener mOnItemClickListener;
    private AppSettings appSettings;

    public AdapterRelatedProducts(Context ctx, ArrayList<Product> listRelated, AppSettings appSettings, OnItemClickListener onItemClickListener) {
        context = ctx;
        productArrayList = listRelated;
        mOnItemClickListener = onItemClickListener;
        this.appSettings = appSettings;
    }


    @Override
    public AdapterRelatedProducts.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_popular_with, parent, false);
        final AdapterRelatedProducts.ViewHolder viewHolder = new AdapterRelatedProducts.ViewHolder(view);


        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });


        viewHolder.increment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });


        viewHolder.decrement.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });


        viewHolder.addIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mOnItemClickListener.onItemClick(v, viewHolder.getAdapterPosition());
            }
        });


        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdapterRelatedProducts.ViewHolder holder, int position) {
        Product product = productArrayList.get(position);
        holder.productPrice.setText(appSettings.getRegionSettingData().getCurrencySymbol() + Converters.roundfloat(product.getPrice()) + "");
        Picasso.get().load(product.getBaseUrl() + product.getId() + "/" + product.getPrimaryImage()).placeholder(R.color.grey).into(holder.productImage);
        holder.productTitle.setText(product.getProductName());
    }

    @Override
    public int getItemCount() {

        if (productArrayList == null || productArrayList.size() == 0) {
            productArrayList = new ArrayList<>();
            return 0;
        } else {
            return productArrayList.size();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView productTitle, productPrice, value;
        private ImageView addIcon, productImage, decrement, increment;
        private CardView cardView;
        private RelativeLayout llEdit;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            productTitle = itemView.findViewById(R.id.productTitle);
            productPrice = itemView.findViewById(R.id.productPrice);
            addIcon = itemView.findViewById(R.id.addIcon);
            productImage = itemView.findViewById(R.id.productImage);
            cardView = itemView.findViewById(R.id.cardView);
            value = itemView.findViewById(R.id.value);
            increment = itemView.findViewById(R.id.increment);
            decrement = itemView.findViewById(R.id.decrement);
            llEdit = itemView.findViewById(R.id.llEdit);
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }
}
