package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Categories {


    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("StoreId")
    @Expose
    private Integer storeId;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("CategoryIcon")
    @Expose
    private String categoryIcon;
    @SerializedName("CategoryBaseUrl")
    @Expose
    private String categoryBaseUrl;
    @SerializedName("CategoryImage")
    @Expose
    private Object categoryImage;
    @SerializedName("OrderBy")
    @Expose
    private Integer orderBy;
    @SerializedName("ParentCategoryId")
    @Expose
    private Integer parentCategoryId;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("IsSubCategory")
    @Expose
    private Boolean isSubCategory;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryBaseUrl() {
        return categoryBaseUrl;
    }

    public void setCategoryBaseUrl(String categoryBaseUrl) {
        this.categoryBaseUrl = categoryBaseUrl;
    }

    public Object getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(Object categoryImage) {
        this.categoryImage = categoryImage;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Integer parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Boolean getIsSubCategory() {
        return isSubCategory;
    }

    public void setIsSubCategory(Boolean isSubCategory) {
        this.isSubCategory = isSubCategory;
    }


}

