package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderModel {

    public static int Waiting = 1;
    public static int Completed = 2;
    public static int Cancelled = 3;
    public static int PreOrder = 4;
    public static int RecentOrder = 5;
    public static int Confirmed = 6;
    public static int Accept = 7;
    public static int Decline = 8;
    public static int Collect = 9;
    public static int GotoCustomer = 10;
    public static int Arrive = 11;
    public static int Deliver = 12;
    public static int Recover = 13;
    public static int AcceptPending = 14;
    public static int Pending = 15;

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("CustomerPhoneNumber")
    @Expose
    private Object customerPhoneNumber;
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("OrderTime")
    @Expose
    private String orderTime;
    @SerializedName("OrderDate")
    @Expose
    private String orderDate;
    @SerializedName("DropoffAddress")
    @Expose
    private String dropoffAddress;
    @SerializedName("DropoffLocationTypeId")
    @Expose
    private String dropoffLocationTypeId;

    @SerializedName("DropoffLocationType")
    @Expose
    private String dropoffLocationType;
    @SerializedName("DropoffLatitude")
    @Expose
    private Double dropoffLatitude;
    @SerializedName("DropoffLongitude")
    @Expose
    private Double dropoffLongitude;
    @SerializedName("SubTotal")
    @Expose
    private Double subTotal;
    @SerializedName("ShippingCharges")
    @Expose
    private Double shippingCharges;
    @SerializedName("TotalAmount")
    @Expose
    private Double totalAmount;
    @SerializedName("PaymentTypeName")
    @Expose
    private String paymentTypeName;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("IsFeedBackGiven")
    @Expose
    private boolean isFeedBackGiven;

    @SerializedName("OrderStatusId")
    @Expose
    private int OrderStatusId;
    @SerializedName("DeliveryTime")
    @Expose
    private String DeliveryTime;


    @SerializedName("TransactionId")
    @Expose
    private String TransactionId;


    @SerializedName("TipCharges")
    @Expose
    private Double tipValue;


    @SerializedName("ServicesCharges")
    @Expose
    private Double serviceCharges;


    public Double getTipValue() {
        return tipValue;
    }

    public void setTipValue(Double tipValue) {
        this.tipValue = tipValue;
    }

    public Double getServiceCharges() {
        return serviceCharges;
    }

    public void setServiceCharges(Double serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    @SerializedName("ProductList")
    @Expose
    private List<ProductDetails> productList = null;

    public String getDeliveryTime() {
        return DeliveryTime == null ? "" : DeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        DeliveryTime = deliveryTime;
    }




    /*@SerializedName("ProductList")
    @Expose
    private List<ProductDetails> productList = null;*/


    public int getOrderStatusId() {
        return OrderStatusId;
    }

    public void setOrderStatusId(int orderStatusId) {
        OrderStatusId = orderStatusId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Object getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(Object customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getDropoffAddress() {
        return dropoffAddress;
    }

    public void setDropoffAddress(String dropoffAddress) {
        this.dropoffAddress = dropoffAddress;
    }

    public String getDropoffLocationTypeId() {
        return dropoffLocationTypeId;
    }

    public void setDropoffLocationTypeId(String dropoffLocationTypeId) {
        this.dropoffLocationTypeId = dropoffLocationTypeId;
    }

    public String getDropoffLocationType() {
        return dropoffLocationType;
    }

    public void setDropoffLocationType(String dropoffLocationType) {
        this.dropoffLocationType = dropoffLocationType;
    }

    public Double getDropoffLatitude() {
        return dropoffLatitude;
    }

    public void setDropoffLatitude(Double dropoffLatitude) {
        this.dropoffLatitude = dropoffLatitude;
    }

    public Double getDropoffLongitude() {
        return dropoffLongitude;
    }

    public void setDropoffLongitude(Double dropoffLongitude) {
        this.dropoffLongitude = dropoffLongitude;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getShippingCharges() {
        return shippingCharges;
    }

    public void setShippingCharges(Double shippingCharges) {
        this.shippingCharges = shippingCharges;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getPaymentTypeName() {
        return paymentTypeName;
    }

    public void setPaymentTypeName(String paymentTypeName) {
        this.paymentTypeName = paymentTypeName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public boolean getIsFeedBackGiven() {
        return isFeedBackGiven;
    }

    public void setIsFeedBackGiven(boolean isFeedBackGiven) {
        this.isFeedBackGiven = isFeedBackGiven;
    }

    public List<ProductDetails> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductDetails> productList) {
        this.productList = productList;
    }

}
