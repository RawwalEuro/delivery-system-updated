package com.eurosoft.customerapp.Utils.db;

import androidx.room.TypeConverter;

import com.eurosoft.customerapp.Model.Product;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class DataConverter {

  @TypeConverter
  public String fromListProduct(List<Product> product) {
    if (product == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<Product>>() {}.getType();
    String json = gson.toJson(product, type);
    return json;
  }

  @TypeConverter
  public List<Product> toListProduct(String product) {
    if (product == null) {
      return (null);
    }
    Gson gson = new Gson();
    Type type = new TypeToken<List<Product>>() {}.getType();
    List<Product> list = gson.fromJson(product, type);
    return list;
  }
}
