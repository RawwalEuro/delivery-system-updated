package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebSettingData {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("websitename")
    @Expose
    private String websitename;
    @SerializedName("websiteurl")
    @Expose
    private String websiteurl;
    @SerializedName("websiteaddress")
    @Expose
    private String websiteaddress;
    @SerializedName("billingaddress")
    @Expose
    private String billingaddress;
    @SerializedName("billingaddressTypeId")
    @Expose
    private String billingaddressTypeId;
    @SerializedName("billingaddressType")
    @Expose
    private String billingaddressType;
    @SerializedName("billingaddressLatitude")
    @Expose
    private Double billingaddressLatitude;
    @SerializedName("billingaddressLongitude")
    @Expose
    private Double billingaddressLongitude;
    @SerializedName("websitelogo")
    @Expose
    private Object websitelogo;
    @SerializedName("contactaddress")
    @Expose
    private String contactaddress;
    @SerializedName("receivingemail")
    @Expose
    private String receivingemail;
    @SerializedName("contactemail")
    @Expose
    private String contactemail;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("internationalphone")
    @Expose
    private String internationalphone;
    @SerializedName("ordernumber")
    @Expose
    private String ordernumber;
    @SerializedName("smtphost")
    @Expose
    private String smtphost;
    @SerializedName("smtpport")
    @Expose
    private Integer smtpport;
    @SerializedName("smtpemail")
    @Expose
    private String smtpemail;
    @SerializedName("smtppassword")
    @Expose
    private String smtppassword;
    @SerializedName("smtpsecureconnection")
    @Expose
    private Boolean smtpsecureconnection;
    @SerializedName("isemailsentonorder")
    @Expose
    private Boolean isemailsentonorder;
    @SerializedName("RegionId")
    @Expose
    private Integer regionId;
    @SerializedName("countrycode")
    @Expose
    private String countrycode;

    @SerializedName("mapapi")
    @Expose
    private String mapapi;

    @SerializedName("IsMultipleResturentOrderAllow")
    @Expose
    private boolean IsMultipleResturentOrderAllow;


    public boolean isMultipleResturentOrderAllow() {
        return IsMultipleResturentOrderAllow;
    }

    public void setMultipleResturentOrderAllow(boolean multipleResturentOrderAllow) {
        IsMultipleResturentOrderAllow = multipleResturentOrderAllow;
    }

    public String getMapapi() {
        return mapapi;
    }

    public void setMapapi(String mapapi) {
        this.mapapi = mapapi;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWebsitename() {
        return websitename;
    }

    public void setWebsitename(String websitename) {
        this.websitename = websitename;
    }

    public String getWebsiteurl() {
        return websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        this.websiteurl = websiteurl;
    }

    public String getWebsiteaddress() {
        return websiteaddress;
    }

    public void setWebsiteaddress(String websiteaddress) {
        this.websiteaddress = websiteaddress;
    }

    public String getBillingaddress() {
        return billingaddress;
    }

    public void setBillingaddress(String billingaddress) {
        this.billingaddress = billingaddress;
    }

    public String getBillingaddressTypeId() {
        return billingaddressTypeId;
    }

    public void setBillingaddressTypeId(String billingaddressTypeId) {
        this.billingaddressTypeId = billingaddressTypeId;
    }

    public String getBillingaddressType() {
        return billingaddressType;
    }

    public void setBillingaddressType(String billingaddressType) {
        this.billingaddressType = billingaddressType;
    }

    public Double getBillingaddressLatitude() {
        return billingaddressLatitude;
    }

    public void setBillingaddressLatitude(Double billingaddressLatitude) {
        this.billingaddressLatitude = billingaddressLatitude;
    }

    public Double getBillingaddressLongitude() {
        return billingaddressLongitude;
    }

    public void setBillingaddressLongitude(Double billingaddressLongitude) {
        this.billingaddressLongitude = billingaddressLongitude;
    }

    public Object getWebsitelogo() {
        return websitelogo;
    }

    public void setWebsitelogo(Object websitelogo) {
        this.websitelogo = websitelogo;
    }

    public String getContactaddress() {
        return contactaddress;
    }

    public void setContactaddress(String contactaddress) {
        this.contactaddress = contactaddress;
    }

    public String getReceivingemail() {
        return receivingemail;
    }

    public void setReceivingemail(String receivingemail) {
        this.receivingemail = receivingemail;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInternationalphone() {
        return internationalphone;
    }

    public void setInternationalphone(String internationalphone) {
        this.internationalphone = internationalphone;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getSmtphost() {
        return smtphost;
    }

    public void setSmtphost(String smtphost) {
        this.smtphost = smtphost;
    }

    public Integer getSmtpport() {
        return smtpport;
    }

    public void setSmtpport(Integer smtpport) {
        this.smtpport = smtpport;
    }

    public String getSmtpemail() {
        return smtpemail;
    }

    public void setSmtpemail(String smtpemail) {
        this.smtpemail = smtpemail;
    }

    public String getSmtppassword() {
        return smtppassword;
    }

    public void setSmtppassword(String smtppassword) {
        this.smtppassword = smtppassword;
    }

    public Boolean getSmtpsecureconnection() {
        return smtpsecureconnection;
    }

    public void setSmtpsecureconnection(Boolean smtpsecureconnection) {
        this.smtpsecureconnection = smtpsecureconnection;
    }

    public Boolean getIsemailsentonorder() {
        return isemailsentonorder;
    }

    public void setIsemailsentonorder(Boolean isemailsentonorder) {
        this.isemailsentonorder = isemailsentonorder;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }
}
