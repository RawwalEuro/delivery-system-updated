package com.eurosoft.customerapp.Utils.db;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.eurosoft.customerapp.Interface.CartDao;
import com.eurosoft.customerapp.Interface.OrderDao;
import com.eurosoft.customerapp.Interface.ProductDao;
import com.eurosoft.customerapp.Interface.UserDao;
import com.eurosoft.customerapp.Model.Cart;
import com.eurosoft.customerapp.Model.Order;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.User;

@Database(entities = {Order.class, Cart.class, User.class, Product.class}, version = 1)
@TypeConverters({DataConverter.class,ConverterProductImages.class,ConvertedOptionDetailData.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract CartDao cartDao();

    public abstract OrderDao orderDao();

    public abstract UserDao userDao();

    public abstract ProductDao productDao();


}
