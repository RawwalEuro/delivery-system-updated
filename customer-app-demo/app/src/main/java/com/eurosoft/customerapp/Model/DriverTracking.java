package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverTracking {

  @SerializedName("Id")
  @Expose
  private Integer id;
  @SerializedName("OrderNo")
  @Expose
  private String orderNo;
  @SerializedName("OrderTime")
  @Expose
  private String orderTime;
  @SerializedName("OrderDate")
  @Expose
  private String orderDate;
  @SerializedName("CustomerName")
  @Expose
  private String customerName;
  @SerializedName("CustomerPhoneNumber")
  @Expose
  private String customerPhoneNumber;
  @SerializedName("DropoffAddress")
  @Expose
  private String dropoffAddress;
  @SerializedName("DropoffLocationTypeId")
  @Expose
  private String dropoffLocationTypeId;
  @SerializedName("DropoffLocationType")
  @Expose
  private String dropoffLocationType;
  @SerializedName("DropoffLatitude")
  @Expose
  private Double dropoffLatitude;
  @SerializedName("DropoffLongitude")
  @Expose
  private Double dropoffLongitude;
  @SerializedName("TotalAmount")
  @Expose
  private Double totalAmount;
  @SerializedName("OrderStatus")
  @Expose
  private String orderStatus;
  @SerializedName("PaymentTypeName")
  @Expose
  private String paymentTypeName;
  @SerializedName("Name")
  @Expose
  private String name;
  @SerializedName("MobileNo")
  @Expose
  private String mobileNo;
  @SerializedName("DriverLatitude")
  @Expose
  private Double driverLatitude;
  @SerializedName("DriverLongitude")
  @Expose
  private Double driverLongitude;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getOrderNo() {
    return orderNo;
  }

  public void setOrderNo(String orderNo) {
    this.orderNo = orderNo;
  }

  public String getOrderTime() {
    return orderTime;
  }

  public void setOrderTime(String orderTime) {
    this.orderTime = orderTime;
  }

  public String getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(String orderDate) {
    this.orderDate = orderDate;
  }

  public String getCustomerName() {
    return customerName;
  }

  public void setCustomerName(String customerName) {
    this.customerName = customerName;
  }

  public String getCustomerPhoneNumber() {
    return customerPhoneNumber;
  }

  public void setCustomerPhoneNumber(String customerPhoneNumber) {
    this.customerPhoneNumber = customerPhoneNumber;
  }

  public String getDropoffAddress() {
    return dropoffAddress;
  }

  public void setDropoffAddress(String dropoffAddress) {
    this.dropoffAddress = dropoffAddress;
  }

  public String getDropoffLocationTypeId() {
    return dropoffLocationTypeId;
  }

  public void setDropoffLocationTypeId(String dropoffLocationTypeId) {
    this.dropoffLocationTypeId = dropoffLocationTypeId;
  }

  public String getDropoffLocationType() {
    return dropoffLocationType;
  }

  public void setDropoffLocationType(String dropoffLocationType) {
    this.dropoffLocationType = dropoffLocationType;
  }

  public Double getDropoffLatitude() {
    return dropoffLatitude;
  }

  public void setDropoffLatitude(Double dropoffLatitude) {
    this.dropoffLatitude = dropoffLatitude;
  }

  public Double getDropoffLongitude() {
    return dropoffLongitude;
  }

  public void setDropoffLongitude(Double dropoffLongitude) {
    this.dropoffLongitude = dropoffLongitude;
  }

  public Double getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(Double totalAmount) {
    this.totalAmount = totalAmount;
  }

  public String getOrderStatus() {
    return orderStatus;
  }

  public void setOrderStatus(String orderStatus) {
    this.orderStatus = orderStatus;
  }

  public String getPaymentTypeName() {
    return paymentTypeName;
  }

  public void setPaymentTypeName(String paymentTypeName) {
    this.paymentTypeName = paymentTypeName;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getMobileNo() {
    return mobileNo;
  }

  public void setMobileNo(String mobileNo) {
    this.mobileNo = mobileNo;
  }

  public Double getDriverLatitude() {
    return driverLatitude;
  }

  public void setDriverLatitude(Double driverLatitude) {
    this.driverLatitude = driverLatitude;
  }

  public Double getDriverLongitude() {
    return driverLongitude;
  }

  public void setDriverLongitude(Double driverLongitude) {
    this.driverLongitude = driverLongitude;
  }
}
