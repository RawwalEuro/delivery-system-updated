package com.eurosoft.customerapp.Utils.NetworkUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CustomWebResponse<T> {

  @SerializedName("message")
  @Expose
  private String message;
  @SerializedName("data")
  @Expose
  private ArrayList<T> data;

  @SerializedName("success")
  @Expose
  private Boolean success;

  public Boolean getSuccess() {
    return success;
  }

  public void setSuccess(Boolean success) {
    this.success = success;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public ArrayList<T> getData() {
    return data;
  }

  public void setData(ArrayList<T> data) {
    this.data = data;
  }
}

