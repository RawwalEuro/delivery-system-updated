package com.eurosoft.customerapp.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.eurosoft.customerapp.ActivityPastOrders;
import com.eurosoft.customerapp.Fragments.FragmentActiveOrders;
import com.eurosoft.customerapp.Fragments.FragmentPastOrders;
import com.eurosoft.customerapp.Model.MasterPojo;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ActivityPastOrders activityPastOrders;
    private MasterPojo masterPojo;

    public ActivityPastOrders getActivityPastOrders() {
        return activityPastOrders;
    }

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);

    }

    @Override
    public Fragment getItem(int position) {
        activityPastOrders = getActivityPastOrders();
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new FragmentActiveOrders();

        }
        else if (position == 1)
        {
            fragment = new FragmentPastOrders();

        }

        return fragment;
    }



    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = masterPojo.getActiveOrders();
        }
        else if (position == 1)
        {
            title = masterPojo.getPastOrders();
        }

        return title;
    }

    public void masterPojo(MasterPojo masterPojo){
        this.masterPojo = masterPojo;
    }

}
