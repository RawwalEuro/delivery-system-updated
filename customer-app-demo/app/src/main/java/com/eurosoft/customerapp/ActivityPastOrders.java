package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Adapters.ViewPagerAdapter;
import com.eurosoft.customerapp.Interface.LoadingListener;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.User;
import com.fxn.stash.Stash;
import com.google.android.material.tabs.TabLayout;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

public class ActivityPastOrders extends AppCompatActivity implements View.OnClickListener, LoadingListener {


    public static final String FRAGMENT_A = "0";
    public static final String FRAGMENT_B = "1";
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    private ImageView bckPress;
    private User currentUser;
    public RelativeLayout rlProgressBar;
    public RelativeLayout mainRl;
    public CircularProgressBar circularProgressBar;
    private Context context;
    private TextView orderTitle;
    private MasterPojo masterPojo;
    private ActivityPastOrders activityPastOrders;
    private String stashType;


    public MasterPojo getMasterPojo() {
        return masterPojo;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_past_orders);
        activityPastOrders = this;
        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        init();
        context = this;


    }


    public ActivityPastOrders getActivityPastOrders() {
        if (activityPastOrders == null) {
            activityPastOrders = this;
        }

        return activityPastOrders;
    }

    public Context getContext() {
        return context;
    }

    public void init() {


        viewPager = (ViewPager) findViewById(R.id.viewPager);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.masterPojo(masterPojo);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        bckPress = (ImageView) findViewById(R.id.bckPress);
        orderTitle = (TextView) findViewById(R.id.orderTitle);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);

        mainRl = findViewById(R.id.mainRl);
        orderTitle.setText(masterPojo.getOrders());
        bckPress.setOnClickListener(this);


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                if (position == 1) {

                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }


    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.bckPress:
                Intent intent = new Intent(this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
                //finish();
                break;


        }
    }


    public void progressVisiblityVisible() {

        rlProgressBar.setVisibility(View.VISIBLE);
        mainRl.setVisibility(View.GONE);
        viewPager.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.VISIBLE);
        mainRl.setEnabled(false);
        circularProgressBar.setIndeterminateMode(true);
        rlProgressBar.bringToFront();
    }

    public void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
        mainRl.setVisibility(View.VISIBLE);
        mainRl.setEnabled(true);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e("Coming Hereintent","Coming hereintent");
       // processIntent(intent);
    };

    @Override
    public void loadingStarted() {
        progressVisiblityVisible();
    }

    @Override
    public void loadingFinished() {
        progressVisiblityGone();
    }

    private void processIntent(Intent intent){
        //get your extras
        Bundle extras = intent.getExtras();
        if(extras != null) {
            Log.e("ExtrasNotNull","ExtrasNotNull");
            stashType = extras.getString("frgToLoad");

            switch (stashType) {
                case FRAGMENT_A:
                    viewPager.setCurrentItem(0);
                    viewPagerAdapter.notifyDataSetChanged();
                    // Load corresponding fragment
                    break;
                case FRAGMENT_B:
                    viewPager.setCurrentItem(1);
                    viewPagerAdapter.notifyDataSetChanged();
                    // Load corresponding fragment
                    break;

            }
        }




    }

}