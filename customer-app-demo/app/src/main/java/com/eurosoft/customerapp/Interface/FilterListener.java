package com.eurosoft.customerapp.Interface;

import com.eurosoft.customerapp.Model.FilterBody;

public interface FilterListener {
  void onFilterClicked(FilterBody filterBody);
}
