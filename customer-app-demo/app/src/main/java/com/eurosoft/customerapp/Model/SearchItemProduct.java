package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchItemProduct {

    public static final int TYPE_CAT=0;
    public static final int TYPE_PRODUCT=1;

    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("StoreId")
    @Expose
    private Integer storeId;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;
    @SerializedName("CategoryIcon")
    @Expose
    private String categoryIcon;
    @SerializedName("CategoryBaseUrl")
    @Expose
    private String categoryBaseUrl;
    @SerializedName("CategoryImage")
    @Expose
    private Object categoryImage;
    @SerializedName("OrderBy")
    @Expose
    private Integer orderBy;
    @SerializedName("ParentCategoryId")
    @Expose
    private Integer parentCategoryId;
    @SerializedName("Status")
    @Expose
    private Boolean status;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;
    @SerializedName("IsSubCategory")
    @Expose
    private Boolean isSubCategory;
    @SerializedName("ProductId")
    @Expose
    private Integer productId;
    @SerializedName("StoreName")
    @Expose
    private String storeName;
    @SerializedName("StoreDeliveryFee")
    @Expose
    private Double storeDeliveryFee;
    @SerializedName("StoreDeliveryEstimatedTime")
    @Expose
    private String storeDeliveryEstimatedTime;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("ProductDesc")
    @Expose
    private String productDesc;
    @SerializedName("ProductModel")
    @Expose
    private String productModel;
    @SerializedName("Quantity")
    @Expose
    private int quantity;
    @SerializedName("MinimumQty")
    @Expose
    private int minimumQty;
    @SerializedName("Price")
    @Expose
    private Double price;
    @SerializedName("RequriesShipping")
    @Expose
    private String requriesShipping;
    @SerializedName("BaseUrl")
    @Expose
    private String baseUrl;
    @SerializedName("PrimaryImage")
    @Expose
    private String primaryImage;
    @SerializedName("DateAvailable")
    @Expose
    private String dateAvailable;
    @SerializedName("ProductOutOfStockName")
    @Expose
    private String productOutOfStockName;
    @SerializedName("ProductOptions")
    @Expose
    private int productOptions;
    @SerializedName("DataType")
    @Expose
    private Integer dataType;


    @SerializedName("IsStoreClosed")
    @Expose
    private Boolean IsStoreClosed;


    @SerializedName("Tip")
    @Expose
    private Boolean Tip;


    @SerializedName("MinimumOrderLimit")
    @Expose
    private Double MinimumOrderLimit;


    @SerializedName("ServiceCharges")
    @Expose
    private Double ServiceCharges;


    @SerializedName("IsWishList")
    @Expose
    private Boolean IsWishList;

    @SerializedName("IsServiceChargesEnable")
    @Expose
    private Boolean IsServiceChargesEnable;


    public Boolean getIsWishList() {
        return IsWishList;
    }

    public void setIsWishList(Boolean isWishList) {
        IsWishList = isWishList;
    }

    public Boolean getTip() {
        if(Tip == null){
            return false;
        }
        return Tip;
    }

    public void setTip(Boolean tip) {
        Tip = tip;
    }

    public Double getMinimumOrderLimit() {
        if(MinimumOrderLimit == null){
            return 0.0;
        }
        return MinimumOrderLimit;
    }

    public void setMinimumOrderLimit(Double minimumOrderLimit) {
        MinimumOrderLimit = minimumOrderLimit;
    }

    public Double getServiceCharges() {

        if(ServiceCharges == null){
            return 0.0;
        }
        return ServiceCharges;
    }

    public void setServiceCharges(Double serviceCharges) {
        ServiceCharges = serviceCharges;
    }

    public Boolean getServiceChargesEnable() {
        if(IsServiceChargesEnable == null){
            return false;
        }
        return IsServiceChargesEnable;
    }

    public void setServiceChargesEnable(Boolean serviceChargesEnable) {
        IsServiceChargesEnable = serviceChargesEnable;
    }

    public Boolean getStoreClosed() {
        return IsStoreClosed;
    }

    public void setStoreClosed(Boolean storeClosed) {
        IsStoreClosed = storeClosed;
    }

    public static int getTypeCat() {
        return TYPE_CAT;
    }

    public static int getTypeProduct() {
        return TYPE_PRODUCT;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public String getCategoryBaseUrl() {
        return categoryBaseUrl;
    }

    public void setCategoryBaseUrl(String categoryBaseUrl) {
        this.categoryBaseUrl = categoryBaseUrl;
    }

    public Object getCategoryImage() {
        return categoryImage;
    }

    public void setCategoryImage(Object categoryImage) {
        this.categoryImage = categoryImage;
    }

    public Integer getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(Integer orderBy) {
        this.orderBy = orderBy;
    }

    public Integer getParentCategoryId() {
        return parentCategoryId;
    }

    public void setParentCategoryId(Integer parentCategoryId) {
        this.parentCategoryId = parentCategoryId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public Boolean getSubCategory() {
        return isSubCategory;
    }

    public void setSubCategory(Boolean subCategory) {
        isSubCategory = subCategory;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Double getStoreDeliveryFee() {
        return storeDeliveryFee;
    }

    public void setStoreDeliveryFee(Double storeDeliveryFee) {
        this.storeDeliveryFee = storeDeliveryFee;
    }

    public String getStoreDeliveryEstimatedTime() {
        return storeDeliveryEstimatedTime;
    }

    public void setStoreDeliveryEstimatedTime(String storeDeliveryEstimatedTime) {
        this.storeDeliveryEstimatedTime = storeDeliveryEstimatedTime;
    }

    public String  getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
       return productDesc == null ? "" : productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductModel() {
        return productModel;
    }

    public void setProductModel(String productModel) {
        this.productModel = productModel;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getMinimumQty() {
        return minimumQty;
    }

    public void setMinimumQty(int minimumQty) {
        this.minimumQty = minimumQty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getRequriesShipping() {
        return requriesShipping;
    }

    public void setRequriesShipping(String requriesShipping) {
        this.requriesShipping = requriesShipping;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getPrimaryImage() {
        return primaryImage;
    }

    public void setPrimaryImage(String primaryImage) {
        this.primaryImage = primaryImage;
    }

    public String getDateAvailable() {
        return dateAvailable;
    }

    public void setDateAvailable(String dateAvailable) {
        this.dateAvailable = dateAvailable;
    }

    public String getProductOutOfStockName() {
        return productOutOfStockName;
    }

    public void setProductOutOfStockName(String productOutOfStockName) {
        this.productOutOfStockName = productOutOfStockName;
    }

    public int getProductOptions() {
        return productOptions;
    }

    public void setProductOptions(int productOptions) {
        this.productOptions = productOptions;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }
}
