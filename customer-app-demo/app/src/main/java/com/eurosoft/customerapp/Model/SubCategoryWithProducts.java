package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SubCategoryWithProducts {

  @SerializedName("Id")
  @Expose
  private Integer id;
  @SerializedName("CategoryId")
  @Expose
  private Integer categoryId;
  @SerializedName("SubCategoryName")
  @Expose
  private String subCategoryName;
  @SerializedName("SubCategoryImage")
  @Expose
  private Object subCategoryImage;
  @SerializedName("OrderBy")
  @Expose
  private Integer orderBy;
  @SerializedName("Product")
  @Expose
  private List<Product> product = null;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(Integer categoryId) {
    this.categoryId = categoryId;
  }

  public String getSubCategoryName() {
    return subCategoryName;
  }

  public void setSubCategoryName(String subCategoryName) {
    this.subCategoryName = subCategoryName;
  }

  public Object getSubCategoryImage() {
    return subCategoryImage;
  }

  public void setSubCategoryImage(Object subCategoryImage) {
    this.subCategoryImage = subCategoryImage;
  }

  public Integer getOrderBy() {
    return orderBy;
  }

  public void setOrderBy(Integer orderBy) {
    this.orderBy = orderBy;
  }

  public List<Product> getProduct() {
    return product;
  }

  public void setProduct(List<Product> product) {
    this.product = product;
  }
}
