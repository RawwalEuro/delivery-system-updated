package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FilterBody implements Serializable {

    @SerializedName("sortId")
    @Expose
    private Object sortId;
    @SerializedName("fromPrice")
    @Expose
    private Integer fromPrice;
    @SerializedName("toPrice")
    @Expose
    private Integer toPrice;
    @SerializedName("freeDelivery")
    @Expose
    private Boolean freeDelivery;
    @SerializedName("dietary")
    @Expose
    private String dietary;

    public Object getSortId() {
        return sortId;
    }

    public void setSortId(Object sortId) {
        this.sortId = sortId;
    }

    public Integer getFromPrice() {
        return fromPrice;
    }

    public void setFromPrice(Integer fromPrice) {
        this.fromPrice = fromPrice;
    }

    public Integer getToPrice() {
        return toPrice;
    }

    public void setToPrice(Integer toPrice) {
        this.toPrice = toPrice;
    }

    public Boolean getFreeDelivery() {
        return freeDelivery;
    }

    public void setFreeDelivery(Boolean freeDelivery) {
        this.freeDelivery = freeDelivery;
    }

    public String getDietary() {
        return dietary;
    }

    public void setDietary(String dietary) {
        this.dietary = dietary;
    }
}
