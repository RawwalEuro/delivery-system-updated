package com.eurosoft.customerapp.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MMSamplePojo {
    @SerializedName("activeOrders")
    @Expose
    private String activeOrders;


    public String getActiveOrders() {
        return activeOrders;
    }

    public void setActiveOrders(String activeOrders) {
        this.activeOrders = activeOrders;
    }
}
