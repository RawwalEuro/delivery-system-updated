package com.eurosoft.customerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.eurosoft.customerapp.Adapters.AdapterRestruantMenu;
import com.eurosoft.customerapp.Adapters.AdapterSubCategory;
import com.eurosoft.customerapp.Adapters.AdapterWishList;
import com.eurosoft.customerapp.Fragments.AddProductDialog;
import com.eurosoft.customerapp.Model.AppSettings;
import com.eurosoft.customerapp.Model.MasterPojo;
import com.eurosoft.customerapp.Model.Product;
import com.eurosoft.customerapp.Model.Store;
import com.eurosoft.customerapp.Model.User;
import com.eurosoft.customerapp.Model.WishList;
import com.eurosoft.customerapp.Utils.Footer.FooterBarLayout;
import com.eurosoft.customerapp.Utils.NetworkUtils.APIClient;
import com.eurosoft.customerapp.Utils.NetworkUtils.ApiInterface;
import com.eurosoft.customerapp.Utils.NetworkUtils.CustomWebResponse;
import com.eurosoft.customerapp.Utils.db.DatabaseClient;
import com.fxn.stash.Stash;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.tabs.TabLayout;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityWishList extends AppCompatActivity {

    private AppBarLayout appBarlayout;
    private LinearLayout toolbarItemLL;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    LinearLayoutManager linearLayoutManager;
    private RecyclerView rvWish;
    private ImageView bckPress;
    private Context context;
    private RelativeLayout rlNoData;
    private FooterBarLayout footerCart;
    private User currentUser;
    private TextView count;

    private FrameLayout frameCart;
    private TextView cart_badge;
    private ArrayList<TabIndexSize> tabIndexesArray;
    private String restruantName = "";
    private TextView title;
    private RelativeLayout rlProgressBar;
    private CircularProgressBar circularProgressBar;
    private AppSettings appSettings;
    private RelativeLayout searchBarRl;
    private TextView noItemFound;
    private MasterPojo masterPojo;
    private Store storeObject;
    private ArrayList<WishList> wishListArrayList;
    private AdapterWishList adapterWishList;
    private List<Product> productsFromDB;
    private RelativeLayout navIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        Stash.init(getApplicationContext());
        masterPojo = (MasterPojo) Stash.getObject(Constants.MASTER_POJO, MasterPojo.class);
        currentUser = (User) Stash.getObject(Constants.USER, User.class);
        appSettings = (AppSettings) Stash.getObject(Constants.APP_SETTINGS, AppSettings.class);
        initViews();
        callApiGetWishList();
        getCartDetails();
    }

    private void callApiGetWishList() {
        progressVisiblityVisible();
        ApiInterface apiService = APIClient.getClient().create(ApiInterface.class);
        Call<CustomWebResponse<WishList>> call = apiService.getWishList(currentUser.getId(), currentUser.getToken());

        call.enqueue(new Callback<CustomWebResponse<WishList>>() {
            @Override
            public void onResponse(Call<CustomWebResponse<WishList>> call, Response<CustomWebResponse<WishList>> response) {
                if (response.code() != 200 || response.body() == null) {
                    progressVisiblityGone();
                    return;
                }

                if (!response.body().getSuccess()) {
                    progressVisiblityGone();
                    Toasty.error(getApplicationContext(), response.body().getMessage());
                    return;
                }

                if (response.isSuccessful() && response.code() == 200) {
                    wishListArrayList = response.body().getData();


                    if (wishListArrayList == null || wishListArrayList.size() == 0) {
                        rlNoData.setVisibility(View.VISIBLE);
                        rvWish.setVisibility(View.GONE);
                        progressVisiblityGone();
                        return;
                    } else {
                        rlNoData.setVisibility(View.GONE);
                        rvWish.setVisibility(View.VISIBLE);
                        progressVisiblityGone();
                    }
                }



                adapterWishList = new AdapterWishList(ActivityWishList.this, wishListArrayList, appSettings, new AdapterWishList.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, WishList wishListItem) {

                    }
                });
                rvWish.setLayoutManager(linearLayoutManager);
                rvWish.setHasFixedSize(true);
                rvWish.setAdapter(adapterWishList);
                progressVisiblityGone();
            }

            @Override
            public void onFailure(Call<CustomWebResponse<WishList>> call, Throwable t) {
                Toasty.error(getApplicationContext(), "Something went wrong", Toasty.LENGTH_SHORT).show();
            }
        });


    }

    private void initViews() {
        linearLayoutManager = new LinearLayoutManager(this);
        context = getApplicationContext();
        appBarlayout = findViewById(R.id.appBarlayout);
        toolbarItemLL = findViewById(R.id.toolbarItemLL);
        toolbar = findViewById(R.id.toolbar);
        rvWish = findViewById(R.id.rvWish);
        bckPress = findViewById(R.id.bckPress);
        rlNoData = findViewById(R.id.rlNoData);
        bckPress = findViewById(R.id.bckPress);
        footerCart = findViewById(R.id.footerCart);
        count = findViewById(R.id.count);
        frameCart = findViewById(R.id.frameCart);
        cart_badge = findViewById(R.id.cart_badge);
        title = findViewById(R.id.titleLocation);
        rlProgressBar = findViewById(R.id.rlProgressBar);
        circularProgressBar = findViewById(R.id.circularProgressBar);
        searchBarRl = findViewById(R.id.searchBarRl);
        noItemFound = findViewById(R.id.noItemFound);
        navIcon = findViewById(R.id.navIcon);

        navIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void progressVisiblityVisible() {
        rlProgressBar.setVisibility(View.VISIBLE);
        circularProgressBar.setIndeterminateMode(true);
    }

    private void progressVisiblityGone() {
        rlProgressBar.setVisibility(View.GONE);
    }

    private void getCartDetails() {
        class GetCartItem extends AsyncTask<Void, Void, List<Product>> {

            @Override
            protected List<Product> doInBackground(Void... voids) {
                productsFromDB = DatabaseClient
                        .getInstance(getApplicationContext())
                        .getAppDatabase()
                        .productDao()
                        .getAllByUserId(currentUser.getId());
                return productsFromDB;
            }

            @Override
            protected void onPostExecute(List<Product> products) {
                super.onPostExecute(products);
                if (productsFromDB == null || productsFromDB.size() == 0) {

                    frameCart.setVisibility(View.GONE);
                } else {

                    count.setText(productsFromDB.size() + "");
                    frameCart.setVisibility(View.GONE);
                    cart_badge.setText(productsFromDB.size() + "");
                }

            }
        }

        GetCartItem gt = new GetCartItem();
        gt.execute();

    }
}